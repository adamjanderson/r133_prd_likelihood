#!/usr/bin/python
# process_root_skims_geant4.py

import numpy as np
from array import array
import ROOT as r
import scipy.io as scio

matfilepath = '/Users/adamanderson/CDMSSoftware/cdmstools/CAP/FCCS/FCCS_util/sim_133_LT_params/'
basepath = '/Users/adamanderson/Documents/CDMS/Analysis/cdmsanalysis/20140228_ROOT_skim/'

# create ROOT file
f_data = r.TFile(basepath+'LT_geant4_skim.root', 'RECREATE')

# declare branch variables
Erecoil = array('d', [0.])
weight = array('d', [0.])

detnum = [1, 4, 5, 11, 12, 14, 15]
n_times = [1, 1, 1, 1, 1, 1, 2]
for jdet in range(len(detnum)):
	for jtime in range(n_times[jdet]):
		t_g4_210Pb_Qo = r.TTree('210Pb_Qo_zip'+str(detnum[jdet]), '210Pb Qouter')
		t_g4_210Bi_Qo = r.TTree('210Bi_Qo_zip'+str(detnum[jdet]), '210Bi Qouter')
		t_g4_206Pb_Qo = r.TTree('206Pb_Qo_zip'+str(detnum[jdet]), '206Pb Qouter')
		t_g4_210Pb_Qi = r.TTree('210Pb_Qi_zip'+str(detnum[jdet]), '210Pb Qinner')
		t_g4_210Bi_Qi = r.TTree('210Bi_Qi_zip'+str(detnum[jdet]), '210Bi Qinner')
		t_g4_206Pb_Qi = r.TTree('206Pb_Qi_zip'+str(detnum[jdet]), '206Pb Qinner')

		mat_210Pb_Qo = scio.loadmat(matfilepath+'HousingEstimates/Pb210_background_estimate/Housing-Pb210-'+str(1100+detnum[jdet])+'.mat')
		mat_210Bi_Qo = scio.loadmat(matfilepath+'HousingEstimates/Bi210_background_estimate/Housing-Bi210-'+str(1100+detnum[jdet])+'.mat')
		mat_206Pb_Qo = scio.loadmat(matfilepath+'HousingEstimates/Pb206_background_estimate/Housing-Pb206-'+str(1100+detnum[jdet])+'.mat')
		mat_210Pb_Qi = scio.loadmat(matfilepath+'Det_Det_Contamination/Pb210_background_estimate/DetDet-Pb210-'+str(1100+detnum[jdet])+'.mat')
		mat_210Bi_Qi = scio.loadmat(matfilepath+'Det_Det_Contamination/Bi210_background_estimate/DetDet-Bi210-'+str(1100+detnum[jdet])+'.mat')
		mat_206Pb_Qi = scio.loadmat(matfilepath+'Det_Det_Contamination/Pb206_background_estimate/DetDet-Pb206-'+str(1100+detnum[jdet])+'.mat')

        vec_Erecoil_210Pb_Qo = mat_210Pb_Qo['EnergySingles']
        vec_weight_210Pb_Qo = mat_210Pb_Qo['Weights']
        vec_Erecoil_210Bi_Qo = mat_210Bi_Qo['EnergySingles']
        vec_weight_210Bi_Qo = mat_210Bi_Qo['Weights']
        vec_Erecoil_206Pb_Qo = mat_206Pb_Qo['EnergySingles']
        vec_weight_206Pb_Qo = mat_206Pb_Qo['Weights']
        vec_Erecoil_210Pb_Qi = mat_210Pb_Qi['EnergySingles']
        vec_weight_210Pb_Qi = mat_210Pb_Qi['Weights']
        vec_Erecoil_210Bi_Qi = mat_210Bi_Qi['EnergySingles']
        vec_weight_210Bi_Qi = mat_210Bi_Qi['Weights']
        vec_Erecoil_206Pb_Qi = mat_206Pb_Qi['Energy']
        vec_weight_206Pb_Qi = mat_206Pb_Qi['Weights']
        
        # write to trees

        nentries = len(vec_Erecoil_210Pb_Qo)
        t_g4_210Pb_Qo.Branch('Erecoil', Erecoil, 'Erecoil/D')
        t_g4_210Pb_Qo.Branch('weight', weight, 'weight/D')
        for j_evt in range(nentries):
        	Erecoil[0] = vec_Erecoil_210Pb_Qo[j_evt]
        	weight[0] = vec_weight_210Pb_Qo[0]
        	t_g4_210Pb_Qo.Fill()

        nentries = len(vec_Erecoil_210Bi_Qo)
        t_g4_210Bi_Qo.Branch('Erecoil', Erecoil, 'Erecoil/D')
        t_g4_210Bi_Qo.Branch('weight', weight, 'weight/D')
        for j_evt in range(nentries):
        	Erecoil[0] = vec_Erecoil_210Bi_Qo[j_evt]
        	weight[0] = vec_weight_210Bi_Qo[0]
        	t_g4_210Bi_Qo.Fill()

        nentries = len(vec_Erecoil_206Pb_Qo)
        t_g4_206Pb_Qo.Branch('Erecoil', Erecoil, 'Erecoil/D')
        t_g4_206Pb_Qo.Branch('weight', weight, 'weight/D')
        for j_evt in range(nentries):
        	Erecoil[0] = vec_Erecoil_206Pb_Qo[j_evt]
        	weight[0] = vec_weight_206Pb_Qo[0]
        	t_g4_206Pb_Qo.Fill()

        nentries = len(vec_Erecoil_210Pb_Qi)
        t_g4_210Pb_Qi.Branch('Erecoil', Erecoil, 'Erecoil/D')
        t_g4_210Pb_Qi.Branch('weight', weight, 'weight/D')
        for j_evt in range(nentries):
        	Erecoil[0] = vec_Erecoil_210Pb_Qi[j_evt]
        	weight[0] = vec_weight_210Pb_Qi[0]
        	t_g4_210Pb_Qi.Fill()

        nentries = len(vec_Erecoil_210Bi_Qi)
        t_g4_210Bi_Qi.Branch('Erecoil', Erecoil, 'Erecoil/D')
        t_g4_210Bi_Qi.Branch('weight', weight, 'weight/D')
        for j_evt in range(nentries):
        	Erecoil[0] = vec_Erecoil_210Bi_Qi[j_evt]
        	weight[0] = vec_weight_210Bi_Qi[0]
        	t_g4_210Bi_Qi.Fill()

        nentries = len(vec_Erecoil_206Pb_Qi)
        t_g4_206Pb_Qi.Branch('Erecoil', Erecoil, 'Erecoil/D')
        t_g4_206Pb_Qi.Branch('weight', weight, 'weight/D')
        for j_evt in range(nentries):
        	Erecoil[0] = vec_Erecoil_206Pb_Qi[j_evt]
        	weight[0] = vec_weight_206Pb_Qi[0]
        	t_g4_206Pb_Qi.Fill()

    	f_data.Write()
f_data.Close()