function ROOT_skim_bg_model()
    close all;

    % useful preliminary variables
    R133LTcolors
    detnum = [1101 1104 1105 1111 1112 1114 1115];
    time_edges = {[0, 1e11], [0, 1e11], [0, 1e11], [0, 1e11], [0, 1e11], [0, 1e11], [11200000000, 11212121650, 999999999999]};
    target_livetime = {129.4, 136.4, 134.4, 143.8, 138.5, 133.8, [31.3, 100.4]};
    detlabels = {'T1Z1','T2Z1','T2Z2','T4Z2','T4Z3','T5Z2','T5Z3'};
    mindet = 1;
    maxdet = 7;
    

    for jdet = mindet:maxdet
        for jtime = 1:(length(time_edges{jdet})-1)
            cSanity = abs(qimeanOF(detnum(jdet),0)) > 1e-10 & abs(qimeanOF(detnum(jdet),0)) < 50;
            sum(~cSanity)
            
            cTime = inrange(SeriesNumber(1101,0), time_edges{jdet}(jtime), time_edges{jdet}(jtime+1));
            cPreSelection = cBgSIM_preselection_v53_LT(detnum(jdet)) & cSanity;
            cAll = cBgSIM_M3_v53_LT(detnum(jdet)) & cTime & cSanity;
            cConsistency = cAll & ...
                           cQin1_v53_LT(detnum(jdet)) & ...
                           cQin2_v53_LT(detnum(jdet)) & ...
                           cQisym_v53_LT(detnum(jdet)) & ...
                           cNR_qimean_3sig_v53_LT(detnum(jdet)) & ...
                           cConsistency_v53_LT(detnum(jdet));
            cSignal = cConsistency & ...
                      cBDT_simul_v53_LT(detnum(jdet));
            [~, BDT_5] = cBDT_v53_LT_5GeV(detnum(jdet));
            [~, BDT_7] = cBDT_v53_LT_7GeV(detnum(jdet));
            [~, BDT_10] = cBDT_v53_LT_10GeV(detnum(jdet));
            [~, BDT_15] = cBDT_v53_LT_15GeV(detnum(jdet));
            

            
            trigger_eff = weights_threshold_eff_LibSim3_133_LT(ptNF(detnum(jdet),0), detnum(jdet), jtime-1, 0);
            [weights_gammas, LT_gammas] = weights_gammas_LibSim3_133_LT(detnum(jdet), cPreSelection);
            weights_gammas = weights_gammas/sum(LT_gammas) * target_livetime{jdet}(jtime);
            [weights_1keV, LT_1keV] = weights_1keV_LibSim3_133_LT(detnum(jdet), 2, cPreSelection);
            weights_1keV = weights_1keV/sum(LT_1keV) * target_livetime{jdet}(jtime);
            [weights_210Pb_Qout, LT_210Pb_Qout] = weights_210Pb_Qout_LibSim3_133_LT(detnum(jdet), cPreSelection);
            weights_210Pb_Qout = weights_210Pb_Qout/sum(LT_210Pb_Qout) * target_livetime{jdet}(jtime);
            [weights_210Bi_Qout, LT_210Bi_Qout] = weights_210Bi_Qout_LibSim3_133_LT(detnum(jdet), cPreSelection);
            weights_210Bi_Qout = weights_210Bi_Qout/sum(LT_210Bi_Qout) * target_livetime{jdet}(jtime);
            [weights_206Pb_Qout, LT_206Pb_Qout] = weights_206Pb_Qout_LibSim3_133_LT(detnum(jdet), cPreSelection);
            weights_206Pb_Qout = weights_206Pb_Qout/sum(LT_206Pb_Qout) * target_livetime{jdet}(jtime);
            [weights_210Pb_Qin, LT_210Pb_Qin] = weights_210Pb_Qin_LibSim3_133_LT(detnum(jdet), cPreSelection);
            weights_210Pb_Qin = weights_210Pb_Qin/sum(LT_210Pb_Qin) * target_livetime{jdet}(jtime);
            [weights_210Bi_Qin, LT_210Bi_Qin] = weights_210Bi_Qin_LibSim3_133_LT(detnum(jdet), cPreSelection);
            weights_210Bi_Qin = weights_210Bi_Qin/sum(LT_210Bi_Qin) * target_livetime{jdet}(jtime);
            [weights_210Pb_Qout_loY, LT_210Pb_Qout] = weights_210Pb_Qout_LibSim3_133_LT(detnum(jdet), cPreSelection);
            weights_210Pb_Qout_loY = weights_210Pb_Qout_loY/sum(LT_210Pb_Qout) * target_livetime{jdet}(jtime);
            [weights_210Bi_Qout_loY, LT_210Bi_Qout] = weights_210Bi_Qout_LibSim3_133_LT(detnum(jdet), cPreSelection);
            weights_210Bi_Qout_loY = weights_210Bi_Qout_loY/sum(LT_210Bi_Qout) * target_livetime{jdet}(jtime);
              
            
            disp([detlabels{jdet} ':']);
            disp(['# all = ' num2str(sum(cAll))]);
            disp(['# consistency = ' num2str(sum(cConsistency))]);
            disp(['# candidates = ' num2str(sum(cSignal))]);
            
            
            dlmwrite(['ROOT_skim_bg_model_zip' num2str(zipnum(detnum(jdet))) '_time' num2str(jtime) '.txt'], ...
                     [ptNF(detnum(jdet),cAll), ...
                      prpartOF(detnum(jdet),cAll), ...
                      pzpartOF(detnum(jdet),cAll), ...
                      qimeanOF(detnum(jdet),cAll), ...
                      SeriesNumber(detnum(jdet),cAll), ...
                      EventNumber(detnum(jdet),cAll), ...
                      BDT_5(cAll), ...
                      BDT_7(cAll), ...
                      BDT_10(cAll), ...
                      BDT_15(cAll), ...
                      double(cConsistency(cAll)), ...
                      double(cSignal(cAll)), ...
                      trigger_eff(cAll), ...
                      weights_gammas(cAll), ...
                      weights_1keV(cAll), ...
                      weights_210Pb_Qout(cAll), ...
                      weights_210Bi_Qout(cAll), ...
                      weights_206Pb_Qout(cAll), ...
                      weights_210Pb_Qin(cAll), ...
                      weights_210Bi_Qin(cAll), ...
                      weights_210Pb_Qout_loY(cAll), ...
                      weights_210Bi_Qout_loY(cAll)], ...
                      'delimiter', '\t', 'precision', 11);
                  
        end;
    end;
end