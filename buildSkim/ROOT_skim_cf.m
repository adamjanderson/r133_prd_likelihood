% ROOT_skim_cf.m
%
% A script for skimming Cf data for use a likelihood analysis.
%
% Adam Anderson
% adama@mit.edu

function ROOT_skim_cf()
    close all;

    % useful preliminary variables
    R133LTcolors
    detnum = [1101 1104 1105 1111 1112 1114 1115];
    time_edges = {[0, 1.1307e10], [0, 1.1307e10], [0, 1.1307e10], ...
                  [0, 1.1307e10], [0, 1.1307e10], [0, 11301202113, 1e11], ...
                  [0, 11212121650, 1.1307e10]};
    detlabels = {'T1Z1','T2Z1','T2Z2','T4Z2','T4Z3','T5Z2','T5Z3'};
    timelabels = {{''}, {''}, {''}, {''}, {''}, {' w/QOS1', ' w/o QOS1'}, {' BS', ' AS'}};
    mindet = 1;
    maxdet = 1;
    
    WIMP_masses = [linspace(3.5, 9.5, 13) linspace(10, 19, 10) linspace(20, 30, 6)];
    

    for jdet = mindet:maxdet
        for jtime = 1:(length(time_edges{jdet})-1)
            % print info
            disp([detlabels{jdet} timelabels{jdet}{jtime}]);
            
            % Cut on time period
            cTime = inrange(SeriesNumber(detnum(jdet),0), time_edges{jdet}(jtime), time_edges{jdet}(jtime+1));
            % exclude August 2012 Cf on T5Z3 because of CDMSlite
            if detnum(jdet) == 1115
                cTime = cTime & ~inrange(SeriesNumber(detnum(jdet),0), 1.1207e11, 1.1210e11);
            end;
            % Define the standard cuts
            cAll =  cTime & ...
                   ~cRandom_133 & ...
                    cGoodEv_v53(detnum(jdet)) & ...
                    cTriggeredEvent_133_LT(detnum(jdet)) & ...
                   ~cPsat_133 & ...
                   ~cQsat_133 & ...
                    cBiasPM_133;
            cConsistency = cAll & ...
                           cQin1_v53_LT(detnum(jdet)) & ...
                           cQin2_v53_LT(detnum(jdet)) & ...
                           cQisym_v53_LT(detnum(jdet)) & ...
                           cNR_qimean_3sig_v53_LT(detnum(jdet)) & ...
                           cConsistency_v53_LT(detnum(jdet));
            cSignal = cConsistency & ...
                      cBDT_simul_v53_LT(detnum(jdet));
            [~, BDT5] = cBDT_v53_LT_5GeV(detnum(jdet));
            [~, BDT7] = cBDT_v53_LT_7GeV(detnum(jdet));
            [~, BDT10] = cBDT_v53_LT_10GeV(detnum(jdet));
            [~, BDT15] = cBDT_v53_LT_15GeV(detnum(jdet));
            if detnum(jdet) == 1114
                trigger_eff = weights_threshold_eff_LibSim3_133_LT(ptNF(detnum(jdet),0), detnum(jdet), 0, 0);
            else
                trigger_eff = weights_threshold_eff_LibSim3_133_LT(ptNF(detnum(jdet),0), detnum(jdet), jtime-1, 0);
            end
            
            WIMP_weights = cell(size(WIMP_masses));
            for jMass = 1:length(WIMP_weights)
                if WIMP_masses(jMass) == 5 || WIMP_masses(jMass) == 7 || ...
                   WIMP_masses(jMass) == 10 || WIMP_masses(jMass) == 15;
                    plots_flag = true;
                else
                    plots_flag = false;
                end
                disp(['Calculating weights for ' num2str(WIMP_masses(jMass)) ' GeV WIMP'])
                WIMP_weights{jMass} = zeros(size(cAll));
                WIMP_weights{jMass}(cAll) = compute_cf_weights_BDT(detnum(jdet), ptNF(detnum(jdet), cAll), qimeanOF(detnum(jdet), cAll), WIMP_masses(jMass), jtime, plots_flag);
            end
            
            
            % Cut events that fail the basic quality cuts
            pt = ptNF(detnum(jdet),cAll);
            prpart = prpartOF(detnum(jdet),cAll);
            pzpart = pzpartOF(detnum(jdet),cAll);
            qimean = qimeanOF(detnum(jdet),cAll);
            seriesNum = SeriesNumber(detnum(jdet),cAll);
            eventNum = EventNumber(detnum(jdet),cAll);
            BDT5 = BDT5(cAll);
            BDT7 = BDT7(cAll);
            BDT10 = BDT10(cAll);
            BDT15 = BDT15(cAll);
            cConsistency = double(cConsistency(cAll));
            cSignal = double(cSignal(cAll));
            trigger_eff = trigger_eff(cAll);
            for jMass = 1:length(WIMP_weights)
                WIMP_weights{jMass} = WIMP_weights{jMass}(cAll);
            end
            
            disp([detlabels{jdet} ':']);
            disp(['# all = ' num2str(sum(cAll))]);
            disp(['# consistency = ' num2str(sum(cConsistency))]);
            disp(['# candidates = ' num2str(sum(cSignal))]);
            
            save(['ROOT_skim_cf_zip' num2str(zipnum(detnum(jdet))) '_time' num2str(jtime) '.mat'], ...
                  'pt', 'qimean', 'prpart', 'pzpart', 'seriesNum', 'eventNum', ...
                  'cConsistency', 'cSignal', 'trigger_eff', ...
                  'BDT5', 'BDT7', 'BDT10', 'BDT15', 'WIMP_weights', 'WIMP_masses');
              
        end;
    end;
end