function ROOT_eff_info()
    close all;

    % useful preliminary variables
    R133LTcolors
    detnum = [1101 1104 1105 1111 1112 1114 1115];
    time_edges = {[0, 1e11], [0, 1e11], [0, 1e11], [0, 1e11], [0, 1e11], ...
                  [0, 11301202113, 1e11], [0, 11212121650, 1e11]};
    livetimes = {131.6, 138.8, 136.8, 146.3, 141.0, [38.80, 95.67], [18.69, 60.55]};
    masses = {0.6095, 0.5974, 0.5913, 0.5974, 0.5943, [0.6065, 0.6065], [0.5913, 0.5913]};
    detlabels = {'T1Z1','T2Z1','T2Z2','T4Z2','T4Z3','T5Z2','T5Z3'};
    timelabels = {{''}, {''}, {''}, {''}, {''}, {' w/QOS1', ' w/o QOS1'}, {' BS', ' AS'}};
    
    pt_eff = linspace(2,13.1,1000)';
    exposures = cell(size(time_edges));
    eff_singles = cell(size(time_edges));
    eff_preselect = cell(size(time_edges));
    eff_BDT = cell(size(time_edges));
    
    for jdet = 1:length(detnum)
        exposures{jdet} = cell(size(time_edges{jdet}));
        eff_singles{jdet} = cell(size(time_edges{jdet}));
        eff_preselect{jdet} = cell(size(time_edges{jdet}));
        eff_BDT{jdet} = cell(size(time_edges{jdet}));
        
        for jtime = 1:(length(time_edges{jdet})-1)
            % [AJA]: TEMPORARY ONLY!
            if detnum(jdet) == 1114
                timeperiod = 1;
            else
                timeperiod = jtime;
            end;
            
            exposures{jdet}{jtime} = livetimes{jdet}(jtime) * masses{jdet}(jtime) * ones(size(pt_eff));
            eff_singles{jdet}{jtime} = combined_eff_unbinned_v53_LT(detnum(jdet), pt_eff', timeperiod, 'singles')';
            eff_preselect{jdet}{jtime} = combined_eff_unbinned_v53_LT(detnum(jdet), pt_eff', timeperiod, 'fullconsistency')';
            eff_BDT{jdet}{jtime} = combined_eff_unbinned_v53_LT(detnum(jdet), pt_eff', timeperiod, 'method3')';
            
            % [AJA]: Kludge to ensure that preselection efficiency is above
            % BDT I think this can occasionally not occur because of the 
            % spline interpolation of the BDT efficiency)
            eff_preselect{jdet}{jtime} = max([eff_preselect{jdet}{jtime}, eff_BDT{jdet}{jtime}], [], 2);
            
            % diagnostic plots
            figure(10*jdet + jtime);
            plot(pt_eff, eff_singles{jdet}{jtime}, '-', 'color', LTcmap(3,:));
            hold on;
            plot(pt_eff, eff_preselect{jdet}{jtime}, '-', 'color', LTcmap(2,:));
            plot(pt_eff, eff_BDT{jdet}{jtime}, '-', 'color', LTcmap(1,:));
            hold off;
            axis([2, 13.1, 0, 1]);
            title([detlabels{jdet} timelabels{jdet}{jtime}]);
            xlabel('total phonon energy');
            ylabel('efficiency');
            legend('quality, thresholds, singles', '+ preselection', '+ BDT', 'location', 'southeast');
            box on;
            grid on;
            print(['figures/eff_skim_' num2str(detnum(jdet)-1100) '_' num2str(jtime) '.png'], '-dpng');
        end;
    end;

    save('ROOT_eff_info.mat', 'pt_eff', 'exposures', 'eff_singles', 'eff_preselect', 'eff_BDT');
end