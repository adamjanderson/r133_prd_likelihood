% compute_cf_weights_BDT.m
%
% This function computes the weights to make Cf data reproduce a WIMP
% spectrum. Note that because of the need to subtract the gammas
% contamination from the Cf data, the Ba skims must be done before running
% this script! We assume that you have Cf data loaded when running this
% script and the Ba skims are located in the same directory as the script.
%
% Adam Anderson
% adama@mit.edu

function cfWeights = compute_cf_weights_BDT(detnum, ptCf, qimCf, WIMPmass, timeperiod, plots)
    addpath('limitstuff');
    listOfDets = [1101 1104 1105 1111 1112 1114 1115];
    detlabels = {'T1Z1','T2Z1','T2Z2','T4Z2','T4Z3','T5Z2','T5Z3'};
    timelabels = {{''}, {''}, {''}, {''}, {''}, {' w/QOS1', ' w/o QOS1'}, {' BS', ' AS'}};

    dRdqAt42 = @(x) drdq_corr(x, 72.63, WIMPmass, 1e-42)';
    dRdptAt42 = @(x) (drdq_corr(pt2prNR_133_LT(x, detnum), 72.63, WIMPmass, 1e-42) .* dprdpt(x, detnum)')';

    R133LTcolors();
    minPt = 2;
    maxPt = 13.1;
    dpt = 0.5;
    pt_edges = minPt:dpt:maxPt;
    minQim = -2;
    maxQim = 7;
    dqim = 0.5;
    qim_edges = minQim:dqim:maxQim;
    
    
    %%%%% STEP 1 %%%%%
    % correct for the gamma leakage
    % first load various data files
    load('cf_efficiency_info.mat');
    load('ba_efficiency_info.mat');
    load(['ROOT_skim_ba_zip' num2str(detnum-1100) '_time' num2str(timeperiod) '.mat'], 'pt', 'qimean');
    ptBa = pt;
    qimBa = qimean;
    clear pt;
    clear qimean;

    edges = {pt_edges, qim_edges};
    [pt_mesh, qim_mesh] = meshgrid(pt_edges, qim_edges);

    cInRangeCf = inrange(ptCf,min(pt_edges),max(pt_edges)) & ...
                 inrange(qimCf,min(qim_edges),max(qim_edges));
    countsCf = hist3([ptCf(cInRangeCf),qimCf(cInRangeCf)], 'Edges', edges);
    [~, ptCfBinInd] = histc(ptCf, pt_edges);
    [~, qimCfBinInd] = histc(qimCf, qim_edges);
%     cInRangeBa = inrange(ptBa,2,13.1) & inrange(qimBa,-2,10);
    countsBa = hist3([ptBa,qimBa], 'Edges', edges);
%     [countsPtBa, ptBaBinInd] = histc(ptBa, pt_edges);
%     weightsBa = zeros(size(ptBaBinInd));
%     weightsBa(ptBaBinInd > 0) = 1./countsPtBa(ptBaBinInd(ptBaBinInd > 0)) / sum(1./countsPtBa(ptBaBinInd(ptBaBinInd > 0))) * sum(cInRangeBa); % normalize weights to number of events in Ba data
%     weightedCountsBa = zeros(size(countsBa));
%     for jpt = 1:(length(pt_edges)-1)
%         for jqim = 1:(length(qim_edges)-1)
%             cBin = inrange(ptBa, pt_edges(jpt), pt_edges(jpt+1)) & ...
%                    inrange(qimBa, qim_edges(jqim), qim_edges(jqim+1));
%             weightedCountsBa(jpt, jqim) = sum(weightsBa(cBin));
%         end;
%     end;
    
    % Normalization region: figure out where ER and NR bands cross
    band_energies = 0:0.1:13.1;
    [gmu, gmin2, gmax2] = gband_qimean_v53_LT(band_energies, detnum, 2);
    [nmu, nmin2, nmax2] = nband_qimean_v53_LT(band_energies, detnum, 2);
    [~, nmin3, nmax3] = nband_qimean_v53_LT(band_energies, detnum, 3);
    [pt_intersect, ~] = polyxpoly(band_energies, nmax3, band_energies, gmin2);

    % Normalization region: compute # of events in Cf & Ba normalization
    % regions
    [~, qimeanCf_low2, qimeanCf_high2] = gband_qimean_v53_LT(ptCf, detnum, 2);
    [~, qimeanBa_low2, qimeanBa_high2] = gband_qimean_v53_LT(ptBa, detnum, 2);
    cNormRegion_Cf = inrange(ptCf, pt_intersect, 13.1) & ...
                     qimCf > qimeanCf_low2 & qimCf < qimeanCf_high2;
    cNormRegion_Ba = inrange(ptBa, pt_intersect, 13.1) & ...
                     qimBa > qimeanBa_low2 & qimBa < qimeanBa_high2;
    N_Cf_in_norm_region = sum(cNormRegion_Cf);
    N_Ba_in_norm_region = sum(cNormRegion_Ba);
    
    % computed the counts, corrected for the gamma contamination
    countsCorrected = countsCf(1:(end-1), 1:(end-1)) ./ repmat(cf_efficiency_array{listOfDets==detnum}{timeperiod}, [1, length(qim_edges)-1])  ...
                    - (N_Cf_in_norm_region / N_Ba_in_norm_region) ...
                    * countsBa(1:(end-1), 1:(end-1)) ./ repmat(cf_efficiency_array{listOfDets==detnum}{timeperiod}, [1, length(qim_edges)-1]);
    countsCorrected(countsCorrected<0) = 0;
    temp = zeros(size(countsCorrected,1)+1, size(countsCorrected,2)+1);
    temp(1:(end-1),1:(end-1)) = countsCorrected;
    countsCorrected = temp;
    
    % compute gamma-corrected weights
    gamma_corrected_weights = zeros(size(ptCfBinInd));
    gamma_corrected_weights(cInRangeCf) = ...
                       countsCorrected(size(countsCorrected,1) * (qimCfBinInd(cInRangeCf)-1) + ptCfBinInd(cInRangeCf)) ./ ...
                       countsCf(size(countsCorrected,1) * (qimCfBinInd(cInRangeCf)-1) + ptCfBinInd(cInRangeCf));

    % set weights to zero of all events above the 3sigma upper edge
    % of the NR band
    [~, ~, nband_qimCf_high3] = nband_qimean_v53_LT(ptCf, detnum, 3);
    gamma_corrected_weights(qimCf > nband_qimCf_high3) = 0;


    %%%%% STEP 2 %%%%%
    % flatten the Cf
    [~, ind_counts_cf] = histc(ptCf, pt_edges);
    gamma_corrected_counts_cf = histc_weighted(pt_edges, ptCf, gamma_corrected_weights);
    flatten_weights = zeros(size(ind_counts_cf))';
    flatten_weights(ind_counts_cf~=0) = 1 ./ gamma_corrected_counts_cf(ind_counts_cf(ind_counts_cf~=0));

    
    %%%%% STEP 3 %%%%%
    % compute SAE
    wimp_weight = dRdptAt42(ptCf');
    if detnum == 1114
        temptime = 1;
    else
        temptime = timeperiod;
    end;
    eff_weight_total = combined_eff_unbinned_v53_LT(detnum,ptCf',temptime,'threshold',3);
    dRdptAt42_post_quality = @(x) dRdptAt42(x) .* combined_eff_unbinned_v53_LT(detnum,x,temptime,'quality');
    dRdptAt42_post_thresh = @(x) dRdptAt42(x) .* combined_eff_unbinned_v53_LT(detnum,x,temptime,'threshold');
    SAE_post_energyrange = integral(dRdptAt42, 2, 13.1) / integral(dRdqAt42, 0, 1000);     
    SAE_post_quality = integral(dRdptAt42_post_quality, 2, 13.1) / integral(dRdqAt42, 0, 1000);
    SAE_post_thresh = integral(dRdptAt42_post_thresh, 2, 13.1) / integral(dRdqAt42, 0, 1000);
    

    %%%%% STEP 4 %%%%%
    % combine all weights together
    normFactor = SAE_post_thresh * integral(dRdqAt42, 0, 1000) / ...
                    sum(gamma_corrected_weights' .* flatten_weights .* wimp_weight .* eff_weight_total);
    if isnan(normFactor) % some detectors have no sensitivity to very low WIMP masses, so we get NaN normalization
        normFactor = 0;
    end;
    cfWeights = normFactor * gamma_corrected_weights' .* flatten_weights .* wimp_weight .* eff_weight_total;
    
    sumWeights = sum(cfWeights);
    SAEfromWeights = sumWeights / integral(dRdqAt42, 0, 1000);

    
    if plots
        pt_plot = linspace(2, 13.1, 100);
        
        % plot of dark matter spectrum with various efficiencies
        % applied
        figure(1);
        plot(pt_plot, dRdptAt42(pt_plot), 'color', LTcmap(1,:));
        hold on;
        plot(pt_plot, dRdptAt42_post_quality(pt_plot), 'color', LTcmap(2,:));
        plot(pt_plot, dRdptAt42_post_thresh(pt_plot), 'color', LTcmap(3,:));
        hold off;
        title([detlabels{listOfDets==detnum} timelabels{listOfDets==detnum}{timeperiod}]);
        legend(['WIMP spectrum (SAE = ' num2str(SAE_post_energyrange) ')'], ...
               ['+ quality cuts (SAE = ' num2str(SAE_post_quality) ')'], ...
               ['+ threshold (SAE = ' num2str(SAE_post_thresh) ')']);
        yl = ylim;
        xlim([2, 13.1]);
        ylim([yl(1) 1.75*yl(2)]);
        xlabel('total phonon energy (ptNF) [keV]');
        ylabel('rate (arb.)');
        print(['figures_cf/SAE_' num2str(WIMPmass) 'GeV_' num2str(zipnum(detnum)) '_' num2str(timeperiod) '.png'], '-dpng')
        

        % plot of spectrum reweighted to uniform distribution
        figure(2);
        gamma_corrected_counts_cf = histc_weighted(pt_edges, ...
                                            ptCf, ...
                                            gamma_corrected_weights);
        flattened_counts_cf = histc_weighted(pt_edges, ...
                                            ptCf, ...
                                            gamma_corrected_weights' .* flatten_weights);
        unweighted_counts_cf = histc(ptCf, pt_edges);
        stairs(pt_edges, ...
                [unweighted_counts_cf(1:(end-1)); unweighted_counts_cf(end)]/sum(unweighted_counts_cf), ...
                'color', LTcmap(2,:));
        hold on;
        stairs(pt_edges, ...
                [gamma_corrected_counts_cf; gamma_corrected_counts_cf(end)] / sum(gamma_corrected_counts_cf), ...
                'color', LTcmap(3,:));
        stairs(pt_edges, ...
                [flattened_counts_cf; flattened_counts_cf(end)] / sum(flattened_counts_cf), ...
                'color', LTcmap(4,:));
        hold off;
        title([detlabels{listOfDets==detnum} timelabels{listOfDets==detnum}{timeperiod}]);
        xlim([0,14]);
        xlabel('total phonon energy (ptNF) [keV]');
        legend('raw', '+ gamma correction', '+ flattening');
        print(['figures_cf/flattening_' num2str(WIMPmass) 'GeV_' num2str(zipnum(detnum)) '_' num2str(timeperiod) '.png'], '-dpng');

        % plot spectrum after reweighting to that appropriate for
        % particular WIMP mass
        figure(3);
        weighted_counts_cf = histc_weighted(pt_edges, ptCf, cfWeights/dpt);
        stairs(pt_edges, [weighted_counts_cf; weighted_counts_cf(end)], ...
               'color', LTcmap(1,:));
        xlim([2, 13]);
        xlabel('total phonon energy [keV]');
        ylabel('WIMP rate (1 / kg / d / keV)');
        title({[num2str(WIMPmass) ' GeV'], ...
               ['total rate = ' num2str(sumWeights)], ...
               ['SAE = ' num2str(SAEfromWeights)]});
        print(['figures_cf/reweighted_' num2str(WIMPmass) 'GeV_' num2str(zipnum(detnum)) '_' num2str(timeperiod) '.png'], '-dpng');

        % normalization region used for the gamma subtraction
        figure(4);
        plot(ptCf, qimCf, '.', 'color', LTcmap_vlight(2,:));
        hold on;
        plot(ptCf(cNormRegion_Cf), qimCf(cNormRegion_Cf), '.', 'color', LTcmap(2,:));
        plot(band_energies, gmin2, '-', 'color', LTcmap(1,:));
        plot(band_energies, gmax2, '-', 'color', LTcmap(1,:));
        plot(band_energies, nmin2, '-', 'color', LTcmap(3,:));
        plot(band_energies, nmax2, '-', 'color', LTcmap(3,:));
        plot(band_energies, nmax3, '--', 'color', LTcmap(3,:));
        plot(band_energies, nmin3, '--', 'color', LTcmap(3,:));
        plot([pt_intersect, pt_intersect], [-2, 8], '--', 'color', [0.5, 0.5, 0.5]);
        hold off;
        axis([2, 13.1, -2, 8]);
        xlabel('total phonon energy (pt) [keV]');
        ylabel('charge energy (qimean) [keVee]');
        title([detlabels{listOfDets==detnum} timelabels{listOfDets==detnum}{timeperiod}]);
        print(['figures_cf/gamma_norm_region_' num2str(WIMPmass) 'GeV_' num2str(zipnum(detnum)) '_' num2str(timeperiod) '.png'], '-dpng');
        
        % 2d histogram of raw counts in Cf data
        figure(5);
        pcolor(pt_mesh, qim_mesh, log10(countsCf'));
        hcb = colorbar;
        colorTitleHandle = get(hcb, 'title');
        titleString = 'log_{10}N';
        set(colorTitleHandle, 'String', titleString);
        caxis([-1,3]);
        xlabel('total phonon energy (pt) [keV]');
        ylabel('charge energy (qimean) [keVee]');
        title([detlabels{listOfDets==detnum} timelabels{listOfDets==detnum}{timeperiod}]);
        print(['figures_cf/cf_counts_' num2str(WIMPmass) 'GeV_' num2str(zipnum(detnum)) '_' num2str(timeperiod) '.png'], '-dpng');

        % 2d histogram of raw counts in Ba data
        figure(6);
        pcolor(pt_mesh, qim_mesh, log10(countsBa'));
        hcb = colorbar;
        colorTitleHandle = get(hcb, 'title');
        titleString = 'log_{10}N';
        set(colorTitleHandle, 'String', titleString);
        caxis([-1,3]);
        xlabel('total phonon energy (pt) [keV]');
        ylabel('charge energy (qimean) [keVee]');
        title([detlabels{listOfDets==detnum} timelabels{listOfDets==detnum}{timeperiod}]);
        print(['figures_cf/ba_counts_' num2str(WIMPmass) 'GeV_' num2str(zipnum(detnum)) '_' num2str(timeperiod) '.png'], '-dpng');

        % 2d histogram of Cf data after gamma subtraction
        figure(7);
        pcolor(pt_mesh, qim_mesh, log10(countsCorrected'));
        hcb = colorbar;
        colorTitleHandle = get(hcb, 'title');
        titleString = 'log_{10}N';
        set(colorTitleHandle, 'String', titleString);
        caxis([-1,3]);
        xlabel('total phonon energy (pt) [keV]');
        ylabel('charge energy (qimean) [keVee]');
        title([detlabels{listOfDets==detnum} timelabels{listOfDets==detnum}{timeperiod}]);
        print(['figures_cf/cf_subtracted_' num2str(WIMPmass) 'GeV_' num2str(zipnum(detnum)) '_' num2str(timeperiod) '.png'], '-dpng');


        sum_gamma_corrected_weights = zeros(size(countsCorrected));
        for jpt = 1:(length(pt_edges)-1)
            for jqim = 1:(length(qim_edges)-1)
                cBin = inrange(ptCf, pt_edges(jpt), pt_edges(jpt+1)) & ...
                       inrange(qimCf, qim_edges(jqim), qim_edges(jqim+1));
                sum_gamma_corrected_weights(jpt, jqim) = sum(gamma_corrected_weights(cBin));
            end;
        end;

        % plot to check that weighted histogram agrees with rescaled
        % counts
        figure(8);
        pcolor(pt_mesh, qim_mesh, log10(sum_gamma_corrected_weights'));
        hcb = colorbar;
        colorTitleHandle = get(hcb, 'title');
        titleString = 'log_{10}N';
        set(colorTitleHandle, 'String', titleString);
        caxis([-1,3]);
        xlabel('total phonon energy (pt) [keV]');
        ylabel('charge energy (qimean) [keVee]');
        title([detlabels{listOfDets==detnum} timelabels{listOfDets==detnum}{timeperiod}]);
        print(['figures_cf/weights_check_' num2str(WIMPmass) 'GeV_' num2str(zipnum(detnum)) '_' num2str(timeperiod) '.png'], '-dpng');
    end;
end
