% ROOT_skim_ba.m
%
% A script for skimming Ba data for use in a likelihood analysis.


function ROOT_skim_ba()
close all;

    version = '11';

    % useful preliminary variables
    R133LTcolors
    detnum = [1101 1104 1105 1111 1112 1114 1115];
    time_edges = {[0, 1e11], [0, 1e11], [0, 1e11], [0, 1e11], [0, 1e11], [0, 1e11], [11200000000, 11212121650, 999999999999]};
    detlabels = {'T1Z1','T2Z1','T2Z2','T4Z2','T4Z3','T5Z2','T5Z3'};
    mindet = 1;
    maxdet = 7;
    
    cutpath = '/data3/cdmsbatsProd/R133/dataReleases/Prodv5-3_June2013/merged/cuts/current/';
    
    for jdet = mindet:maxdet
        for jtime = 1:(length(time_edges{jdet})-1)
            cAll = CAP_load_rootcut(cutpath, 'cGoodEv_v53', detnum(jdet)) & ...
                  ~CAP_load_rootcut(cutpath, 'cRandom_133', detnum(jdet)) & ...
                   CAP_load_rootcut(cutpath, 'cTriggeredEvent_133_LT', detnum(jdet)) & ...
                  ~CAP_load_rootcut(cutpath, 'cPmultTight_blind_v53', detnum(jdet)) & ...
                  ~CAP_load_rootcut(cutpath, 'cVTStrict_133', detnum(jdet)) & ...
                   CAP_load_rootcut(cutpath, 'cLiveTime_v53_LT', detnum(jdet)) & ...
                   CAP_load_rootcut(cutpath, 'cStableTrigThresh_133_LT', detnum(jdet)) & ...
                  ~CAP_load_rootcut(cutpath, 'cPsat_133', detnum(jdet)) & ...
                  ~CAP_load_rootcut(cutpath, 'cQsat_133', detnum(jdet)) & ...
                   CAP_load_rootcut(cutpath, 'cBiasPM_133', detnum(jdet)) & ...
                  ~CAP_load_rootcut(cutpath, 'cNuMI_133', detnum(jdet)) & ...
                   CAP_load_rootcut(cutpath, 'cLiveTime_v53_LT', detnum(jdet)) & ...
                   inrange(SeriesNumber(1101,0), time_edges{jdet}(jtime), time_edges{jdet}(jtime+1)) & ...
                   inrange(ptNF(detnum(jdet),0), 2, 2000);
            cConsistency = cAll & ...
                           CAP_load_rootcut(cutpath, 'cQin1_v53_LT', detnum(jdet)) & ...
                           CAP_load_rootcut(cutpath, 'cQin2_v53_LT', detnum(jdet)) & ...
                           CAP_load_rootcut(cutpath, 'cQisym_v53_LT', detnum(jdet)) & ...
                           CAP_load_rootcut(cutpath, 'cNR_qimean_3sig_v53_LT', detnum(jdet)) & ...
                           CAP_load_rootcut(cutpath, 'cConsistency_v53_LT', detnum(jdet));
            cAnalysisThreshold = cAll & ...
                           CAP_load_rootcut(cutpath, 'cAnalysisThreshold_v53_LT', detnum(jdet));
            cSignal = cConsistency & ...
                           CAP_load_rootcut(cutpath, 'cBDT_simul_v53_LT', detnum(jdet));
              
            
            
            disp([detlabels{jdet} ':']);
            disp(['# all = ' num2str(sum(cAll))]);
            disp(['# consistency = ' num2str(sum(cConsistency))]);
            disp(['# candidates = ' num2str(sum(cSignal))]);
            
            
            figure(jdet);
            linstep(ptNF(detnum(jdet), cAll & inrange(ptNF(detnum(jdet),0), 0, 1000)), 0:5:1000);
            
            pt = ptNF(detnum(jdet),cAll);
            prpart = prpartOF(detnum(jdet),cAll);
            pzpart = pzpartOF(detnum(jdet),cAll);
            qimean = qimeanOF(detnum(jdet),cAll);
            seriesNum = SeriesNumber(detnum(jdet),cAll);
            eventNum = EventNumber(detnum(jdet),cAll);
            qi1 = qi1OF(detnum(jdet),cAll);
            qo1 = qo1OF(detnum(jdet),cAll);
            qi2 = qi2OF(detnum(jdet),cAll);
            qo2 = qo2OF(detnum(jdet),cAll);
            cAnalysisThresh = double(cAnalysisThreshold(cAll));
            cConsistency = double(cConsistency(cAll));
            cSignal = double(cSignal(cAll));
            
            dlmwrite(['ROOT_skim_ba_zip' num2str(zipnum(detnum(jdet))) '_time' num2str(jtime) '.txt'], ...
                     [], ...
                      'delimiter', '\t', 'precision', 11);
                  
            save(['ROOT_skim_ba_zip' num2str(zipnum(detnum(jdet))) '_time' num2str(jtime) '.mat'], ...
                  'pt', 'qimean', 'prpart', 'pzpart', 'seriesNum', 'eventNum', ...
                  'qi1', 'qo1', 'qi2', 'qo2', ...
                  'cAnalysisThres', 'cConsistency', 'cSignal');
        end;
    end;
end