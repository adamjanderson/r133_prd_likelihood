# build_root_skim_simv4.py
#
# Python script for taking skimmed pulse simulation data and repackaging it 
# into ROOT files that can be used more easily in a likelihood analysis.


import numpy as np
from array import array
import ROOT as r
import scipy.io as scio

basepath = '/Users/adamanderson/Documents/CDMS/Analysis/R133_PRD_Likelihood/buildSkim/' #'/Users/adamanderson/Documents/CDMS/Analysis/cdmsanalysis/20140228_ROOT_skim/'

# 1 file per data type
f_data = r.TFile(basepath+'LT_skim_sim_v4.root', 'RECREATE')

# declare branch variables
pt = array('d', [0.])
qimean = array('d', [0.])
qsummax_custom = array('d', [0.])
qi1 = array('d', [0.])
qo1 = array('d', [0.])
qi2 = array('d', [0.])
qo2 = array('d', [0.])
prpart = array('d', [0.])
pzpart = array('d', [0.])
prpartSIMcorr = array('d', [0.])
pzpartSIMcorr = array('d', [0.])
SeriesNumber = array('d', [0.])
EventNumber = array('d', [0.])
SIMSeriesNumber = array('d', [0.])
SIMEventNumber = array('d', [0.])
SIMLibNum = array('d', [0.])
weights_1keV = array('d', [0.])
weights_206Pb_Qo = array('d', [0.])
weights_210Bi_Qo = array('d', [0.])
weights_210Bi_Qo_lowYield = array('d', [0.])
weights_210Pb_Qo = array('d', [0.])
weights_210Pb_Qo_lowYield = array('d', [0.])
weights_206Pb_QiS1 = array('d', [0.])
weights_210Bi_QiS1 = array('d', [0.])
weights_210Pb_QiS1 = array('d', [0.])
weights_206Pb_QiS2 = array('d', [0.])
weights_210Bi_QiS2 = array('d', [0.])
weights_210Pb_QiS2 = array('d', [0.])
weights_eff = array('d', [0.])
BDT5 = array('d', [0.])
BDT7 = array('d', [0.])
BDT10 = array('d', [0.])
BDT15 = array('d', [0.])

t_bg = [[],[],[],[],[],[],[]]
detnum = [1, 4, 5, 11, 12, 14, 15]
n_times = [1, 1, 1, 1, 1, 2, 2]
for jdet in range(len(detnum)):
	print "Processing detector #" + str(detnum[jdet])
	for jtime in range(n_times[jdet]):
		# get the different data sets
		bgdata = scio.loadmat('data/ROOT_skim_bg_model_v4_partcorr_zip'+str(detnum[jdet])+'_time'+str(jtime+1)+'.mat');


		# build the bg tree
		t_bg[jdet].append(r.TTree('sim_zip'+str(detnum[jdet])+'_time'+str(jtime+1), 'Cf calibration data'))
		t_bg[jdet][jtime].Branch('pt', pt, 'pt/D')
		t_bg[jdet][jtime].Branch('prpart', prpart, 'prpart/D')
		t_bg[jdet][jtime].Branch('pzpart', pzpart, 'pzpart/D')
		t_bg[jdet][jtime].Branch('prpartSIMcorr', prpartSIMcorr, 'prpartSIMcorr/D')
		t_bg[jdet][jtime].Branch('pzpartSIMcorr', pzpartSIMcorr, 'pzpartSIMcorr/D')
		t_bg[jdet][jtime].Branch('qimean', qimean, 'qimean/D')
		t_bg[jdet][jtime].Branch('qsummax_custom', qsummax_custom, 'qsummax_custom/D')
		t_bg[jdet][jtime].Branch('SeriesNumber', SeriesNumber, 'SeriesNumber/D')
		t_bg[jdet][jtime].Branch('EventNumber', EventNumber, 'EventNumber/D')
		t_bg[jdet][jtime].Branch('SIMSeriesNumber', SIMSeriesNumber, 'SIMSeriesNumber/D')
		t_bg[jdet][jtime].Branch('SIMEventNumber', SIMEventNumber, 'SIMEventNumber/D')
		t_bg[jdet][jtime].Branch('SIMLibNum', SIMLibNum, 'SIMLibNum/D')
		t_bg[jdet][jtime].Branch('weights_1keV', weights_1keV, 'weights_1keV/D')
		t_bg[jdet][jtime].Branch('weights_206Pb_Qo', weights_206Pb_Qo, 'weights_206Pb_Qo/D')
		t_bg[jdet][jtime].Branch('weights_210Bi_Qo', weights_210Bi_Qo, 'weights_210Bi_Qo/D')
		t_bg[jdet][jtime].Branch('weights_210Bi_Qo_lowYield', weights_210Bi_Qo_lowYield, 'weights_210Bi_Qo_lowYield/D')
		t_bg[jdet][jtime].Branch('weights_210Pb_Qo', weights_210Pb_Qo, 'weights_210Pb_Qo/D')
		t_bg[jdet][jtime].Branch('weights_210Pb_Qo_lowYield', weights_210Pb_Qo_lowYield, 'weights_210Pb_Qo_lowYield/D')
		t_bg[jdet][jtime].Branch('weights_206Pb_QiS1', weights_206Pb_QiS1, 'weights_206Pb_QiS1/D')
		t_bg[jdet][jtime].Branch('weights_210Bi_QiS1', weights_210Bi_QiS1, 'weights_210Bi_QiS1/D')
		t_bg[jdet][jtime].Branch('weights_210Pb_QiS1', weights_210Pb_QiS1, 'weights_210Pb_QiS1/D')
		t_bg[jdet][jtime].Branch('weights_206Pb_QiS2', weights_206Pb_QiS2, 'weights_206Pb_QiS2/D')
		t_bg[jdet][jtime].Branch('weights_210Bi_QiS2', weights_210Bi_QiS2, 'weights_210Bi_QiS2/D')
		t_bg[jdet][jtime].Branch('weights_210Pb_QiS2', weights_210Pb_QiS2, 'weights_210Pb_QiS2/D')
		t_bg[jdet][jtime].Branch('weights_eff', weights_eff, 'weights_eff/D')
		t_bg[jdet][jtime].Branch('BDT5', BDT5, 'BDT5/D')
		t_bg[jdet][jtime].Branch('BDT7', BDT7, 'BDT7/D')
		t_bg[jdet][jtime].Branch('BDT10', BDT10, 'BDT10/D')
		t_bg[jdet][jtime].Branch('BDT15', BDT15, 'BDT15/D')

		nevt = len(bgdata['pt'])
		for jevt in range(nevt):
			pt[0] = bgdata['pt'][jevt,0]
			prpart[0] = bgdata['prpart'][jevt,0]
			pzpart[0] = bgdata['pzpart'][jevt,0]
			qimean[0] = bgdata['qimean'][jevt,0]
			qi1[0] = bgdata['qi1'][jevt,0]
			qo1[0] = bgdata['qo1'][jevt,0]
			qi2[0] = bgdata['qi2'][jevt,0]
			qo2[0] = bgdata['qo2'][jevt,0]
			qsummax_custom[0] = np.amax(np.array([qi1[0] + qo1[0], qi2[0] + qo2[0]]))
			pzpartSIMcorr[0] = bgdata['pzpartSIMcorr'][jevt,0]
			prpartSIMcorr[0] = bgdata['prpartSIMcorr'][jevt,0]
			SeriesNumber[0] = bgdata['seriesNum'][jevt,0]
			EventNumber[0] = bgdata['eventNum'][jevt,0]
			SIMSeriesNumber[0] = bgdata['SIMSeriesNum'][jevt,0]
			SIMEventNumber[0] = bgdata['SIMEventNum'][jevt,0]
			SIMLibNum[0] = bgdata['SIMLibNum'][jevt,0]
			weights_1keV[0] = bgdata['weights_1keV'][jevt,0]
			weights_206Pb_Qo[0] = bgdata['weights_206Pb_Qout'][jevt,0]
			weights_210Bi_Qo[0] = bgdata['weights_210Bi_Qout'][jevt,0]
			weights_210Bi_Qo_lowYield[0] = bgdata['weights_210Bi_Qout_lowYield'][jevt,0]
			weights_210Pb_Qo[0] = bgdata['weights_210Pb_Qout'][jevt,0]
			weights_210Pb_Qo_lowYield[0] = bgdata['weights_210Pb_Qout_lowYield'][jevt,0]
			weights_206Pb_QiS1[0] = bgdata['weights_206Pb_Qin_S1'][jevt,0]
			weights_210Bi_QiS1[0] = bgdata['weights_210Bi_Qin_S1'][jevt,0]
			weights_210Pb_QiS1[0] = bgdata['weights_210Pb_Qin_S1'][jevt,0]
			weights_206Pb_QiS2[0] = bgdata['weights_206Pb_Qin_S2'][jevt,0]
			weights_210Bi_QiS2[0] = bgdata['weights_210Bi_Qin_S2'][jevt,0]
			weights_210Pb_QiS2[0] = bgdata['weights_210Pb_Qin_S2'][jevt,0]
			weights_eff[0] = bgdata['weights_eff'][jevt,0]
			BDT5[0] = bgdata['BDT5'][jevt,0]
			BDT7[0] = bgdata['BDT7'][jevt,0]
			BDT10[0] = bgdata['BDT10'][jevt,0]
			BDT15[0] = bgdata['BDT15'][jevt,0]

			t_bg[jdet][jtime].Fill()

f_data.Write()

f_data.Close()
