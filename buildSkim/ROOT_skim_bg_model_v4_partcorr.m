% ROOT_skim_bg_model_v4_partcorr.m
%
% A script for skimming simulation data for use a likelihood analysis.
%
% Adam Anderson
% adama@mit.edu


function ROOT_skim_bg_model_v4_partcorr()
    close all;

    % useful preliminary variables
    R133LTcolors
    detnum = [1101 1104 1105 1111 1112 1114 1115];
    time_edges = {[0, 1e11], [0, 1e11], [0, 1e11], [0, 1e11], [0, 1e11], ...
                  [11200000000, 11212121650, 999999999999], ...
                  [11200000000, 11212121650, 999999999999]};
    target_livetime = {129.4, 136.4, 134.4, 143.8, 138.5, 133.8, [31.3, 100.4]};
    detlabels = {'T1Z1','T2Z1','T2Z2','T4Z2','T4Z3','T5Z2','T5Z3'};
    mindet = 1;
    maxdet = 7;
    

    for jdet = mindet:maxdet
        for jtime = 1:(length(time_edges{jdet})-1)
            detnum(jdet)
            cSanity = abs(qimeanOF(detnum(jdet),0)) > 1e-10 & abs(qimeanOF(detnum(jdet),0)) < 30;
            
            if detnum(jdet) ~= 1114
                cTime = inrange(SeriesNumber(detnum(jdet),0), time_edges{jdet}(jtime), time_edges{jdet}(jtime+1)) & ...
                        inrange(PTSIMSeriesNumber(detnum(jdet),0), time_edges{jdet}(jtime), time_edges{jdet}(jtime+1));
            else
                if jtime == 1
                    cTime = ~inrange(SeriesNumber(detnum(jdet),0), 11301202113, 11303160402);
                end
                if jtime == 2
                    cTime = inrange(SeriesNumber(detnum(jdet),0), 11301202113, 11303160402);
                end
            end
                    cAll = cTime & cSanity;
            
            
%             dlmwrite(['/fast/data/R133/skims/ROOT_skim_bg_model_v4_partcorr_zip' num2str(zipnum(detnum(jdet))) '_time' num2str(jtime) '.txt'], ...
%                      [ptNF(detnum(jdet),cAll), ...
%                       prpartOF(detnum(jdet),cAll), ...
%                       pzpartOF(detnum(jdet),cAll), ...
%                       prpartOFSIMcorr_v4(detnum(jdet),cAll), ...
%                       pzpartOFSIMcorr_v4(detnum(jdet),cAll), ...
%                       qimeanOF(detnum(jdet),cAll), ...
%                       SeriesNumber(detnum(jdet),cAll), ...
%                       EventNumber(detnum(jdet),cAll), ...
%                       PTSIMlibnum(detnum(jdet),cAll), ...
%                       PTSIMEventNumber(detnum(jdet),cAll), ...
%                       PTSIMSeriesNumber(detnum(jdet),cAll)], ...
%                       'delimiter', '\t', 'precision', 11);

            pt = ptNF(detnum(jdet),cAll);
            prpart = prpartOF(detnum(jdet),cAll);
            pzpart = pzpartOF(detnum(jdet),cAll);
            prpartSIMcorr = prpartOFSIMcorr_v4(detnum(jdet),cAll);
            pzpartSIMcorr = pzpartOFSIMcorr_v4(detnum(jdet),cAll);
            qimean = qimeanOF(detnum(jdet),cAll);
            qi1 = qi1OF(detnum(jdet),cAll);
            qo1 = qo1OF(detnum(jdet),cAll);
            qi2 = qi2OF(detnum(jdet),cAll);
            qo2 = qo2OF(detnum(jdet),cAll);
            seriesNum = SeriesNumber(detnum(jdet),cAll);
            eventNum = EventNumber(detnum(jdet),cAll);
            SIMLibNum = PTSIMlibnum(detnum(jdet),cAll);
            SIMEventNum = PTSIMEventNumber(detnum(jdet),cAll);
            SIMSeriesNum = PTSIMSeriesNumber(detnum(jdet),cAll);
            temp_weights_206Pb_Qout = weights_206Pb_Qout_LibSim4_133_LT(detnum(jdet), true);
            temp_weights_210Bi_Qout = weights_210Bi_Qout_LibSim4_133_LT(detnum(jdet), true);
            temp_weights_210Bi_Qout_lowYield = weights_210Bi_Qout_lowYield_LibSim4_133_LT(detnum(jdet), true);
            temp_weights_210Pb_Qout = weights_210Pb_Qout_LibSim4_133_LT(detnum(jdet), true);
            temp_weights_210Pb_Qout_lowYield = weights_210Pb_Qout_lowYield_LibSim4_133_LT(detnum(jdet), true);
            temp_weights_206Pb_Qin_S1 = weights_206Pb_Qin_S1_LibSim4_133_LT(detnum(jdet), true);
            temp_weights_210Bi_Qin_S1 = weights_210Bi_Qin_S1_LibSim4_133_LT(detnum(jdet), true);
            temp_weights_210Pb_Qin_S1 = weights_210Pb_Qin_S1_LibSim4_133_LT(detnum(jdet), true);
            temp_weights_206Pb_Qin_S2 = weights_206Pb_Qin_S2_LibSim4_133_LT(detnum(jdet), true);
            temp_weights_210Bi_Qin_S2 = weights_210Bi_Qin_S2_LibSim4_133_LT(detnum(jdet), true);
            temp_weights_210Pb_Qin_S2 = weights_210Pb_Qin_S2_LibSim4_133_LT(detnum(jdet), true);
            temp_weights_1keV = weights_1keV_LibSim4_133_LT(detnum(jdet));
            weights_gammas = double(SIMLibNum == 0);
            weights_206Pb_Qout = temp_weights_206Pb_Qout(cAll);
            weights_210Bi_Qout = temp_weights_210Bi_Qout(cAll);
            weights_210Bi_QoLowYield = temp_weights_210Bi_Qout_lowYield(cAll);
            weights_210Pb_Qout = temp_weights_210Pb_Qout(cAll);
            weights_210Pb_QoLowYield = temp_weights_210Pb_Qout_lowYield(cAll);
            weights_206Pb_Qi1 = temp_weights_206Pb_Qin_S1(cAll);
            weights_210Bi_Qi1 = temp_weights_210Bi_Qin_S1(cAll);
            weights_210Pb_Qi1 = temp_weights_210Pb_Qin_S1(cAll);
            weights_206Pb_Qi2 = temp_weights_206Pb_Qin_S2(cAll);
            weights_210Bi_Qi2 = temp_weights_210Bi_Qin_S2(cAll);
            weights_210Pb_Qi2 = temp_weights_210Pb_Qin_S2(cAll);
            weights_1keV = temp_weights_1keV(cAll);
            threshold_eff = combined_eff_unbinned_v53_LT(detnum(jdet), pt', jtime, 'threshold',3)';
            [~, temp_BDT_5] = cBDT_v53_LT_5GeV(detnum(jdet));
            [~, temp_BDT_7] = cBDT_v53_LT_7GeV(detnum(jdet));
            [~, temp_BDT_10] = cBDT_v53_LT_10GeV(detnum(jdet));
            [~, temp_BDT_15] = cBDT_v53_LT_15GeV(detnum(jdet));
            BDT5 = temp_BDT_5(cAll);
            BDT7 = temp_BDT_7(cAll);
            BDT10 = temp_BDT_10(cAll);
            BDT15 = temp_BDT_15(cAll);

            save(['ROOT_skim_bg_model_v4_partcorr_zip' num2str(zipnum(detnum(jdet))) '_time' num2str(jtime) '.mat'], ...
                  'pt', 'prpart', 'pzpart', 'prpartSIMcorr', 'pzpartSIMcorr', ...
                  'qi1', 'qo1', 'qi2', 'qo2', ...
                  'qimean', 'seriesNum', 'eventNum', 'SIMLibNum', 'SIMEventNum', 'SIMSeriesNum', ...
                  'weights_gammas', 'weights_206Pb_Qout', 'weights_210Bi_Qout', 'weights_210Pb_Qout', ...
                  'weights_210Bi_QoLowYield', 'weights_210Pb_QoLowYield', ...
                  'weights_206Pb_Qi1', 'weights_210Bi_Qi1', 'weights_210Pb_Qi1', ...
                  'weights_206Pb_Qi2', 'weights_210Bi_Qi2', 'weights_210Pb_Qi2', ...
                  'weights_1keV', 'threshold_eff', 'BDT5', 'BDT7', 'BDT10', 'BDT15');
        end;
    end;
end