# build_root_skim.py
#
# Python script for taking skimmed R133 data and repackaging it into ROOT files
# that can be used more easily in a likelihood analysis.


import numpy as np
from array import array
import ROOT as r
import scipy.io as scio

# basepath = '/Users/adamanderson/Documents/CDMS/Analysis/cdmsanalysis/20140228_ROOT_skim/'
basepath = '/Users/adamanderson/Documents/CDMS/Analysis/R133_PRD_Likelihood/buildSkim/' 

# 1 file per data type
f_data = r.TFile(basepath+'LT_skim.root', 'RECREATE')

# declare branch variables
pt = array('d', [0.])
qimean = array('d', [0.])
prpart = array('d', [0.])
pr_custompart = array('d', [0.])
pzpart = array('d', [0.])
qi1 = array('d', [0.])
qo1 = array('d', [0.])
qi2 = array('d', [0.])
qo2 = array('d', [0.])
qsummax_custom = array('d', [0.])
pr_custom = array('d', [0.])
yt_custom = array('d', [0.])
SeriesNumber = array('d', [0.])
EventNumber = array('d', [0.])
BDT5 = array('d', [0.])
BDT7 = array('d', [0.])
BDT10 = array('d', [0.])
BDT15 = array('d', [0.])
cZC = array('d', [0.])
cLT = array('d', [0.])
cQouter = array('d', [0.])
cConsistency = array('d', [0.])
cAnalysisThreshold = array('d', [0.])
cBDT = array('d', [0.])
cQin = array('d', [0.])
cQsym = array('d', [0.])
TriggerEff = array('d', [0.])
weights_gamma = array('d', [0.])
weights_1keV = array('d', [0.])
weights_210Pb_Qout = array('d', [0.])
weights_210Bi_Qout = array('d', [0.])
weights_206Pb_Qout = array('d', [0.])
weights_210Pb_Qin = array('d', [0.])
weights_210Bi_Qin = array('d', [0.])
weights_210Pb_Qout_loY = array('d', [0.])
weights_210Bi_Qout_loY = array('d', [0.])
weights_gamma = array('d', [0.])
weights_1keV = array('d', [0.])
weights_210Pb_Qout = array('d', [0.])
weights_210Bi_Qout = array('d', [0.])
weights_206Pb_Qout = array('d', [0.])
weights_210Pb_Qin = array('d', [0.])
weights_210Bi_Qin = array('d', [0.])
weights_210Pb_Qout_loY = array('d', [0.])
weights_210Bi_Qout_loY = array('d', [0.])
weights_cf_5GeV = array('d', [0.])
weights_cf_7GeV = array('d', [0.])
weights_cf_10GeV = array('d', [0.])
weights_cf_15GeV = array('d', [0.])

pt_eff = array('d', [0.])
exposure = array('d', [0.])
eff_singles = array('d', [0.])
eff_preselect = array('d', [0.])
eff_BDT = array('d', [0.])



t_cf = [[],[],[],[],[],[],[]]
t_ba = [[],[],[],[],[],[],[]]
t_bg = [[],[],[],[],[],[],[]]
t_info = [[],[],[],[],[],[],[]]
# t_sim = [[],[],[],[],[],[],[]]
detnum = [1, 4, 5, 11, 12, 14, 15]
n_times = [1, 1, 1, 1, 1, 2, 2]
for jdet in range(len(detnum)):
	print "Processing detector #" + str(detnum[jdet])
	for jtime in range(n_times[jdet]):
		##### LOAD DATA #####
		cfdata = scio.loadmat('data/ROOT_skim_cf_zip'+str(detnum[jdet])+'_time'+str(jtime+1)+'.mat')
		badata = scio.loadmat('data/ROOT_skim_ba_zip'+str(detnum[jdet])+'_time'+str(jtime+1)+'.mat')
		bgdata = np.loadtxt('data/ROOT_skim_lowbg_zip'+str(detnum[jdet])+'_time'+str(jtime+1)+'.txt')
		infodata = scio.loadmat('data/ROOT_eff_info_zip'+str(detnum[jdet])+'_time'+str(jtime+1)+'.mat')

		weights_cf = [array('d', [0.]) for number in range(cfdata['WIMP_masses'].size)]


		##### BUILD THE CF TREE #####
		t_cf[jdet].append(r.TTree('cf_zip'+str(detnum[jdet])+'_time'+str(jtime+1), 'Cf calibration data'))
		t_cf[jdet][jtime].Branch('pt', pt, 'pt/D')
		t_cf[jdet][jtime].Branch('prpart', pr_custompart, 'prpart/D')
		t_cf[jdet][jtime].Branch('pzpart', pzpart, 'pzpart/D')
		t_cf[jdet][jtime].Branch('qimean', qimean, 'qimean/D')
		t_cf[jdet][jtime].Branch('SeriesNumber', SeriesNumber, 'SeriesNumber/D')
		t_cf[jdet][jtime].Branch('EventNumber', EventNumber, 'EventNumber/D')
		t_cf[jdet][jtime].Branch('BDT5', BDT5, 'BDT5/D')
		t_cf[jdet][jtime].Branch('BDT7', BDT7, 'BDT7/D')
		t_cf[jdet][jtime].Branch('BDT10', BDT10, 'BDT10/D')
		t_cf[jdet][jtime].Branch('BDT15', BDT15, 'BDT15/D')
		t_cf[jdet][jtime].Branch('cConsistency', cConsistency, 'cConsistency/D')
		t_cf[jdet][jtime].Branch('cBDT', cBDT, 'BDT/D')
		t_cf[jdet][jtime].Branch('TriggerEff', TriggerEff, 'TriggerEff/D')
		for jmass in range(cfdata['WIMP_masses'].size):
			weights_branch_name = 'weights_cf_%(mass).2f_GeV' % {"mass": cfdata['WIMP_masses'][jmass, 0]} #[0,jmass]}
			t_cf[jdet][jtime].Branch(weights_branch_name, weights_cf[jmass], weights_branch_name+'/D')

		nevt = len(cfdata['pt'])
		for jevt in range(nevt):
			pt[0] = cfdata['pt'][jevt,0]
			pr_custompart[0] = cfdata['prpart'][jevt,0]
			pzpart[0] = cfdata['pzpart'][jevt,0]
			qimean[0] = cfdata['qimean'][jevt,0]
			SeriesNumber[0] = cfdata['seriesNum'][jevt,0]
			EventNumber[0] = cfdata['eventNum'][jevt,0]
			BDT5[0] = cfdata['BDT5'][jevt,0]
			BDT7[0] = cfdata['BDT7'][jevt,0]
			BDT10[0] = cfdata['BDT10'][jevt,0]
			BDT15[0] = cfdata['BDT15'][jevt,0]
			cConsistency[0] = cfdata['cConsistency'][jevt,0]
			cBDT[0] = cfdata['cSignal'][jevt,0]
			TriggerEff[0] = cfdata['trigger_eff'][jevt,0]

			for jmass in range(cfdata['WIMP_masses'].size):
				weights_branch_name = 'weights_cf_%(mass).2f_GeV' % {"mass": cfdata['WIMP_masses'][jmass,0]} #[0,jmass]}
				weights_cf[jmass][0] = cfdata['WIMP_weights'][jmass,0][jevt,0]

			t_cf[jdet][jtime].Fill()



		##### BUILD THE BA TREE #####
		t_ba[jdet].append(r.TTree('ba_zip'+str(detnum[jdet])+'_time'+str(jtime+1), 'Ba calibration data'))
		t_ba[jdet][jtime].Branch('pt', pt, 'pt/D')
		t_ba[jdet][jtime].Branch('prpart', pr_custompart, 'prpart/D')
		t_ba[jdet][jtime].Branch('pzpart', pzpart, 'pzpart/D')
		t_ba[jdet][jtime].Branch('qimean', qimean, 'qimean/D')
		t_ba[jdet][jtime].Branch('SeriesNumber', SeriesNumber, 'SeriesNumber/D')
		t_ba[jdet][jtime].Branch('EventNumber', EventNumber, 'EventNumber/D')
		t_ba[jdet][jtime].Branch('qi1', qi1, 'qi1/D')
		t_ba[jdet][jtime].Branch('qo1', qo1, 'qo1/D')
		t_ba[jdet][jtime].Branch('qi2', qi2, 'qi2/D')
		t_ba[jdet][jtime].Branch('qo2', qo2, 'qo2/D')
		t_ba[jdet][jtime].Branch('qsummax_custom', qsummax_custom, 'qsummax_custom/D')
		t_ba[jdet][jtime].Branch('pr_custom', pr_custom, 'pr_custom/D')
		t_ba[jdet][jtime].Branch('yt_custom', yt_custom, 'yt_custom/D')
		t_ba[jdet][jtime].Branch('cAnalysisThreshold', cAnalysisThreshold, 'cAnalysisThreshold/D')
		t_ba[jdet][jtime].Branch('cConsistency', cConsistency, 'cConsistency/D')
		t_ba[jdet][jtime].Branch('cBDT', cBDT, 'BDT/D')

		nevt = len(badata)
		for jevt in range(nevt):
			pt[0] = badata['pt'][jevt,0]
			pr_custompart[0] = badata['prpart'][jevt,0]
			pzpart[0] = badata['pzpart'][jevt,0]
			qimean[0] = badata['qimean'][jevt,0]
			SeriesNumber[0] = badata['seriesNum'][jevt,0]
			EventNumber[0] = badata['eventNum'][jevt,0]
			qi1[0] = badata['qi1'][jevt,0]
			qo1[0] = badata['qo1'][jevt,0]
			qi2[0] = badata['qi2'][jevt,0]
			qo2[0] = badata['qo2'][jevt,0]
			cAnalysisThreshold[0] = badata['cAnalysisThresh'][jevt,0]
			cConsistency[0] = badata['cConsistency'][jevt,0]
			cBDT[0] = badata['cSignal'][jevt,0]
			qsummax_custom[0] = np.amax(np.array([qi1[0] + qo1[0], qi2[0] + qo2[0]]))
			qsummin = np.amin(np.array([qi1[0] + qo1[0], qi2[0] + qo2[0]]))
			pr_custom[0] = pt[0] - ((4./3.)*qsummin + (2./3.)*np.abs(qsummax_custom[0] - qsummin))
			yt_custom[0] = qsummax_custom[0] / pr_custom[0]

			t_ba[jdet][jtime].Fill()



		##### BUILD THE BG TREE #####
		t_bg[jdet].append(r.TTree('bg_zip'+str(detnum[jdet])+'_time'+str(jtime+1), 'Cf calibration data'))
		t_bg[jdet][jtime].Branch('pt', pt, 'pt/D')
		t_bg[jdet][jtime].Branch('prpart', prpart, 'prpart/D')
		t_bg[jdet][jtime].Branch('pzpart', pzpart, 'pzpart/D')
		t_bg[jdet][jtime].Branch('qimean', qimean, 'qimean/D')
		t_bg[jdet][jtime].Branch('SeriesNumber', SeriesNumber, 'SeriesNumber/D')
		t_bg[jdet][jtime].Branch('EventNumber', EventNumber, 'EventNumber/D')
		t_bg[jdet][jtime].Branch('qi1', qi1, 'qi1/D')
		t_bg[jdet][jtime].Branch('qo1', qo1, 'qo1/D')
		t_bg[jdet][jtime].Branch('qi2', qi2, 'qi2/D')
		t_bg[jdet][jtime].Branch('qo2', qo2, 'qo2/D')
		t_bg[jdet][jtime].Branch('qsummax_custom', qsummax_custom, 'qsummax_custom/D')
		t_bg[jdet][jtime].Branch('pr_custom', pr_custom, 'pr_custom/D')
		t_bg[jdet][jtime].Branch('yt_custom', yt_custom, 'yt_custom/D')
		t_bg[jdet][jtime].Branch('BDT5', BDT5, 'BDT5/D')
		t_bg[jdet][jtime].Branch('BDT7', BDT7, 'BDT7/D')
		t_bg[jdet][jtime].Branch('BDT10', BDT10, 'BDT10/D')
		t_bg[jdet][jtime].Branch('BDT15', BDT15, 'BDT15/D')
		t_bg[jdet][jtime].Branch('cQouter', cQouter, 'cQouter/D')
		t_bg[jdet][jtime].Branch('cZC', cZC, 'cZC/D')
		t_bg[jdet][jtime].Branch('cLT', cLT, 'cLT/D')
		t_bg[jdet][jtime].Branch('cAnalysisThreshold', cAnalysisThreshold, 'cAnalysisThreshold/D')
		t_bg[jdet][jtime].Branch('cConsistency', cConsistency, 'cConsistency/D')
		t_bg[jdet][jtime].Branch('cBDT', cBDT, 'cBDT/D')
		t_bg[jdet][jtime].Branch('cQin', cQin, 'cQin/D')
		t_bg[jdet][jtime].Branch('cQsym', cQsym, 'cQsym/D')
		t_bg[jdet][jtime].Branch('TriggerEff', TriggerEff, 'TriggerEff/D')

		nevt = len(bgdata)
		for jevt in range(nevt):
			pt[0] = bgdata[jevt,0]
			prpart[0] = bgdata[jevt,1]
			pzpart[0] = bgdata[jevt,2]
			qimean[0] = bgdata[jevt,3]
			SeriesNumber[0] = bgdata[jevt,4]
			EventNumber[0] = bgdata[jevt,5]
			qi1[0] = bgdata[jevt,6]
			qo1[0] = bgdata[jevt,7]
			qi2[0] = bgdata[jevt,8]
			qo2[0] = bgdata[jevt,9]
			BDT5[0] = bgdata[jevt,12]
			BDT7[0] = bgdata[jevt,13]
			BDT10[0] = bgdata[jevt,14]
			BDT15[0] = bgdata[jevt,15]
			cZC[0] = bgdata[jevt,16]
			cLT[0] = bgdata[jevt,17]
			cQouter[0] = bgdata[jevt,18]
			cAnalysisThreshold[0] = bgdata[jevt,19]
			cConsistency[0] = bgdata[jevt,20]
			cBDT[0] = bgdata[jevt,21]
			cQin[0] = bgdata[jevt,22]
			cQsym[0] = bgdata[jevt,23]
			TriggerEff[0] = bgdata[jevt,24]
			qsummax_custom[0] = np.amax(np.array([qi1[0] + qo1[0], qi2[0] + qo2[0]]))
			qsummin = np.amin(np.array([qi1[0] + qo1[0], qi2[0] + qo2[0]]))
			pr_custom[0] = pt[0] - ((4./3.)*qsummin + (2./3.)*np.abs(qi1[0] + qo1[0] - qi2[0] - qo2[0]))
			yt_custom[0] = qsummax_custom[0] / pr_custom[0]

			t_bg[jdet][jtime].Fill()



		##### BUILD THE EFFICIENCY TREE #####
		t_info[jdet].append(r.TTree('eff_zip'+str(detnum[jdet])+'_time'+str(jtime+1), 'Efficiency info'))
		t_info[jdet][jtime].Branch('pt_eff', pt_eff, 'pt_eff/D')
		t_info[jdet][jtime].Branch('exposure', exposure, 'exposure/D')
		t_info[jdet][jtime].Branch('eff_singles', eff_singles, 'eff_singles/D')
		t_info[jdet][jtime].Branch('eff_preselect', eff_preselect, 'eff_preselect/D')
		t_info[jdet][jtime].Branch('eff_BDT', eff_BDT, 'eff_BDT/D')

		npoints = len(infodata['pt_eff'])
		for jpoint in range(npoints):
			pt_eff[0] = infodata['pt_eff'][jpoint,0]
			exposure[0] = infodata['exposure'][jpoint,0]
			eff_singles[0] = infodata['eff_singles'][jpoint,0]
			eff_preselect[0] = infodata['eff_preselect'][jpoint,0]
			eff_BDT[0] = infodata['eff_BDT'][jpoint,0]

			t_info[jdet][jtime].Fill();

f_data.Write()

f_data.Close()
