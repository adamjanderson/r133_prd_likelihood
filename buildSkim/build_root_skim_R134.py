#!/usr/bin/pyt_customhon
# pr_customocess_root_skims.py

import numpy as np
from array import array
import ROOT as r

basepath = '/Users/adamanderson/Documents/CDMS/Analysis/cdmsanalysis/20140228_ROOT_skim/'

# 1 file per data type
f_data = r.TFile(basepath+'LT_skim_R134.root', 'RECREATE')

# declare branch variables
pt = array('d', [0.])
qimean = array('d', [0.])
pr_custompart = array('d', [0.])
pzpart = array('d', [0.])
qi1 = array('d', [0.])
qo1 = array('d', [0.])
qi2 = array('d', [0.])
qo2 = array('d', [0.])
qsummax_custom = array('d', [0.])
pr_custom = array('d', [0.])
yt_custom = array('d', [0.])
SeriesNumber = array('d', [0.])
EventNumber = array('d', [0.])

t_bg = [[],[],[],[],[],[],[]]
detnum = [1, 4, 5, 11, 12, 14, 15]
n_times = [1, 1, 1, 1, 1, 1, 1]
for jdet in range(len(detnum)):
	print "Processing detector #" + str(detnum[jdet])
	for jtime in range(n_times[jdet]):
		# get the different data sets
		bgdata = np.loadtxt('ROOT_skim_lowbg_R134_zip'+str(detnum[jdet])+'_time'+str(jtime+1)+'.txt')


		# build the bg tree
		t_bg[jdet].append(r.TTree('bg_zip'+str(detnum[jdet])+'_time'+str(jtime+1), 'Cf calibration data'))
		t_bg[jdet][jtime].Branch('pt', pt, 'pt/D')
		t_bg[jdet][jtime].Branch('pr_custompart', pr_custompart, 'pr_custompart/D')
		t_bg[jdet][jtime].Branch('pzpart', pzpart, 'pzpart/D')
		t_bg[jdet][jtime].Branch('qimean', qimean, 'qimean/D')
		t_bg[jdet][jtime].Branch('SeriesNumber', SeriesNumber, 'SeriesNumber/D')
		t_bg[jdet][jtime].Branch('EventNumber', EventNumber, 'EventNumber/D')
		t_bg[jdet][jtime].Branch('qi1', qi1, 'qi1/D')
		t_bg[jdet][jtime].Branch('qo1', qo1, 'qo1/D')
		t_bg[jdet][jtime].Branch('qi2', qi2, 'qi2/D')
		t_bg[jdet][jtime].Branch('qo2', qo2, 'qo2/D')
		t_bg[jdet][jtime].Branch('qsummax_custom', qsummax_custom, 'qsummax_custom/D')
		t_bg[jdet][jtime].Branch('pr_custom', pr_custom, 'pr_custom/D')
		t_bg[jdet][jtime].Branch('yt_custom', yt_custom, 'yt_custom/D')

		nevt = len(bgdata)
		for jevt in range(nevt):
			pt[0] = bgdata[jevt,0]
			pr_custompart[0] = bgdata[jevt,1]
			pzpart[0] = bgdata[jevt,2]
			qimean[0] = bgdata[jevt,3]
			SeriesNumber[0] = bgdata[jevt,4]
			EventNumber[0] = bgdata[jevt,5]
			qi1[0] = bgdata[jevt,6]
			qo1[0] = bgdata[jevt,7]
			qi2[0] = bgdata[jevt,8]
			qo2[0] = bgdata[jevt,9]
			qsummax_custom[0] = np.amax(np.array([qi1[0] + qo1[0], qi2[0] + qo2[0]]))
			qsummin = np.amin(np.array([qi1[0] + qo1[0], qi2[0] + qo2[0]]))
			pr_custom[0] = pt[0] - ((4./3.)*qsummin + (2./3.)*np.abs(qi1[0] + qo1[0] - qi2[0] - qo2[0]))
			yt_custom[0] = qsummax_custom[0] / pr_custom[0]
			# print str(qi1[0]) + ', ' + str(qo1[0]) + ', ' + str(qi2[0]) + ', ' + str(qo2[0]) + ', ' + str(qsummax_custom[0]) + ', ' + \
			# 	str(qsummin) + ', ' + str(pt[0]) + ', ' + str(pr_custom[0]) + ', ' + str(yt_custom[0])

			t_bg[jdet][jtime].Fill()

f_data.Write()

f_data.Close()
