function rate = drdq_corr_Ge(Q, m_WIMP, sigma)
    dm_params;

    A = [70, 72, 73, 74, 76];
    frac = [0.2123, 0.2766, 0.0773, 0.3594, 0.0744];
    
    rate = 0;
    for jIsotope = 1:length(A)
        rate = rate + frac(jIsotope) * drdq_corr(Q, A(jIsotope), m_WIMP, sigma);
    end
end