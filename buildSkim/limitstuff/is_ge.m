function ge_flag = is_ge(det)
%function to take list of det numbers and return 1 if they're ge

%let's make a logic variable for Ge detectors
lgc_ge=true(30,1);
%we now make the Si false
lgc_ge(zipnum(404))=false;
lgc_ge(zipnum(406))=false;

lgc_ge(zipnum(411))=false;
lgc_ge(zipnum(412))=false;
lgc_ge(zipnum(414))=false;
lgc_ge(zipnum(416))=false;

lgc_ge(zipnum(421))=false;
lgc_ge(zipnum(423))=false;

lgc_ge(zipnum(431))=false;
lgc_ge(zipnum(433))=false;

lgc_ge(zipnum(443))=false;


det(det > 400) = zipnum(det(det > 400));

ge_flag = lgc_ge(det);