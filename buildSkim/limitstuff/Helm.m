% Helm.m function to return Helm (Woods-Saxon) form-factor squared
%
% F2 = Helm(eR,AT)
%
% eR = vector of recoil energies (in keV)
% AT = (root-mean-squared) atomic weight of target (Si =28, Ge=72.7)
%
% Uses L&S eqn 4.7 and takes rn, s from next-to-last paragraph of 
% section 4 (right after eqn 4.11)
% Should be valid for all nuclei.
%
% SG  2000/02/15
% RWS 2000/03/06 allow AT to be passed as global A_g, add comments
%%%%%%%%%%%%%%%

function F2 = Helm(eR,AT)

if nargin < 2
   global A_g
   AT = A_g;
end

mN = 0.9315; % GeV/c2
eR = eR(:);

% Helm form factor eqn 4.7
% DAMA98 paper explicitly states they use this

% need spherical bessel function:
% 3 * j1(qrn)/qrn * exp(- (qs)^2 /2 )
% which is just solid sphere * exp(- (qs)^2 /2)

% q = sqrt(2 * mNucleus * eR);
% right before eqn 4.1
q = sqrt(2 * mN * AT .* eR);  % MeV/c

% there is some indeterminacy as to the parameters DAMA use for Helm 
% form factor.  Smith and Lewin offer three different forms.
% 1) rn = 1.14A^1/3, s = 0.9 fm
% 2) r_rms = 0.89A^1/3 + 0.30, r_rms^2 = (3/5) r_n^2 + 3 s^2, s = 1 fm
% 3) c = 1.23A^1/3 - 0.60, r_n^2 = c^2 + (7/3)*pi^2*a^2 - 5s^2, 
%    s = 0.9 fm, a = 0.52 fm
% The last is supposed to be most accurate
a = 0.52; 
s = 0.9;
c = 1.23 * AT.^(1/3) - 0.60;
rn = sqrt(c.^2 + (7/3)*pi^2.*a.^2 - 5*s.^2); 

hbarc = 197.3; % MeV fm

% in normal units, qrn/hbar is unitless; need hbarc in this case
qrn = q.*rn/hbarc;
qs = q.*s/hbarc;

F2 = (3*(sin(qrn)-qrn.*cos(qrn)) ./ (eps+qrn).^3).^2;
F2 = F2 .* exp(-(qs).^2 );

% Correct qr<=0 points
F2(isnan(F2)) = 1; 
