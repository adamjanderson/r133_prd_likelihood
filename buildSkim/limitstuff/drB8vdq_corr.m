function [dRdT,R] = drB8vdq_corr(Tk,A,Z)
%
% Coherent Nuclear Scattering Rate for Boron 8 Neutrinos
% 
% Inputs:
%   1) Tk:  Nuclear Recoil Energy (keV)
%   2) A:  Atomic Number for Nucleus (We usually use the average)
%   3) Z:  # of protons in nucleus
% Outputs:
%   1) dRdEr: differential signal rate [evt/keVr/kgd]
%   2) R: total integrated signal rate (NO THRESHOLD)[evt/kgd]
%--------------------------------------------------------------------------
lgc_plt=true;

if nargin < 2
    %let's just assume that it's Ge
    A = 72.64;
    Z = 32;
end    
%-------------- Physical Constants ----------------------------------------
% Fermi coupling constant
Gf = 1.166364e-5; %[1/GeV^2]  NIST website

% weak mixing angle
sin2w = 0.2397;

% nucleon mass
Mn =0.93957*2/3+0.93827*1/3; %GeV

hbar= 6.58211928e-25;%[GeVs]
c=2.99792458e10;%[cm/s]

%Avogardo's constant
Na_g =6.02214129e23; %1/molg
Na_kg = Na_g*1e3;
%--------------------------------------------------------------------------

%-------------- Neutrino Flux ---------------------------------------------
load('B8nu_flux')
%let's convert B8 to units of GeV
B8.pdf = B8.pdf*1e3;%[1/GeV]
B8.E = B8.E*1e-3; %[GeV]
dE = B8.E(2)-B8.E(1);%[GeV]

if lgc_plt
    figure(1)
    plot(B8.E*1e3,B8.pdf*1e-3,'-k')
    xlabel('E (MeV)')
    ylabel('pdf (1/MeV)')
    title('^{8} Boron \nu energy spectrum')
    %set(gca,'xscale','log','yscale','log')
end
%--------------------------------------------------------------------------
% Number of seconds per day
Nsd = 24*60*60; %[s/d]

% Nuclear recoil energy in GeVr
Tg = Tk*1e-6; %[GeV]

% weak charge for this nucleus
Qw = (A-Z) -Z*(1-4*sin2w);

% mass of this nucleus/atom
Ma = A*Mn;%GeV
Ma_kg = A /Na_kg; %[kg]

% Nuclear Scattering Coherence Factor
F2 = Helm(Tk,A);               %Helm (Woods-Saxon) nuclear form factor

%------------- Total Rate Calculation -----------------
% We need to integrate over the Neutrino energy spectrum

% Coherent Neutrino Scattering total cross section:
xs = (hbar.*c).^2 .* Gf.^2 ./(4*pi) .*Qw.^2 .* B8.E.^2; %[cm^2]

dRdEv = B8.flux .* B8.pdf .*xs /Ma_kg *Nsd; %[#/kgd/GeVv]
R = sum(dRdEv.*dE); %[#/kgd]

%------------- dRdT Calculations ----------------
%Initialize Outputs
nT=length(Tg);
dRdT= zeros(nT,1);
for jT=1:nT
    % We need to integrate over the Neutrino energy spectrum

    % Coherent Neutrino Scattering differential cross section:
    dxsdT = (hbar.*c).^2 .* Gf.^2 ./(4*pi) .* Ma .* Qw.^2 .*(1 - Ma.*Tg(jT)./2./B8.E.^2).*F2(jT) *1e-6; %[cm^2/keV]

    % The above equation is only valid when T < 2*Ev^2/Ma
    lgc= dxsdT<0 | isnan(dxsdT);
    dxsdT(lgc)=0;
    
    dRdEvdT = B8.flux .* B8.pdf .*dxsdT /Ma_kg *Nsd; %[#/kgd/keVr/GeVv]
    
    %the differential rate
    dRdT(jT) = sum(dRdEvdT.*dE); %[#/keVrkgd]
end    
end



