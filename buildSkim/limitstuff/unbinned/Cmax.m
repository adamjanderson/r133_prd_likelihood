function CLdiff =Cmax(sigma)
global MuAt42 Xmaxat42 PrintFlag

if sigma < 0
    CLdiff = sigma;
    if PrintFlag
    	disp(['returning CLdiff=' num2str(CLdiff) ' for negative sigma']);
    end
    return;
end

mu = MuAt42   * sigma;
x  = Xmaxat42 * sigma; % note this is vector of length (Nevents+1)

mMax = min(floor(mu),length(x)-1);
if length(mu)>1; disp(['lenght(mu)=' length(mu) ]);end;

ConfLev = zeros(mMax+1,1);
for m=0:mMax
%disp(['m=' int2str(m) ' of ' int2str(mMax)]);
   [ConfLev(m+1),icode] = Cm(m,x(m+1),mu);
   if icode==2
     disp('Sorry. Program cannot provide any useful information.');
   elseif icode==1
     disp(['True value may be below the returned value '...
		 num2str(ConfLev(m+1))]);
   end
end

[maxConfLev, imax] = max(ConfLev);
Cmax90mu = Cmax90(mu);
CLdiff = maxConfLev - Cmax90mu;

if PrintFlag
    disp(['mu=' num2str(mu)]);
    disp(['sigma=' num2str(sigma) ' yields maxCL=' num2str(maxConfLev) ...
	' vs. CMax90=' num2str(Cmax90mu) ' for m=' int2str(imax-1)]);
end

