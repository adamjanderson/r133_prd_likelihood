function CL= C0_analytic(sigma);
%function CL= C0_analytic(sigma);
%
%function returns confidence level of a given cross-section sigma based on
%global variables MuAt42 (the expected number of events in the entire region 
%of interest for a wimp-nucleon cross-section of 10^-42 cm^2) and Xmaxat42 
%(the size of the maximal gap for the same cross-section).  Both variables
%may be vectors running over several different detectors.
%
% rws 97 Aug based on Steve Yellin's CDMS memo
% revised 12 Jul 98 rws reordered terms so that never divide by zero
%         15 Jul 98 rws Added capability to combine several detectors 
global MuAt42 Xmaxat42 PrintFlag

% keep any search algorithms from checking for negative cross-sections:
if sigma<=0
  CL = -sigma;
  if PrintFlag
     disp(['confidence coefficient set to ' num2str(CL) ' for sigma=' ... 
            num2str(sigma) ]);
  end
  return
end

mu = MuAt42   * sigma;
x  = Xmaxat42 * sigma;
x(x>mu)=mu(x>mu);      % in case roundoff in integration causes problem
m = floor(mu./x);
nDet = length(m);

CL1 = zeros(1,nDet);
for j=1:nDet
    %some limits suggested by Steve to keep this from crashing due to
    % roundoff error or overflows:
    if ( ( x(j)<0.03*mu(j) & mu(j)<60 ) | ( x(j)<1.8 & mu(j)>60 ) | ...
	( x(j)<4 & mu(j)>700 ) )
        CL1(j)=1;
	if PrintFlag
	    disp(['sigma=' num2str(sigma) ' sets CL1 to one for mu=' ...
		num2str(mu(j)) ...
		' and x=' num2str(x(j)) ]);
	end
    else
        k = 0:m(j);
        terms = exp(-k*x(j)) .* ( (k*x(j)-mu(j)).^k - ...
    			k.*(k*x(j)-mu(j)).^(k-1) ) ./ gamma(k+1);
%terms = (k*x-mu).^k  .* exp(-k*x) .* (1+ k./(mu-k*x) )./ gamma(k+1);
        CL1(j) = 1 - sum(terms(isfinite(terms)));
        if PrintFlag
   	    disp(['sigma=' num2str(sigma) ' yields CL1=' num2str(CL1(j)) ]);
            if CL1(j)==0
               disp(['mu=' num2str(mu) ', x=' num2str(x) ' m=' int2str(m)]);
               terms
            end
        end
    end
end

%combine individual confidence levels using incomplete gamma function:
CL1(CL1<0 | CL1>1) = ones(1,sum(CL1<0 | CL1>1));
prodCL = prod(CL1);
if prodCL<=0
  CL = 0;
else
  CL = 1-gammainc(-log(prod(CL1)),nDet);
end
