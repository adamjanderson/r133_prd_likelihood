function [ConfLev,icode] = Cm(m,x,mu)
% ConfLev(m,x,mu,icode) = C_m(x,mu):
% Evaluate the probability that the maximum expected contents of an interval
% with at most m points is less than x given that the total range is mu.
% The return code, icode, is 0 if all is ok, 1 if the true value is guaranteed
% to not be above the returned value, 2 if the program can't supply any useful
% information.  ConfLev is supposed to be used only for finding confidence
% levels above 50%; if the program finds it below this, it may not bother
% finding a more accurate result.  Also in this file is real function CMax90,
% which finds the maximum C_m at the 90% CL as a function of the mean, mu.
% A return value of -1.0 means that CMax90 could not be evaluated.
%

%All variables read from the CLtable.tx must be stored as globals so they 
%will survive from call to call of this routine:
global CL meanv xcl Nxcl muval CMaxval meanlog

%disp(['Entering Cm.m with m=' int2str(m) ', x=' num2str(x) ', mu=' num2str(mu) ]);

%
% Tabulated information
N=50; Nmeans=41; NCLs=22; NCMaxmu=70;

      if ~exist('muval','var') | isempty(muval) 
	 disp('Will parse ascii CLtable.txt');
         ParseCLtable;			% input the table        
%      else
%	 disp('Did not try to parse CLtable.txt');
      end;

% Make some simple checks on reasonableness of the input parameters.  If
% they are unreasonable, either return a reasonable output anyway, or give up.
      if (mu<0 | m<0) 
   	 ConfLev=-1.;
	 icode=2;
	 return;
      elseif(x>1.00001*mu) 
         disp(['Setting ConfLev=1 since x=' num2str(x) ' > mu=' num2str(mu) ]);
         ConfLev=1.;
      elseif(x<=0.)
         disp(['Setting ConfLev=0 since x=' num2str(x) ' < 0' ]);
         ConfLev=0.;
      elseif(mu<1. | mu>=meanv(Nmeans) | m>N) 
% The table hasn't the needed information, but at least we know that the
% answer is smaller than the confidence level for x=mu-.
         ConfLev=gammainc(mu,m+1);
	 icode=1;
         return;
      else % The table might include this m,mu
% Find which meanv(I) is just below mu.  Since mu is less than meanv(Nmeans),
% the I must be less than Nmeans; so there is information for I+1.
         mulog=log(mu); % Since mu.ge.1, mulog is at least 0.
         I=floor(10.*mulog + 1.);
         Nxcl2=min(Nxcl(m+1,I),Nxcl(m+1,I+1));	% note +1 offset vs. fortran
         
         if(Nxcl2<2) 
% The m must be too large for the mu; CL is very low.
           	ConfLev=CL(2)
 		icode=1;
     		return;
         end
         
% Nxcl(m+1,I) is at least 2 after this point.
% Find clxmu, the confidence level for x=mu-.
         clxmu=gammainc(mu,m+1);
         
% Return clxmu if x is equal to mu to within one part in 100,000.
         if(x > 0.99999*mu) 
            ConfLev=clxmu;
            icode=0;
            return
         end
         
% Interpolate table entries to apply to this value of mu
         for J=1:Nxcl2
%disp([int2str(m) ' ' int2str(J) ' ' int2str(I)]);
            xcl2(J)=xcl(m+1,J,I) + (mulog-meanlog(I))* ...
            (xcl(m+1,J,I+1)-xcl(m+1,J,I))/(meanlog(I+1)-meanlog(I));
            CL2(J)=CL(J);
         end
         xcl2(Nxcl2+1)=mu;
         CL2(Nxcl2+1)=clxmu;
% Interpolate xcl2 to find ConfLev at x.  K = 2 or 3 xcl2's are used,
% beginning with xcl2(J).  xcl2(1) and xcl2(2) must exist to get here.
% If x is before xcl2(2), then xcl2(1), xcl2(2), and xcl2(3) are
% used.  Analogously, if x is after xcl2(Nxcl(m+1,I)), use that xcl2, the one
% above it, and the one below it.  Otherwise use the two xcl2's 
% between which x lies, and the nearest other one to x.
         K=3;
         if(x<xcl2(1)) 
% Only linearly extrapolate before xcl2(1); otherwise the parabola could
% result in smaller x getting larger confidence level
            J=1;
            K=2;
         elseif(x<=xcl2(2)) 
            J=1;
%        elseif (Nxcl(m+1,I)==NCLs & x>xcl2(NCLs)) 
% Only linearly extrapolate beyond xcl2(NCLs); otherwise the parabola could
% result in larger x getting smaller confidence level.
%            J=NCLs;
%            K=2;
         elseif (x>=xcl2(Nxcl2)) 
            J=Nxcl2-1;
         else % x is between xcl2(2) and xcl2(Nxcl2); Nxcl2>3.
            for L=3:Nxcl2 % Find the first xcl2 that's above x
               if(x<=xcl2(L)) 
                  if(xcl2(L+1)-x < x-xcl2(L-2)) 
                     J=L-1;
                  else
                     J=L-2;
                  end
                  break;	% exit for loop
               end
            end
         end
         if(K==2)  % Linearly interpolate
            ConfLev=CL2(J) + (CL2(J+1)-CL2(J))*(x - xcl2(J))/ ...
            (xcl2(J+1) - xcl2(J));
         else % Quadratically interpolate
            ConfLev=CL2(J) + ((x-xcl2(J))/(xcl2(J+1)-xcl2(J+2))) * ( ...
             (CL2(J+1)-CL2(J))*(x-xcl2(J+2))/(xcl2(J+1)-xcl2(J)) +   ...
             (CL2(J+2)-CL2(J))*(xcl2(J+1)-x)/(xcl2(J+2)-xcl2(J)) ) ;
         end
         ConfLev=max(ConfLev,0.);
         ConfLev=min(ConfLev,clxmu);
      end % End of the case in which the table can include the m and mu.
icode =0;
