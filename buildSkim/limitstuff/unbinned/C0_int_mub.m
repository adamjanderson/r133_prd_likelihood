function CL = C0_int_mub(sigma)
%function CL = C0_int_mub(sigma)
%
%function returns confidence level CL for a given corss-section sigma,
%using function PbackC0 to integrate over all possible expected backgrounds
%mub.  Requires global variables
%       minMub      smallest value of mub to consider (singularity at 0)
%       maxMub      largest value of mub to consinder (so we don't go to inf)
% rws 11 May 99
global sigma_g minMub maxMub PrintFlag

sigma_g = sigma;
CL = quadrws('PbackC0',minMub,maxMub);

if PrintFlag
    disp(['sigma=' num2str(sigma_g) ' yields CL=' num2str(CL) ]);
end

