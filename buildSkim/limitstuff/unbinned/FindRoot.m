%function root = FindRoot(FuncHandle,x1,x2,tol,FACTOR,NTRY)
%
%Finds root of function (FuncHandle - a function handle) to precision tol.
%x1 and x2 are the user's guess to bracket the root. 
%FACTOR (defaults to 1.6) is how much to widen guess by if we are not
%yet bracketing a root; NTRY (defaults to 50) is the maximum number of
%times to try thus widening the search area. 
%Uses Brent's method (see algorithm in Numerical Recipes)
%Richard Schnee 11 Jan 1996
%rws 12 Jul 98 fixed bug causing crash under some circumstances
%rws 17 Dec 99 added optional arguments
%jpf 20 Feb 07 use function handle rather than eval

function root = FindRoot(FuncHandle,x1,x2,tol,FACTOR,NTRY)

if nargin<5
   FACTOR=1.6;
end
if nargin<6
   NTRY= 50;
end

% Bracket a root by expanding range, 
% moving point that yields result closest to 0:

if (x1 == x2) 
	disp ('Bad initial range (x1=x2) in FindRoot');
	return ;
else
	%eval ([ 'f1 = ' FuncName '(x1);' ]);
	%eval ([ 'f2 = ' FuncName '(x2);' ]);
	f1 = FuncHandle(x1);
        f2 = FuncHandle(x2);
        while (j<=NTRY & f1*f2>0.0) 
		if (abs(f1) < abs(f2))
			x1 = x1 + FACTOR*(x1-x2);
			%eval ([ 'f1 = ' FuncName '(x1);' ]);
                        f1 = FuncHandle(x1);
		else
			x2 = x2 + FACTOR*(x2-x1);
			%eval ([ 'f2 = ' FuncName '(x2);' ]);
                        f2 = FuncHandle(x2);
		end
	end
end

% Use Brent's method to find root to desired precision :

ITMAX =100;
EPS = 3.0e-8;
a = x1;
b = x2;
fa = f1;
fb = f2;

if (fb*fa > 0.0) 
	disp('Unable to expand range to bracket root in FindRoot');
else
	c =b;
	fc=fb;
	for iter=1:ITMAX;
		if (fb*fc > 0.0) 
			c=a;
			fc=fa;
			e=b-a;d=b-a;
		end
		if (abs(fc) < abs(fb)) 
			a=b;
			b=c;
			c=a;
			fa=fb;
			fb=fc;
			fc=fa;
		end
		tol1=2.0*EPS*abs(b)+0.5*tol;
		xm=0.5*(c-b);
		if (abs(xm) <= tol1 | fb == 0.0) 
			root =  b;
			return;
		elseif (abs(e) >= tol1 & abs(fa) > abs(fb)) 
			s=fb/fa;
			if (a == c) 
				p=2.0*xm*s;
				q=1.0-s;
			else 
				q=fa/fc;
				r=fb/fc;
				p=s*(2.0*xm*q*(q-r)-(b-a)*(r-1.0));
				q=(q-1.0)*(r-1.0)*(s-1.0);
			end
			if (p > 0.0)  
				q = -q;
			end
			p=abs(p);
			min1=3.0*xm*q-abs(tol1*q);
			min2=abs(e*q);
			if (2.0*p < (min(min1 , min2)) )
				e=d;
				d=p/q;
			else 
				d=xm;
				e=d;
			end
		else
			d=xm;
			e=d;
		end
		a=b;
		fa=fb;
		if (abs(d) > tol1)
			b = b+d;
		else
			b = b+ ( (xm>0.0) - (xm<= 0.0) )*abs(tol1) ;
		end 
		%eval([ 'fb = ' FuncName '(b);']);
                fb = FuncHandle(b);
	end
	disp('Maximum number of iterations exceeded in FindRoot');
end


