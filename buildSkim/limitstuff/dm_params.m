% DMRateParam.m
%
% little script to give standard values of parameters for 
% calc of DM rates
%
% QFI = 0.09;
% QFNa = 0.30;
% mN = 0.9315; % GeV
% c = 3e5; % km/s
% v0 = 220;
% vSun = 12; % km/s sun relative to nearby stars
% vEsc = 650; % km/s
%
% SG  2000/02/15
% RWS 2000/03/06 allow for v0_global 
%%%%%%%%%%%%%%%

global v0_global

% PLB 389, 757 (1996 limit paper)
QFI = 0.09;
QFNa = 0.30;

mN = 0.9315; % GeV
c = 3e5; % km/s

% following is S&L corrected for v0 -> 220
%v0 = 220; % km/s
%vEarthMax = 258 - 230 + v0; % km/s
%vEarthMin = 229 - 230 + v0; % km/s
%vEarthMean = 243.5 - 230 + v0; % km/s

% PLB 424, 195 (1998) DAMA-I paper
% no number given for v0; vSun = 232, vEarth = 30 * cos(60 deg)
% PLB 389 757: DAMA-0 limit: <vEarth> = 232, v0 = 220
% so it's all self-consistent
v0 = 220;
vSun = 12; % km/s sun relative to nearby stars 
%vEsc = 650; % km/s
vEsc = 544; %new RAVE value

rhoW = 0.3; % GeV/cm3

dayMax = 152.5;
dayMin = 152.5 + 0.5*365.25;
dayMean = 152.5 + 0.25*365.25;

%Replace these standard values with non-standard globals filled by user
if ~isempty(v0_global)
    v0 = v0_global;
end
vEarthMax  = v0+vSun+15; % km/s
vEarthMin  = v0+vSun-15; % km/s
vEarthMean = v0+vSun;    % km/s

