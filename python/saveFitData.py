# saveFitData.py

import CDMSLikelihood
import numpy as np
import matplotlib.pyplot as plt
import cPickle as pickle

dataPath = '/Users/adamanderson/Documents/CDMS/Analysis/R133_PRD_Likelihood/buildSkim/data/'
det_list = [1101, 1104, 1105, 1111, 1112, 1114, 1115]
time_list = {1101: [1],
            1104: [1],
            1105: [1],
            1111: [1],
            1112: [1],
            1114: [1, 2],
            1115: [1, 2]}

# set up the dictionary to data files
bgDataFiles = dict()
cfDataFiles = dict()
mcDataFiles = dict()
for det in det_list:
    bgDataFiles[det] = dict()
    cfDataFiles[det] = dict()
    mcDataFiles[det] = dict()
    for time in time_list[det]:
        bgDataFiles[det][time] = dataPath + 'ROOT_skim_lowbg_zip'+str(det-1100)+'_time'+str(time)+'.mat'
        cfDataFiles[det][time] = dataPath + 'ROOT_skim_cf_zip'+str(det-1100)+'_time'+str(time)+'.mat'
        mcDataFiles[det][time] = dataPath + 'ROOT_skim_bg_model_v4_partcorr_zip'+str(det-1100)+'_time'+str(time)+'.mat'

# actually load the data
bg_data = CDMSLikelihood.load_bg_data(bgDataFiles)
cf_data = CDMSLikelihood.load_cf_data(cfDataFiles)
mc_data = CDMSLikelihood.load_mc_data(mcDataFiles)

picklef_bg = open('bg_data.pkl', 'w')
picklef_cf = open('cf_data.pkl', 'w')
picklef_mc = open('mc_data.pkl', 'w')
pickle.dump(bg_data, picklef_bg, protocol=2)
pickle.dump(cf_data, picklef_cf, protocol=2)
pickle.dump(mc_data, picklef_mc, protocol=2)
picklef_bg.close()
picklef_cf.close()
picklef_mc.close()
