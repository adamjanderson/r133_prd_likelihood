# sensitivity_plots.py
#
# Python script to make sensitivity plots.
#
# Adam Anderson
# adama@fnal.gov

import cPickle as pickle
import glob
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np

# aesthetics
sns.set_style('white')
sns.set_context("notebook", font_scale=1.5)


data_path = '/Users/adamanderson/Documents/CDMS/Analysis/R133_PRD_Likelihood/python/grid/20160202_test6/'
masses = np.array([7, 10, 15, 20, 26, 30])
plot_lo_edge = {5:-30, 7:-5, 10:-5, 15:-5, 20:-2, 26:-1.5, 30:-1.5}
plot_up_edge = {5:30, 7:5, 10:5, 15:5, 20:2, 26:1.5, 30:1.5}
median_UL = np.zeros(len(masses))
UL_up_1sigma = np.zeros(len(masses))
UL_down_1sigma = np.zeros(len(masses))

for jmass, mass in enumerate(masses):
    mass_path = '%s/data_M=%.0fGeV/' % (data_path, mass)
    pkl_filenames = glob.glob(mass_path + '*.pkl')
    data = dict()
    for filename in pkl_filenames:
        f = file(filename, 'r')
        filedata = pickle.load(f)
        f.close()
        if len(data) == 0:
            data = filedata['results']
            errors = filedata['errors']
        else:
            for key1 in data.keys():
                if isinstance(data[key1], dict):
                    for key2 in data[key1].keys():
                        data[key1][key2].extend(filedata['results'][key1][key2])
                        errors[key1][key2].extend(filedata['errors'][key1][key2])
                else:
                    data[key1].extend(filedata['results'][key1])
                    errors[key1].extend(filedata['errors'][key1])

    WIMP_cs_values = np.array(data['WIMP_cs'])
    WIMP_cs_errors = np.array(errors['WIMP_cs'])
    cNotNaN = np.logical_and(~np.isnan(WIMP_cs_values), ~np.isnan(WIMP_cs_errors))
    WIMP_cs_UL = WIMP_cs_values[cNotNaN] + 1.28*WIMP_cs_errors[cNotNaN]
    median_UL[jmass] = np.median(WIMP_cs_UL)
    UL_up_1sigma[jmass] = np.percentile(WIMP_cs_UL, 83)
    UL_down_1sigma[jmass] = np.percentile(WIMP_cs_UL, 17)

    plt.figure()
    WIMPcs_edges = np.linspace(start=plot_lo_edge[mass], stop=plot_up_edge[mass], num=51)
    # plt.hist(WIMP_cs_UL, bins=WIMPcs_edges, histtype='step')
    # plt.hist(WIMP_cs_UL, bins=WIMPcs_edges, histtype='stepfilled', alpha=0.5)
    plt.hist(WIMP_cs_values, bins=WIMPcs_edges, histtype='step')
    plt.hist(WIMP_cs_values, bins=WIMPcs_edges, histtype='stepfilled', alpha=0.5)
    plt.xlabel('WIMP-n cross section [10^{-42} cm^2]')
    plt.title('expected sensitivity to %.1fGeV WIMP' % mass)
    plt.tight_layout()
    plt.savefig('MC_bias_M=%.1fGeV.png' % mass)

R133limit = np.loadtxt('R133_LT_limit_dmtools.txt', delimiter=',')

print median_UL

plt.figure()
plt.fill_between(masses, UL_down_1sigma*1e-42, UL_up_1sigma*1e-42, color='g', alpha=0.3, label='+/-1 sigma expected') #, color='#00FF3C')
plt.loglog(masses, median_UL*1e-42, 'k-', label='median expected')
# plt.loglog(masses, UL_up_1sigma*1e-42, 'k--', label='+/-1 sigma expected')
# plt.loglog(masses, UL_down_1sigma*1e-42, 'k--')
plt.loglog(R133limit[:,0], R133limit[:,1], label='PRL limit')
plt.axis([5, 30, 1e-45, 1e-40])
plt.legend()
plt.xlabel('WIMP mass [GeV]')
plt.ylabel('WIMP-nucleon cross section [cm^2]')
plt.tight_layout()
plt.savefig('MC_limitplot.png')


plt.show()
