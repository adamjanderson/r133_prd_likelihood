# CDMSLikelihood.py
#
# Module for evaluating the CDMS likelihood. Ideally this should be structured
# a way that makes minimization with iminuit and MCMC sampling with emcee
# straightforward. There are also lots of tools for loading the CDMS data skims
# from the .mat files, bypassing the whole mess of ROOT and RooFit.
#
# Adam Anderson
# adama@fnal.gov
# 27 September 2015

import numpy as np
import scipy.io as scio
import scipy.stats as sps


bins_for_vars = {'pt': np.linspace(start=2.0, stop=13.0, num=(13.0-2.0)/0.25 + 1),
                 'qimean': np.linspace(start=-2.0, stop=5.0, num=(5.0+2.0)/0.2 + 1),
                 'prpartSIMcorr': np.linspace(start=0.15, stop=0.35, num=(0.35-0.15)/0.005 + 1),
                 'pzpartSIMcorr': np.linspace(start=-0.4, stop=0.4, num=0.8/0.02 + 1),
                 'prpart': np.linspace(start=0.15, stop=0.35, num=(0.35-0.15)/0.005 + 1),
                 'pzpart': np.linspace(start=-0.4, stop=0.4, num=0.8/0.02 + 1),
                 'BDT5': np.linspace(start=-1.0, stop=1.0, num=2/0.05 + 1),
                 'BDT7': np.linspace(start=-1.0, stop=1.0, num=2/0.05 + 1),
                 'BDT10': np.linspace(start=-1.0, stop=1.0, num=2/0.05 + 1),
                 'BDT15': np.linspace(start=-1.0, stop=1.0, num=2/0.05 + 1)}
bin_centers_for_vars = dict()
for var in bins_for_vars.keys():
    bin_centers_for_vars[var] = (bins_for_vars[var][1:] + bins_for_vars[var][0:-1]) / 2.0
bin_width = {var: (bins_for_vars[var][1] - bins_for_vars[var][0]) for var in bins_for_vars.keys()}

r133_exposures = {(1101,1):80.21, (1104,1):82.92, (1105,1):80.89, (1111,1):87.40, (1112,1):83.80,
                  (1114,1):23.53, (1114,2):58.02, (1115,1):18.69, (1115,2):60.55}

# mapping of background types to "libnum" numbers
libnum_codes = {'weights_gammas':0., 'weights_1keV':1.,
                'weights_210Pb_Qout':2., 'weights_210Pb_QoLowYield':3.,
                'weights_210Bi_Qout':2., 'weights_210Bi_QoLowYield':3.,
                'weights_206Pb_Qout':3.,
                'weights_210Pb_Qi1':4., 'weights_210Bi_Qi1':4., 'weights_206Pb_Qi1':6.,
                'weights_210Pb_Qi2':5., 'weights_210Bi_Qi2':5., 'weights_206Pb_Qi2':7.}

def load_bg_data(dataFile):
    """
    Load low-background data.

    Arguments:
    dataFile - a python dictionary {detector # -> string identifying file path}

    Returns:
    data - a python dictionary:
                {detector -> {time -> {variable -> events}}}
    """
    vars_to_load = ['pt', 'qimean', 'prpart', 'pzpart',
                    'BDT5', 'BDT7', 'BDT10', 'BDT15', 'cAnalysisThreshold']
    vars_to_keep = ['pt', 'qimean', 'prpart', 'pzpart',
                    'BDT5', 'BDT7', 'BDT10', 'BDT15']
    data = dict()
    for det in dataFile.keys():
        data[det] = dict()
        for time in dataFile[det].keys():
            matData = scio.loadmat(dataFile[det][time], variable_names=vars_to_load)

            # define selection criteria
            cMatrix = matData['cAnalysisThreshold'] == 1.0
            for var in vars_to_keep:
                cMatrix = np.hstack([cMatrix,
                                     matData[var] >= np.amin(bins_for_vars[var]),
                                     matData[var] <= np.amax(bins_for_vars[var])])
            cSelect = np.all(cMatrix, axis=1)
            data[det][time] = {var: matData[var][cSelect].transpose()[0] for var in vars_to_keep}
    return data


def load_cf_data(dataFile):
    """
    Load Cf data and massage into histograms that can form the basis for the PDF
    construction.

    Arguments:
    dataFile - a dictionary of file paths, mapping detector numbers to the data
        files corresponding to those detectors

    Returns:
    histos - a python dictionary:
                {detector -> {time -> {WIMP mass -> {variables -> histogram points}}}}

    TO-DO:
    --Make appropriate cut on each variable to ensure that data is in-range.
    --Allow both 1D and 2D distributions
    """
    vars_to_keep = ['pt', 'qimean', 'pt,qimean', 'prpart', 'pzpart',
                    'BDT5', 'BDT7', 'BDT10', 'BDT15']
    vars_to_load = ['pt', 'qimean', 'prpart', 'pzpart',
                    'BDT5', 'BDT7', 'BDT10', 'BDT15']
    vars_to_cut = ['pt', 'qimean', 'prpart', 'pzpart',
                    'BDT5', 'BDT7', 'BDT10', 'BDT15']
    pdf_data = dict()
    for det in dataFile.keys():
        pdf_data[det] = dict()
        for time in dataFile[det].keys():
            data = scio.loadmat(dataFile[det][time], variable_names=['WIMP_masses', 'WIMP_weights'])
            WIMP_masses = data['WIMP_masses']
            WIMP_weights = data['WIMP_weights']
            data = scio.loadmat(dataFile[det][time], variable_names=vars_to_load)

            # selection criteria just ensure that all data are in the range of
            # box used to select data
            cSelect = []
            for var in vars_to_cut:
                cSelect = np.hstack([data[var] >= np.min(bins_for_vars[var]),
                                     data[var] <= np.max(bins_for_vars[var])])
            cSelect = np.all(cSelect, axis=1)

            # define selection criteria
            cSelect = np.all(np.hstack([(data['pt'] > 2.0),
                                        (data['pt'] < 13.1)]), axis=1)

            pdf_data[det][time] = dict()
            for jMass in range(len(WIMP_masses)):
                pdf_data[det][time][WIMP_masses[jMass][0]] = dict()
                for var in vars_to_keep:
                    if len(var.split(',')) == 1:
                        pdf_data[det][time][WIMP_masses[jMass][0]][var], _ = \
                            np.histogram(data[var][cSelect],
                                        bins=bins_for_vars[var],
                                        weights=WIMP_weights[jMass][0][cSelect],
                                        density=True) # outputs a histogram whose integral is 1
                    elif len(var.split(',')) == 2:
                        xvar, yvar = var.split(',')
                        pdf_data[det][time][WIMP_masses[jMass][0]][var], _, _ = \
                            np.histogram2d(np.squeeze(data[xvar][cSelect]), np.squeeze(data[yvar][cSelect]),
                                            bins=[bins_for_vars[xvar], bins_for_vars[yvar]],
                                            weights=np.squeeze(WIMP_weights[jMass][0][cSelect]),
                                            normed=True) # histogram automatically normalized to 1
                    rateAt42 = np.sum(WIMP_weights[jMass][0][cSelect])
                    pdf_data[det][time][WIMP_masses[jMass][0]][var] = pdf_data[det][time][WIMP_masses[jMass][0]][var] * rateAt42 # renormalize so that weights sum to the rate expected at 1e-42 cm2
    return pdf_data


def load_mc_data(dataFile):
    """
    Load MC data and massage into histograms that can form the basis for the PDF
    construction.

    Parameters:
    -----------
    dataFile - a dictionary of file paths, mapping detector numbers to the data
        files corresponding to those detectors

    Returns:
    histos - a python dictionary:
        {detector -> {time -> {variables -> {bg type -> histogram points}}}}
    """
    vars_to_keep = ['pt', 'qimean', 'pt,qimean', 'prpartSIMcorr', 'pzpartSIMcorr',
                    'BDT5', 'BDT7', 'BDT10', 'BDT15']
    vars_to_load = ['pt', 'qimean', 'prpartSIMcorr', 'pzpartSIMcorr',
                    'BDT5', 'BDT7', 'BDT10', 'BDT15',
                    'cAnalysisThreshold', 'SIMLibNum', 'threshold_eff']
    vars_to_cut = ['pt', 'qimean', 'prpartSIMcorr', 'pzpartSIMcorr',
                    'BDT5', 'BDT7', 'BDT10', 'BDT15']
    weights_to_load = ['weights_206Pb_Qi1', 'weights_206Pb_Qi2', 'weights_206Pb_Qout',
                       'weights_210Bi_Qi1', 'weights_210Bi_Qi2', 'weights_210Bi_Qout', 'weights_210Bi_QoLowYield',
                       'weights_210Pb_Qi1', 'weights_210Pb_Qi2', 'weights_210Pb_Qout', 'weights_210Pb_QoLowYield',
                       'weights_1keV', 'weights_gammas']
    pdf_data = dict()
    for det in dataFile.keys():
        pdf_data[det] = dict()

        for time in dataFile[det].keys():
            data = scio.loadmat(dataFile[det][time], variable_names=vars_to_load)
            weights_data = scio.loadmat(dataFile[det][time], variable_names=weights_to_load)

            # selection criteria just ensure that all MC points are in range of
            # box used to select data
            cRange = []
            for var in vars_to_cut:
                cRange = np.hstack([data[var] >= np.min(bins_for_vars[var]),
                                    data[var] <= np.max(bins_for_vars[var])])
            cRange = np.all(cRange, axis=1)

            pdf_data[det][time] = dict()
            for var in vars_to_keep:
                # rename p*partSIMcorr as just p*part
                if 'partSIMcorr' in var:
                    varNameToStore = var.rstrip('SIMcorr')
                else:
                    varNameToStore = var
                pdf_data[det][time][varNameToStore] = dict()

                # loop over weights for each background type
                for weights in weights_to_load:
                    cLib = np.squeeze(data['SIMLibNum'] == libnum_codes[weights])
                    cSelect = np.all(np.vstack([cRange, cLib]), axis=0)
                    keys = ['gammas', '1keV', 'Qout', 'QoLowYield', 'Qi1', 'Qi2']
                    matching_key = [key for key in keys if key in weights][0]
                    num_of_component_pdfs = len([key for key in weights_to_load if matching_key in key])

                    if np.sum(weights_data[weights][cSelect]) > 0:
                        if len(var.split(',')) == 1:        # 1D distributions
                            hist_vals, _ = np.histogram(data[var][cSelect],
                                                        bins=bins_for_vars[var],
                                                        weights=(weights_data[weights][cSelect] * data['threshold_eff'][cSelect]),
                                                        density=True) # histogram automatically normalized to 1
                        elif len(var.split(',')) == 2:      # 2D distributions
                            xvar, yvar = var.split(',')
                            if np.sum(weights_data[weights][cSelect]) > 0:
                                hist_vals, _, _ = np.histogram2d(np.squeeze(data[xvar][cSelect]), np.squeeze(data[yvar][cSelect]),
                                                                bins=[bins_for_vars[xvar], bins_for_vars[yvar]],
                                                                weights=np.squeeze(weights_data[weights][cSelect] * data['threshold_eff'][cSelect]),
                                                                normed=True) # histogram automatically normalized to 1
                        else:                               # otherwise, throw an error
                            raise IndexError('You are attempting to import an MC pdf with dimension > 2. ' + \
                                             'No handler for this case has been implemented!')

                        total_eff = np.sum(weights_data[weights][cSelect] * data['threshold_eff'][cSelect]) / \
                                    np.sum(weights_data[weights][cSelect])  # calculate true total efficiency of this bg component
                        hist_vals = hist_vals * total_eff / num_of_component_pdfs  # weighted histogram, corrected for efficiency, corrected for number of component PDFs of fit PDF
                        if matching_key in pdf_data[det][time][varNameToStore]:
                            pdf_data[det][time][varNameToStore][matching_key] = pdf_data[det][time][varNameToStore][matching_key] + hist_vals
                        else:
                            pdf_data[det][time][varNameToStore][matching_key] = hist_vals
                    else:
                        if len(var.split(',')) == 1:        # 1D distributions
                            pdf_data[det][time][varNameToStore][matching_key] = np.zeros(len(bins_for_vars[var])-1)
                        elif len(var.split(',')) == 2:      # 2D distributions
                            pdf_data[det][time][varNameToStore][matching_key] = np.zeros((len(bins_for_vars[xvar])-1, len(bins_for_vars[yvar])-1))
                        else:                               # otherwise, throw an error
                            raise IndexError('You are attempting to import an MC pdf with dimension > 2. ' + \
                                     'No handler for this case has been implemented!')
    return pdf_data


class LHFunction:
    """
    Class for handling the CDMS likelihood function.

    Methods:
        __init__(self, bgDataFile, cfDataFile, MCDataFile)
        LLH(self, params)
        detector_LLH(self, params)
        analysis_cuts_bg(self, data)

    Instance variables:

    """
    def __init__(self, bgData, cfData, MCData):
        """
        Default constructor.
        Loads data and massages it into a more useful, memory-efficient format.
        """
        # load and downsample the data
        self.bg_data = bgData
        self.cf_data = cfData
        self.MC_data = MCData
        self.observables = ['pt', 'qimean', 'prpart', 'pzpart']
        self.detectors = [(1101,1), (1104,1), (1105,1), (1111,1), (1112,1), (1114,1), (1114,2), (1115,1), (1115,2)]
        self.S1_fit = {1101: True, 1104: True, 1105: False, 1111: False, 1112: False, 1114: False, 1115: False}
        self.S2_fit = {1101: False, 1104: False, 1105: False, 1111: False, 1112: True, 1114: False, 1115: True}
        self.exposures = r133_exposures
        self.WIMP_mass = 10.0


    def LLH(self, rate_gammas_1101, rate_1keV_1101, rate_Qout_1101, rate_Qi1_1101, rate_Qi2_1101,
                  rate_gammas_1104, rate_1keV_1104, rate_Qout_1104, rate_Qi1_1104, rate_Qi2_1104,
                  rate_gammas_1105, rate_1keV_1105, rate_Qout_1105, rate_Qi1_1105, rate_Qi2_1105,
                  rate_gammas_1111, rate_1keV_1111, rate_Qout_1111, rate_Qi1_1111, rate_Qi2_1111,
                  rate_gammas_1112, rate_1keV_1112, rate_Qout_1112, rate_Qi1_1112, rate_Qi2_1112,
                  rate_gammas_1114, rate_1keV_1114, rate_Qout_1114, rate_Qi1_1114, rate_Qi2_1114,
                  rate_gammas_1115, rate_1keV_1115, rate_Qout_1115, rate_Qi1_1115, rate_Qi2_1115,
                  WIMP_cs):
        """
        This function returns -2log(L) for the entire experiment. Note that the
        -2 factor is included by default! The argument list for this function is
        unfortunately quite a mess, but this appears to be an inevitable feature
        of iminuit.
        """
        # parse the arguments into a dict... this is ugly as hell, but an
        # inevitable consequence of explicitly listing all of the arguments
        rates = {1101: {'rate_gammas': rate_gammas_1101, 'rate_1keV': rate_1keV_1101, 'rate_Qout': rate_Qout_1101, 'rate_Qi1': rate_Qi1_1101, 'rate_Qi2': rate_Qi2_1101, 'WIMP_cs': WIMP_cs},
                 1104: {'rate_gammas': rate_gammas_1104, 'rate_1keV': rate_1keV_1104, 'rate_Qout': rate_Qout_1104, 'rate_Qi1': rate_Qi1_1104, 'rate_Qi2': rate_Qi2_1104, 'WIMP_cs': WIMP_cs},
                 1105: {'rate_gammas': rate_gammas_1105, 'rate_1keV': rate_1keV_1105, 'rate_Qout': rate_Qout_1105, 'rate_Qi1': rate_Qi1_1105, 'rate_Qi2': rate_Qi2_1105, 'WIMP_cs': WIMP_cs},
                 1111: {'rate_gammas': rate_gammas_1111, 'rate_1keV': rate_1keV_1111, 'rate_Qout': rate_Qout_1111, 'rate_Qi1': rate_Qi1_1111, 'rate_Qi2': rate_Qi2_1111, 'WIMP_cs': WIMP_cs},
                 1112: {'rate_gammas': rate_gammas_1112, 'rate_1keV': rate_1keV_1112, 'rate_Qout': rate_Qout_1112, 'rate_Qi1': rate_Qi1_1112, 'rate_Qi2': rate_Qi2_1112, 'WIMP_cs': WIMP_cs},
                 1114: {'rate_gammas': rate_gammas_1114, 'rate_1keV': rate_1keV_1114, 'rate_Qout': rate_Qout_1114, 'rate_Qi1': rate_Qi1_1114, 'rate_Qi2': rate_Qi2_1114, 'WIMP_cs': WIMP_cs},
                 1115: {'rate_gammas': rate_gammas_1115, 'rate_1keV': rate_1keV_1115, 'rate_Qout': rate_Qout_1115, 'rate_Qi1': rate_Qi1_1115, 'rate_Qi2': rate_Qi2_1115, 'WIMP_cs': WIMP_cs}}
        LLH = 0.
        for det in self.detectors:
            kwargs = rates[det[0]]
            kwargs['det'] = det[0]
            kwargs['time'] = det[1]
            LLH = LLH + self.detector_LLH(**kwargs)
        return LLH


    def detector_LLH(self, det, time, rate_gammas, rate_1keV, rate_Qout, rate_Qi1, rate_Qi2, WIMP_cs):
        """
        This function returns -2log(L) for a single detector. Again, note that
        the -2 factor is include by default!
        """
        rate_total = rate_gammas * np.sum(self.MC_data[det][time]['pt']['gammas']) * bin_width['pt'] + \
                     rate_1keV * np.sum(self.MC_data[det][time]['pt']['1keV']) * bin_width['pt'] + \
                     rate_Qout * np.sum(self.MC_data[det][time]['pt']['Qout']) * bin_width['pt'] + \
                     rate_Qi1 * np.sum(self.MC_data[det][time]['pt']['Qi1']) * bin_width['pt'] + \
                     rate_Qi2 * np.sum(self.MC_data[det][time]['pt']['Qi2']) * bin_width['pt'] + \
                     WIMP_cs * np.sum(self.cf_data[det][time][self.WIMP_mass]['pt']) * bin_width['pt']
        mu_total = rate_total * self.exposures[(det, time)]

        LLH = 2.*mu_total - 2.*len(self.bg_data[det][time]['pt'])*np.log(mu_total)
        for var in self.observables:
            # construct data points on which to evaluate PDF; this needs to be
            # compatible with both 1-D and N-D histograms
            subvars = var.split(',')
            points_to_eval = [self.bg_data[det][time][avar] for avar in subvars]
            pdf_points = (rate_gammas * self.eval_MC_pdf(det, time, var, "gammas", points_to_eval) + \
                                          rate_1keV * self.eval_MC_pdf(det, time, var, "1keV", points_to_eval) + \
                                          rate_Qout * self.eval_MC_pdf(det, time, var, "Qout", points_to_eval) + \
                                          rate_Qi1 * self.eval_MC_pdf(det, time, var, "Qi1", points_to_eval) + \
                                          rate_Qi2 * self.eval_MC_pdf(det, time, var, "Qi2", points_to_eval) + \
                                          WIMP_cs * self.eval_WIMP_pdf(det, time, self.WIMP_mass, var, points_to_eval)) / rate_total
            LLH_points = np.log((rate_gammas * self.eval_MC_pdf(det, time, var, "gammas", points_to_eval) + \
                                          rate_1keV * self.eval_MC_pdf(det, time, var, "1keV", points_to_eval) + \
                                          rate_Qout * self.eval_MC_pdf(det, time, var, "Qout", points_to_eval) + \
                                          rate_Qi1 * self.eval_MC_pdf(det, time, var, "Qi1", points_to_eval) + \
                                          rate_Qi2 * self.eval_MC_pdf(det, time, var, "Qi2", points_to_eval) + \
                                          WIMP_cs * self.eval_WIMP_pdf(det, time, self.WIMP_mass, var, points_to_eval)) / rate_total)
            LLH = LLH + -2.*np.sum(LLH_points)
        return LLH


    def eval_MC_pdf(self, det, time, var, bgtype, points):
        '''
        eval_MC_pdf(self, det, time, var, bgtype, points)

        Evaluate a background pdf for a specific detector / time combination and
        a specific variable.

        Parameters:
        -----------
        det:    detector number (int)
        time:   time number (int)
        var:    variable to evaulate (comma separated list of names)
        bgtype: background type (string)
        points: list of np.array's, one per dimension requested by 'var'

        Returns:
        --------
        values: value of pdf, evaluated at the requested points
        '''
        subvars = var.split(',')
        bin_edges = [bins_for_vars[avar] for avar in subvars]

        # run digitize once per dimension to get coordinates of points in histogram
        bin_numbers = []
        for dim in range(len(points)):
            bin_numbers.append(np.digitize(points[dim], bin_edges[dim]))

        # zero-pad the MC histogram values in order to compensate for
        # under/over-flow bins from digitize
        hist_heights = self.MC_data[det][time][var][bgtype]
        padded_hist_heights = np.pad(hist_heights, pad_width=1, mode='constant', constant_values=0)
        values = padded_hist_heights[bin_numbers]

        if False in np.isfinite(values):
            print 'encountered inf or nan!!!'

        return values


    def eval_WIMP_pdf(self, det, time, WIMP_mass, var, points):
        '''
        eval_WIMP_pdf(self, det, time, var, points)

        Evaluate the WIMP pdf for a specific detector / time combination and a
        specific variable.

        Parameters:
        -----------
        det:        detector number (int)
        time:       time number (int)
        WIMP_mass:  mass of the WIMP [GeV]; must be one of the masses loaded
                        with the WIMP data in 'load_cf_data'
        var:        variable to evaulate (comma separated list of names)
        points: list of np.array's, one per dimension requested by 'var'

        Returns:
        --------
        values: value of pdf, evaluated at the requested points
        '''
        subvars = var.split(',')
        bin_edges = [bins_for_vars[avar] for avar in subvars]

        # run digitize once per dimension to get coordinates of points in histogram
        bin_numbers = []
        for dim in range(len(points)):
            bin_numbers.append(np.digitize(points[dim], bin_edges[dim]))

        # zero-pad the MC histogram values in order to compensate for
        # under/over-flow bins from digitize
        hist_heights = self.cf_data[det][time][WIMP_mass][var]
        padded_hist_heights = np.pad(hist_heights, pad_width=1, mode='constant', constant_values=0)
        values = padded_hist_heights[bin_numbers]

        if False in np.isfinite(values):
            print 'encountered inf or nan!!!'

        return values


    def generate_MC(self, bg_rates, WIMP_cs):
        '''
        Generates a Monte Carlo pseudoexperiment for an entire experiment,
        looping over all detectors, times, and observables.

        Parameters:
        -----------
        bg_rates - dictionary with rates for all detectors and backgrounds; of the
                form:
                    {detector (int) -> {bg type (string) -> rate}}
        WIMP_cs - WIMP-nucleon cross section in units of 1e-42 cm^2

        Returns:
        --------
        fake_data - a dictionary containing the MC data, in a compatible format
                  the low-background data stored in each object:
                    {detector -> {time -> {variable -> events}}}
        '''
        detector_numbers = np.unique([det_pair[0] for det_pair in self.detectors])
        fake_data = dict()
        for det in detector_numbers:
            fake_data[det] = dict()

            # for each time period, convert rate into an event rate, and generate
            # a realization
            det_times = [pair[1] for pair in self.detectors if pair[0] == det]
            for time in det_times:
                fake_data[det][time] = dict()

                # generate number of events in this realization
                total_bg_rate = np.sum([bg_rates[det][bg_type] * np.sum(self.MC_data[det][time]['pt'][bg_type]) * bin_width['pt'] for bg_type in bg_rates[det].keys()])
                WIMP_rate = WIMP_cs * np.sum(self.cf_data[det][time][self.WIMP_mass]['pt']) * bin_width['pt']
                N_evt = np.random.poisson((total_bg_rate + WIMP_rate) * r133_exposures[(det, time)])

                # distribute these events over observables according to MC
                for var in self.observables:
                    # construct total MC distribution, by summing over bg types
                    # NB: doing this for each iteration is inefficient, and we
                    # could speed this up by doing it once on data load, but
                    # doing it here is a little more transparent.
                    MC_total = np.zeros(self.MC_data[det][time][var][self.MC_data[det][time][var].keys()[0]].shape)
                    for bgtype in bg_rates[det].keys():
                        MC_total = MC_total + self.MC_data[det][time][var][bgtype] * bg_rates[det][bgtype]
                    MC_total = MC_total + WIMP_cs * self.cf_data[det][time][self.WIMP_mass][var]

                    if len(var.split(',')) == 1:    # 1D distribution
                        fake_data[det][time][var] = np.random.choice(bin_centers_for_vars[var], size=N_evt, replace=True, p=MC_total / np.sum(MC_total))
                    elif len(var.split(',')) == 2:    # 2D distribution
                        MC_total_raveled = np.ravel(MC_total)
                        fake_data_ind_raveled = np.random.choice(np.arange(len(MC_total_raveled)), size=N_evt, replace=True, p=MC_total_raveled / np.sum(MC_total_raveled))
                        v0, v1 = np.meshgrid(bin_centers_for_vars[var.split(',')[0]], bin_centers_for_vars[var.split(',')[1]])
                        fake_data[det][time][var.split(',')[0]] = np.ravel(v0.transpose())[fake_data_ind_raveled]
                        fake_data[det][time][var.split(',')[1]] = np.ravel(v1.transpose())[fake_data_ind_raveled]
        return fake_data
