import CDMSLikelihood
import numpy as np
import cPickle as pickle
from iminuit import Minuit
import sys


# parse command line arguments
if len(sys.argv) != 4:
    print "Error!: Wrong number of arguments to SensitivityMC.py!"
WIMP_mass = float(sys.argv[1])
WIMP_cs = float(sys.argv[2])
N_MC_samples = int(sys.argv[3])

# load the data from the pickle files
picklef_bg = open('bg_data.pkl', 'r')
picklef_cf = open('cf_data.pkl', 'r')
picklef_mc = open('mc_data.pkl', 'r')
bg_data = pickle.load(picklef_bg)
cf_data = pickle.load(picklef_cf)
mc_data = pickle.load(picklef_mc)

# construct the likelihood
cdmsLH = CDMSLikelihood.LHFunction(bg_data, cf_data, mc_data)
cdmsLH.WIMP_mass = WIMP_mass
cdmsLH.detectors = [(1101,1), (1104,1), (1105,1), (1111,1), (1112,1), (1114,1), (1114,2), (1115,1), (1115,2)]
cdmsLH.observables = ['pt,qimean', 'prpart', 'pzpart']
observables_to_plot = ['pt', 'qimean', 'prpart', 'pzpart']
det_names = {1101: 'T1Z1', 1104: 'T2Z1', 1105: 'T2Z2', 1111: 'T4Z2', 1112: 'T4Z31', 1114: 'T5Z2', 1115: 'T5Z3'}

kwargs = dict()
for det in cdmsLH.detectors:
    kwargs['WIMP_cs'] = 0.
    kwargs['fix_WIMP_cs'] = True
    kwargs['rate_gammas_' + str(det[0])] = 3.
    kwargs['limit_rate_gammas_' + str(det[0])] = (0, 1000.)
    kwargs['error_rate_gammas_' + str(det[0])] = 0.1
    kwargs['rate_1keV_' + str(det[0])] = 0.5
    kwargs['limit_rate_1keV_' + str(det[0])] = (0, 1000.)
    kwargs['error_rate_1keV_' + str(det[0])] = 0.1
    kwargs['rate_Qout_' + str(det[0])] = 3.
    kwargs['limit_rate_Qout_' + str(det[0])] = (0, 1000.)
    kwargs['error_rate_Qout_' + str(det[0])] = 0.1
    if cdmsLH.S1_fit[det[0]]:
        kwargs['rate_Qi1_' + str(det[0])]=0.5
        kwargs['limit_rate_Qi1_' + str(det[0])]=(0, 1000)
    else:
        kwargs['rate_Qi1_' + str(det[0])]=0.
        kwargs['fix_rate_Qi1_' + str(det[0])]=True
    if cdmsLH.S2_fit[det[0]]:
        kwargs['rate_Qi2_' + str(det[0])]=0.5
        kwargs['limit_rate_Qi2_' + str(det[0])]=(0, 1000)
    else:
        kwargs['rate_Qi2_' + str(det[0])]=0.
        kwargs['fix_rate_Qi2_' + str(det[0])]=True

# Do initial fit to get parameters
m = Minuit(cdmsLH.LLH, **kwargs)
m.migrad()

# convert the Minuit output into a dictionary suitable for generate_MC
bg_IDs = {'1keV', 'gammas', 'Qout', 'Qi1', 'Qi2'}
rates = dict()
for rate_key in [key for key in m.values.keys() if key!='WIMP_cs']:
    det_key = int(rate_key.split('_')[2])
    bg_key = rate_key.split('_')[1]
    if det_key not in rates:
        rates[det_key] = dict()
    rates[det_key][bg_key] = m.values[rate_key]
WIMP_cs = 0 #m.values['WIMP_cs']

# actually test the MC generation
fit_results = dict()
fit_errors = dict()
fit_results['WIMP_cs'] = []
fit_errors['WIMP_cs'] = []
for jSim in range(N_MC_samples):
    # MC-generate the data
    print 'WIMP_cs = ' + str(WIMP_cs)
    fake_data = cdmsLH.generate_MC(rates, WIMP_cs)

    # reshape the MC-generated rates into a form that we can feed to Minuit
    kwargs = dict()
    for det in rates.keys():
        for bg_type in rates[det].keys():
            kwargs['rate_'+bg_type+'_' + str(det)] = rates[det][bg_type]
            kwargs['limit_rate_'+bg_type+'_' + str(det)] = (0, 1000.)
            kwargs['error_rate_'+bg_type+'_' + str(det)] = 0.1
        if cdmsLH.S1_fit[det]:
            kwargs['rate_Qi1_' + str(det)] = rates[det]['Qi1']
        else:
            kwargs['rate_Qi1_' + str(det)] = rates[det]['Qi1']
            kwargs['fix_rate_Qi1_' + str(det)] = True
        if cdmsLH.S2_fit[det]:
            kwargs['rate_Qi2_' + str(det)] = rates[det]['Qi2']
        else:
            kwargs['rate_Qi2_' + str(det)] = rates[det]['Qi2']
            kwargs['fix_rate_Qi2_' + str(det)] = True

    # fit the MC data
    cdmsLH.bg_data = fake_data
    m = Minuit(cdmsLH.LLH, **kwargs)
    m.migrad()

    # save the results
    print m.values['WIMP_cs']
    print m.errors['WIMP_cs']

    for rate_key in [key for key in m.values.keys() if key!='WIMP_cs']:
        det_key = int(rate_key.split('_')[2])
        bg_key = rate_key.split('_')[1]
        if det_key not in fit_results.keys():
            fit_results[det_key] = dict()
        if det_key not in fit_errors.keys():
            fit_errors[det_key] = dict()
        if bg_key not in fit_results[det_key].keys():
            fit_results[det_key][bg_key] = []
        if bg_key not in fit_errors[det_key].keys():
            fit_errors[det_key][bg_key] = []
        fit_results[det_key][bg_key].append(m.values[rate_key])
        fit_errors[det_key][bg_key].append(m.errors[rate_key])
    fit_results['WIMP_cs'].append(m.values['WIMP_cs'])
    fit_errors['WIMP_cs'].append(m.errors['WIMP_cs'])

fit_output = {'results': fit_results, 'errors': fit_errors}
f = open('MC_output.pkl', 'w')
pickle.dump(fit_output, f)
f.close()
