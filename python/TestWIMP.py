# TestWIMP.py
#
# A script for testing the shape of the WIMP distribution
#
# Adam Anderson
# 25 October 2015
# adama@fnal.gov

import CDMSLikelihood
import numpy as np
import matplotlib.pyplot as plt
import cPickle as pickle
from iminuit import Minuit
import seaborn as sns

# aesthetics
sns.set_style('white')
sns.set_context("notebook", font_scale=1.5)

# load the data from the pickle files
picklef_bg = open('bg_data.pkl', 'r')
picklef_cf = open('cf_data.pkl', 'r')
picklef_mc = open('mc_data.pkl', 'r')
bg_data = pickle.load(picklef_bg)
cf_data = pickle.load(picklef_cf)
mc_data = pickle.load(picklef_mc)

# construct the likelihood
cdmsLH = CDMSLikelihood.LHFunction(bg_data, cf_data, mc_data)
cdmsLH.detectors = [(1101,1), (1104,1), (1105,1), (1111,1), (1112,1), (1114,1), (1114,2), (1115,1), (1115,2)]
cdmsLH.observables = ['pt,qimean', 'prpart', 'pzpart']
observables_to_plot = ['pt', 'qimean', 'prpart', 'pzpart']
det_names = {1101: 'T1Z1', 1104: 'T2Z1', 1105: 'T2Z2', 1111: 'T4Z2', 1112: 'T4Z31', 1114: 'T5Z2', 1115: 'T5Z3'}

kwargs = dict()
for det in cdmsLH.detectors:
    kwargs['rate_gammas_' + str(det[0])] = 3.
    kwargs['limit_rate_gammas_' + str(det[0])] = (0, 1000.)
    kwargs['error_rate_gammas_' + str(det[0])] = 0.1
    kwargs['rate_1keV_' + str(det[0])] = 0.5
    kwargs['limit_rate_1keV_' + str(det[0])] = (0, 1000.)
    kwargs['error_rate_1keV_' + str(det[0])] = 0.1
    kwargs['rate_Qout_' + str(det[0])] = 3.
    kwargs['limit_rate_Qout_' + str(det[0])] = (0, 1000.)
    kwargs['error_rate_Qout_' + str(det[0])] = 0.1
    if cdmsLH.S1_fit[det[0]]:
        kwargs['rate_Qi1_' + str(det[0])]=0.5
        kwargs['limit_rate_Qi1_' + str(det[0])]=(0, 1000)
    else:
        kwargs['rate_Qi1_' + str(det[0])]=0.
        kwargs['fix_rate_Qi1_' + str(det[0])]=True
    if cdmsLH.S2_fit[det[0]]:
        kwargs['rate_Qi2_' + str(det[0])]=0.5
        kwargs['limit_rate_Qi2_' + str(det[0])]=(0, 1000)
    else:
        kwargs['rate_Qi2_' + str(det[0])]=0.
        kwargs['fix_rate_Qi2_' + str(det[0])]=True


# loop over each detector and plot individually
if True:
    for det in cdmsLH.detectors:
        # load the individual PDFs for plotting purposes
        pdf_points_WIMP = dict()
        for var in observables_to_plot:
            dVar = CDMSLikelihood.bins_for_vars[var][1] - CDMSLikelihood.bins_for_vars[var][0]

            pdf_points_WIMP[var] = cdmsLH.eval_WIMP_pdf(det[0], det[1], 10.0, var, [CDMSLikelihood.bins_for_vars[var]])

            # do the plotting
            f = plt.figure(1000*observables_to_plot.index(var) + det[0] + (det[1]-1)*100)
            total_pdf = pdf_points_WIMP[var] * 1.0 * dVar * cdmsLH.exposures[det]
            plt.step(CDMSLikelihood.bins_for_vars[var], total_pdf, where='post', color='k', label='total')
            plt.xlabel(var)
            plt.ylabel('events / bin')
            plt.legend()
            plt.title(det_names[det[0]] + ' time ' + str(det[1]))
            plt.savefig('figures/' + var + '_' + str(det[0]) + '_' + str(det[1]) + '.png', dpi=300, format='png')
            del f


plt.show()
