import CDMSLikelihood
import numpy as np
import matplotlib.pyplot as plt
import cPickle as pickle
from iminuit import Minuit

# load the data from the pickle files
picklef_bg = open('bg_data.pkl', 'r')
picklef_cf = open('cf_data.pkl', 'r')
picklef_mc = open('mc_data.pkl', 'r')
bg_data = pickle.load(picklef_bg)
cf_data = pickle.load(picklef_cf)
mc_data = pickle.load(picklef_mc)

# construct the likelihood
cdmsLH = CDMSLikelihood.LHFunction(bg_data, cf_data, mc_data)
pt_points = np.linspace(start=2.0, stop=13.0, num=1000)

# lists
det_list = [(1101,1), (1104,1), (1105,1), (1111,1), (1112,1), (1114,1), (1114,2), (1115,1), (1115,2)]
S1_fit = {1101: True, 1104: True, 1105: False, 1111: False, 1112: False, 1114: False, 1115: False}
S2_fit = {1101: False, 1104: False, 1105: False, 1111: False, 1112: True, 1114: False, 1115: True}

# loop over each detector and fit individually
for det in det_list:
    # do the fitting
    kwargs = dict(det=det[0], fix_det=True, time=det[1], fix_time=True,
            rate_gammas=3., limit_rate_gammas=(0, 1000),
            rate_1keV=0.5, limit_rate_1keV=(0, 1000),
            rate_210Pb_Qo=3., limit_rate_210Pb_Qo=(0, 1000))
    if S1_fit[det[0]]:
        kwargs['rate_210Pb_Qi1']=0.5
        kwargs['limit_rate_210Pb_Qi1']=(0, 1000)
    else:
        kwargs['rate_210Pb_Qi1']=0.
        kwargs['fix_rate_210Pb_Qi1']=True
    if S2_fit[det[0]]:
        kwargs['rate_210Pb_Qi2']=0.5
        kwargs['limit_rate_210Pb_Qi2']=(0, 1000)
    else:
        kwargs['rate_210Pb_Qi2']=0.
        kwargs['fix_rate_210Pb_Qi2']=True
    m = Minuit(cdmsLH.detector_LLH, **kwargs)
    m.migrad()
    print(m.values)
    print(m.errors)

    dE = CDMSLikelihood.bins_for_vars['pt'][1] - CDMSLikelihood.bins_for_vars['pt'][0]

    # load the individual PDFs for plotting purposes
    pdf_points_1keV = cdmsLH.eval_MC_pdf(det[0], det[1], 'pt', '1keV', pt_points)
    pdf_points_gammas = cdmsLH.eval_MC_pdf(det[0], det[1], 'pt', 'gammas', pt_points)
    pdf_points_Pb_Qout = cdmsLH.eval_MC_pdf(det[0], det[1], 'pt', 'Qout', pt_points)
    pdf_points_Pb_Qin_S1 = cdmsLH.eval_MC_pdf(det[0], det[1], 'pt', 'Qin_S1', pt_points)
    pdf_points_Pb_Qin_S2 = cdmsLH.eval_MC_pdf(det[0], det[1], 'pt', 'Qin_S2', pt_points)

    # do the plotting
    plt.figure(det[0] + (det[1]-1)*100)
    total_pdf = pdf_points_1keV * m.values['rate_1keV'] * dE * cdmsLH.exposures[det] + \
                pdf_points_gammas * m.values['rate_gammas'] * dE * cdmsLH.exposures[det] + \
                pdf_points_Pb_Qout * m.values['rate_210Pb_Qo'] * dE * cdmsLH.exposures[det] + \
                pdf_points_Pb_Qin_S1 * m.values['rate_210Pb_Qi1'] * dE * cdmsLH.exposures[det] + \
                pdf_points_Pb_Qin_S2 * m.values['rate_210Pb_Qi2'] * dE * cdmsLH.exposures[det]
    plt.step(pt_points, pdf_points_1keV * m.values['rate_1keV'] * dE * cdmsLH.exposures[det], label='1keV')
    plt.step(pt_points, pdf_points_gammas * m.values['rate_gammas'] * dE * cdmsLH.exposures[det], label='gammas')
    plt.step(pt_points, pdf_points_Pb_Qout * m.values['rate_210Pb_Qo'] * dE * cdmsLH.exposures[det], label='sidewall surf.')
    plt.step(pt_points, pdf_points_Pb_Qin_S1 * m.values['rate_210Pb_Qi1'] * dE * cdmsLH.exposures[det], label='S1 surf.')
    plt.step(pt_points, pdf_points_Pb_Qin_S2 * m.values['rate_210Pb_Qi2'] * dE * cdmsLH.exposures[det], label='S2 surf.')
    plt.step(pt_points, total_pdf, color='k', label='total')
    counts,bin_edges=np.histogram(bg_data[det[0]][det[1]]['pt'], CDMSLikelihood.bins_for_vars['pt'])
    bin_centers = (bin_edges[:-1] + bin_edges[1:])/2.
    plt.errorbar(bin_centers, counts, yerr=np.sqrt(counts), linestyle='none')
    plt.legend()
plt.show()
