tar xf sensitivity_tools.tar.gz

module load python/2.7
module load all-pkgs

cd sensitivity_tools

chmod 755 *
python SensitivityMC.py 10.0 0.0 100
mv MC_output.pkl ../MC_output_$1_$2.pkl
ls -lah