import os, shutil

#masses = [7.0, 10.0, 15.0] #[5.0, 7.0, 10.0, 15.0, 20.0, 26.0, 30.0]
masses = [5.0, 20.0, 26.0, 30.0]
base_dir = os.getcwd()
data_file = 'sensitivity_tools.tar.gz'

for mass in masses:
    # directory for data
    data_dir = 'data_M=%.1dGeV' % mass

    # make directory to hold submission scripts
    os.mkdir(data_dir)
    os.mkdir('%s/%s' % (data_dir, 'log'))
    os.mkdir('%s/%s' % (data_dir, 'out'))
    os.mkdir('%s/%s' % (data_dir, 'err'))

    # replace the line with the mass info in the sensitivity script
    f_read = open('worker_prototype.sh', 'r')
    f_write = open('%s/worker_M=%.1dGeV.sh' % (data_dir, mass), 'w')
    for line in f_read:
        print line
        if line == 'python SensitivityMC.py 10.0 0.0 20\n':
            f_write.write('python SensitivityMC.py %d 0.0 100\n' % mass)
        else:
            f_write.write(line)
    f_read.close()
    f_write.close()

    # replace the line with the mass info in the submission script
    f_read = open('submit_prototype.sub', 'r')
    f_write = open('%s/submit_M=%.1dGeV.sub' % (data_dir, mass), 'w')
    for line in f_read:
        print line
        if line == 'executable = worker_prototype.sh\n':
            f_write.write('executable = worker_M=%.1dGeV.sh\n' % mass)
        else:
            f_write.write(line)
    f_read.close()
    f_write.close()

    # copy over the tar with data and scripts
    shutil.copy(data_file, data_dir)

    # make directory to hold data and launch condor from this directory
    # so that output gets directed there implicitly
    #os.system('cp submit_prototype.sub data_M=%.1dGeV' % mass)
    #os.system('cd %s' % data_dir)
    os.chdir(data_dir)
    os.system('condor_submit submit_M=%.1dGeV.sub' % mass)
    os.chdir(base_dir)
