#!/bin/bash
# Submit a supersim job to run on fermigrid
# 
# Syntax: ./jobsub_submitjob.sh [mymacro.mac] [# jobs]

# function to parse a supersim macro file to find other macros on which this
# macro depends
getsubmacros(){
    subs="$(grep -e /control/execute -e /control/loop -e /control/foreach $1 \
            | awk '{print $2}')"
    if [ -n "$subs" ] ; then
	for sub in $subs ; do 
	    subs="$subs $(getsubmacros $sub)"
	done
    fi
    echo $subs
}


# setup the environment with commands from grid submission
if [ -z "$(which jobsub_submit 2>/dev/null)" ] ; then
    source setup_jobsub.sh
fi
echo "setup done"

# output directory for dcache:
OUTDIR=/pnfs/cdms/scratch/adama/supersim/output/

# parse the submacros of the macro supplied from the command line
MAINMACRO=$1
MACROS="$MAINMACRO $(getsubmacros $1)"
echo "Assuming that we need these macros on grid nodes: $MACROS"

# parse the number of jobs
NJOBS=$2

# create the submission command
JOBSUBARGS="-G cdms --resource-provides=usage_model=DEDICATED,OPPORTUNISTIC --disk=20GB --memory=1GB --OS=SL6 -dSUPERSIMOUTPUT $OUTDIR"
for macro in $MACROS ; do
    JOBSUBARGS="$JOBSUBARGS -f dropbox://$macro"
done
JOBSUBARGS="$JOBSUBARGS -f dropbox://Pb210_extract_lt.py"
[ -z "$NJOBS" ] || JOBSUBARGS="-N $NJOBS $JOBSUBARGS"

echo "Submitting $NJOBS jobs with command:"
echo "jobsub_submit $JOBSUBARGS file://run_supersim_lt.sh $MAINMACRO"

jobsub_submit $JOBSUBARGS file://run_supersim_lt.sh $MAINMACRO
