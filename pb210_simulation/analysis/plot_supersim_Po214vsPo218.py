import numpy as np
import matplotlib.pyplot as plt
import cPickle as pickle

dfiles = ['collated_data_g4_10.3.1_v2.pkl',
          'collated_data_g4_10.3.1_v2_Po218.pkl']
labels = {'collated_data_g4_10.3.1_v2.pkl': 'geant4 10.3.1 / Po214',
          'collated_data_g4_10.3.1_v2_Po218.pkl': 'geant4 10.3.1 / Po218'}
filetag = 'Po214vsPo218'
zip = 5

ddict = dict()
for dfile in dfiles:
    d = pickle.load(file(dfile,'r'))
    ddict[dfile] = d


for decay in ['Bi210', 'Pb210', 'Po210', 'Po214']:
    plt.figure()
    for dfile in dfiles:
        plt.hist(ddict[dfile][zip][decay]['Edep']/1000, bins=np.linspace(0,120,121), histtype='step', label=labels[dfile], normed=1)
    plt.title(decay)
    plt.legend()
    plt.xlabel('')
    plt.savefig('{}_{}.png'.format(filetag, decay))

    plt.figure()
    for dfile in dfiles:
        plt.hist(ddict[dfile][zip][decay]['Edep']/1000, bins=np.linspace(0,12,61), histtype='step', label=labels[dfile], normed=1)
    plt.title(decay)
    plt.legend()
    plt.xlabel('')
    plt.savefig('{}_{}_zoom.png'.format(filetag, decay))

plt.show()
