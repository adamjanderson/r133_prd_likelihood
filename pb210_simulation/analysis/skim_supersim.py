import cPickle as pickle
import matplotlib.pyplot as plt
import numpy as np
import glob

data_dirs = ['/cdms_scratch_dcache/adama/supersim/output/collated_data_g4_10.1.2_v2/',
             '/cdms_scratch_dcache/adama/supersim/output/collated_data_g4_10.2.3_v2/',
             '/cdms_scratch_dcache/adama/supersim/output/collated_data_g4_10.3.1_v2/',
             '/cdms_scratch_dcache/adama/supersim/output/collated_data_g4_10.1.2_v2_Po218/',
             '/cdms_scratch_dcache/adama/supersim/output/collated_data_g4_10.2.3_v2_Po218/',
             '/cdms_scratch_dcache/adama/supersim/output/collated_data_g4_10.3.1_v2_Po218/']

# skim the data and repackage into single file
ddict = {jzip: {source: {'Edep': np.array([])} for source in ['Bi210', 'Pb210', 'Po210', 'Po214']} for jzip in np.arange(1,16)}
for ddir in data_dirs:
    datafiles = glob.glob('{}/*.pkl'.format(ddir))
    for dfile in datafiles:
        d = pickle.load(file(dfile, 'r'))
        for jzip in d.keys():
            for source in d[jzip].keys():
                ddict[jzip][source]['Edep'] = np.append(ddict[jzip][source]['Edep'], d[jzip][source]['Edep'])
    with open(ddir.split('/')[-2] + '.pkl', 'w') as f:
        pickle.dump(ddict, f)
