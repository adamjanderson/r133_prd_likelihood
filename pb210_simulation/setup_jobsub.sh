#!/bin/bash 

#make sure we're using system default python
if [ -z "$(python --version 2>&1 | grep 2)" ] ; then
    export PATH=${PATH//anaconda/__anaconda__}
fi

#make sure we're mounted
#[ -n "$(mount | grep '/grid/fermiapp')" ] || mountgrid.sh

. /fermigrid-fermiapp/products/common/etc/setups.sh
setup jobsub_client

#export X509_CERT_DIR=/home/adama/certificates
