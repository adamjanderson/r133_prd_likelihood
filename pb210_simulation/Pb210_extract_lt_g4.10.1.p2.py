from ROOT import TFile, TTree
import ROOT.std
from array import array
import numpy as np
import cPickle as pickle
import sys

infilename = sys.argv[1] #'HousingImplanted_10160520_0000.root'
infile = TFile(infilename, 'READ')

EventNum = array('d', [0.])
Edep = array('d', [0.])
x = array('d', [0.])
y = array('d', [0.])
z = array('d', [0.])
time = array('d', [0.])
ptype = array('d', [0.])
volname = array('c',list('Housing'))

# extract times and particle types from the decays tree in order
# to group hits by type
decaytree = infile.Get('G4SimDir/mcHousing')
decaytree.SetBranchAddress('EventNum', EventNum)
decaytree.SetBranchAddress('Edep', Edep)
decaytree.SetBranchAddress('X1', x)
decaytree.SetBranchAddress('Y1', y)
decaytree.SetBranchAddress('Z1', z)
decaytree.SetBranchAddress('Time1', time)
decaytree.SetBranchAddress('PType', ptype)
decaytree.SetBranchAddress('VolName', volname)
nevt = decaytree.GetEntries()
decays = dict()
decays['EventNum'] = np.zeros(nevt)
decays['Edep'] = np.zeros(nevt)
decays['x'] = np.zeros(nevt)
decays['y'] = np.zeros(nevt)
decays['z'] = np.zeros(nevt)
decays['time'] = np.zeros(nevt)
decays['ptype'] = np.zeros(nevt)
decays['volname'] = np.array([], dtype=str)
for jevt in range(nevt):
    decaytree.GetEntry(jevt)
    decays['EventNum'][jevt] = EventNum[0]
    decays['Edep'][jevt] = Edep[0]
    decays['x'][jevt] = x[0]
    decays['y'][jevt] = y[0]
    decays['z'][jevt] = z[0]
    decays['time'][jevt] = time[0]
    decays['ptype'][jevt] = ptype[0]
    decays['volname'] = np.append(decays['volname'], volname.tostring())

# now do the same for each of the zip trees
eventdata = dict()
ziplist = np.arange(1,16)
for zipnum in ziplist:
    zipname = 'mczip%d' % zipnum
    ziptree = infile.Get('G4SimDir/'+zipname)
    ziptree.SetBranchAddress('EventNum', EventNum)
    ziptree.SetBranchAddress('Edep', Edep)
    ziptree.SetBranchAddress('X1', x)
    ziptree.SetBranchAddress('Y1', y)
    ziptree.SetBranchAddress('Z1', z)
    ziptree.SetBranchAddress('Time1', time)
    ziptree.SetBranchAddress('PType', ptype)
    nevt = ziptree.GetEntries()
    hits = dict()
    hits['EventNum'] = np.zeros(nevt)
    hits['Edep'] = np.zeros(nevt)
    hits['x'] = np.zeros(nevt)
    hits['y'] = np.zeros(nevt)
    hits['z'] = np.zeros(nevt)
    hits['time'] = np.zeros(nevt)
    hits['ptype'] = np.zeros(nevt)
    for jevt in range(nevt):
        ziptree.GetEntry(jevt)
        hits['EventNum'][jevt] = EventNum[0]
        hits['Edep'][jevt] = Edep[0]
        hits['x'][jevt] = x[0]
        hits['y'][jevt] = y[0]
        hits['z'][jevt] = z[0]
        hits['time'][jevt] = time[0]
        hits['ptype'][jevt] = ptype[0]

    fields = ['Edep']
    decaytypes = ['Po214', 'Pb210', 'Bi210', 'Po210']
    zipdata = dict()
    nevents = np.max(hits['EventNum'])
    for type in decaytypes:
        zipdata[type] = dict()
        for field in fields:
            zipdata[type][field] = np.zeros(nevents+1)

    for evnum in np.unique(hits['EventNum']):
        decaytimes = decays['time'][decays['EventNum'] == evnum]
        ptypes = decays['ptype'][decays['EventNum'] == evnum]

        # find hits in this event
        cEvent = hits['EventNum'] == evnum
        hitedep = hits['Edep'][cEvent]
        hittimes = hits['time'][cEvent]
        hitptypes = hits['ptype'][cEvent]

        # all hits associated with a single decay occur within
        # about 1 second of each other, so we group hits here
        # by their times up to 1 second
        unique_decaytimes = np.unique(np.round(decaytimes * 1.e-4)) * 1.e4
        for this_time in unique_decaytimes:
            this_ptypes = ptypes[(np.round(decaytimes * 1.e-4) * 1.e4) == this_time]

            # identify type of decay by the decay products
            # 210Pb decay
            cTime = np.abs(hittimes - this_time) < 1.e5
            decayStr = ''
            # only count events if the Pb210 decay occurs in the housing
            if 'Housing' in decays['volname'][(decays['ptype'] == 210083) & (decays['EventNum'] == evnum)]:
                if 210082 in this_ptypes:
                    decayStr = 'Po214'
                    zipdata[decayStr]['Edep'][int(evnum)] = np.sum(hitedep[cTime])
                if 210083 in this_ptypes:
                    decayStr = 'Pb210'
                    zipdata[decayStr]['Edep'][int(evnum)] = np.sum(hitedep[cTime])
                if 210084 in this_ptypes:
                    decayStr = 'Bi210'
                    zipdata[decayStr]['Edep'][int(evnum)] = np.sum(hitedep[cTime])
                if 206082 in this_ptypes:
                    decayStr = 'Po210'
                    zipdata[decayStr]['Edep'][int(evnum)] = np.sum(hitedep[cTime])

    # remove events with no hits from dictionary
    for decayStr in zipdata.keys():
        for var in zipdata[decayStr].keys():
            zipdata[decayStr][var] = zipdata[decayStr][var][zipdata[decayStr][var] != 0]
    eventdata[zipnum] = zipdata
            
outfile = file('collated_data.pkl', 'w')
pickle.dump(eventdata, outfile)
outfile.close()

#outfile = file('decays_data.pkl', 'w')
#pickle.dump(decays, outfile)
#outfile.close()
