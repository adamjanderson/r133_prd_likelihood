#!/bin/bash

umask 002
#copy stderr to stdout so it show up in the .log 
exec 2> >(tee /dev/stderr)

# get dependencies
source /cvmfs/cdms.opensciencegrid.org/externals/root/root_v5.34.30_slc6/bin/thisroot.sh
source /cvmfs/cdms.opensciencegrid.org/externals/geant4/Geant4-10.2.p3-Linux/share/Geant4-10.2.3/geant4make/geant4make.sh
source /cvmfs/oasis.opensciencegrid.org/osg/modules/lmod/current/init/bash
export CDMS_SUPERSIM=/cvmfs/cdms.opensciencegrid.org/supersim/supersim-g4.10.02.p03-V02-01-01/
module load gcc/4.9.2

export CONDA_DIR=/cvmfs/des.opensciencegrid.org/fnal/anaconda2/
export PATH=$CONDA_DIR/bin:$PATH
source activate $CONDA_DIR/envs/default

#make sure our grid variables are defined correctly and give local links
if [ -z "$CONDOR_DIR_INPUT" ] ; then
    echo "ERROR: CONDOR_DIR_INPUT is not defined" >&2
    exit 1
fi

if [ -z "$CONDOR_DIR_SUPERSIMOUTPUT" ] ; then
    echo "ERROR: CONDOR_DIR_SUPERSIMOUTPUT is not defined" >&2
    exit 2
fi

ln -s $CONDOR_DIR_SUPERSIMOUTPUT output
ln -s $CONDOR_DIR_INPUT mymacros


#run supersim
export SUPERSIM_SEEDS="$(date +%s) ${CLUSTER}${PROCESS}"
echo "Using SUPERSIM_SEEDS=$SUPERSIM_SEEDS"
echo "****** Starting supersim at $(date) ******" 
time $CDMS_SUPERSIM/bin/Linux-g++/CDMS_LeadImplant $*
ret=$?
if [ "$ret" -ne 0 ] ; then
    echo "supersim generated error code $ret"
    exit $ret
fi

echo "****** supersim completed at $(date) *****" 

#just in case output went into the working directory...
mv -f *.root output/ 2>/dev/null
#append seeds to filename
outfile=`ls output/*.root`
outfile2=${outfile/.root/_${SUPERSIM_SEEDS/ /_}.root}
mv $outfile $outfile2

# process root files to extract edep of each event
python $CONDOR_DIR_INPUT/Pb210_extract_lt_g4.10.2.p3.py $outfile2

# move the output pkl file to output directory
pklfile=`ls *.pkl`
pklfile2=output/${pklfile/.pkl/_${SUPERSIM_SEEDS/ /_}.pkl}
mv $pklfile $pklfile2

# delete all extant root files
rm -rf output/*.root

#done

echo "******* Processing completed at $(date) *********" 
exit 0
 