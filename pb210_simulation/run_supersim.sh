#!/bin/bash

umask 002
#copy stderr to stdout so it show up in the .log 
exec 2> >(tee /dev/stderr)

#make sure our grid variables are defined correctly and give local links

if [ -z "$CONDOR_DIR_INPUT" ] ; then
    echo "ERROR: CONDOR_DIR_INPUT is not defined" >&2
    exit 1
fi

if [ -z "$CONDOR_DIR_SUPERSIMOUTPUT" ] ; then
    echo "ERROR: CONDOR_DIR_SUPERSIMOUTPUT is not defined" >&2
    exit 2
fi

ln -s $CONDOR_DIR_SUPERSIMOUTPUT output
ln -s $CONDOR_DIR_INPUT mymacros

#also put macros in working directory in case expected there...
for file in `ls $CONDOR_DIR_INPUT` ; do 
    ln -s $file .
done

#echo "Local directory listing:"
#ls -RF

#set up supersim environment from the /grid/fermiapp install locations
. /grid/fermiapp/cdms/processing_source/supersim/setup_environemnt.sh

#run supersim
export SUPERSIM_SEEDS="$(date +%s) ${CLUSTER}${PROCESS}"
echo "Using SUPERSIM_SEEDS=$SUPERSIM_SEEDS"
echo "****** Starting supersim at $(date) ******" 
time CDMS_GGSim $*
ret=$?
if [ "$ret" -ne 0 ] ; then
    echo "supersim generated error code $ret"
    exit $ret
fi

echo "****** supersim completed at $(date) *****" 

#find the CombineTrees executable
COMBINE_EXE="$CDMS_SUPERSIM/CombineTrees"
if [ -x $COMBINE_EXE ] ; then
    ln -s $COMBINE_EXE .
else
    echo "Compiling CombineTrees locally..."
    g++ -o CombineTrees $CDMS_SUPERSIM/CDMSscripts/analysis/CombineTrees.C `root-config --cflags --libs`
fi

#just in case output went into the working directory...
mv -f *.root output/ 2>/dev/null
#append seeds to filename
outfile=`ls output/*.root`
outfile2=${outfile/.root/_${SUPERSIM_SEEDS/ /_}.root}
mv $outfile $outfile2
echo "******* Running CombineTrees over output ********" 
time ./CombineTrees $outfile2
ret=$?
if [ "$ret" -ne 0 ]; then
    echo "CombineTrees generated error code $ret"
    exit $ret
fi
#we don't care about the unmerged file
rm -f $outfile2
echo "******* Processing completed at $(date) *********" 
exit 0
 