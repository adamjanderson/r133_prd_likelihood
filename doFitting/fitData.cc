#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooAddPdf.h"
#include "RooWorkspace.h"
#include "RooPlot.h"
#include "RooFit.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "TFile.h"
#include "TH1.h"
#include "Math/Interpolator.h"
#include "Math/Functor.h"

#include "TTree.h"
#include "TStyle.h"
#include "TColor.h"
#include "TApplication.h"
#include "TLegend.h"
#include "RooStats/ProfileLikelihoodCalculator.h"
#include "RooStats/LikelihoodInterval.h"
#include "RooStats/LikelihoodIntervalPlot.h"
#include "RooGaussian.h"
#include "RooPoisson.h"
#include "RooPolynomial.h"
#include "RooHistPdf.h"
#include "RooDataHist.h"
#include "RooExtendPdf.h"
#include "RooBinning.h"
#include "RooCategory.h"
#include "RooSimultaneous.h"
#include "RooFunctor1DBinding.h"
#include "RooEffProd.h"


void buildLegend(std::string leg_names[], RooPlot* rp)
{
    TLegend* leg = new TLegend(0.6, 0.5, 0.88, 0.88);
    // std::string leg_names[7] = {"data","total background", "gammas", "sidewall surf. ev.", "S1 surf. ev.", "S2 surf. ev.", "1 keV"};
    // std::cout << "# of items = " << rp_1d_BDT10->numItems() << std::endl;
    for (int i=1; i<rp->numItems(); i++)
    {
        TString obj_name=rp->nameOf(i);
        TObject *obj = rp->findObject(obj_name.Data());
        leg->AddEntry(obj,leg_names[i].c_str(),"l");
    }
    leg->Draw();
}

int main(int argc, char *argv[])
{
    gStyle->SetTitleSize(0.045, "xyz");
    gStyle->SetLabelSize(0.045, "xyz");
    TColor *myColor = new TColor(1001, 31./255., 120./255., 180./255.);
    TApplication* rootapp = new TApplication("myApp", &argc, argv);

    bool bgOnlyFit = true;
    bool sigBgFit = false;

    int mindet = 0;
    int maxdet = 7;
    std::string detNames[] = {"T1Z1", "T2Z1", "T2Z2", "T4Z2", "T4Z3", "T5Z2", "T5Z3 (BS)", "T5Z3 (AS)"};
    std::string leg_names_bg[7] = {"data","total background", "gammas", "sidewall surf. ev.", "S1 surf. ev.", "S2 surf. ev.", "1 keV"};
    int detnum[] = {1, 4, 5, 11, 12, 14, 15, 15};
    int timenum[] = {1, 1, 1, 1, 1, 1, 1, 2};

    
    TFile f_workspace("LT_data+model.root") ;

    // Retrieve workspace from file
    RooWorkspace* w = (RooWorkspace*) f_workspace.Get("wSave");


    //***** GET DATA + MODEL FROM FILE *****//
    RooDataSet* combData = (RooDataSet*)w->data("combData");
    RooSimultaneous* model = (RooSimultaneous*)w->pdf("simul_pdf_bg");
    RooRealVar* pt = (RooRealVar*)w->var("pt");
    RooRealVar* qimean = (RooRealVar*)w->var("qimean");
    RooRealVar* prpart = (RooRealVar*)w->var("prpart");
    RooRealVar* pzpart = (RooRealVar*)w->var("pzpart");
    RooCategory* sampleType = (RooCategory*)w->cat("sampleType");

    // for(int jEvent = 0; jEvent < combData->numEntries(); jEvent++)
    // {
    //     const RooArgSet* event = combData->get(jEvent);
    //     std::cout << event->getCatLabel("sampleType") << std::endl;
    // }

    RooBinning bin_pt(2.0, 13.0);
    bin_pt.addUniform(44, 2.0, 13.);
    RooBinning bin_qimean(-2.0, 7.0);
    bin_qimean.addUniform(45, -2.0, 7.0);
    RooBinning bin_prpart(0.15, 0.35);
    bin_prpart.addUniform(50, 0.15, 0.35);
    RooBinning bin_pzpart(-0.4, 0.4);
    bin_pzpart.addUniform(40, -0.4, 0.4);


    //***** FITTING *****//
    // std::cout << "value = " << model->evaluate() << std::endl;
    model->fitTo(*combData);
    
    if(true)
    {
        for(int jdet = 0; jdet <=7; jdet++)
        {
            TCanvas* c_pt_data = new TCanvas(Form("c_pt_data_%d_%d", detnum[jdet], timenum[jdet]));
            // pt->setRange(2.0, 13.0);
            RooPlot* rp_1d_pt = pt->frame();
            rp_1d_pt->SetTitle(detNames[jdet].c_str());
            combData->plotOn(rp_1d_pt, RooFit::DrawOption("EZ"), RooFit::LineWidth(2), RooFit::Cut(Form("sampleType==sampleType::pt_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::Binning(bin_pt));
            model->plotOn(rp_1d_pt, RooFit::Slice(*sampleType,Form("pt_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*combData));
            model->plotOn(rp_1d_pt, RooFit::Slice(*sampleType,Form("pt_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*combData),RooFit::Components(Form("histpdf_MC_gammas_pt_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kCyan));
            model->plotOn(rp_1d_pt, RooFit::Slice(*sampleType,Form("pt_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*combData),RooFit::Components(Form("pdf_MC_Pb_Qo_pt_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kRed));
            model->plotOn(rp_1d_pt, RooFit::Slice(*sampleType,Form("pt_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*combData),RooFit::Components(Form("pdf_MC_Pb_QiS1_pt_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kGreen));
            model->plotOn(rp_1d_pt, RooFit::Slice(*sampleType,Form("pt_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*combData),RooFit::Components(Form("pdf_MC_Pb_QiS2_pt_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kMagenta));
            model->plotOn(rp_1d_pt, RooFit::Slice(*sampleType,Form("pt_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*combData),RooFit::Components(Form("histpdf_MC_1keV_pt_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kOrange));
            rp_1d_pt->Draw();
            // c_pt_data->SetLogy();
            c_pt_data->Update();

            TCanvas* c_qimean_data = new TCanvas(Form("c_qimean_data_%d_%d", detnum[jdet], timenum[jdet]));
            // qimean->setRange(2.0, 13.0);
            RooPlot* rp_1d_qimean = qimean->frame();
            rp_1d_qimean->SetTitle(detNames[jdet].c_str());
            combData->plotOn(rp_1d_qimean, RooFit::DrawOption("EZ"), RooFit::LineWidth(2), RooFit::Cut(Form("sampleType==sampleType::qimean_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::Binning(bin_qimean));
            model->plotOn(rp_1d_qimean, RooFit::Slice(*sampleType,Form("qimean_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*combData));
            model->plotOn(rp_1d_qimean, RooFit::Slice(*sampleType,Form("qimean_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*combData),RooFit::Components(Form("histpdf_MC_gammas_qimean_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kCyan));
            model->plotOn(rp_1d_qimean, RooFit::Slice(*sampleType,Form("qimean_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*combData),RooFit::Components(Form("pdf_MC_Pb_Qo_qimean_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kRed));
            model->plotOn(rp_1d_qimean, RooFit::Slice(*sampleType,Form("qimean_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*combData),RooFit::Components(Form("pdf_MC_Pb_QiS1_qimean_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kGreen));
            model->plotOn(rp_1d_qimean, RooFit::Slice(*sampleType,Form("qimean_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*combData),RooFit::Components(Form("pdf_MC_Pb_QiS2_qimean_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kMagenta));
            model->plotOn(rp_1d_qimean, RooFit::Slice(*sampleType,Form("qimean_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*combData),RooFit::Components(Form("histpdf_MC_1keV_qimean_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kOrange));
            rp_1d_qimean->Draw();
            // c_qimean_data->SetLogy();
            c_qimean_data->Update();

            TCanvas* c_prpart_data = new TCanvas(Form("c_prpart_data_%d_%d", detnum[jdet], timenum[jdet]));
            // prpart->setRange(2.0, 13.0);
            RooPlot* rp_1d_prpart = prpart->frame();
            rp_1d_prpart->SetTitle(detNames[jdet].c_str());
            combData->plotOn(rp_1d_prpart, RooFit::DrawOption("EZ"), RooFit::LineWidth(2), RooFit::Cut(Form("sampleType==sampleType::prpart_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::Binning(bin_prpart));
            model->plotOn(rp_1d_prpart, RooFit::Slice(*sampleType,Form("prpart_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*combData));
            model->plotOn(rp_1d_prpart, RooFit::Slice(*sampleType,Form("prpart_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*combData),RooFit::Components(Form("histpdf_MC_gammas_prpart_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kCyan));
            model->plotOn(rp_1d_prpart, RooFit::Slice(*sampleType,Form("prpart_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*combData),RooFit::Components(Form("pdf_MC_Pb_Qo_prpart_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kRed));
            model->plotOn(rp_1d_prpart, RooFit::Slice(*sampleType,Form("prpart_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*combData),RooFit::Components(Form("pdf_MC_Pb_QiS1_prpart_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kGreen));
            model->plotOn(rp_1d_prpart, RooFit::Slice(*sampleType,Form("prpart_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*combData),RooFit::Components(Form("pdf_MC_Pb_QiS2_prpart_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kMagenta));
            model->plotOn(rp_1d_prpart, RooFit::Slice(*sampleType,Form("prpart_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*combData),RooFit::Components(Form("histpdf_MC_1keV_prpart_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kOrange));
            rp_1d_prpart->Draw();
            // c_prpart_data->SetLogy();
            c_prpart_data->Update();

            TCanvas* c_pzpart_data = new TCanvas(Form("c_pzpart_data_%d_%d", detnum[jdet], timenum[jdet]));
            // pt->setRange(2.0, 13.0);
            RooPlot* rp_1d_pzpart = pzpart->frame();
            rp_1d_pzpart->SetTitle(detNames[jdet].c_str());
            combData->plotOn(rp_1d_pzpart, RooFit::DrawOption("EZ"), RooFit::LineWidth(2), RooFit::Cut(Form("sampleType==sampleType::pzpart_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::Binning(bin_pzpart));
            model->plotOn(rp_1d_pzpart, RooFit::Slice(*sampleType,Form("pzpart_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*combData));
            model->plotOn(rp_1d_pzpart, RooFit::Slice(*sampleType,Form("pzpart_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*combData),RooFit::Components(Form("histpdf_MC_gammas_pzpart_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kCyan));
            model->plotOn(rp_1d_pzpart, RooFit::Slice(*sampleType,Form("pzpart_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*combData),RooFit::Components(Form("pdf_MC_Pb_Qo_pzpart_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kRed));
            model->plotOn(rp_1d_pzpart, RooFit::Slice(*sampleType,Form("pzpart_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*combData),RooFit::Components(Form("pdf_MC_Pb_QiS1_pzpart_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kGreen));
            model->plotOn(rp_1d_pzpart, RooFit::Slice(*sampleType,Form("pzpart_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*combData),RooFit::Components(Form("pdf_MC_Pb_QiS2_pzpart_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kMagenta));
            model->plotOn(rp_1d_pzpart, RooFit::Slice(*sampleType,Form("pzpart_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*combData),RooFit::Components(Form("histpdf_MC_1keV_pzpart_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kOrange));
            rp_1d_pzpart->Draw();
            // c_pzpart_data->SetLogy();
            c_pzpart_data->Update();
        }
    }


    rootapp->Run();
    return 0;
}
