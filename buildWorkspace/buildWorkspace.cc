/////////////////////////////////////////////////////////////////////////
//
// 'ORGANIZATION AND SIMULTANEOUS FITS' RooFit tutorial macro #502
// 
// Creating and writing a workspace
//
//
// 07/2008 - Wouter Verkerke 
//
/////////////////////////////////////////////////////////////////////////


#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "RooConstVar.h"
#include "RooChebychev.h"
#include "RooAddPdf.h"
#include "RooHistPdf.h"
#include "RooDataHist.h"
#include "RooWorkspace.h"
#include "RooGaussian.h"
#include "RooBinning.h"
#include "RooPolynomial.h"
#include "RooPlot.h"
#include "RooCategory.h"
#include "RooSimultaneous.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "TFile.h"
#include "TH1.h"
#include "RooCategory.h"
#include "RooMomentMorph.h"

#include "TTree.h"


int main(int argc, char *argv[])
{
    // load the data from the skim
    TFile* datafile = new TFile("../buildSkim/LT_skim.root");
    TFile* simfile = new TFile("../buildSkim/LT_skim_sim_v4.root");
    // TFile* dmcfile = new TFile("../buildSkim/LT_skim_dmc.root");


    int mindet = 0;
    int maxdet = 8;
    std::string detNames[] = {"T1Z1", "T2Z1", "T2Z2", "T4Z2", "T4Z3", "T5Z2 (w/QOS1)", "T5Z2 (w/o QOS1)", "T5Z3 (BS)", "T5Z3 (AS)"};
    std::string leg_names_bg[7] = {"data","total background", "gammas", "sidewall surf. ev.", "S1 surf. ev.", "S2 surf. ev.", "1 keV"};
    int detnum[] = {1, 4, 5, 11, 12, 14, 14, 15, 15};
    int timenum[] = {1, 1, 1, 1, 1, 1, 2, 1, 2};
    std::vector<std::string> detstr = {"1", "4", "5", "11", "12", "14", "14", "15", "15"};
    std::vector<std::string> timestr = {"1", "1", "1", "1", "1", "1", "2", "1", "2"};

    double ptMin = 2.0;
    double ptMax = 13.0;
    double dPt = 0.25;


    //***** MASSES *****//
    std::vector<double> WIMPmasses = {5.0, 7.0, 10.0, 15.0};


    //***** EXPOSURES *****//
    std::vector<double> vec_exposures = {80.21, 82.92, 80.89, 87.40, 83.8, 23.53, 58.02, 18.69, 60.55};


    // ***** OBSERVABLES ***** //
    RooRealVar pt("pt", "total phonon energy [keV]", ptMin, ptMax);
    RooRealVar qimean("qimean", "charge energy (qimean) [keV]", -1.4, 5.);
    RooRealVar prpart("prpart", "radial phonon partition", 0.15, 0.35);
    RooRealVar pzpart("pzpart", "z phonon partition", -0.4, 0.4);
    RooRealVar prpartSIMcorr("prpartSIMcorr", "radial phonon partition", 0.15, 0.35);
    RooRealVar pzpartSIMcorr("pzpartSIMcorr", "z phonon partition", -0.4, 0.4);
    RooRealVar cLT("cLT", "cut to identify events in the low-threshold dataset", -1., 2.);
    RooRealVar cAnalysisThreshold("cAnalysisThreshold", "cut to identify events above the analysis threshold", -1., 2.);
    RooRealVar cConsistency("cConsistency", "consistency cut to identify NRs in Cf data", 0., 1.);
    RooRealVar cQin("cQin", "cut to identify charge inner events", -1., 2.);
    RooRealVar cQsym("cQsym", "cut to identify charge symmetric events", -1., 2.);
    RooRealVar libnum("SIMLibNum", "library number of simulation", -1, 10);
    RooRealVar weights_eff("weights_eff", "trigger and quality cut efficiency", 0.0, 1.0);
    RooRealVar BDT5("BDT5", "5 GeV BDT output", -1.0, 1.0);
    RooRealVar BDT7("BDT7", "7 GeV BDT output", -1.0, 1.0);
    RooRealVar BDT10("BDT10", "10 GeV BDT output", -1.0, 1.0);
    RooRealVar BDT15("BDT15", "15 GeV BDT output", -1.0, 1.0);

    RooRealVar weights_1keV("weights_1keV", "1keV weights", 0., 1.0e6);
    RooRealVar weights_206Pb_Qo("weights_206Pb_Qo", "206Pb Qo weights", 0., 1.0e6);
    RooRealVar weights_210Bi_Qo("weights_210Bi_Qo", "210Bi Qo weights", 0., 1.0e6);
    RooRealVar weights_210Bi_Qo_lowYield("weights_210Bi_Qo_lowYield", "210Bi Qo weights (low yield)", 0., 1.0e6);
    RooRealVar weights_210Pb_Qo("weights_210Pb_Qo", "210Pb Qo weights", 0., 1.0e6);
    RooRealVar weights_210Pb_Qo_lowYield("weights_210Pb_Qo_lowYield", "210Pb Qo weights (low yield)", 0., 1.0e6);
    RooRealVar weights_206Pb_QiS1("weights_206Pb_QiS1", "206Pb Qi S1 weights", 0., 1.0e6);
    RooRealVar weights_210Bi_QiS1("weights_210Bi_QiS1", "210Bi Qi S1 weights", 0., 1.0e6);
    RooRealVar weights_210Pb_QiS1("weights_210Pb_QiS1", "210Pb Qi S1 weights", 0., 1.0e6);
    RooRealVar weights_206Pb_QiS2("weights_206Pb_QiS2", "206Pb Qi S2 weights", 0., 1.0e6);
    RooRealVar weights_210Bi_QiS2("weights_210Bi_QiS2", "210Bi Qi S2 weights", 0., 1.0e6);
    RooRealVar weights_210Pb_QiS2("weights_210Pb_QiS2", "210Pb Qi S2 weights", 0., 1.0e6);
    RooFormulaVar weights_1keV_eff("weights_1keV_eff", "1keV weights * trigger eff", "weights_1keV * weights_eff", RooArgList(weights_eff, weights_1keV));
    RooFormulaVar weights_206Pb_Qo_eff("weights_206Pb_Qo_eff", "206Pb Qo weights * trigger eff", "weights_206Pb_Qo * weights_eff", RooArgList(weights_eff, weights_206Pb_Qo));
    RooFormulaVar weights_210Bi_Qo_eff("weights_210Bi_Qo_eff", "210Bi Qo weights * trigger eff", "weights_210Bi_Qo * weights_eff", RooArgList(weights_eff, weights_210Bi_Qo));
    RooFormulaVar weights_210Bi_Qo_lowYield_eff("weights_210Bi_Qo_lowYield_eff", "210Bi Qo weights (low yield) * trigger eff", "weights_210Bi_Qo_lowYield * weights_eff", RooArgList(weights_eff, weights_210Bi_Qo_lowYield));
    RooFormulaVar weights_210Pb_Qo_eff("weights_210Pb_Qo_eff", "210Pb Qo weights * trigger eff", "weights_210Pb_Qo * weights_eff", RooArgList(weights_eff, weights_210Pb_Qo));
    RooFormulaVar weights_210Pb_Qo_lowYield_eff("weights_210Pb_Qo_lowYield_eff", "210Pb Qo weights (low yield) * trigger eff", "weights_210Pb_Qo_lowYield * weights_eff", RooArgList(weights_eff, weights_210Pb_Qo_lowYield));
    RooFormulaVar weights_206Pb_QiS1_eff("weights_206Pb_QiS1_eff", "206Pb Qi S1 weights * trigger eff", "weights_206Pb_QiS1 * weights_eff", RooArgList(weights_eff, weights_206Pb_QiS1));
    RooFormulaVar weights_210Bi_QiS1_eff("weights_210Bi_QiS1_eff", "210Bi Qi S1 weights * trigger eff", "weights_210Bi_QiS1 * weights_eff", RooArgList(weights_eff, weights_210Bi_QiS1));
    RooFormulaVar weights_210Pb_QiS1_eff("weights_210Pb_QiS1_eff", "210Pb Qi S1 weights * trigger eff", "weights_210Pb_QiS1 * weights_eff", RooArgList(weights_eff, weights_210Pb_QiS1));
    RooFormulaVar weights_206Pb_QiS2_eff("weights_206Pb_QiS2_eff", "206Pb Qi S2 weights * trigger eff", "weights_206Pb_QiS2 * weights_eff", RooArgList(weights_eff, weights_206Pb_QiS2));
    RooFormulaVar weights_210Bi_QiS2_eff("weights_210Bi_QiS2_eff", "210Bi Qi S2 weights * trigger eff", "weights_210Bi_QiS2 * weights_eff", RooArgList(weights_eff, weights_210Bi_QiS2));
    RooFormulaVar weights_210Pb_QiS2_eff("weights_210Pb_QiS2_eff", "210Pb Qi S2 weights * trigger eff", "weights_210Pb_QiS2 * weights_eff", RooArgList(weights_eff, weights_210Pb_QiS2));
    RooFormulaVar weights_1keV_dmc_eff("weights_1keV_dmc_eff", "1keV weights (DMC) * trigger eff", "weights_1keV * weights_eff", RooArgList(weights_eff, weights_1keV));

    // cf weight
    std::vector<RooRealVar> weights_cf;
    for(int jMass = 0; jMass < WIMPmasses.size(); jMass++)
        weights_cf.emplace_back(Form("weights_cf_%.2f_GeV", WIMPmasses[jMass]), Form("%.2f GeV cf weights", WIMPmasses[jMass]), 0, 1.0);
    // RooRealVar weights_cf_5GeV("weights_cf_5GeV", "5 GeV cf weights", 0, 1.0);
    // RooRealVar weights_cf_7GeV("weights_cf_7GeV", "7 GeV cf weights", 0, 1.0);
    // RooRealVar weights_cf_10GeV("weights_cf_10GeV", "10 GeV cf weights", 0, 1.0);
    // RooRealVar weights_cf_15GeV("weights_cf_15GeV", "15 GeV cf weights", 0, 1.0);




    //***** CUSTOM BINNING *****//
    RooBinning bin_pt(ptMin, ptMax);
    bin_pt.addUniform(floor((ptMax - ptMin) / dPt), ptMin, ptMax);
    RooBinning bin_qimean(-1.4, 5.0);
    bin_qimean.addUniform(32, -1.4, 5.0);
    RooBinning bin_prpart(0.15, 0.35);
    bin_prpart.addUniform(50, 0.15, 0.35);
    RooBinning bin_pzpart(-0.4, 0.4);
    bin_pzpart.addUniform(40, -0.4, 0.4);

    pt.setBinning(bin_pt);
    qimean.setBinning(bin_qimean);
    prpart.setBinning(bin_prpart);
    pzpart.setBinning(bin_pzpart);
    prpartSIMcorr.setBinning(bin_prpart);
    pzpartSIMcorr.setBinning(bin_pzpart);


    // Systematics morphing parameter
    RooRealVar* alpha_Qo_syst = new RooRealVar("alpha_Qo_syst", "nuisance parameter for yield systematic", 0.0, 1.0);
    TVectorD shapeParamPoints(2);
    shapeParamPoints[0] = 0.0;
    shapeParamPoints[1] = 1.0;



    // Stuff for all detectors
    RooRealVar* relative_cross_sec = new RooRealVar("relative_cross_sec", "WIMP-n cross section in units of 10^-42 cm^2", 0, 1000.0);
    // Need to do pre-loop to define all sample types in RooSimultaneous pdf
    RooCategory sampleTypeAll("sampleType", "sampleTypeAll");
    for(int jdet = mindet; jdet<=maxdet; jdet++)
    {
        sampleTypeAll.defineType(Form("pt_sample_%d_%d", detnum[jdet], timenum[jdet]));
        sampleTypeAll.defineType(Form("qimean_sample_%d_%d", detnum[jdet], timenum[jdet]));
        sampleTypeAll.defineType(Form("prpart_sample_%d_%d", detnum[jdet], timenum[jdet]));
        sampleTypeAll.defineType(Form("pzpart_sample_%d_%d", detnum[jdet], timenum[jdet]));
        sampleTypeAll.defineType(Form("BDT_sample_%d_%d", detnum[jdet], timenum[jdet]));
    }
    RooDataSet combData("combData", "combined data", 
                            RooArgSet(sampleTypeAll, pt, qimean, prpart, pzpart, BDT5, BDT7, BDT10, BDT15));
    RooCategory sampleType2d("sampleType2d", "sampleType2d");
    for(int jdet = mindet; jdet<=maxdet; jdet++)
    {
        sampleType2d.defineType(Form("ptVqimean_sample_%d_%d", detnum[jdet], timenum[jdet]));
        sampleType2d.defineType(Form("prpart_sample_%d_%d", detnum[jdet], timenum[jdet]));
        sampleType2d.defineType(Form("pzpart_sample_%d_%d", detnum[jdet], timenum[jdet]));
        sampleType2d.defineType(Form("BDT_sample_%d_%d", detnum[jdet], timenum[jdet]));
    }
    RooDataSet combData2d("combData2d", "combined data 2d", 
                            RooArgSet(sampleType2d, pt, qimean, prpart, pzpart, BDT5, BDT7, BDT10, BDT15));
    // Loop over WIMP masses, defining a different model for each mass
    std::vector<RooSimultaneous> simul_pdf_bg;
    std::vector<RooSimultaneous> simul_pdf_bg_2d;
    for(int jMass = 0; jMass < WIMPmasses.size(); jMass++)
    {
        simul_pdf_bg.emplace_back(Form("simul_pdf_bg_%.2f_GeV", WIMPmasses[jMass]), "simultaneous pdf", sampleTypeAll);
        simul_pdf_bg_2d.emplace_back(Form("simul_pdf_bg_2d_%.2f_GeV", WIMPmasses[jMass]), "simultaneous pdf", sampleType2d);
    }


    for(int jdet = mindet; jdet <= maxdet; jdet++)
    {
        //***** READ DETECTOR-DEPENDENT TREES *****//
        // tree of low-background data
        std::string bg_name = "bg_zip"+detstr[jdet]+"_time"+timestr[jdet];
        TTree* t_bg = (TTree*)datafile->Get(bg_name.c_str());

        // tree with californium data
        std::string cf_name = "cf_zip"+detstr[jdet]+"_time"+timestr[jdet];
        TTree* t_cf = (TTree*)datafile->Get(cf_name.c_str());

        // tree with simulated data
        std::string sim_name = "sim_zip"+detstr[jdet]+"_time"+timestr[jdet];
        TTree* t_sim = (TTree*)simfile->Get(sim_name.c_str());

        // std::string dmc_name = "dmc_zip"+detstr[jdet]+"_time"+timestr[jdet];
        // TTree* t_dmc = (TTree*)dmcfile->Get(dmc_name.c_str());

        TCanvas* cCheck = new TCanvas("cCheck");
        t_sim->Draw("prpart");
        cCheck->Update();



        //***** DATASETS *****//
        // the real data
        RooDataSet data_signal("data_signal", "signal region data", 
                                RooArgSet(pt, qimean, prpart, pzpart, BDT5, BDT7, BDT10, BDT15, cAnalysisThreshold), RooFit::Import(*t_bg), 
                                RooFit::Cut(Form("pt>%f && pt<%f && cAnalysisThreshold==1", ptMin, ptMax)));

        // // cf data
        RooArgSet cf_vars(pt, qimean, prpart, pzpart, BDT5, BDT7, BDT10, BDT15, cConsistency);
        for(int jMass = 0; jMass < WIMPmasses.size(); jMass++)
            cf_vars.add(weights_cf[jMass]);
        RooDataSet data_cf_noweight("data_cf_noweight", "cf data (unweighted)", cf_vars, RooFit::Import(*t_cf), 
                           RooFit::Cut(Form("pt>%f && pt<%f", ptMin, ptMax)));


        // unweighted data
        RooArgSet argset_MC_gammas(pt, qimean, prpartSIMcorr, pzpartSIMcorr, libnum, weights_eff);
        RooArgSet argset_MC_1keV(pt, qimean, prpartSIMcorr, pzpartSIMcorr, libnum, weights_1keV, weights_eff);
        RooArgSet argset_MC_210Pb_Qo(pt, qimean, prpartSIMcorr, pzpartSIMcorr, libnum, weights_210Pb_Qo, weights_eff);
        RooArgSet argset_MC_210Pb_Qo_lowYield(pt, qimean, prpartSIMcorr, pzpartSIMcorr, libnum, weights_210Pb_Qo_lowYield, weights_eff);
        RooArgSet argset_MC_210Bi_Qo(pt, qimean, prpartSIMcorr, pzpartSIMcorr, libnum, weights_210Bi_Qo, weights_eff);
        RooArgSet argset_MC_210Bi_Qo_lowYield(pt, qimean, prpartSIMcorr, pzpartSIMcorr, libnum, weights_210Bi_Qo_lowYield, weights_eff);
        RooArgSet argset_MC_206Pb_Qo(pt, qimean, prpartSIMcorr, pzpartSIMcorr, libnum, weights_206Pb_Qo, weights_eff);
        RooArgSet argset_MC_210Pb_QiS1(pt, qimean, prpartSIMcorr, pzpartSIMcorr, libnum, weights_210Pb_QiS1, weights_eff);
        RooArgSet argset_MC_210Bi_QiS1(pt, qimean, prpartSIMcorr, pzpartSIMcorr, libnum, weights_210Bi_QiS1, weights_eff);
        RooArgSet argset_MC_206Pb_QiS1(pt, qimean, prpartSIMcorr, pzpartSIMcorr, libnum, weights_206Pb_QiS1, weights_eff);
        RooArgSet argset_MC_210Pb_QiS2(pt, qimean, prpartSIMcorr, pzpartSIMcorr, libnum, weights_210Pb_QiS2, weights_eff);
        RooArgSet argset_MC_210Bi_QiS2(pt, qimean, prpartSIMcorr, pzpartSIMcorr, libnum, weights_210Bi_QiS2, weights_eff);
        RooArgSet argset_MC_206Pb_QiS2(pt, qimean, prpartSIMcorr, pzpartSIMcorr, libnum, weights_206Pb_QiS2, weights_eff);
        // RooArgSet argset_MC_1keV_dmc(pt, qimean, prpart, pzpart, libnum, weights_1keV, weights_eff);
        argset_MC_gammas.add(RooArgSet(BDT5, BDT7, BDT10, BDT15));
        argset_MC_1keV.add(RooArgSet(BDT5, BDT7, BDT10, BDT15));
        argset_MC_210Pb_Qo.add(RooArgSet(BDT5, BDT7, BDT10, BDT15));
        argset_MC_210Pb_Qo_lowYield.add(RooArgSet(BDT5, BDT7, BDT10, BDT15));
        argset_MC_210Bi_Qo.add(RooArgSet(BDT5, BDT7, BDT10, BDT15));
        argset_MC_210Bi_Qo_lowYield.add(RooArgSet(BDT5, BDT7, BDT10, BDT15));
        argset_MC_206Pb_Qo.add(RooArgSet(BDT5, BDT7, BDT10, BDT15));
        argset_MC_210Pb_QiS1.add(RooArgSet(BDT5, BDT7, BDT10, BDT15));
        argset_MC_210Bi_QiS1.add(RooArgSet(BDT5, BDT7, BDT10, BDT15));
        argset_MC_206Pb_QiS1.add(RooArgSet(BDT5, BDT7, BDT10, BDT15));
        argset_MC_210Pb_QiS2.add(RooArgSet(BDT5, BDT7, BDT10, BDT15));
        argset_MC_210Bi_QiS2.add(RooArgSet(BDT5, BDT7, BDT10, BDT15));
        argset_MC_206Pb_QiS2.add(RooArgSet(BDT5, BDT7, BDT10, BDT15));
        // argset_MC_1keV_dmc.add(RooArgSet(BDT5, BDT7, BDT10, BDT15));

        // RooDataSet data_MC_1keV_dmc_noweight("data_MC_1keV_dmc_noweight", "MC 1keV (DMC)", argset_MC_1keV_dmc, 
        //                             RooFit::Import(*t_dmc), RooFit::Cut(Form("pt>%f && pt<%f", ptMin, ptMax)));
        // data_MC_1keV_dmc_noweight.Print();
        RooDataSet data_MC_gammas("data_MC_gammas", "MC gammas", argset_MC_gammas, 
                                    RooFit::Import(*t_sim), RooFit::Cut(Form("pt>%f && pt<%f && SIMLibNum==0", ptMin, ptMax)));
        RooDataSet data_MC_1keV_noweight("data_MC_1keV_noweight", "MC 1keV", argset_MC_1keV, 
                                    RooFit::Import(*t_sim), RooFit::Cut(Form("pt>%f && pt<%f && SIMLibNum==1", ptMin, ptMax)));
        RooDataSet data_MC_210Pb_Qo_noweight("data_MC_210Pb_Qo_noweight", "MC 210Pb Qo", argset_MC_210Pb_Qo,
                                    RooFit::Import(*t_sim), RooFit::Cut(Form("pt>%f && pt<%f && SIMLibNum==2", ptMin, ptMax)));
        RooDataSet data_MC_210Pb_Qo_lowYield_noweight("data_MC_210Pb_Qo_lowYield_noweight", "MC 210Pb Qo (low yield)", argset_MC_210Pb_Qo_lowYield,
                                    RooFit::Import(*t_sim), RooFit::Cut(Form("pt>%f && pt<%f && SIMLibNum==3", ptMin, ptMax)));
        RooDataSet data_MC_210Bi_Qo_noweight("data_MC_210Bi_Qo_noweight", "MC 210Bi Qo", argset_MC_210Bi_Qo, 
                                    RooFit::Import(*t_sim), RooFit::Cut(Form("pt>%f && pt<%f && SIMLibNum==2", ptMin, ptMax)));
        RooDataSet data_MC_210Bi_Qo_lowYield_noweight("data_MC_210Bi_Qo_lowYield_noweight", "MC 210Bi Qo (low yield)", argset_MC_210Bi_Qo_lowYield, 
                                    RooFit::Import(*t_sim), RooFit::Cut(Form("pt>%f && pt<%f && SIMLibNum==3", ptMin, ptMax)));
        RooDataSet data_MC_206Pb_Qo_noweight("data_MC_206Pb_Qo_noweight", "MC 206Pb Qo", argset_MC_206Pb_Qo, 
                                    RooFit::Import(*t_sim), RooFit::Cut(Form("pt>%f && pt<%f && SIMLibNum==3", ptMin, ptMax)));
        RooDataSet data_MC_210Pb_QiS1_noweight("data_MC_210Pb_QiS1_noweight", "MC 210Pb QiS1", argset_MC_210Pb_QiS1, 
                                    RooFit::Import(*t_sim), RooFit::Cut(Form("pt>%f && pt<%f && SIMLibNum==4", ptMin, ptMax)));
        RooDataSet data_MC_210Bi_QiS1_noweight("data_MC_210Bi_QiS1_noweight", "MC 210Bi QiS1", argset_MC_210Bi_QiS1, 
                                    RooFit::Import(*t_sim), RooFit::Cut(Form("pt>%f && pt<%f && SIMLibNum==4", ptMin, ptMax)));
        RooDataSet data_MC_206Pb_QiS1_noweight("data_MC_206Pb_QiS1_noweight", "MC 206Pb QiS1", argset_MC_206Pb_QiS1, 
                                    RooFit::Import(*t_sim), RooFit::Cut(Form("pt>%f && pt<%f && SIMLibNum==6", ptMin, ptMax)));
        RooDataSet data_MC_210Pb_QiS2_noweight("data_MC_210Pb_QiS2_noweight", "MC 210Pb QiS2", argset_MC_210Pb_QiS2, 
                                    RooFit::Import(*t_sim), RooFit::Cut(Form("pt>%f && pt<%f && SIMLibNum==5", ptMin, ptMax)));
        RooDataSet data_MC_210Bi_QiS2_noweight("data_MC_210Bi_QiS2_noweight", "MC 210Bi QiS2", argset_MC_210Bi_QiS2, 
                                    RooFit::Import(*t_sim), RooFit::Cut(Form("pt>%f && pt<%f && SIMLibNum==5", ptMin, ptMax)));
        RooDataSet data_MC_206Pb_QiS2_noweight("data_MC_206Pb_QiS2_noweight", "MC 206Pb QiS2", argset_MC_206Pb_QiS2, 
                                    RooFit::Import(*t_sim), RooFit::Cut(Form("pt>%f && pt<%f && SIMLibNum==7", ptMin, ptMax)));
        // RooDataSet data_MC_1keV_dmc_noweight("data_MC_1keV_dmc_noweight", "MC 1keV (DMC)", argset_MC_1keV_dmc, 
        //                             RooFit::Import(*t_dmc), RooFit::Cut(Form("pt>%f && pt<%f && SIMLibNum==1", ptMin, ptMax)));



        // add columns for (type weight) * (efficiency)
        RooRealVar* weights_1keV_eff_var = (RooRealVar*) data_MC_1keV_noweight.addColumn(weights_1keV_eff);
        RooRealVar* weights_210Pb_Qo_eff_var = (RooRealVar*) data_MC_210Pb_Qo_noweight.addColumn(weights_210Pb_Qo_eff);
        RooRealVar* weights_210Pb_Qo_lowYield_eff_var = (RooRealVar*) data_MC_210Pb_Qo_lowYield_noweight.addColumn(weights_210Pb_Qo_lowYield_eff);
        RooRealVar* weights_210Bi_Qo_eff_var = (RooRealVar*) data_MC_210Bi_Qo_noweight.addColumn(weights_210Bi_Qo_eff);
        RooRealVar* weights_210Bi_Qo_lowYield_eff_var = (RooRealVar*) data_MC_210Bi_Qo_lowYield_noweight.addColumn(weights_210Bi_Qo_lowYield_eff);
        RooRealVar* weights_206Pb_Qo_eff_var = (RooRealVar*) data_MC_206Pb_Qo_noweight.addColumn(weights_206Pb_Qo_eff);
        RooRealVar* weights_210Pb_QiS1_eff_var = (RooRealVar*) data_MC_210Pb_QiS1_noweight.addColumn(weights_210Pb_QiS1_eff);
        RooRealVar* weights_210Bi_QiS1_eff_var = (RooRealVar*) data_MC_210Bi_QiS1_noweight.addColumn(weights_210Bi_QiS1_eff);
        RooRealVar* weights_206Pb_QiS1_eff_var = (RooRealVar*) data_MC_206Pb_QiS1_noweight.addColumn(weights_206Pb_QiS1_eff);
        RooRealVar* weights_210Pb_QiS2_eff_var = (RooRealVar*) data_MC_210Pb_QiS2_noweight.addColumn(weights_210Pb_QiS2_eff);
        RooRealVar* weights_210Bi_QiS2_eff_var = (RooRealVar*) data_MC_210Bi_QiS2_noweight.addColumn(weights_210Bi_QiS2_eff);
        RooRealVar* weights_206Pb_QiS2_eff_var = (RooRealVar*) data_MC_206Pb_QiS2_noweight.addColumn(weights_206Pb_QiS2_eff);
        // RooRealVar* weights_1keV_dmc_eff_var = (RooRealVar*) data_MC_1keV_dmc_noweight.addColumn(weights_1keV_dmc_eff);

        // save weighted datasets
        RooDataSet data_MC_1keV("data_MC_1keV", "MC 1keV", &data_MC_1keV_noweight, *data_MC_1keV_noweight.get(), 0, weights_1keV.GetName());
        data_MC_1keV.changeObservableName("prpartSIMcorr", "prpart");
        data_MC_1keV.changeObservableName("pzpartSIMcorr", "pzpart");
        RooDataSet data_MC_210Pb_Qo("data_MC_210Pb_Qo", "MC 210Pb_Qo", &data_MC_210Pb_Qo_noweight, *data_MC_210Pb_Qo_noweight.get(), 0, weights_210Pb_Qo.GetName());
        data_MC_210Pb_Qo.changeObservableName("prpartSIMcorr", "prpart");
        data_MC_210Pb_Qo.changeObservableName("pzpartSIMcorr", "pzpart");
        RooDataSet data_MC_210Pb_Qo_lowYield("data_MC_210Pb_Qo_lowYield", "MC 210Pb_Qo (low yield)", &data_MC_210Pb_Qo_lowYield_noweight, *data_MC_210Pb_Qo_lowYield_noweight.get(), 0, weights_210Pb_Qo_lowYield.GetName());
        data_MC_210Pb_Qo_lowYield.changeObservableName("prpartSIMcorr", "prpart");
        data_MC_210Pb_Qo_lowYield.changeObservableName("pzpartSIMcorr", "pzpart");
        RooDataSet data_MC_210Bi_Qo("data_MC_210Bi_Qo", "MC 210Bi_Qo", &data_MC_210Bi_Qo_noweight, *data_MC_210Bi_Qo_noweight.get(), 0, weights_210Bi_Qo.GetName());
        data_MC_210Bi_Qo.changeObservableName("prpartSIMcorr", "prpart");
        data_MC_210Bi_Qo.changeObservableName("pzpartSIMcorr", "pzpart");
        RooDataSet data_MC_210Bi_Qo_lowYield("data_MC_210Bi_Qo_lowYield", "MC 210Bi_Qo (low yield)", &data_MC_210Bi_Qo_lowYield_noweight, *data_MC_210Bi_Qo_lowYield_noweight.get(), 0, weights_210Bi_Qo_lowYield.GetName());
        data_MC_210Bi_Qo_lowYield.changeObservableName("prpartSIMcorr", "prpart");
        data_MC_210Bi_Qo_lowYield.changeObservableName("pzpartSIMcorr", "pzpart");
        RooDataSet data_MC_206Pb_Qo("data_MC_206Pb_Qo", "MC 206Pb_Qo", &data_MC_206Pb_Qo_noweight, *data_MC_206Pb_Qo_noweight.get(), 0, weights_206Pb_Qo.GetName());
        data_MC_206Pb_Qo.changeObservableName("prpartSIMcorr", "prpart");
        data_MC_206Pb_Qo.changeObservableName("pzpartSIMcorr", "pzpart");
        RooDataSet data_MC_210Pb_QiS1("data_MC_210Pb_QiS1", "MC 210Pb_QiS1", &data_MC_210Pb_QiS1_noweight, *data_MC_210Pb_QiS1_noweight.get(), 0, weights_210Pb_QiS1.GetName());
        data_MC_210Pb_QiS1.changeObservableName("prpartSIMcorr", "prpart");
        data_MC_210Pb_QiS1.changeObservableName("pzpartSIMcorr", "pzpart");
        RooDataSet data_MC_210Bi_QiS1("data_MC_210Bi_QiS1", "MC 210Bi_QiS1", &data_MC_210Bi_QiS1_noweight, *data_MC_210Bi_QiS1_noweight.get(), 0, weights_210Bi_QiS1.GetName());
        data_MC_210Bi_QiS1.changeObservableName("prpartSIMcorr", "prpart");
        data_MC_210Bi_QiS1.changeObservableName("pzpartSIMcorr", "pzpart");
        RooDataSet data_MC_206Pb_QiS1("data_MC_206Pb_QiS1", "MC 206Pb_QiS1", &data_MC_206Pb_QiS1_noweight, *data_MC_206Pb_QiS1_noweight.get(), 0, weights_206Pb_QiS1.GetName());
        data_MC_206Pb_QiS1.changeObservableName("prpartSIMcorr", "prpart");
        data_MC_206Pb_QiS1.changeObservableName("pzpartSIMcorr", "pzpart");
        RooDataSet data_MC_210Pb_QiS2("data_MC_210Pb_QiS2", "MC 210Pb_QiS2", &data_MC_210Pb_QiS2_noweight, *data_MC_210Pb_QiS2_noweight.get(), 0, weights_210Pb_QiS2.GetName());
        data_MC_210Pb_QiS2.changeObservableName("prpartSIMcorr", "prpart");
        data_MC_210Pb_QiS2.changeObservableName("pzpartSIMcorr", "pzpart");
        RooDataSet data_MC_210Bi_QiS2("data_MC_210Bi_QiS2", "MC 210Bi_QiS2", &data_MC_210Bi_QiS2_noweight, *data_MC_210Bi_QiS2_noweight.get(), 0, weights_210Bi_QiS2.GetName());
        data_MC_210Bi_QiS2.changeObservableName("prpartSIMcorr", "prpart");
        data_MC_210Bi_QiS2.changeObservableName("pzpartSIMcorr", "pzpart");
        RooDataSet data_MC_206Pb_QiS2("data_MC_206Pb_QiS2", "MC 206Pb_QiS2", &data_MC_206Pb_QiS2_noweight, *data_MC_206Pb_QiS2_noweight.get(), 0, weights_206Pb_QiS2.GetName());
        data_MC_206Pb_QiS2.changeObservableName("prpartSIMcorr", "prpart");
        data_MC_206Pb_QiS2.changeObservableName("pzpartSIMcorr", "pzpart");
        // RooDataSet data_MC_1keV_dmc("data_MC_1keV_dmc", "MC 1keV (DMC)", &data_MC_1keV_dmc_noweight, *data_MC_1keV_dmc_noweight.get(), 0, weights_1keV.GetName());
        
        RooDataSet data_MC_gammas_effcor("data_MC_gammas_effcor", "MC 1keV", &data_MC_gammas, *data_MC_gammas.get(), 0, weights_eff.GetName());
        data_MC_gammas_effcor.changeObservableName("prpartSIMcorr", "prpart");
        data_MC_gammas_effcor.changeObservableName("pzpartSIMcorr", "pzpart");
        RooDataSet data_MC_1keV_effcor("data_MC_1keV_effcor", "MC 1keV", &data_MC_1keV_noweight, *data_MC_1keV_noweight.get(), 0, weights_1keV_eff_var->GetName());
        data_MC_1keV_effcor.changeObservableName("prpartSIMcorr", "prpart");
        data_MC_1keV_effcor.changeObservableName("pzpartSIMcorr", "pzpart");
        RooDataSet data_MC_210Pb_Qo_effcor("data_MC_210Pb_Qo_effcor", "MC 210Pb_Qo", &data_MC_210Pb_Qo_noweight, *data_MC_210Pb_Qo_noweight.get(), 0, weights_210Pb_Qo_eff_var->GetName());
        data_MC_210Pb_Qo_effcor.changeObservableName("prpartSIMcorr", "prpart");
        data_MC_210Pb_Qo_effcor.changeObservableName("pzpartSIMcorr", "pzpart");
        RooDataSet data_MC_210Pb_Qo_lowYield_effcor("data_MC_210Pb_Qo_lowYield_effcor", "MC 210Pb_Qo (low yield)", &data_MC_210Pb_Qo_lowYield_noweight, *data_MC_210Pb_Qo_lowYield_noweight.get(), 0, weights_210Pb_Qo_lowYield_eff_var->GetName());
        data_MC_210Pb_Qo_lowYield_effcor.changeObservableName("prpartSIMcorr", "prpart");
        data_MC_210Pb_Qo_lowYield_effcor.changeObservableName("pzpartSIMcorr", "pzpart");
        RooDataSet data_MC_210Bi_Qo_effcor("data_MC_210Bi_Qo_effcor", "MC 210Bi_Qo", &data_MC_210Bi_Qo_noweight, *data_MC_210Bi_Qo_noweight.get(), 0, weights_210Bi_Qo_eff_var->GetName());
        data_MC_210Bi_Qo_effcor.changeObservableName("prpartSIMcorr", "prpart");
        data_MC_210Bi_Qo_effcor.changeObservableName("pzpartSIMcorr", "pzpart");
        RooDataSet data_MC_210Bi_Qo_lowYield_effcor("data_MC_210Bi_Qo_lowYield_effcor", "MC 210Bi_Qo (low yield)", &data_MC_210Bi_Qo_lowYield_noweight, *data_MC_210Bi_Qo_lowYield_noweight.get(), 0, weights_210Bi_Qo_lowYield_eff_var->GetName());
        data_MC_210Bi_Qo_lowYield_effcor.changeObservableName("prpartSIMcorr", "prpart");
        data_MC_210Bi_Qo_lowYield_effcor.changeObservableName("pzpartSIMcorr", "pzpart");
        RooDataSet data_MC_206Pb_Qo_effcor("data_MC_206Pb_Qo_effcor", "MC 206Pb_Qo", &data_MC_206Pb_Qo_noweight, *data_MC_206Pb_Qo_noweight.get(), 0, weights_206Pb_Qo_eff_var->GetName());
        data_MC_206Pb_Qo_effcor.changeObservableName("prpartSIMcorr", "prpart");
        data_MC_206Pb_Qo_effcor.changeObservableName("pzpartSIMcorr", "pzpart");
        RooDataSet data_MC_210Pb_QiS1_effcor("data_MC_210Pb_QiS1_effcor", "MC 210Pb_QiS1", &data_MC_210Pb_QiS1_noweight, *data_MC_210Pb_QiS1_noweight.get(), 0, weights_210Pb_QiS1_eff_var->GetName());
        data_MC_210Pb_QiS1_effcor.changeObservableName("prpartSIMcorr", "prpart");
        data_MC_210Pb_QiS1_effcor.changeObservableName("pzpartSIMcorr", "pzpart");
        RooDataSet data_MC_210Bi_QiS1_effcor("data_MC_210Bi_QiS1_effcor", "MC 210Bi_QiS1", &data_MC_210Bi_QiS1_noweight, *data_MC_210Bi_QiS1_noweight.get(), 0, weights_210Bi_QiS1_eff_var->GetName());
        data_MC_210Bi_QiS1_effcor.changeObservableName("prpartSIMcorr", "prpart");
        data_MC_210Bi_QiS1_effcor.changeObservableName("pzpartSIMcorr", "pzpart");
        RooDataSet data_MC_206Pb_QiS1_effcor("data_MC_206Pb_QiS1_effcor", "MC 206Pb_QiS1", &data_MC_206Pb_QiS1_noweight, *data_MC_206Pb_QiS1_noweight.get(), 0, weights_206Pb_QiS1_eff_var->GetName());
        data_MC_206Pb_QiS1_effcor.changeObservableName("prpartSIMcorr", "prpart");
        data_MC_206Pb_QiS1_effcor.changeObservableName("pzpartSIMcorr", "pzpart");
        RooDataSet data_MC_210Pb_QiS2_effcor("data_MC_210Pb_QiS2_effcor", "MC 210Pb_QiS2", &data_MC_210Pb_QiS2_noweight, *data_MC_210Pb_QiS2_noweight.get(), 0, weights_210Pb_QiS2_eff_var->GetName());
        data_MC_210Pb_QiS2_effcor.changeObservableName("prpartSIMcorr", "prpart");
        data_MC_210Pb_QiS2_effcor.changeObservableName("pzpartSIMcorr", "pzpart");
        RooDataSet data_MC_210Bi_QiS2_effcor("data_MC_210Bi_QiS2_effcor", "MC 210Bi_QiS2", &data_MC_210Bi_QiS2_noweight, *data_MC_210Bi_QiS2_noweight.get(), 0, weights_210Bi_QiS2_eff_var->GetName());
        data_MC_210Bi_QiS2_effcor.changeObservableName("prpartSIMcorr", "prpart");
        data_MC_210Bi_QiS2_effcor.changeObservableName("pzpartSIMcorr", "pzpart");
        RooDataSet data_MC_206Pb_QiS2_effcor("data_MC_206Pb_QiS2_effcor", "MC 206Pb_QiS2", &data_MC_206Pb_QiS2_noweight, *data_MC_206Pb_QiS2_noweight.get(), 0, weights_206Pb_QiS2_eff_var->GetName());
        data_MC_206Pb_QiS2_effcor.changeObservableName("prpartSIMcorr", "prpart");
        data_MC_206Pb_QiS2_effcor.changeObservableName("pzpartSIMcorr", "pzpart");
        // RooDataSet data_MC_1keV_dmc_effcor("data_MC_1keV_dmc_effcor", "MC 1keV (DMC)", &data_MC_1keV_dmc_noweight, *data_MC_1keV_dmc_noweight.get(), 0, weights_1keV_dmc_eff_var->GetName());
        
        // print info on datasets
        data_signal.Print();
        data_cf_noweight.Print();
        data_MC_gammas.Print();
        data_MC_1keV.Print();
        data_MC_210Pb_Qo.Print();
        data_MC_210Pb_Qo_lowYield.Print();
        data_MC_210Bi_Qo.Print();
        data_MC_210Bi_Qo_lowYield.Print();
        data_MC_206Pb_Qo.Print();
        data_MC_210Pb_QiS1.Print();
        data_MC_210Bi_QiS1.Print();
        data_MC_206Pb_QiS1.Print();
        data_MC_210Pb_QiS2.Print();
        data_MC_210Bi_QiS2.Print();
        data_MC_206Pb_QiS2.Print();
        // data_MC_1keV_dmc.Print();

        data_MC_gammas_effcor.Print();
        data_MC_1keV_effcor.Print();
        data_MC_210Pb_Qo_effcor.Print();
        data_MC_210Pb_Qo_lowYield_effcor.Print();
        data_MC_210Bi_Qo_effcor.Print();
        data_MC_210Bi_Qo_lowYield_effcor.Print();
        data_MC_206Pb_Qo_effcor.Print();
        data_MC_210Pb_QiS1_effcor.Print();
        data_MC_210Bi_QiS1_effcor.Print();
        data_MC_206Pb_QiS1_effcor.Print();
        data_MC_210Pb_QiS2_effcor.Print();
        data_MC_210Bi_QiS2_effcor.Print();
        data_MC_206Pb_QiS2_effcor.Print();
        // data_MC_1keV_dmc_effcor.Print();

        

        // Arrange the data to be imported in the correct arrangement of
        // categories. Note that this is inherently awkward because all of our
        // "samples" are in fact the same events, but just use different 
        // variables for the fitting. We therefore just repeated import the
        // same dataset, assigning it to a different category each time.
        RooDataSet detectorData("detectorData", "detectorData", 
                            RooArgSet(sampleTypeAll, pt, qimean, prpart, pzpart, BDT5, BDT7, BDT10, BDT15),
                            RooFit::Index(sampleTypeAll),
                            RooFit::Import(Form("pt_sample_%d_%d", detnum[jdet], timenum[jdet]), data_signal),
                            RooFit::Import(Form("qimean_sample_%d_%d", detnum[jdet], timenum[jdet]), data_signal),
                            RooFit::Import(Form("prpart_sample_%d_%d", detnum[jdet], timenum[jdet]), data_signal),
                            RooFit::Import(Form("pzpart_sample_%d_%d", detnum[jdet], timenum[jdet]), data_signal),
                            RooFit::Import(Form("BDT_sample_%d_%d", detnum[jdet], timenum[jdet]), data_signal));
        combData.append(detectorData);
        RooDataSet detectorData2d("detectorData2d", "detectorData2d", 
                            RooArgSet(sampleType2d, pt, qimean, prpart, pzpart, BDT5, BDT7, BDT10, BDT15),
                            RooFit::Index(sampleType2d),
                            RooFit::Import(Form("ptVqimean_sample_%d_%d", detnum[jdet], timenum[jdet]), data_signal),
                            RooFit::Import(Form("prpart_sample_%d_%d", detnum[jdet], timenum[jdet]), data_signal),
                            RooFit::Import(Form("pzpart_sample_%d_%d", detnum[jdet], timenum[jdet]), data_signal),
                            RooFit::Import(Form("BDT_sample_%d_%d", detnum[jdet], timenum[jdet]), data_signal));
        combData2d.append(detectorData2d);



        //***** DEFINE PDFs *****//
        // Variables for the number of background events in each category
        RooRealVar* N_1keV = new RooRealVar(Form("N_1keV_%d_%d", detnum[jdet], timenum[jdet]), "number of 1keV line events", 400, 0., 1000.);
        RooRealVar* N_gammas = new RooRealVar(Form("N_gammas_%d_%d", detnum[jdet], timenum[jdet]), "number of gamma events", 300, 0., 2000.);
        RooRealVar* N_210Pb_Qo = new RooRealVar(Form("N_210Pb_Qo_%d_%d", detnum[jdet], timenum[jdet]), "number of 210Pb Qouter events", 200, 0., 1000.);
        RooRealVar* N_210Pb_QiS1 = new RooRealVar(Form("N_210Pb_QiS1_%d_%d", detnum[jdet], timenum[jdet]), "number of 210Pb Qinner S1 events", 20, 0., 1000.);
        RooRealVar* N_210Pb_QiS2 = new RooRealVar(Form("N_210Pb_QiS2_%d_%d", detnum[jdet], timenum[jdet]), "number of 210Pb Qinner S2 events", 20, 0., 1000.);
        RooFormulaVar* N_bg = new RooFormulaVar(Form("N_bg_%d_%d", detnum[jdet], timenum[jdet]), Form("%s + %s + %s + %s + %s", N_1keV->GetName(), N_gammas->GetName(), N_210Pb_Qo->GetName(), N_210Pb_QiS1->GetName(), N_210Pb_QiS2->GetName()),
                                                RooArgList(*N_1keV, *N_gammas, *N_210Pb_Qo, *N_210Pb_QiS1, *N_210Pb_QiS2));

        // Relative fractions of the 210Pb Qouter events
        double sum_weights_210Pb_Qo = data_MC_210Pb_Qo_effcor.sumEntries();
        double sum_weights_210Pb_Qo_lowYield = data_MC_210Pb_Qo_lowYield_effcor.sumEntries();
        double sum_weights_210Bi_Qo = data_MC_210Bi_Qo_effcor.sumEntries();
        double sum_weights_210Bi_Qo_lowYield = data_MC_210Bi_Qo_lowYield_effcor.sumEntries();
        double sum_weights_206Pb_Qo = data_MC_206Pb_Qo_effcor.sumEntries();
        double sum_weights_Pb_Qo = sum_weights_210Pb_Qo + sum_weights_210Bi_Qo + sum_weights_206Pb_Qo;
        double sum_weights_Pb_Qo_lowYield = sum_weights_210Pb_Qo_lowYield + sum_weights_210Bi_Qo_lowYield + sum_weights_206Pb_Qo;
        RooRealVar* frac_210Pb_Qo = new RooRealVar(Form("frac_210Pb_Qo_%d_%d", detnum[jdet], timenum[jdet]), "fraction of Pb Qo that is 210Pb", sum_weights_210Pb_Qo / sum_weights_Pb_Qo);
        RooRealVar* frac_210Pb_Qo_lowYield = new RooRealVar(Form("frac_210Pb_Qo_lowYield_%d_%d", detnum[jdet], timenum[jdet]), "fraction of Pb Qo that is 210Pb (low yield)", sum_weights_210Pb_Qo_lowYield / sum_weights_Pb_Qo_lowYield);
        RooRealVar* frac_210Bi_Qo = new RooRealVar(Form("frac_210Bi_Qo_%d_%d", detnum[jdet], timenum[jdet]), "fraction of Pb Qo that is 210Bi", sum_weights_210Bi_Qo / sum_weights_Pb_Qo);
        RooRealVar* frac_210Bi_Qo_lowYield = new RooRealVar(Form("frac_210Bi_Qo_lowYield_%d_%d", detnum[jdet], timenum[jdet]), "fraction of Pb Qo that is 210Bi (low yield)", sum_weights_210Bi_Qo_lowYield / sum_weights_Pb_Qo_lowYield);
        RooRealVar* frac_206Pb_Qo = new RooRealVar(Form("frac_206Pb_Qo_%d_%d", detnum[jdet], timenum[jdet]), "fraction of Pb Qo that is 206Pb", sum_weights_206Pb_Qo / sum_weights_Pb_Qo);
        std::cout << "sum_weights_210Pb_Qo: " << sum_weights_210Pb_Qo << std::endl;
        std::cout << "sum_weights_210Pb_Qo_lowYield: " << sum_weights_210Pb_Qo_lowYield << std::endl;
        std::cout << "sum_weights_210Bi_Qo: " << sum_weights_210Bi_Qo << std::endl;
        std::cout << "sum_weights_210Bi_Qo_lowYield: " << sum_weights_210Bi_Qo_lowYield << std::endl;
        std::cout << "sum_weights_206Pb_Qo: " << sum_weights_206Pb_Qo << std::endl;

        // Relative fractions of the 210Pb Qinner S1 events
        double sum_weights_210Pb_QiS1 = data_MC_210Pb_QiS1_effcor.sumEntries();
        double sum_weights_210Bi_QiS1 = data_MC_210Bi_QiS1_effcor.sumEntries();
        double sum_weights_206Pb_QiS1 = data_MC_206Pb_QiS1_effcor.sumEntries();
        double sum_weights_Pb_QiS1 = sum_weights_210Pb_QiS1 + sum_weights_210Bi_QiS1 + sum_weights_206Pb_QiS1;
        RooRealVar* frac_210Pb_QiS1 = new RooRealVar(Form("frac_210Pb_QiS1_%d_%d", detnum[jdet], timenum[jdet]), "fraction of Pb QiS1 that is 210Pb", sum_weights_210Pb_QiS1 / sum_weights_Pb_QiS1);
        RooRealVar* frac_210Bi_QiS1 = new RooRealVar(Form("frac_210Bi_QiS1_%d_%d", detnum[jdet], timenum[jdet]), "fraction of Pb QiS1 that is 210Bi", sum_weights_210Bi_QiS1 / sum_weights_Pb_QiS1);
        RooRealVar* frac_206Pb_QiS1 = new RooRealVar(Form("frac_206Pb_QiS1_%d_%d", detnum[jdet], timenum[jdet]), "fraction of Pb QiS1 that is 206Pb", sum_weights_206Pb_QiS1 / sum_weights_Pb_QiS1);
        std::cout << "sum_weights_210Pb_QiS1: " << sum_weights_210Pb_QiS1 << std::endl;
        std::cout << "sum_weights_210Bi_QiS1: " << sum_weights_210Bi_QiS1 << std::endl;
        std::cout << "sum_weights_206Pb_QiS1: " << sum_weights_206Pb_QiS1 << std::endl;

        // Relative fractions of the 210Pb Qinner S2 events
        double sum_weights_210Pb_QiS2 = data_MC_210Pb_QiS2_effcor.sumEntries();
        double sum_weights_210Bi_QiS2 = data_MC_210Bi_QiS2_effcor.sumEntries();
        double sum_weights_206Pb_QiS2 = data_MC_206Pb_QiS2_effcor.sumEntries();
        double sum_weights_Pb_QiS2 = sum_weights_210Pb_QiS2 + sum_weights_210Bi_QiS2 + sum_weights_206Pb_QiS2;
        RooRealVar* frac_210Pb_QiS2 = new RooRealVar(Form("frac_210Pb_QiS2_%d_%d", detnum[jdet], timenum[jdet]), "fraction of Pb QiS2 that is 210Pb", sum_weights_210Pb_QiS2 / sum_weights_Pb_QiS2);
        RooRealVar* frac_210Bi_QiS2 = new RooRealVar(Form("frac_210Bi_QiS2_%d_%d", detnum[jdet], timenum[jdet]), "fraction of Pb QiS2 that is 210Bi", sum_weights_210Bi_QiS2 / sum_weights_Pb_QiS2);
        RooRealVar* frac_206Pb_QiS2 = new RooRealVar(Form("frac_206Pb_QiS2_%d_%d", detnum[jdet], timenum[jdet]), "fraction of Pb QiS2 that is 206Pb", sum_weights_206Pb_QiS2 / sum_weights_Pb_QiS2);


        // PDFs
        // gammas
        RooDataHist*     datahist_MC_gammas_pt       = new RooDataHist(Form("datahist_MC_gammas_pt_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of gamma MC", RooArgSet(pt), data_MC_gammas_effcor);
        RooDataHist*     datahist_MC_gammas_qimean   = new RooDataHist(Form("datahist_MC_gammas_qimean_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of gamma MC", RooArgSet(qimean), data_MC_gammas_effcor);
        RooDataHist*     datahist_MC_gammas_ptVqimean   = new RooDataHist(Form("datahist_MC_gammas_ptVqimean_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of gamma MC", RooArgSet(pt, qimean), data_MC_gammas_effcor);
        RooDataHist*     datahist_MC_gammas_prpart   = new RooDataHist(Form("datahist_MC_gammas_prpart_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of gamma MC", RooArgSet(prpart), data_MC_gammas_effcor);
        RooDataHist*     datahist_MC_gammas_pzpart   = new RooDataHist(Form("datahist_MC_gammas_pzpart_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of gamma MC", RooArgSet(pzpart), data_MC_gammas_effcor);
        RooDataHist*     datahist_MC_gammas_BDT5     = new RooDataHist(Form("datahist_MC_gammas_BDT5_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of gamma MC", RooArgSet(BDT5), data_MC_gammas_effcor);
        RooDataHist*     datahist_MC_gammas_BDT7     = new RooDataHist(Form("datahist_MC_gammas_BDT7_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of gamma MC", RooArgSet(BDT7), data_MC_gammas_effcor);
        RooDataHist*     datahist_MC_gammas_BDT10    = new RooDataHist(Form("datahist_MC_gammas_BDT10_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of gamma MC", RooArgSet(BDT10), data_MC_gammas_effcor);
        RooDataHist*     datahist_MC_gammas_BDT15    = new RooDataHist(Form("datahist_MC_gammas_BDT15_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of gamma MC", RooArgSet(BDT15), data_MC_gammas_effcor);
        RooHistPdf*      histpdf_MC_gammas_pt        = new RooHistPdf(Form("histpdf_MC_gammas_pt_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of gamma MC", RooArgSet(pt), *datahist_MC_gammas_pt, 0);
        RooHistPdf*      histpdf_MC_gammas_qimean    = new RooHistPdf(Form("histpdf_MC_gammas_qimean_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of gamma MC", RooArgSet(qimean), *datahist_MC_gammas_qimean, 0);
        RooHistPdf*      histpdf_MC_gammas_ptVqimean    = new RooHistPdf(Form("histpdf_MC_gammas_ptVqimean_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of gamma MC", RooArgSet(pt, qimean), *datahist_MC_gammas_ptVqimean, 0);
        RooHistPdf*      histpdf_MC_gammas_prpart    = new RooHistPdf(Form("histpdf_MC_gammas_prpart_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of gamma MC", RooArgSet(prpart), *datahist_MC_gammas_prpart, 0);
        RooHistPdf*      histpdf_MC_gammas_pzpart    = new RooHistPdf(Form("histpdf_MC_gammas_pzpart_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of gamma MC", RooArgSet(pzpart), *datahist_MC_gammas_pzpart, 0);
        RooHistPdf*      histpdf_MC_gammas_BDT5     = new RooHistPdf(Form("histpdf_MC_gammas_BDT5_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of gamma MC", RooArgSet(BDT5), *datahist_MC_gammas_BDT5, 0);
        RooHistPdf*      histpdf_MC_gammas_BDT7     = new RooHistPdf(Form("histpdf_MC_gammas_BDT7_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of gamma MC", RooArgSet(BDT7), *datahist_MC_gammas_BDT7, 0);
        RooHistPdf*      histpdf_MC_gammas_BDT10     = new RooHistPdf(Form("histpdf_MC_gammas_BDT10_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of gamma MC", RooArgSet(BDT10), *datahist_MC_gammas_BDT10, 0);
        RooHistPdf*      histpdf_MC_gammas_BDT15     = new RooHistPdf(Form("histpdf_MC_gammas_BDT15_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of gamma MC", RooArgSet(BDT15), *datahist_MC_gammas_BDT15, 0);

        // 1keV
        RooDataHist*     datahist_MC_1keV_pt         = new RooDataHist(Form("datahist_MC_1keV_pt_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 1keV MC", RooArgSet(pt), data_MC_1keV_effcor);
        RooDataHist*     datahist_MC_1keV_qimean     = new RooDataHist(Form("datahist_MC_1keV_qimean_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 1keV MC", RooArgSet(qimean), data_MC_1keV_effcor);
        RooDataHist*     datahist_MC_1keV_ptVqimean     = new RooDataHist(Form("datahist_MC_1keV_ptVqimean_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 1keV MC", RooArgSet(pt, qimean), data_MC_1keV_effcor);
        RooDataHist*     datahist_MC_1keV_prpart     = new RooDataHist(Form("datahist_MC_1keV_prpart_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 1keV MC", RooArgSet(prpart), data_MC_1keV_effcor);
        RooDataHist*     datahist_MC_1keV_pzpart     = new RooDataHist(Form("datahist_MC_1keV_pzpart_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 1keV MC", RooArgSet(pzpart), data_MC_1keV_effcor);
        RooDataHist*     datahist_MC_1keV_BDT5     = new RooDataHist(Form("datahist_MC_1keV_BDT5_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 1keV MC", RooArgSet(BDT5), data_MC_1keV_effcor);
        RooDataHist*     datahist_MC_1keV_BDT7     = new RooDataHist(Form("datahist_MC_1keV_BDT7_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 1keV MC", RooArgSet(BDT7), data_MC_1keV_effcor);
        RooDataHist*     datahist_MC_1keV_BDT10     = new RooDataHist(Form("datahist_MC_1keV_BDT10_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 1keV MC", RooArgSet(BDT10), data_MC_1keV_effcor);
        RooDataHist*     datahist_MC_1keV_BDT15     = new RooDataHist(Form("datahist_MC_1keV_BDT15_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 1keV MC", RooArgSet(BDT15), data_MC_1keV_effcor);
        RooHistPdf*      histpdf_MC_1keV_pt          = new RooHistPdf(Form("histpdf_MC_1keV_pt_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 1keV MC", RooArgSet(pt), *datahist_MC_1keV_pt, 0);
        RooHistPdf*      histpdf_MC_1keV_qimean      = new RooHistPdf(Form("histpdf_MC_1keV_qimean_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 1keV MC", RooArgSet(qimean), *datahist_MC_1keV_qimean, 0);
        RooHistPdf*      histpdf_MC_1keV_ptVqimean      = new RooHistPdf(Form("histpdf_MC_1keV_ptVqimean_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 1keV MC", RooArgSet(pt, qimean), *datahist_MC_1keV_ptVqimean, 0);
        RooHistPdf*      histpdf_MC_1keV_prpart      = new RooHistPdf(Form("histpdf_MC_1keV_prpart_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 1keV MC", RooArgSet(prpart), *datahist_MC_1keV_prpart, 0);
        RooHistPdf*      histpdf_MC_1keV_pzpart      = new RooHistPdf(Form("histpdf_MC_1keV_pzpart_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 1keV MC", RooArgSet(pzpart), *datahist_MC_1keV_pzpart, 0);
        RooHistPdf*      histpdf_MC_1keV_BDT5      = new RooHistPdf(Form("histpdf_MC_1keV_BDT5_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 1keV MC", RooArgSet(BDT5), *datahist_MC_1keV_BDT5, 0);
        RooHistPdf*      histpdf_MC_1keV_BDT7      = new RooHistPdf(Form("histpdf_MC_1keV_BDT7_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 1keV MC", RooArgSet(BDT7), *datahist_MC_1keV_BDT7, 0);
        RooHistPdf*      histpdf_MC_1keV_BDT10      = new RooHistPdf(Form("histpdf_MC_1keV_BDT10_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 1keV MC", RooArgSet(BDT10), *datahist_MC_1keV_BDT10, 0);
        RooHistPdf*      histpdf_MC_1keV_BDT15      = new RooHistPdf(Form("histpdf_MC_1keV_BDT15_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 1keV MC", RooArgSet(BDT15), *datahist_MC_1keV_BDT15, 0);

        // 210Pb Qouter
        RooDataHist*     datahist_MC_210Pb_Qo_pt         = new RooDataHist(Form("datahist_MC_210Pb_Qo_pt_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Pb_Qo MC", RooArgSet(pt), data_MC_210Pb_Qo_effcor);
        RooDataHist*     datahist_MC_210Pb_Qo_qimean     = new RooDataHist(Form("datahist_MC_210Pb_Qo_qimean_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Pb_Qo MC", RooArgSet(qimean), data_MC_210Pb_Qo_effcor);
        RooDataHist*     datahist_MC_210Pb_Qo_ptVqimean     = new RooDataHist(Form("datahist_MC_210Pb_Qo_ptVqimean_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Pb_Qo MC", RooArgSet(pt, qimean), data_MC_210Pb_Qo_effcor);
        RooDataHist*     datahist_MC_210Pb_Qo_prpart     = new RooDataHist(Form("datahist_MC_210Pb_Qo_prpart_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Pb_Qo MC", RooArgSet(prpart), data_MC_210Pb_Qo_effcor);
        RooDataHist*     datahist_MC_210Pb_Qo_pzpart     = new RooDataHist(Form("datahist_MC_210Pb_Qo_pzpart_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Pb_Qo MC", RooArgSet(pzpart), data_MC_210Pb_Qo_effcor);
        RooDataHist*     datahist_MC_210Pb_Qo_BDT5     = new RooDataHist(Form("datahist_MC_210Pb_Qo_BDT5_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Pb_Qo MC", RooArgSet(BDT5), data_MC_210Pb_Qo_effcor);
        RooDataHist*     datahist_MC_210Pb_Qo_BDT7     = new RooDataHist(Form("datahist_MC_210Pb_Qo_BDT7_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Pb_Qo MC", RooArgSet(BDT7), data_MC_210Pb_Qo_effcor);
        RooDataHist*     datahist_MC_210Pb_Qo_BDT10     = new RooDataHist(Form("datahist_MC_210Pb_Qo_BDT10_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Pb_Qo MC", RooArgSet(BDT10), data_MC_210Pb_Qo_effcor);
        RooDataHist*     datahist_MC_210Pb_Qo_BDT15     = new RooDataHist(Form("datahist_MC_210Pb_Qo_BDT15_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Pb_Qo MC", RooArgSet(BDT15), data_MC_210Pb_Qo_effcor);
        RooHistPdf*      histpdf_MC_210Pb_Qo_pt          = new RooHistPdf(Form("histpdf_MC_210Pb_Qo_pt_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Pb_Qo MC", RooArgSet(pt), *datahist_MC_210Pb_Qo_pt, 0);
        RooHistPdf*      histpdf_MC_210Pb_Qo_qimean      = new RooHistPdf(Form("histpdf_MC_210Pb_Qo_qimean_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Pb_Qo MC", RooArgSet(qimean), *datahist_MC_210Pb_Qo_qimean, 0);
        RooHistPdf*      histpdf_MC_210Pb_Qo_ptVqimean      = new RooHistPdf(Form("histpdf_MC_210Pb_Qo_ptVqimean_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Pb_Qo MC", RooArgSet(pt, qimean), *datahist_MC_210Pb_Qo_ptVqimean, 0);
        RooHistPdf*      histpdf_MC_210Pb_Qo_prpart      = new RooHistPdf(Form("histpdf_MC_210Pb_Qo_prpart_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Pb_Qo MC", RooArgSet(prpart), *datahist_MC_210Pb_Qo_prpart, 0);
        RooHistPdf*      histpdf_MC_210Pb_Qo_pzpart      = new RooHistPdf(Form("histpdf_MC_210Pb_Qo_pzpart_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Pb_Qo MC", RooArgSet(pzpart), *datahist_MC_210Pb_Qo_pzpart, 0);
        RooHistPdf*      histpdf_MC_210Pb_Qo_BDT5      = new RooHistPdf(Form("histpdf_MC_210Pb_Qo_BDT5_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Pb_Qo MC", RooArgSet(BDT5), *datahist_MC_210Pb_Qo_BDT5, 0);
        RooHistPdf*      histpdf_MC_210Pb_Qo_BDT7      = new RooHistPdf(Form("histpdf_MC_210Pb_Qo_BDT7_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Pb_Qo MC", RooArgSet(BDT7), *datahist_MC_210Pb_Qo_BDT7, 0);
        RooHistPdf*      histpdf_MC_210Pb_Qo_BDT10      = new RooHistPdf(Form("histpdf_MC_210Pb_Qo_BDT10_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Pb_Qo MC", RooArgSet(BDT10), *datahist_MC_210Pb_Qo_BDT10, 0);
        RooHistPdf*      histpdf_MC_210Pb_Qo_BDT15      = new RooHistPdf(Form("histpdf_MC_210Pb_Qo_BDT15_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Pb_Qo MC", RooArgSet(BDT15), *datahist_MC_210Pb_Qo_BDT15, 0);

        RooDataHist*     datahist_MC_210Pb_Qo_lowYield_pt         = new RooDataHist(Form("datahist_MC_210Pb_Qo_lowYield_pt_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Pb_Qo_lowYield MC", RooArgSet(pt), data_MC_210Pb_Qo_lowYield_effcor);
        RooDataHist*     datahist_MC_210Pb_Qo_lowYield_qimean     = new RooDataHist(Form("datahist_MC_210Pb_Qo_lowYield_qimean_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Pb_Qo_lowYield MC", RooArgSet(qimean), data_MC_210Pb_Qo_lowYield_effcor);
        RooDataHist*     datahist_MC_210Pb_Qo_lowYield_ptVqimean     = new RooDataHist(Form("datahist_MC_210Pb_Qo_lowYield_ptVqimean_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Pb_Qo_lowYield MC", RooArgSet(pt, qimean), data_MC_210Pb_Qo_lowYield_effcor);
        RooDataHist*     datahist_MC_210Pb_Qo_lowYield_prpart     = new RooDataHist(Form("datahist_MC_210Pb_Qo_lowYield_prpart_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Pb_Qo_lowYield MC", RooArgSet(prpart), data_MC_210Pb_Qo_lowYield_effcor);
        RooDataHist*     datahist_MC_210Pb_Qo_lowYield_pzpart     = new RooDataHist(Form("datahist_MC_210Pb_Qo_lowYield_pzpart_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Pb_Qo_lowYield MC", RooArgSet(pzpart), data_MC_210Pb_Qo_lowYield_effcor);
        RooDataHist*     datahist_MC_210Pb_Qo_lowYield_BDT5     = new RooDataHist(Form("datahist_MC_210Pb_Qo_lowYield_BDT5_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Pb_Qo_lowYield MC", RooArgSet(BDT5), data_MC_210Pb_Qo_lowYield_effcor);
        RooDataHist*     datahist_MC_210Pb_Qo_lowYield_BDT7     = new RooDataHist(Form("datahist_MC_210Pb_Qo_lowYield_BDT7_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Pb_Qo_lowYield MC", RooArgSet(BDT7), data_MC_210Pb_Qo_lowYield_effcor);
        RooDataHist*     datahist_MC_210Pb_Qo_lowYield_BDT10     = new RooDataHist(Form("datahist_MC_210Pb_Qo_lowYield_BDT10_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Pb_Qo_lowYield MC", RooArgSet(BDT10), data_MC_210Pb_Qo_lowYield_effcor);
        RooDataHist*     datahist_MC_210Pb_Qo_lowYield_BDT15     = new RooDataHist(Form("datahist_MC_210Pb_Qo_lowYield_BDT15_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Pb_Qo_lowYield MC", RooArgSet(BDT15), data_MC_210Pb_Qo_lowYield_effcor);
        RooHistPdf*      histpdf_MC_210Pb_Qo_lowYield_pt          = new RooHistPdf(Form("histpdf_MC_210Pb_Qo_lowYield_pt_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Pb_Qo_lowYield MC", RooArgSet(pt), *datahist_MC_210Pb_Qo_lowYield_pt, 0);
        RooHistPdf*      histpdf_MC_210Pb_Qo_lowYield_qimean      = new RooHistPdf(Form("histpdf_MC_210Pb_Qo_lowYield_qimean_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Pb_Qo_lowYield MC", RooArgSet(qimean), *datahist_MC_210Pb_Qo_lowYield_qimean, 0);
        RooHistPdf*      histpdf_MC_210Pb_Qo_lowYield_ptVqimean      = new RooHistPdf(Form("histpdf_MC_210Pb_Qo_lowYield_ptVqimean_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Pb_Qo_lowYield MC", RooArgSet(pt, qimean), *datahist_MC_210Pb_Qo_lowYield_ptVqimean, 0);
        RooHistPdf*      histpdf_MC_210Pb_Qo_lowYield_prpart      = new RooHistPdf(Form("histpdf_MC_210Pb_Qo_lowYield_prpart_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Pb_Qo_lowYield MC", RooArgSet(prpart), *datahist_MC_210Pb_Qo_lowYield_prpart, 0);
        RooHistPdf*      histpdf_MC_210Pb_Qo_lowYield_pzpart      = new RooHistPdf(Form("histpdf_MC_210Pb_Qo_lowYield_pzpart_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Pb_Qo_lowYield MC", RooArgSet(pzpart), *datahist_MC_210Pb_Qo_lowYield_pzpart, 0);
        RooHistPdf*      histpdf_MC_210Pb_Qo_lowYield_BDT5      = new RooHistPdf(Form("histpdf_MC_210Pb_Qo_lowYield_BDT5_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Pb_Qo_lowYield MC", RooArgSet(BDT5), *datahist_MC_210Pb_Qo_lowYield_BDT5, 0);
        RooHistPdf*      histpdf_MC_210Pb_Qo_lowYield_BDT7      = new RooHistPdf(Form("histpdf_MC_210Pb_Qo_lowYield_BDT7_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Pb_Qo_lowYield MC", RooArgSet(BDT7), *datahist_MC_210Pb_Qo_lowYield_BDT7, 0);
        RooHistPdf*      histpdf_MC_210Pb_Qo_lowYield_BDT10      = new RooHistPdf(Form("histpdf_MC_210Pb_Qo_lowYield_BDT10_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Pb_Qo_lowYield MC", RooArgSet(BDT10), *datahist_MC_210Pb_Qo_lowYield_BDT10, 0);
        RooHistPdf*      histpdf_MC_210Pb_Qo_lowYield_BDT15      = new RooHistPdf(Form("histpdf_MC_210Pb_Qo_lowYield_BDT15_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Pb_Qo_lowYield MC", RooArgSet(BDT15), *datahist_MC_210Pb_Qo_lowYield_BDT15, 0);

        RooDataHist*     datahist_MC_210Bi_Qo_pt         = new RooDataHist(Form("datahist_MC_210Bi_Qo_pt_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Bi_Qo MC", RooArgSet(pt), data_MC_210Bi_Qo_effcor);
        RooDataHist*     datahist_MC_210Bi_Qo_qimean     = new RooDataHist(Form("datahist_MC_210Bi_Qo_qimean_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Bi_Qo MC", RooArgSet(qimean), data_MC_210Bi_Qo_effcor);
        RooDataHist*     datahist_MC_210Bi_Qo_ptVqimean     = new RooDataHist(Form("datahist_MC_210Bi_Qo_ptVqimean_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Bi_Qo MC", RooArgSet(pt, qimean), data_MC_210Bi_Qo_effcor);
        RooDataHist*     datahist_MC_210Bi_Qo_prpart     = new RooDataHist(Form("datahist_MC_210Bi_Qo_prpart_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Bi_Qo MC", RooArgSet(prpart), data_MC_210Bi_Qo_effcor);
        RooDataHist*     datahist_MC_210Bi_Qo_pzpart     = new RooDataHist(Form("datahist_MC_210Bi_Qo_pzpart_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Bi_Qo MC", RooArgSet(pzpart), data_MC_210Bi_Qo_effcor);
        RooDataHist*     datahist_MC_210Bi_Qo_BDT5     = new RooDataHist(Form("datahist_MC_210Bi_Qo_BDT5_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Bi_Qo MC", RooArgSet(BDT5), data_MC_210Bi_Qo_effcor);
        RooDataHist*     datahist_MC_210Bi_Qo_BDT7     = new RooDataHist(Form("datahist_MC_210Bi_Qo_BDT7_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Bi_Qo MC", RooArgSet(BDT7), data_MC_210Bi_Qo_effcor);
        RooDataHist*     datahist_MC_210Bi_Qo_BDT10     = new RooDataHist(Form("datahist_MC_210Bi_Qo_BDT10_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Bi_Qo MC", RooArgSet(BDT10), data_MC_210Bi_Qo_effcor);
        RooDataHist*     datahist_MC_210Bi_Qo_BDT15     = new RooDataHist(Form("datahist_MC_210Bi_Qo_BDT15_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Bi_Qo MC", RooArgSet(BDT15), data_MC_210Bi_Qo_effcor);
        datahist_MC_210Bi_Qo_ptVqimean->Print(); //[AJA]
        RooHistPdf*      histpdf_MC_210Bi_Qo_pt          = new RooHistPdf(Form("histpdf_MC_210Bi_Qo_pt_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Bi_Qo MC", RooArgSet(pt), *datahist_MC_210Bi_Qo_pt, 0);
        RooHistPdf*      histpdf_MC_210Bi_Qo_qimean      = new RooHistPdf(Form("histpdf_MC_210Bi_Qo_qimean_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Bi_Qo MC", RooArgSet(qimean), *datahist_MC_210Bi_Qo_qimean, 0);
        RooHistPdf*      histpdf_MC_210Bi_Qo_ptVqimean      = new RooHistPdf(Form("histpdf_MC_210Bi_Qo_ptVqimean_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Bi_Qo MC", RooArgSet(pt, qimean), *datahist_MC_210Bi_Qo_ptVqimean, 0);
        RooHistPdf*      histpdf_MC_210Bi_Qo_prpart      = new RooHistPdf(Form("histpdf_MC_210Bi_Qo_prpart_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Bi_Qo MC", RooArgSet(prpart), *datahist_MC_210Bi_Qo_prpart, 0);
        RooHistPdf*      histpdf_MC_210Bi_Qo_pzpart      = new RooHistPdf(Form("histpdf_MC_210Bi_Qo_pzpart_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Bi_Qo MC", RooArgSet(pzpart), *datahist_MC_210Bi_Qo_pzpart, 0);
        RooHistPdf*      histpdf_MC_210Bi_Qo_BDT5      = new RooHistPdf(Form("histpdf_MC_210Bi_Qo_BDT5_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Bi_Qo MC", RooArgSet(BDT5), *datahist_MC_210Bi_Qo_BDT5, 0);
        RooHistPdf*      histpdf_MC_210Bi_Qo_BDT7      = new RooHistPdf(Form("histpdf_MC_210Bi_Qo_BDT7_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Bi_Qo MC", RooArgSet(BDT7), *datahist_MC_210Bi_Qo_BDT7, 0);
        RooHistPdf*      histpdf_MC_210Bi_Qo_BDT10      = new RooHistPdf(Form("histpdf_MC_210Bi_Qo_BDT10_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Bi_Qo MC", RooArgSet(BDT10), *datahist_MC_210Bi_Qo_BDT10, 0);
        RooHistPdf*      histpdf_MC_210Bi_Qo_BDT15      = new RooHistPdf(Form("histpdf_MC_210Bi_Qo_BDT15_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Bi_Qo MC", RooArgSet(BDT15), *datahist_MC_210Bi_Qo_BDT15, 0);

        RooDataHist*     datahist_MC_210Bi_Qo_lowYield_pt         = new RooDataHist(Form("datahist_MC_210Bi_Qo_lowYield_pt_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Bi_Qo_lowYield MC", RooArgSet(pt), data_MC_210Bi_Qo_lowYield_effcor);
        RooDataHist*     datahist_MC_210Bi_Qo_lowYield_qimean     = new RooDataHist(Form("datahist_MC_210Bi_Qo_lowYield_qimean_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Bi_Qo_lowYield MC", RooArgSet(qimean), data_MC_210Bi_Qo_lowYield_effcor);
        RooDataHist*     datahist_MC_210Bi_Qo_lowYield_ptVqimean     = new RooDataHist(Form("datahist_MC_210Bi_Qo_lowYield_ptVqimean_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Bi_Qo_lowYield MC", RooArgSet(pt, qimean), data_MC_210Bi_Qo_lowYield_effcor);
        RooDataHist*     datahist_MC_210Bi_Qo_lowYield_prpart     = new RooDataHist(Form("datahist_MC_210Bi_Qo_lowYield_prpart_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Bi_Qo_lowYield MC", RooArgSet(prpart), data_MC_210Bi_Qo_lowYield_effcor);
        RooDataHist*     datahist_MC_210Bi_Qo_lowYield_pzpart     = new RooDataHist(Form("datahist_MC_210Bi_Qo_lowYield_pzpart_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Bi_Qo_lowYield MC", RooArgSet(pzpart), data_MC_210Bi_Qo_lowYield_effcor);
        RooDataHist*     datahist_MC_210Bi_Qo_lowYield_BDT5     = new RooDataHist(Form("datahist_MC_210Bi_Qo_lowYield_BDT5_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Bi_Qo_lowYield MC", RooArgSet(BDT5), data_MC_210Bi_Qo_lowYield_effcor);
        RooDataHist*     datahist_MC_210Bi_Qo_lowYield_BDT7     = new RooDataHist(Form("datahist_MC_210Bi_Qo_lowYield_BDT7_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Bi_Qo_lowYield MC", RooArgSet(BDT7), data_MC_210Bi_Qo_lowYield_effcor);
        RooDataHist*     datahist_MC_210Bi_Qo_lowYield_BDT10     = new RooDataHist(Form("datahist_MC_210Bi_Qo_lowYield_BDT10_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Bi_Qo_lowYield MC", RooArgSet(BDT10), data_MC_210Bi_Qo_lowYield_effcor);
        RooDataHist*     datahist_MC_210Bi_Qo_lowYield_BDT15     = new RooDataHist(Form("datahist_MC_210Bi_Qo_lowYield_BDT15_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Bi_Qo_lowYield MC", RooArgSet(BDT15), data_MC_210Bi_Qo_lowYield_effcor);
        datahist_MC_210Bi_Qo_lowYield_ptVqimean->Print(); //[AJA]
        RooHistPdf*      histpdf_MC_210Bi_Qo_lowYield_pt          = new RooHistPdf(Form("histpdf_MC_210Bi_Qo_lowYield_pt_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Bi_Qo_lowYield MC", RooArgSet(pt), *datahist_MC_210Bi_Qo_lowYield_pt, 0);
        RooHistPdf*      histpdf_MC_210Bi_Qo_lowYield_qimean      = new RooHistPdf(Form("histpdf_MC_210Bi_Qo_lowYield_qimean_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Bi_Qo_lowYield MC", RooArgSet(qimean), *datahist_MC_210Bi_Qo_lowYield_qimean, 0);
        RooHistPdf*      histpdf_MC_210Bi_Qo_lowYield_ptVqimean      = new RooHistPdf(Form("histpdf_MC_210Bi_Qo_lowYield_ptVqimean_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Bi_Qo_lowYield MC", RooArgSet(pt, qimean), *datahist_MC_210Bi_Qo_lowYield_ptVqimean, 0);
        RooHistPdf*      histpdf_MC_210Bi_Qo_lowYield_prpart      = new RooHistPdf(Form("histpdf_MC_210Bi_Qo_lowYield_prpart_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Bi_Qo_lowYield MC", RooArgSet(prpart), *datahist_MC_210Bi_Qo_lowYield_prpart, 0);
        RooHistPdf*      histpdf_MC_210Bi_Qo_lowYield_pzpart      = new RooHistPdf(Form("histpdf_MC_210Bi_Qo_lowYield_pzpart_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Bi_Qo_lowYield MC", RooArgSet(pzpart), *datahist_MC_210Bi_Qo_lowYield_pzpart, 0);
        RooHistPdf*      histpdf_MC_210Bi_Qo_lowYield_BDT5      = new RooHistPdf(Form("histpdf_MC_210Bi_Qo_lowYield_BDT5_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Bi_Qo_lowYield MC", RooArgSet(BDT5), *datahist_MC_210Bi_Qo_lowYield_BDT5, 0);
        RooHistPdf*      histpdf_MC_210Bi_Qo_lowYield_BDT7      = new RooHistPdf(Form("histpdf_MC_210Bi_Qo_lowYield_BDT7_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Bi_Qo_lowYield MC", RooArgSet(BDT7), *datahist_MC_210Bi_Qo_lowYield_BDT7, 0);
        RooHistPdf*      histpdf_MC_210Bi_Qo_lowYield_BDT10      = new RooHistPdf(Form("histpdf_MC_210Bi_Qo_lowYield_BDT10_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Bi_Qo_lowYield MC", RooArgSet(BDT10), *datahist_MC_210Bi_Qo_lowYield_BDT10, 0);
        RooHistPdf*      histpdf_MC_210Bi_Qo_lowYield_BDT15      = new RooHistPdf(Form("histpdf_MC_210Bi_Qo_lowYield_BDT15_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Bi_Qo_lowYield MC", RooArgSet(BDT15), *datahist_MC_210Bi_Qo_lowYield_BDT15, 0);

        RooDataHist*     datahist_MC_206Pb_Qo_pt         = new RooDataHist(Form("datahist_MC_206Pb_Qo_pt_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 206Pb_Qo MC", RooArgSet(pt), data_MC_206Pb_Qo_effcor);
        RooDataHist*     datahist_MC_206Pb_Qo_qimean     = new RooDataHist(Form("datahist_MC_206Pb_Qo_qimean_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 206Pb_Qo MC", RooArgSet(qimean), data_MC_206Pb_Qo_effcor);
        RooDataHist*     datahist_MC_206Pb_Qo_ptVqimean     = new RooDataHist(Form("datahist_MC_206Pb_Qo_ptVqimean_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 206Pb_Qo MC", RooArgSet(pt, qimean), data_MC_206Pb_Qo_effcor);
        RooDataHist*     datahist_MC_206Pb_Qo_prpart     = new RooDataHist(Form("datahist_MC_206Pb_Qo_prpart_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 206Pb_Qo MC", RooArgSet(prpart), data_MC_206Pb_Qo_effcor);
        RooDataHist*     datahist_MC_206Pb_Qo_pzpart     = new RooDataHist(Form("datahist_MC_206Pb_Qo_pzpart_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 206Pb_Qo MC", RooArgSet(pzpart), data_MC_206Pb_Qo_effcor);
        RooDataHist*     datahist_MC_206Pb_Qo_BDT5     = new RooDataHist(Form("datahist_MC_206Pb_Qo_BDT5_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 206Pb_Qo MC", RooArgSet(BDT5), data_MC_206Pb_Qo_effcor);
        RooDataHist*     datahist_MC_206Pb_Qo_BDT7     = new RooDataHist(Form("datahist_MC_206Pb_Qo_BDT7_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 206Pb_Qo MC", RooArgSet(BDT7), data_MC_206Pb_Qo_effcor);
        RooDataHist*     datahist_MC_206Pb_Qo_BDT10     = new RooDataHist(Form("datahist_MC_206Pb_Qo_BDT10_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 206Pb_Qo MC", RooArgSet(BDT10), data_MC_206Pb_Qo_effcor);
        RooDataHist*     datahist_MC_206Pb_Qo_BDT15     = new RooDataHist(Form("datahist_MC_206Pb_Qo_BDT15_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 206Pb_Qo MC", RooArgSet(BDT15), data_MC_206Pb_Qo_effcor);
        RooHistPdf*      histpdf_MC_206Pb_Qo_pt          = new RooHistPdf(Form("histpdf_MC_206Pb_Qo_pt_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 206Pb_Qo MC", RooArgSet(pt), *datahist_MC_206Pb_Qo_pt, 0);
        RooHistPdf*      histpdf_MC_206Pb_Qo_qimean      = new RooHistPdf(Form("histpdf_MC_206Pb_Qo_qimean_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 206Pb_Qo MC", RooArgSet(qimean), *datahist_MC_206Pb_Qo_qimean, 0);
        RooHistPdf*      histpdf_MC_206Pb_Qo_ptVqimean      = new RooHistPdf(Form("histpdf_MC_206Pb_Qo_ptVqimean_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 206Pb_Qo MC", RooArgSet(pt, qimean), *datahist_MC_206Pb_Qo_ptVqimean, 0);
        RooHistPdf*      histpdf_MC_206Pb_Qo_prpart      = new RooHistPdf(Form("histpdf_MC_206Pb_Qo_prpart_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 206Pb_Qo MC", RooArgSet(prpart), *datahist_MC_206Pb_Qo_prpart, 0);
        RooHistPdf*      histpdf_MC_206Pb_Qo_pzpart      = new RooHistPdf(Form("histpdf_MC_206Pb_Qo_pzpart_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 206Pb_Qo MC", RooArgSet(pzpart), *datahist_MC_206Pb_Qo_pzpart, 0);
        RooHistPdf*      histpdf_MC_206Pb_Qo_BDT5      = new RooHistPdf(Form("histpdf_MC_206Pb_Qo_BDT5_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 206Pb_Qo MC", RooArgSet(BDT5), *datahist_MC_206Pb_Qo_BDT5, 0);
        RooHistPdf*      histpdf_MC_206Pb_Qo_BDT7      = new RooHistPdf(Form("histpdf_MC_206Pb_Qo_BDT7_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 206Pb_Qo MC", RooArgSet(BDT7), *datahist_MC_206Pb_Qo_BDT7, 0);
        RooHistPdf*      histpdf_MC_206Pb_Qo_BDT10      = new RooHistPdf(Form("histpdf_MC_206Pb_Qo_BDT10_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 206Pb_Qo MC", RooArgSet(BDT10), *datahist_MC_206Pb_Qo_BDT10, 0);
        RooHistPdf*      histpdf_MC_206Pb_Qo_BDT15      = new RooHistPdf(Form("histpdf_MC_206Pb_Qo_BDT15_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 206Pb_Qo MC", RooArgSet(BDT15), *datahist_MC_206Pb_Qo_BDT15, 0);

        // morphing PDF
        RooMomentMorph*  pdf_MC_210Bi_Qo_ptVqimean  = new RooMomentMorph(Form("pdf_MC_210Bi_Qo_ptVqimean_%d_%d", detnum[jdet], timenum[jdet]), "pdf of 210Bi Qo MC", *alpha_Qo_syst, 
                                                        RooArgList(pt, qimean), RooArgList(*histpdf_MC_210Bi_Qo_ptVqimean, *histpdf_MC_210Bi_Qo_lowYield_ptVqimean), shapeParamPoints, RooMomentMorph::Linear);
        pdf_MC_210Bi_Qo_ptVqimean->useHorizontalMorphing(false);
        RooMomentMorph*  pdf_MC_210Pb_Qo_ptVqimean  = new RooMomentMorph(Form("pdf_MC_210Pb_Qo_ptVqimean_%d_%d", detnum[jdet], timenum[jdet]), "pdf of 210Pb Qo MC", *alpha_Qo_syst, 
                                                        RooArgList(pt, qimean), RooArgList(*histpdf_MC_210Pb_Qo_ptVqimean, *histpdf_MC_210Pb_Qo_lowYield_ptVqimean), shapeParamPoints, RooMomentMorph::Linear);
        pdf_MC_210Pb_Qo_ptVqimean->useHorizontalMorphing(false);
        RooMomentMorph*  pdf_MC_210Bi_Qo_pt         = new RooMomentMorph(Form("pdf_MC_210Bi_Qo_pt_%d_%d", detnum[jdet], timenum[jdet]), "pdf of 210Bi Qo MC", *alpha_Qo_syst, 
                                                        RooArgList(pt), RooArgList(*histpdf_MC_210Bi_Qo_pt, *histpdf_MC_210Bi_Qo_lowYield_pt), shapeParamPoints, RooMomentMorph::Linear);
        pdf_MC_210Bi_Qo_pt->useHorizontalMorphing(false);
        RooMomentMorph*  pdf_MC_210Pb_Qo_pt         = new RooMomentMorph(Form("pdf_MC_210Pb_Qo_pt_%d_%d", detnum[jdet], timenum[jdet]), "pdf of 210Pb Qo MC", *alpha_Qo_syst, 
                                                        RooArgList(pt), RooArgList(*histpdf_MC_210Pb_Qo_pt, *histpdf_MC_210Pb_Qo_lowYield_pt), shapeParamPoints, RooMomentMorph::Linear);
        pdf_MC_210Pb_Qo_pt->useHorizontalMorphing(false);
        RooMomentMorph*  pdf_MC_210Bi_Qo_qimean     = new RooMomentMorph(Form("pdf_MC_210Bi_Qo_qimean_%d_%d", detnum[jdet], timenum[jdet]), "pdf of 210Bi Qo MC", *alpha_Qo_syst, 
                                                        RooArgList(qimean), RooArgList(*histpdf_MC_210Bi_Qo_qimean, *histpdf_MC_210Bi_Qo_lowYield_qimean), shapeParamPoints, RooMomentMorph::Linear);
        pdf_MC_210Bi_Qo_qimean->useHorizontalMorphing(false);
        RooMomentMorph*  pdf_MC_210Pb_Qo_qimean     = new RooMomentMorph(Form("pdf_MC_210Pb_Qo_qimean_%d_%d", detnum[jdet], timenum[jdet]), "pdf of 210Pb Qo MC", *alpha_Qo_syst, 
                                                        RooArgList(qimean), RooArgList(*histpdf_MC_210Pb_Qo_qimean, *histpdf_MC_210Pb_Qo_lowYield_qimean), shapeParamPoints, RooMomentMorph::Linear);
        pdf_MC_210Pb_Qo_qimean->useHorizontalMorphing(false);
        RooMomentMorph*  pdf_MC_210Bi_Qo_prpart     = new RooMomentMorph(Form("pdf_MC_210Bi_Qo_prpart_%d_%d", detnum[jdet], timenum[jdet]), "pdf of 210Bi Qo MC", *alpha_Qo_syst, 
                                                        RooArgList(prpart), RooArgList(*histpdf_MC_210Bi_Qo_prpart, *histpdf_MC_210Bi_Qo_lowYield_prpart), shapeParamPoints, RooMomentMorph::Linear);
        pdf_MC_210Bi_Qo_prpart->useHorizontalMorphing(false);
        RooMomentMorph*  pdf_MC_210Pb_Qo_prpart     = new RooMomentMorph(Form("pdf_MC_210Pb_Qo_prpart_%d_%d", detnum[jdet], timenum[jdet]), "pdf of 210Pb Qo MC", *alpha_Qo_syst, 
                                                        RooArgList(prpart), RooArgList(*histpdf_MC_210Pb_Qo_prpart, *histpdf_MC_210Pb_Qo_lowYield_prpart), shapeParamPoints, RooMomentMorph::Linear);
        pdf_MC_210Pb_Qo_prpart->useHorizontalMorphing(false);
        RooMomentMorph*  pdf_MC_210Bi_Qo_pzpart     = new RooMomentMorph(Form("pdf_MC_210Bi_Qo_pzpart_%d_%d", detnum[jdet], timenum[jdet]), "pdf of 210Bi Qo MC", *alpha_Qo_syst, 
                                                        RooArgList(pzpart), RooArgList(*histpdf_MC_210Bi_Qo_pzpart, *histpdf_MC_210Bi_Qo_lowYield_pzpart), shapeParamPoints, RooMomentMorph::Linear);
        pdf_MC_210Bi_Qo_pzpart->useHorizontalMorphing(false);
        RooMomentMorph*  pdf_MC_210Pb_Qo_pzpart     = new RooMomentMorph(Form("pdf_MC_210Pb_Qo_pzpart_%d_%d", detnum[jdet], timenum[jdet]), "pdf of 210Pb Qo MC", *alpha_Qo_syst, 
                                                        RooArgList(pzpart), RooArgList(*histpdf_MC_210Pb_Qo_pzpart, *histpdf_MC_210Pb_Qo_lowYield_pzpart), shapeParamPoints, RooMomentMorph::Linear);
        pdf_MC_210Pb_Qo_pzpart->useHorizontalMorphing(false);
        RooMomentMorph*  pdf_MC_210Bi_Qo_BDT5       = new RooMomentMorph(Form("pdf_MC_210Bi_Qo_BDT5_%d_%d", detnum[jdet], timenum[jdet]), "pdf of 210Bi Qo MC", *alpha_Qo_syst, 
                                                        RooArgList(BDT5), RooArgList(*histpdf_MC_210Bi_Qo_BDT5, *histpdf_MC_210Bi_Qo_lowYield_BDT5), shapeParamPoints, RooMomentMorph::Linear);
        pdf_MC_210Bi_Qo_BDT5->useHorizontalMorphing(false);
        RooMomentMorph*  pdf_MC_210Pb_Qo_BDT5       = new RooMomentMorph(Form("pdf_MC_210Pb_Qo_BDT5_%d_%d", detnum[jdet], timenum[jdet]), "pdf of 210Pb Qo MC", *alpha_Qo_syst, 
                                                        RooArgList(BDT5), RooArgList(*histpdf_MC_210Pb_Qo_BDT5, *histpdf_MC_210Pb_Qo_lowYield_BDT5), shapeParamPoints, RooMomentMorph::Linear);
        pdf_MC_210Pb_Qo_BDT5->useHorizontalMorphing(false);
        RooMomentMorph*  pdf_MC_210Bi_Qo_BDT7       = new RooMomentMorph(Form("pdf_MC_210Bi_Qo_BDT7_%d_%d", detnum[jdet], timenum[jdet]), "pdf of 210Bi Qo MC", *alpha_Qo_syst, 
                                                        RooArgList(BDT7), RooArgList(*histpdf_MC_210Bi_Qo_BDT7, *histpdf_MC_210Bi_Qo_lowYield_BDT7), shapeParamPoints, RooMomentMorph::Linear);
        pdf_MC_210Bi_Qo_BDT7->useHorizontalMorphing(false);
        RooMomentMorph*  pdf_MC_210Pb_Qo_BDT7       = new RooMomentMorph(Form("pdf_MC_210Pb_Qo_BDT7_%d_%d", detnum[jdet], timenum[jdet]), "pdf of 210Pb Qo MC", *alpha_Qo_syst, 
                                                        RooArgList(BDT7), RooArgList(*histpdf_MC_210Pb_Qo_BDT7, *histpdf_MC_210Pb_Qo_lowYield_BDT7), shapeParamPoints, RooMomentMorph::Linear);
        pdf_MC_210Pb_Qo_BDT7->useHorizontalMorphing(false);
        RooMomentMorph*  pdf_MC_210Bi_Qo_BDT10      = new RooMomentMorph(Form("pdf_MC_210Bi_Qo_BDT10_%d_%d", detnum[jdet], timenum[jdet]), "pdf of 210Bi Qo MC", *alpha_Qo_syst, 
                                                        RooArgList(BDT10), RooArgList(*histpdf_MC_210Bi_Qo_BDT10, *histpdf_MC_210Bi_Qo_lowYield_BDT10), shapeParamPoints, RooMomentMorph::Linear);
        pdf_MC_210Bi_Qo_BDT10->useHorizontalMorphing(false);
        RooMomentMorph*  pdf_MC_210Pb_Qo_BDT10      = new RooMomentMorph(Form("pdf_MC_210Pb_Qo_BDT10_%d_%d", detnum[jdet], timenum[jdet]), "pdf of 210Pb Qo MC", *alpha_Qo_syst, 
                                                        RooArgList(BDT10), RooArgList(*histpdf_MC_210Pb_Qo_BDT10, *histpdf_MC_210Pb_Qo_lowYield_BDT10), shapeParamPoints, RooMomentMorph::Linear);
        pdf_MC_210Pb_Qo_BDT10->useHorizontalMorphing(false);
        RooMomentMorph*  pdf_MC_210Bi_Qo_BDT15      = new RooMomentMorph(Form("pdf_MC_210Bi_Qo_BDT15_%d_%d", detnum[jdet], timenum[jdet]), "pdf of 210Bi Qo MC", *alpha_Qo_syst, 
                                                        RooArgList(BDT15), RooArgList(*histpdf_MC_210Bi_Qo_BDT15, *histpdf_MC_210Bi_Qo_lowYield_BDT15), shapeParamPoints, RooMomentMorph::Linear);
        pdf_MC_210Bi_Qo_BDT15->useHorizontalMorphing(false);
        RooMomentMorph*  pdf_MC_210Pb_Qo_BDT15      = new RooMomentMorph(Form("pdf_MC_210Pb_Qo_BDT15_%d_%d", detnum[jdet], timenum[jdet]), "pdf of 210Pb Qo MC", *alpha_Qo_syst, 
                                                        RooArgList(BDT15), RooArgList(*histpdf_MC_210Pb_Qo_BDT15, *histpdf_MC_210Pb_Qo_lowYield_BDT15), shapeParamPoints, RooMomentMorph::Linear);
        pdf_MC_210Pb_Qo_BDT15->useHorizontalMorphing(false);


        RooAddPdf*       pdf_MC_Pb_Qo_pt        = new RooAddPdf(Form("pdf_MC_Pb_Qo_pt_%d_%d", detnum[jdet], timenum[jdet]), "sum of all 210Pb components in pt", 
                                                        RooArgList(*pdf_MC_210Pb_Qo_pt, *pdf_MC_210Bi_Qo_pt, *histpdf_MC_206Pb_Qo_pt), 
                                                        RooArgList(*frac_210Pb_Qo, *frac_210Bi_Qo));
        RooAddPdf*       pdf_MC_Pb_Qo_qimean     = new RooAddPdf(Form("pdf_MC_Pb_Qo_qimean_%d_%d", detnum[jdet], timenum[jdet]), "sum of all 210Pb components in qimean", 
                                                        RooArgList(*pdf_MC_210Pb_Qo_qimean, *pdf_MC_210Bi_Qo_qimean, *histpdf_MC_206Pb_Qo_qimean), 
                                                        RooArgList(*frac_210Pb_Qo, *frac_210Bi_Qo));
        RooAddPdf*       pdf_MC_Pb_Qo_ptVqimean     = new RooAddPdf(Form("pdf_MC_Pb_Qo_ptVqimean_%d_%d", detnum[jdet], timenum[jdet]), "sum of all 210Pb components in qimean", 
                                                        RooArgList(*pdf_MC_210Pb_Qo_ptVqimean, *pdf_MC_210Bi_Qo_ptVqimean, *histpdf_MC_206Pb_Qo_ptVqimean), 
                                                        RooArgList(*frac_210Pb_Qo, *frac_210Bi_Qo));
        RooAddPdf*       pdf_MC_Pb_Qo_prpart     = new RooAddPdf(Form("pdf_MC_Pb_Qo_prpart_%d_%d", detnum[jdet], timenum[jdet]), "sum of all 210Pb components in prpart", 
                                                        RooArgList(*pdf_MC_210Pb_Qo_prpart, *pdf_MC_210Bi_Qo_prpart, *histpdf_MC_206Pb_Qo_prpart), 
                                                        RooArgList(*frac_210Pb_Qo, *frac_210Bi_Qo));
        RooAddPdf*       pdf_MC_Pb_Qo_pzpart     = new RooAddPdf(Form("pdf_MC_Pb_Qo_pzpart_%d_%d", detnum[jdet], timenum[jdet]), "sum of all 210Pb components in pzpart", 
                                                        RooArgList(*pdf_MC_210Pb_Qo_pzpart, *pdf_MC_210Bi_Qo_pzpart, *histpdf_MC_206Pb_Qo_pzpart), 
                                                        RooArgList(*frac_210Pb_Qo, *frac_210Bi_Qo));
        RooAddPdf*       pdf_MC_Pb_Qo_BDT5      = new RooAddPdf(Form("pdf_MC_Pb_Qo_BDT5_%d_%d", detnum[jdet], timenum[jdet]), "sum of all 210Pb components in BDT5", 
                                                        RooArgList(*pdf_MC_210Pb_Qo_BDT5, *pdf_MC_210Bi_Qo_BDT5, *histpdf_MC_206Pb_Qo_BDT5), 
                                                        RooArgList(*frac_210Pb_Qo, *frac_210Bi_Qo));
        RooAddPdf*       pdf_MC_Pb_Qo_BDT7      = new RooAddPdf(Form("pdf_MC_Pb_Qo_BDT7_%d_%d", detnum[jdet], timenum[jdet]), "sum of all 210Pb components in BDT7", 
                                                        RooArgList(*pdf_MC_210Pb_Qo_BDT7, *pdf_MC_210Bi_Qo_BDT7, *histpdf_MC_206Pb_Qo_BDT7), 
                                                        RooArgList(*frac_210Pb_Qo, *frac_210Bi_Qo));
        RooAddPdf*       pdf_MC_Pb_Qo_BDT10      = new RooAddPdf(Form("pdf_MC_Pb_Qo_BDT10_%d_%d", detnum[jdet], timenum[jdet]), "sum of all 210Pb components in BDT10", 
                                                        RooArgList(*pdf_MC_210Pb_Qo_BDT10, *pdf_MC_210Bi_Qo_BDT10, *histpdf_MC_206Pb_Qo_BDT10), 
                                                        RooArgList(*frac_210Pb_Qo, *frac_210Bi_Qo));
        RooAddPdf*       pdf_MC_Pb_Qo_BDT15      = new RooAddPdf(Form("pdf_MC_Pb_Qo_BDT15_%d_%d", detnum[jdet], timenum[jdet]), "sum of all 210Pb components in BDT15", 
                                                        RooArgList(*pdf_MC_210Pb_Qo_BDT15, *pdf_MC_210Bi_Qo_BDT15, *histpdf_MC_206Pb_Qo_BDT15), 
                                                        RooArgList(*frac_210Pb_Qo, *frac_210Bi_Qo));

        // 210Pb QiS1
        RooDataHist*     datahist_MC_210Pb_QiS1_pt          = new RooDataHist(Form("datahist_MC_210Pb_QiS1_pt_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Pb_QiS1 MC", RooArgSet(pt), data_MC_210Pb_QiS1_effcor);
        RooDataHist*     datahist_MC_210Pb_QiS1_qimean      = new RooDataHist(Form("datahist_MC_210Pb_QiS1_qimean_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Pb_QiS1 MC", RooArgSet(qimean), data_MC_210Pb_QiS1_effcor);
        RooDataHist*     datahist_MC_210Pb_QiS1_ptVqimean   = new RooDataHist(Form("datahist_MC_210Pb_QiS1_ptVqimean_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Pb_QiS1 MC", RooArgSet(pt, qimean), data_MC_210Pb_QiS1_effcor);
        RooDataHist*     datahist_MC_210Pb_QiS1_prpart      = new RooDataHist(Form("datahist_MC_210Pb_QiS1_prpart_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Pb_QiS1 MC", RooArgSet(prpart), data_MC_210Pb_QiS1_effcor);
        RooDataHist*     datahist_MC_210Pb_QiS1_pzpart      = new RooDataHist(Form("datahist_MC_210Pb_QiS1_pzpart_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Pb_QiS1 MC", RooArgSet(pzpart), data_MC_210Pb_QiS1_effcor);
        RooDataHist*     datahist_MC_210Pb_QiS1_BDT5        = new RooDataHist(Form("datahist_MC_210Pb_QiS1_BDT5_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Pb_QiS1 MC", RooArgSet(BDT5), data_MC_210Pb_QiS1_effcor);
        RooDataHist*     datahist_MC_210Pb_QiS1_BDT7        = new RooDataHist(Form("datahist_MC_210Pb_QiS1_BDT7_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Pb_QiS1 MC", RooArgSet(BDT7), data_MC_210Pb_QiS1_effcor);
        RooDataHist*     datahist_MC_210Pb_QiS1_BDT10       = new RooDataHist(Form("datahist_MC_210Pb_QiS1_BDT10_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Pb_QiS1 MC", RooArgSet(BDT10), data_MC_210Pb_QiS1_effcor);
        RooDataHist*     datahist_MC_210Pb_QiS1_BDT15       = new RooDataHist(Form("datahist_MC_210Pb_QiS1_BDT15_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Pb_QiS1 MC", RooArgSet(BDT15), data_MC_210Pb_QiS1_effcor);
        RooHistPdf*      histpdf_MC_210Pb_QiS1_pt           = new RooHistPdf(Form("histpdf_MC_210Pb_QiS1_pt_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Pb_QiS1 MC", RooArgSet(pt), *datahist_MC_210Pb_QiS1_pt, 0);
        RooHistPdf*      histpdf_MC_210Pb_QiS1_qimean       = new RooHistPdf(Form("histpdf_MC_210Pb_QiS1_qimean_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Pb_QiS1 MC", RooArgSet(qimean), *datahist_MC_210Pb_QiS1_qimean, 0);
        RooHistPdf*      histpdf_MC_210Pb_QiS1_ptVqimean    = new RooHistPdf(Form("histpdf_MC_210Pb_QiS1_ptVqimean_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Pb_QiS1 MC", RooArgSet(pt, qimean), *datahist_MC_210Pb_QiS1_ptVqimean, 0);
        RooHistPdf*      histpdf_MC_210Pb_QiS1_prpart       = new RooHistPdf(Form("histpdf_MC_210Pb_QiS1_prpart_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Pb_QiS1 MC", RooArgSet(prpart), *datahist_MC_210Pb_QiS1_prpart, 0);
        RooHistPdf*      histpdf_MC_210Pb_QiS1_pzpart       = new RooHistPdf(Form("histpdf_MC_210Pb_QiS1_pzpart_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Pb_QiS1 MC", RooArgSet(pzpart), *datahist_MC_210Pb_QiS1_pzpart, 0);
        RooHistPdf*      histpdf_MC_210Pb_QiS1_BDT5         = new RooHistPdf(Form("histpdf_MC_210Pb_QiS1_BDT5_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Pb_QiS1 MC", RooArgSet(BDT5), *datahist_MC_210Pb_QiS1_BDT5, 0);
        RooHistPdf*      histpdf_MC_210Pb_QiS1_BDT7         = new RooHistPdf(Form("histpdf_MC_210Pb_QiS1_BDT7_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Pb_QiS1 MC", RooArgSet(BDT7), *datahist_MC_210Pb_QiS1_BDT7, 0);
        RooHistPdf*      histpdf_MC_210Pb_QiS1_BDT10        = new RooHistPdf(Form("histpdf_MC_210Pb_QiS1_BDT10_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Pb_QiS1 MC", RooArgSet(BDT10), *datahist_MC_210Pb_QiS1_BDT10, 0);
        RooHistPdf*      histpdf_MC_210Pb_QiS1_BDT15        = new RooHistPdf(Form("histpdf_MC_210Pb_QiS1_BDT15_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Pb_QiS1 MC", RooArgSet(BDT15), *datahist_MC_210Pb_QiS1_BDT15, 0);

        RooDataHist*     datahist_MC_210Bi_QiS1_pt          = new RooDataHist(Form("datahist_MC_210Bi_QiS1_pt_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Bi_QiS1 MC", RooArgSet(pt), data_MC_210Bi_QiS1_effcor);
        RooDataHist*     datahist_MC_210Bi_QiS1_qimean      = new RooDataHist(Form("datahist_MC_210Bi_QiS1_qimean_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Bi_QiS1 MC", RooArgSet(qimean), data_MC_210Bi_QiS1_effcor);
        RooDataHist*     datahist_MC_210Bi_QiS1_ptVqimean   = new RooDataHist(Form("datahist_MC_210Bi_QiS1_ptVqimean_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Bi_QiS1 MC", RooArgSet(pt, qimean), data_MC_210Bi_QiS1_effcor);
        RooDataHist*     datahist_MC_210Bi_QiS1_prpart      = new RooDataHist(Form("datahist_MC_210Bi_QiS1_prpart_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Bi_QiS1 MC", RooArgSet(prpart), data_MC_210Bi_QiS1_effcor);
        RooDataHist*     datahist_MC_210Bi_QiS1_pzpart      = new RooDataHist(Form("datahist_MC_210Bi_QiS1_pzpart_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Bi_QiS1 MC", RooArgSet(pzpart), data_MC_210Bi_QiS1_effcor);
        RooDataHist*     datahist_MC_210Bi_QiS1_BDT5        = new RooDataHist(Form("datahist_MC_210Bi_QiS1_BDT5_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Bi_QiS1 MC", RooArgSet(BDT5), data_MC_210Bi_QiS1_effcor);
        RooDataHist*     datahist_MC_210Bi_QiS1_BDT7        = new RooDataHist(Form("datahist_MC_210Bi_QiS1_BDT7_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Bi_QiS1 MC", RooArgSet(BDT7), data_MC_210Bi_QiS1_effcor);
        RooDataHist*     datahist_MC_210Bi_QiS1_BDT10       = new RooDataHist(Form("datahist_MC_210Bi_QiS1_BDT10_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Bi_QiS1 MC", RooArgSet(BDT10), data_MC_210Bi_QiS1_effcor);
        RooDataHist*     datahist_MC_210Bi_QiS1_BDT15       = new RooDataHist(Form("datahist_MC_210Bi_QiS1_BDT15_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Bi_QiS1 MC", RooArgSet(BDT15), data_MC_210Bi_QiS1_effcor);
        RooHistPdf*      histpdf_MC_210Bi_QiS1_pt           = new RooHistPdf(Form("histpdf_MC_210Bi_QiS1_pt_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Bi_QiS1 MC", RooArgSet(pt), *datahist_MC_210Bi_QiS1_pt, 0);
        RooHistPdf*      histpdf_MC_210Bi_QiS1_qimean       = new RooHistPdf(Form("histpdf_MC_210Bi_QiS1_qimean_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Bi_QiS1 MC", RooArgSet(qimean), *datahist_MC_210Bi_QiS1_qimean, 0);
        RooHistPdf*      histpdf_MC_210Bi_QiS1_ptVqimean    = new RooHistPdf(Form("histpdf_MC_210Bi_QiS1_ptVqimean_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Bi_QiS1 MC", RooArgSet(pt, qimean), *datahist_MC_210Bi_QiS1_ptVqimean, 0);
        RooHistPdf*      histpdf_MC_210Bi_QiS1_prpart       = new RooHistPdf(Form("histpdf_MC_210Bi_QiS1_prpart_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Bi_QiS1 MC", RooArgSet(prpart), *datahist_MC_210Bi_QiS1_prpart, 0);
        RooHistPdf*      histpdf_MC_210Bi_QiS1_pzpart       = new RooHistPdf(Form("histpdf_MC_210Bi_QiS1_pzpart_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Bi_QiS1 MC", RooArgSet(pzpart), *datahist_MC_210Bi_QiS1_pzpart, 0);
        RooHistPdf*      histpdf_MC_210Bi_QiS1_BDT5         = new RooHistPdf(Form("histpdf_MC_210Bi_QiS1_BDT5_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Bi_QiS1 MC", RooArgSet(BDT5), *datahist_MC_210Bi_QiS1_BDT5, 0);
        RooHistPdf*      histpdf_MC_210Bi_QiS1_BDT7         = new RooHistPdf(Form("histpdf_MC_210Bi_QiS1_BDT7_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Bi_QiS1 MC", RooArgSet(BDT7), *datahist_MC_210Bi_QiS1_BDT7, 0);
        RooHistPdf*      histpdf_MC_210Bi_QiS1_BDT10        = new RooHistPdf(Form("histpdf_MC_210Bi_QiS1_BDT10_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Bi_QiS1 MC", RooArgSet(BDT10), *datahist_MC_210Bi_QiS1_BDT10, 0);
        RooHistPdf*      histpdf_MC_210Bi_QiS1_BDT15        = new RooHistPdf(Form("histpdf_MC_210Bi_QiS1_BDT15_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Bi_QiS1 MC", RooArgSet(BDT15), *datahist_MC_210Bi_QiS1_BDT15, 0);

        RooDataHist*     datahist_MC_206Pb_QiS1_pt          = new RooDataHist(Form("datahist_MC_206Pb_QiS1_pt_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 206Pb_QiS1 MC", RooArgSet(pt), data_MC_206Pb_QiS1_effcor);
        RooDataHist*     datahist_MC_206Pb_QiS1_qimean      = new RooDataHist(Form("datahist_MC_206Pb_QiS1_qimean_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 206Pb_QiS1 MC", RooArgSet(qimean), data_MC_206Pb_QiS1_effcor);
        RooDataHist*     datahist_MC_206Pb_QiS1_ptVqimean   = new RooDataHist(Form("datahist_MC_206Pb_QiS1_ptVqimean_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 206Pb_QiS1 MC", RooArgSet(pt, qimean), data_MC_206Pb_QiS1_effcor);
        RooDataHist*     datahist_MC_206Pb_QiS1_prpart      = new RooDataHist(Form("datahist_MC_206Pb_QiS1_prpart_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 206Pb_QiS1 MC", RooArgSet(prpart), data_MC_206Pb_QiS1_effcor);
        RooDataHist*     datahist_MC_206Pb_QiS1_pzpart      = new RooDataHist(Form("datahist_MC_206Pb_QiS1_pzpart_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 206Pb_QiS1 MC", RooArgSet(pzpart), data_MC_206Pb_QiS1_effcor);
        RooDataHist*     datahist_MC_206Pb_QiS1_BDT5        = new RooDataHist(Form("datahist_MC_206Pb_QiS1_BDT5_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 206Pb_QiS1 MC", RooArgSet(BDT5), data_MC_206Pb_QiS1_effcor);
        RooDataHist*     datahist_MC_206Pb_QiS1_BDT7        = new RooDataHist(Form("datahist_MC_206Pb_QiS1_BDT7_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 206Pb_QiS1 MC", RooArgSet(BDT7), data_MC_206Pb_QiS1_effcor);
        RooDataHist*     datahist_MC_206Pb_QiS1_BDT10       = new RooDataHist(Form("datahist_MC_206Pb_QiS1_BDT10_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 206Pb_QiS1 MC", RooArgSet(BDT10), data_MC_206Pb_QiS1_effcor);
        RooDataHist*     datahist_MC_206Pb_QiS1_BDT15       = new RooDataHist(Form("datahist_MC_206Pb_QiS1_BDT15_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 206Pb_QiS1 MC", RooArgSet(BDT15), data_MC_206Pb_QiS1_effcor);
        RooHistPdf*      histpdf_MC_206Pb_QiS1_pt           = new RooHistPdf(Form("histpdf_MC_206Pb_QiS1_pt_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 206Pb_QiS1 MC", RooArgSet(pt), *datahist_MC_206Pb_QiS1_pt, 0);
        RooHistPdf*      histpdf_MC_206Pb_QiS1_qimean       = new RooHistPdf(Form("histpdf_MC_206Pb_QiS1_qimean_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 206Pb_QiS1 MC", RooArgSet(qimean), *datahist_MC_206Pb_QiS1_qimean, 0);
        RooHistPdf*      histpdf_MC_206Pb_QiS1_ptVqimean    = new RooHistPdf(Form("histpdf_MC_206Pb_QiS1_ptVqimean_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 206Pb_QiS1 MC", RooArgSet(pt, qimean), *datahist_MC_206Pb_QiS1_ptVqimean, 0);
        RooHistPdf*      histpdf_MC_206Pb_QiS1_prpart       = new RooHistPdf(Form("histpdf_MC_206Pb_QiS1_prpart_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 206Pb_QiS1 MC", RooArgSet(prpart), *datahist_MC_206Pb_QiS1_prpart, 0);
        RooHistPdf*      histpdf_MC_206Pb_QiS1_pzpart       = new RooHistPdf(Form("histpdf_MC_206Pb_QiS1_pzpart_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 206Pb_QiS1 MC", RooArgSet(pzpart), *datahist_MC_206Pb_QiS1_pzpart, 0);
        RooHistPdf*      histpdf_MC_206Pb_QiS1_BDT5         = new RooHistPdf(Form("histpdf_MC_206Pb_QiS1_BDT5_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 206Pb_QiS1 MC", RooArgSet(BDT5), *datahist_MC_206Pb_QiS1_BDT5, 0);
        RooHistPdf*      histpdf_MC_206Pb_QiS1_BDT7         = new RooHistPdf(Form("histpdf_MC_206Pb_QiS1_BDT7_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 206Pb_QiS1 MC", RooArgSet(BDT7), *datahist_MC_206Pb_QiS1_BDT7, 0);
        RooHistPdf*      histpdf_MC_206Pb_QiS1_BDT10        = new RooHistPdf(Form("histpdf_MC_206Pb_QiS1_BDT10_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 206Pb_QiS1 MC", RooArgSet(BDT10), *datahist_MC_206Pb_QiS1_BDT10, 0);
        RooHistPdf*      histpdf_MC_206Pb_QiS1_BDT15        = new RooHistPdf(Form("histpdf_MC_206Pb_QiS1_BDT15_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 206Pb_QiS1 MC", RooArgSet(BDT15), *datahist_MC_206Pb_QiS1_BDT15, 0);

        RooAddPdf*       pdf_MC_Pb_QiS1_pt;
        RooAddPdf*       pdf_MC_Pb_QiS1_qimean;
        RooAddPdf*       pdf_MC_Pb_QiS1_ptVqimean;
        RooAddPdf*       pdf_MC_Pb_QiS1_prpart;
        RooAddPdf*       pdf_MC_Pb_QiS1_pzpart;
        RooAddPdf*       pdf_MC_Pb_QiS1_BDT5;
        RooAddPdf*       pdf_MC_Pb_QiS1_BDT7;
        RooAddPdf*       pdf_MC_Pb_QiS1_BDT10;
        RooAddPdf*       pdf_MC_Pb_QiS1_BDT15;

        if(sum_weights_206Pb_QiS1 == 0.)
        {
            pdf_MC_Pb_QiS1_pt           = new RooAddPdf(Form("pdf_MC_Pb_QiS1_pt_%d_%d", detnum[jdet], timenum[jdet]), "sum of all 210Pb components in pt", 
                                                            RooArgList(*histpdf_MC_210Pb_QiS1_pt, *histpdf_MC_210Bi_QiS1_pt), 
                                                            RooArgList(*frac_210Pb_QiS1));
            pdf_MC_Pb_QiS1_qimean       = new RooAddPdf(Form("pdf_MC_Pb_QiS1_qimean_%d_%d", detnum[jdet], timenum[jdet]), "sum of all 210Pb components in qimean", 
                                                            RooArgList(*histpdf_MC_210Pb_QiS1_qimean, *histpdf_MC_210Bi_QiS1_qimean), 
                                                            RooArgList(*frac_210Pb_QiS1));
            pdf_MC_Pb_QiS1_ptVqimean    = new RooAddPdf(Form("pdf_MC_Pb_QiS1_ptVqimean_%d_%d", detnum[jdet], timenum[jdet]), "sum of all 210Pb components in qimean", 
                                                            RooArgList(*histpdf_MC_210Pb_QiS1_ptVqimean, *histpdf_MC_210Bi_QiS1_ptVqimean), 
                                                            RooArgList(*frac_210Pb_QiS1));
            pdf_MC_Pb_QiS1_prpart       = new RooAddPdf(Form("pdf_MC_Pb_QiS1_prpart_%d_%d", detnum[jdet], timenum[jdet]), "sum of all 210Pb components in prpart", 
                                                            RooArgList(*histpdf_MC_210Pb_QiS1_prpart, *histpdf_MC_210Bi_QiS1_prpart), 
                                                            RooArgList(*frac_210Pb_QiS1));
            pdf_MC_Pb_QiS1_pzpart       = new RooAddPdf(Form("pdf_MC_Pb_QiS1_pzpart_%d_%d", detnum[jdet], timenum[jdet]), "sum of all 210Pb components in pzpart", 
                                                            RooArgList(*histpdf_MC_210Pb_QiS1_pzpart, *histpdf_MC_210Bi_QiS1_pzpart), 
                                                            RooArgList(*frac_210Pb_QiS1));
            pdf_MC_Pb_QiS1_BDT5         = new RooAddPdf(Form("pdf_MC_Pb_QiS1_BDT5_%d_%d", detnum[jdet], timenum[jdet]), "sum of all 210Pb components in BDT5", 
                                                            RooArgList(*histpdf_MC_210Pb_QiS1_BDT5, *histpdf_MC_210Bi_QiS1_BDT5), 
                                                            RooArgList(*frac_210Pb_QiS1));
            pdf_MC_Pb_QiS1_BDT7         = new RooAddPdf(Form("pdf_MC_Pb_QiS1_BDT7_%d_%d", detnum[jdet], timenum[jdet]), "sum of all 210Pb components in BDT7", 
                                                            RooArgList(*histpdf_MC_210Pb_QiS1_BDT7, *histpdf_MC_210Bi_QiS1_BDT7), 
                                                            RooArgList(*frac_210Pb_QiS1));
            pdf_MC_Pb_QiS1_BDT10        = new RooAddPdf(Form("pdf_MC_Pb_QiS1_BDT10_%d_%d", detnum[jdet], timenum[jdet]), "sum of all 210Pb components in BDT10", 
                                                            RooArgList(*histpdf_MC_210Pb_QiS1_BDT10, *histpdf_MC_210Bi_QiS1_BDT10), 
                                                            RooArgList(*frac_210Pb_QiS1));
            pdf_MC_Pb_QiS1_BDT15        = new RooAddPdf(Form("pdf_MC_Pb_QiS1_BDT15_%d_%d", detnum[jdet], timenum[jdet]), "sum of all 210Pb components in BDT15", 
                                                            RooArgList(*histpdf_MC_210Pb_QiS1_BDT15, *histpdf_MC_210Bi_QiS1_BDT15), 
                                                            RooArgList(*frac_210Pb_QiS1));
        }
        else
        {
            pdf_MC_Pb_QiS1_pt           = new RooAddPdf(Form("pdf_MC_Pb_QiS1_pt_%d_%d", detnum[jdet], timenum[jdet]), "sum of all 210Pb components in pt", 
                                                            RooArgList(*histpdf_MC_210Pb_QiS1_pt, *histpdf_MC_210Bi_QiS1_pt, *histpdf_MC_206Pb_QiS1_pt), 
                                                            RooArgList(*frac_210Pb_QiS1, *frac_210Bi_QiS1));
            pdf_MC_Pb_QiS1_qimean       = new RooAddPdf(Form("pdf_MC_Pb_QiS1_qimean_%d_%d", detnum[jdet], timenum[jdet]), "sum of all 210Pb components in qimean", 
                                                            RooArgList(*histpdf_MC_210Pb_QiS1_qimean, *histpdf_MC_210Bi_QiS1_qimean, *histpdf_MC_206Pb_QiS1_qimean), 
                                                            RooArgList(*frac_210Pb_QiS1, *frac_210Bi_QiS1));
            pdf_MC_Pb_QiS1_ptVqimean    = new RooAddPdf(Form("pdf_MC_Pb_QiS1_ptVqimean_%d_%d", detnum[jdet], timenum[jdet]), "sum of all 210Pb components in qimean", 
                                                            RooArgList(*histpdf_MC_210Pb_QiS1_ptVqimean, *histpdf_MC_210Bi_QiS1_ptVqimean, *histpdf_MC_206Pb_QiS1_ptVqimean), 
                                                            RooArgList(*frac_210Pb_QiS1, *frac_210Bi_QiS1));
            pdf_MC_Pb_QiS1_prpart       = new RooAddPdf(Form("pdf_MC_Pb_QiS1_prpart_%d_%d", detnum[jdet], timenum[jdet]), "sum of all 210Pb components in prpart", 
                                                            RooArgList(*histpdf_MC_210Pb_QiS1_prpart, *histpdf_MC_210Bi_QiS1_prpart, *histpdf_MC_206Pb_QiS1_prpart), 
                                                            RooArgList(*frac_210Pb_QiS1, *frac_210Bi_QiS1));
            pdf_MC_Pb_QiS1_pzpart       = new RooAddPdf(Form("pdf_MC_Pb_QiS1_pzpart_%d_%d", detnum[jdet], timenum[jdet]), "sum of all 210Pb components in pzpart", 
                                                            RooArgList(*histpdf_MC_210Pb_QiS1_pzpart, *histpdf_MC_210Bi_QiS1_pzpart, *histpdf_MC_206Pb_QiS1_pzpart), 
                                                            RooArgList(*frac_210Pb_QiS1, *frac_210Bi_QiS1));
            pdf_MC_Pb_QiS1_BDT5         = new RooAddPdf(Form("pdf_MC_Pb_QiS1_BDT5_%d_%d", detnum[jdet], timenum[jdet]), "sum of all 210Pb components in BDT5", 
                                                            RooArgList(*histpdf_MC_210Pb_QiS1_BDT5, *histpdf_MC_210Bi_QiS1_BDT5, *histpdf_MC_206Pb_QiS1_BDT5), 
                                                            RooArgList(*frac_210Pb_QiS1, *frac_210Bi_QiS1));
            pdf_MC_Pb_QiS1_BDT7         = new RooAddPdf(Form("pdf_MC_Pb_QiS1_BDT7_%d_%d", detnum[jdet], timenum[jdet]), "sum of all 210Pb components in BDT7", 
                                                            RooArgList(*histpdf_MC_210Pb_QiS1_BDT7, *histpdf_MC_210Bi_QiS1_BDT7, *histpdf_MC_206Pb_QiS1_BDT7), 
                                                            RooArgList(*frac_210Pb_QiS1, *frac_210Bi_QiS1));
            pdf_MC_Pb_QiS1_BDT10        = new RooAddPdf(Form("pdf_MC_Pb_QiS1_BDT10_%d_%d", detnum[jdet], timenum[jdet]), "sum of all 210Pb components in BDT10", 
                                                            RooArgList(*histpdf_MC_210Pb_QiS1_BDT10, *histpdf_MC_210Bi_QiS1_BDT10, *histpdf_MC_206Pb_QiS1_BDT10), 
                                                            RooArgList(*frac_210Pb_QiS1, *frac_210Bi_QiS1));
            pdf_MC_Pb_QiS1_BDT15        = new RooAddPdf(Form("pdf_MC_Pb_QiS1_BDT15_%d_%d", detnum[jdet], timenum[jdet]), "sum of all 210Pb components in BDT15", 
                                                            RooArgList(*histpdf_MC_210Pb_QiS1_BDT15, *histpdf_MC_210Bi_QiS1_BDT15, *histpdf_MC_206Pb_QiS1_BDT15), 
                                                            RooArgList(*frac_210Pb_QiS1, *frac_210Bi_QiS1));
        }

        // 210Pb QiS2
        RooDataHist*     datahist_MC_210Pb_QiS2_pt         = new RooDataHist(Form("datahist_MC_210Pb_QiS2_pt_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Pb_QiS2 MC", RooArgSet(pt), data_MC_210Pb_QiS2_effcor);
        RooDataHist*     datahist_MC_210Pb_QiS2_qimean     = new RooDataHist(Form("datahist_MC_210Pb_QiS2_qimean_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Pb_QiS2 MC", RooArgSet(qimean), data_MC_210Pb_QiS2_effcor);
        RooDataHist*     datahist_MC_210Pb_QiS2_ptVqimean  = new RooDataHist(Form("datahist_MC_210Pb_QiS2_ptVqimean_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Pb_QiS2 MC", RooArgSet(pt, qimean), data_MC_210Pb_QiS2_effcor);
        RooDataHist*     datahist_MC_210Pb_QiS2_prpart     = new RooDataHist(Form("datahist_MC_210Pb_QiS2_prpart_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Pb_QiS2 MC", RooArgSet(prpart), data_MC_210Pb_QiS2_effcor);
        RooDataHist*     datahist_MC_210Pb_QiS2_pzpart     = new RooDataHist(Form("datahist_MC_210Pb_QiS2_pzpart_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Pb_QiS2 MC", RooArgSet(pzpart), data_MC_210Pb_QiS2_effcor);
        RooDataHist*     datahist_MC_210Pb_QiS2_BDT5       = new RooDataHist(Form("datahist_MC_210Pb_QiS2_BDT5_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Pb_QiS2 MC", RooArgSet(BDT5), data_MC_210Pb_QiS2_effcor);
        RooDataHist*     datahist_MC_210Pb_QiS2_BDT7       = new RooDataHist(Form("datahist_MC_210Pb_QiS2_BDT7_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Pb_QiS2 MC", RooArgSet(BDT7), data_MC_210Pb_QiS2_effcor);
        RooDataHist*     datahist_MC_210Pb_QiS2_BDT10      = new RooDataHist(Form("datahist_MC_210Pb_QiS2_BDT10_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Pb_QiS2 MC", RooArgSet(BDT10), data_MC_210Pb_QiS2_effcor);
        RooDataHist*     datahist_MC_210Pb_QiS2_BDT15      = new RooDataHist(Form("datahist_MC_210Pb_QiS2_BDT15_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Pb_QiS2 MC", RooArgSet(BDT15), data_MC_210Pb_QiS2_effcor);
        RooHistPdf*      histpdf_MC_210Pb_QiS2_pt          = new RooHistPdf(Form("histpdf_MC_210Pb_QiS2_pt_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Pb_QiS2 MC", RooArgSet(pt), *datahist_MC_210Pb_QiS2_pt, 0);
        RooHistPdf*      histpdf_MC_210Pb_QiS2_qimean      = new RooHistPdf(Form("histpdf_MC_210Pb_QiS2_qimean_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Pb_QiS2 MC", RooArgSet(qimean), *datahist_MC_210Pb_QiS2_qimean, 0);
        RooHistPdf*      histpdf_MC_210Pb_QiS2_ptVqimean   = new RooHistPdf(Form("histpdf_MC_210Pb_QiS2_ptVqimean_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Pb_QiS2 MC", RooArgSet(pt, qimean), *datahist_MC_210Pb_QiS2_ptVqimean, 0);
        RooHistPdf*      histpdf_MC_210Pb_QiS2_prpart      = new RooHistPdf(Form("histpdf_MC_210Pb_QiS2_prpart_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Pb_QiS2 MC", RooArgSet(prpart), *datahist_MC_210Pb_QiS2_prpart, 0);
        RooHistPdf*      histpdf_MC_210Pb_QiS2_pzpart      = new RooHistPdf(Form("histpdf_MC_210Pb_QiS2_pzpart_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Pb_QiS2 MC", RooArgSet(pzpart), *datahist_MC_210Pb_QiS2_pzpart, 0);
        RooHistPdf*      histpdf_MC_210Pb_QiS2_BDT5        = new RooHistPdf(Form("histpdf_MC_210Pb_QiS2_BDT5_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Pb_QiS2 MC", RooArgSet(BDT5), *datahist_MC_210Pb_QiS2_BDT5, 0);
        RooHistPdf*      histpdf_MC_210Pb_QiS2_BDT7        = new RooHistPdf(Form("histpdf_MC_210Pb_QiS2_BDT7_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Pb_QiS2 MC", RooArgSet(BDT7), *datahist_MC_210Pb_QiS2_BDT7, 0);
        RooHistPdf*      histpdf_MC_210Pb_QiS2_BDT10       = new RooHistPdf(Form("histpdf_MC_210Pb_QiS2_BDT10_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Pb_QiS2 MC", RooArgSet(BDT10), *datahist_MC_210Pb_QiS2_BDT10, 0);
        RooHistPdf*      histpdf_MC_210Pb_QiS2_BDT15       = new RooHistPdf(Form("histpdf_MC_210Pb_QiS2_BDT15_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Pb_QiS2 MC", RooArgSet(BDT15), *datahist_MC_210Pb_QiS2_BDT15, 0);

        RooDataHist*     datahist_MC_210Bi_QiS2_pt         = new RooDataHist(Form("datahist_MC_210Bi_QiS2_pt_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Bi_QiS2 MC", RooArgSet(pt), data_MC_210Bi_QiS2_effcor);
        RooDataHist*     datahist_MC_210Bi_QiS2_qimean     = new RooDataHist(Form("datahist_MC_210Bi_QiS2_qimean_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Bi_QiS2 MC", RooArgSet(qimean), data_MC_210Bi_QiS2_effcor);
        RooDataHist*     datahist_MC_210Bi_QiS2_ptVqimean  = new RooDataHist(Form("datahist_MC_210Bi_QiS2_ptVqimean_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Bi_QiS2 MC", RooArgSet(pt, qimean), data_MC_210Bi_QiS2_effcor);
        RooDataHist*     datahist_MC_210Bi_QiS2_prpart     = new RooDataHist(Form("datahist_MC_210Bi_QiS2_prpart_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Bi_QiS2 MC", RooArgSet(prpart), data_MC_210Bi_QiS2_effcor);
        RooDataHist*     datahist_MC_210Bi_QiS2_pzpart     = new RooDataHist(Form("datahist_MC_210Bi_QiS2_pzpart_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Bi_QiS2 MC", RooArgSet(pzpart), data_MC_210Bi_QiS2_effcor);
        RooDataHist*     datahist_MC_210Bi_QiS2_BDT5       = new RooDataHist(Form("datahist_MC_210Bi_QiS2_BDT5_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Bi_QiS2 MC", RooArgSet(BDT5), data_MC_210Bi_QiS2_effcor);
        RooDataHist*     datahist_MC_210Bi_QiS2_BDT7       = new RooDataHist(Form("datahist_MC_210Bi_QiS2_BDT7_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Bi_QiS2 MC", RooArgSet(BDT7), data_MC_210Bi_QiS2_effcor);
        RooDataHist*     datahist_MC_210Bi_QiS2_BDT10      = new RooDataHist(Form("datahist_MC_210Bi_QiS2_BDT10_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Bi_QiS2 MC", RooArgSet(BDT10), data_MC_210Bi_QiS2_effcor);
        RooDataHist*     datahist_MC_210Bi_QiS2_BDT15      = new RooDataHist(Form("datahist_MC_210Bi_QiS2_BDT15_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 210Bi_QiS2 MC", RooArgSet(BDT15), data_MC_210Bi_QiS2_effcor);
        RooHistPdf*      histpdf_MC_210Bi_QiS2_pt          = new RooHistPdf(Form("histpdf_MC_210Bi_QiS2_pt_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Bi_QiS2 MC", RooArgSet(pt), *datahist_MC_210Bi_QiS2_pt, 0);
        RooHistPdf*      histpdf_MC_210Bi_QiS2_qimean      = new RooHistPdf(Form("histpdf_MC_210Bi_QiS2_qimean_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Bi_QiS2 MC", RooArgSet(qimean), *datahist_MC_210Bi_QiS2_qimean, 0);
        RooHistPdf*      histpdf_MC_210Bi_QiS2_ptVqimean   = new RooHistPdf(Form("histpdf_MC_210Bi_QiS2_ptVqimean_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Bi_QiS2 MC", RooArgSet(pt, qimean), *datahist_MC_210Bi_QiS2_ptVqimean, 0);
        RooHistPdf*      histpdf_MC_210Bi_QiS2_prpart      = new RooHistPdf(Form("histpdf_MC_210Bi_QiS2_prpart_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Bi_QiS2 MC", RooArgSet(prpart), *datahist_MC_210Bi_QiS2_prpart, 0);
        RooHistPdf*      histpdf_MC_210Bi_QiS2_pzpart      = new RooHistPdf(Form("histpdf_MC_210Bi_QiS2_pzpart_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Bi_QiS2 MC", RooArgSet(pzpart), *datahist_MC_210Bi_QiS2_pzpart, 0);
        RooHistPdf*      histpdf_MC_210Bi_QiS2_BDT5        = new RooHistPdf(Form("histpdf_MC_210Bi_QiS2_BDT5_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Bi_QiS2 MC", RooArgSet(BDT5), *datahist_MC_210Bi_QiS2_BDT5, 0);
        RooHistPdf*      histpdf_MC_210Bi_QiS2_BDT7        = new RooHistPdf(Form("histpdf_MC_210Bi_QiS2_BDT7_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Bi_QiS2 MC", RooArgSet(BDT7), *datahist_MC_210Bi_QiS2_BDT7, 0);
        RooHistPdf*      histpdf_MC_210Bi_QiS2_BDT10       = new RooHistPdf(Form("histpdf_MC_210Bi_QiS2_BDT10_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Bi_QiS2 MC", RooArgSet(BDT10), *datahist_MC_210Bi_QiS2_BDT10, 0);
        RooHistPdf*      histpdf_MC_210Bi_QiS2_BDT15       = new RooHistPdf(Form("histpdf_MC_210Bi_QiS2_BDT15_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 210Bi_QiS2 MC", RooArgSet(BDT15), *datahist_MC_210Bi_QiS2_BDT15, 0);

        RooDataHist*     datahist_MC_206Pb_QiS2_pt         = new RooDataHist(Form("datahist_MC_206Pb_QiS2_pt_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 206Pb_QiS2 MC", RooArgSet(pt), data_MC_206Pb_QiS2_effcor);
        RooDataHist*     datahist_MC_206Pb_QiS2_qimean     = new RooDataHist(Form("datahist_MC_206Pb_QiS2_qimean_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 206Pb_QiS2 MC", RooArgSet(qimean), data_MC_206Pb_QiS2_effcor);
        RooDataHist*     datahist_MC_206Pb_QiS2_ptVqimean  = new RooDataHist(Form("datahist_MC_206Pb_QiS2_ptVqimean_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 206Pb_QiS2 MC", RooArgSet(pt, qimean), data_MC_206Pb_QiS2_effcor);
        RooDataHist*     datahist_MC_206Pb_QiS2_prpart     = new RooDataHist(Form("datahist_MC_206Pb_QiS2_prpart_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 206Pb_QiS2 MC", RooArgSet(prpart), data_MC_206Pb_QiS2_effcor);
        RooDataHist*     datahist_MC_206Pb_QiS2_pzpart     = new RooDataHist(Form("datahist_MC_206Pb_QiS2_pzpart_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 206Pb_QiS2 MC", RooArgSet(pzpart), data_MC_206Pb_QiS2_effcor);
        RooDataHist*     datahist_MC_206Pb_QiS2_BDT5       = new RooDataHist(Form("datahist_MC_206Pb_QiS2_BDT5_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 206Pb_QiS2 MC", RooArgSet(BDT5), data_MC_206Pb_QiS2_effcor);
        RooDataHist*     datahist_MC_206Pb_QiS2_BDT7       = new RooDataHist(Form("datahist_MC_206Pb_QiS2_BDT7_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 206Pb_QiS2 MC", RooArgSet(BDT7), data_MC_206Pb_QiS2_effcor);
        RooDataHist*     datahist_MC_206Pb_QiS2_BDT10      = new RooDataHist(Form("datahist_MC_206Pb_QiS2_BDT10_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 206Pb_QiS2 MC", RooArgSet(BDT10), data_MC_206Pb_QiS2_effcor);
        RooDataHist*     datahist_MC_206Pb_QiS2_BDT15      = new RooDataHist(Form("datahist_MC_206Pb_QiS2_BDT15_%d_%d", detnum[jdet], timenum[jdet]), "data histogram of 206Pb_QiS2 MC", RooArgSet(BDT15), data_MC_206Pb_QiS2_effcor);
        RooHistPdf*      histpdf_MC_206Pb_QiS2_pt          = new RooHistPdf(Form("histpdf_MC_206Pb_QiS2_pt_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 206Pb_QiS2 MC", RooArgSet(pt), *datahist_MC_206Pb_QiS2_pt, 0);
        RooHistPdf*      histpdf_MC_206Pb_QiS2_qimean      = new RooHistPdf(Form("histpdf_MC_206Pb_QiS2_qimean_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 206Pb_QiS2 MC", RooArgSet(qimean), *datahist_MC_206Pb_QiS2_qimean, 0);
        RooHistPdf*      histpdf_MC_206Pb_QiS2_ptVqimean   = new RooHistPdf(Form("histpdf_MC_206Pb_QiS2_ptVqimean_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 206Pb_QiS2 MC", RooArgSet(pt, qimean), *datahist_MC_206Pb_QiS2_ptVqimean, 0);
        RooHistPdf*      histpdf_MC_206Pb_QiS2_prpart      = new RooHistPdf(Form("histpdf_MC_206Pb_QiS2_prpart_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 206Pb_QiS2 MC", RooArgSet(prpart), *datahist_MC_206Pb_QiS2_prpart, 0);
        RooHistPdf*      histpdf_MC_206Pb_QiS2_pzpart      = new RooHistPdf(Form("histpdf_MC_206Pb_QiS2_pzpart_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 206Pb_QiS2 MC", RooArgSet(pzpart), *datahist_MC_206Pb_QiS2_pzpart, 0);
        RooHistPdf*      histpdf_MC_206Pb_QiS2_BDT5        = new RooHistPdf(Form("histpdf_MC_206Pb_QiS2_BDT5_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 206Pb_QiS2 MC", RooArgSet(BDT5), *datahist_MC_206Pb_QiS2_BDT5, 0);
        RooHistPdf*      histpdf_MC_206Pb_QiS2_BDT7        = new RooHistPdf(Form("histpdf_MC_206Pb_QiS2_BDT7_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 206Pb_QiS2 MC", RooArgSet(BDT7), *datahist_MC_206Pb_QiS2_BDT7, 0);
        RooHistPdf*      histpdf_MC_206Pb_QiS2_BDT10       = new RooHistPdf(Form("histpdf_MC_206Pb_QiS2_BDT10_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 206Pb_QiS2 MC", RooArgSet(BDT10), *datahist_MC_206Pb_QiS2_BDT10, 0);
        RooHistPdf*      histpdf_MC_206Pb_QiS2_BDT15       = new RooHistPdf(Form("histpdf_MC_206Pb_QiS2_BDT15_%d_%d", detnum[jdet], timenum[jdet]), "hist PDF of 206Pb_QiS2 MC", RooArgSet(BDT15), *datahist_MC_206Pb_QiS2_BDT15, 0);


        RooAddPdf*       pdf_MC_Pb_QiS2_pt;
        RooAddPdf*       pdf_MC_Pb_QiS2_qimean;
        RooAddPdf*       pdf_MC_Pb_QiS2_ptVqimean;
        RooAddPdf*       pdf_MC_Pb_QiS2_prpart;
        RooAddPdf*       pdf_MC_Pb_QiS2_pzpart;
        RooAddPdf*       pdf_MC_Pb_QiS2_BDT5;
        RooAddPdf*       pdf_MC_Pb_QiS2_BDT7;
        RooAddPdf*       pdf_MC_Pb_QiS2_BDT10;
        RooAddPdf*       pdf_MC_Pb_QiS2_BDT15;
        if(sum_weights_206Pb_QiS2 == 0.)
        {
            pdf_MC_Pb_QiS2_pt           = new RooAddPdf(Form("pdf_MC_Pb_QiS2_pt_%d_%d", detnum[jdet], timenum[jdet]), "sum of all 210Pb components in pt", 
                                                            RooArgList(*histpdf_MC_210Pb_QiS2_pt, *histpdf_MC_210Bi_QiS2_pt), 
                                                            RooArgList(*frac_210Pb_QiS2));
            pdf_MC_Pb_QiS2_qimean       = new RooAddPdf(Form("pdf_MC_Pb_QiS2_qimean_%d_%d", detnum[jdet], timenum[jdet]), "sum of all 210Pb components in qimean", 
                                                            RooArgList(*histpdf_MC_210Pb_QiS2_qimean, *histpdf_MC_210Bi_QiS2_qimean), 
                                                            RooArgList(*frac_210Pb_QiS2));
            pdf_MC_Pb_QiS2_ptVqimean    = new RooAddPdf(Form("pdf_MC_Pb_QiS2_ptVqimean_%d_%d", detnum[jdet], timenum[jdet]), "sum of all 210Pb components in qimean", 
                                                            RooArgList(*histpdf_MC_210Pb_QiS2_ptVqimean, *histpdf_MC_210Bi_QiS2_ptVqimean), 
                                                            RooArgList(*frac_210Pb_QiS2));
            pdf_MC_Pb_QiS2_prpart       = new RooAddPdf(Form("pdf_MC_Pb_QiS2_prpart_%d_%d", detnum[jdet], timenum[jdet]), "sum of all 210Pb components in prpart", 
                                                            RooArgList(*histpdf_MC_210Pb_QiS2_prpart, *histpdf_MC_210Bi_QiS2_prpart), 
                                                            RooArgList(*frac_210Pb_QiS2));
            pdf_MC_Pb_QiS2_pzpart       = new RooAddPdf(Form("pdf_MC_Pb_QiS2_pzpart_%d_%d", detnum[jdet], timenum[jdet]), "sum of all 210Pb components in pzpart", 
                                                            RooArgList(*histpdf_MC_210Pb_QiS2_pzpart, *histpdf_MC_210Bi_QiS2_pzpart), 
                                                            RooArgList(*frac_210Pb_QiS2));
            pdf_MC_Pb_QiS2_BDT5         = new RooAddPdf(Form("pdf_MC_Pb_QiS2_BDT5_%d_%d", detnum[jdet], timenum[jdet]), "sum of all 210Pb components in BDT5", 
                                                            RooArgList(*histpdf_MC_210Pb_QiS2_BDT5, *histpdf_MC_210Bi_QiS2_BDT5), 
                                                            RooArgList(*frac_210Pb_QiS2));
            pdf_MC_Pb_QiS2_BDT7         = new RooAddPdf(Form("pdf_MC_Pb_QiS2_BDT7_%d_%d", detnum[jdet], timenum[jdet]), "sum of all 210Pb components in BDT7", 
                                                            RooArgList(*histpdf_MC_210Pb_QiS2_BDT7, *histpdf_MC_210Bi_QiS2_BDT7), 
                                                            RooArgList(*frac_210Pb_QiS2));
            pdf_MC_Pb_QiS2_BDT10        = new RooAddPdf(Form("pdf_MC_Pb_QiS2_BDT10_%d_%d", detnum[jdet], timenum[jdet]), "sum of all 210Pb components in BDT10", 
                                                            RooArgList(*histpdf_MC_210Pb_QiS2_BDT10, *histpdf_MC_210Bi_QiS2_BDT10), 
                                                            RooArgList(*frac_210Pb_QiS2));
            pdf_MC_Pb_QiS2_BDT15        = new RooAddPdf(Form("pdf_MC_Pb_QiS2_BDT15_%d_%d", detnum[jdet], timenum[jdet]), "sum of all 210Pb components in BDT15", 
                                                            RooArgList(*histpdf_MC_210Pb_QiS2_BDT15, *histpdf_MC_210Bi_QiS2_BDT15), 
                                                            RooArgList(*frac_210Pb_QiS2));
        }
        else
        {
            pdf_MC_Pb_QiS2_pt           = new RooAddPdf(Form("pdf_MC_Pb_QiS2_pt_%d_%d", detnum[jdet], timenum[jdet]), "sum of all 210Pb components in pt", 
                                                            RooArgList(*histpdf_MC_210Pb_QiS2_pt, *histpdf_MC_210Bi_QiS2_pt, *histpdf_MC_206Pb_QiS2_pt), 
                                                            RooArgList(*frac_210Pb_QiS2, *frac_210Bi_QiS2));
            pdf_MC_Pb_QiS2_qimean       = new RooAddPdf(Form("pdf_MC_Pb_QiS2_qimean_%d_%d", detnum[jdet], timenum[jdet]), "sum of all 210Pb components in qimean", 
                                                            RooArgList(*histpdf_MC_210Pb_QiS2_qimean, *histpdf_MC_210Bi_QiS2_qimean, *histpdf_MC_206Pb_QiS2_qimean), 
                                                            RooArgList(*frac_210Pb_QiS2, *frac_210Bi_QiS2));
            pdf_MC_Pb_QiS2_ptVqimean    = new RooAddPdf(Form("pdf_MC_Pb_QiS2_ptVqimean_%d_%d", detnum[jdet], timenum[jdet]), "sum of all 210Pb components in qimean", 
                                                            RooArgList(*histpdf_MC_210Pb_QiS2_ptVqimean, *histpdf_MC_210Bi_QiS2_ptVqimean, *histpdf_MC_206Pb_QiS2_ptVqimean), 
                                                            RooArgList(*frac_210Pb_QiS2, *frac_210Bi_QiS2));
            pdf_MC_Pb_QiS2_prpart       = new RooAddPdf(Form("pdf_MC_Pb_QiS2_prpart_%d_%d", detnum[jdet], timenum[jdet]), "sum of all 210Pb components in prpart", 
                                                            RooArgList(*histpdf_MC_210Pb_QiS2_prpart, *histpdf_MC_210Bi_QiS2_prpart, *histpdf_MC_206Pb_QiS2_prpart), 
                                                            RooArgList(*frac_210Pb_QiS2, *frac_210Bi_QiS2));
            pdf_MC_Pb_QiS2_pzpart       = new RooAddPdf(Form("pdf_MC_Pb_QiS2_pzpart_%d_%d", detnum[jdet], timenum[jdet]), "sum of all 210Pb components in pzpart", 
                                                            RooArgList(*histpdf_MC_210Pb_QiS2_pzpart, *histpdf_MC_210Bi_QiS2_pzpart, *histpdf_MC_206Pb_QiS2_pzpart), 
                                                            RooArgList(*frac_210Pb_QiS2, *frac_210Bi_QiS2));
            pdf_MC_Pb_QiS2_BDT5         = new RooAddPdf(Form("pdf_MC_Pb_QiS2_BDT5_%d_%d", detnum[jdet], timenum[jdet]), "sum of all 210Pb components in BDT5", 
                                                            RooArgList(*histpdf_MC_210Pb_QiS2_BDT5, *histpdf_MC_210Bi_QiS2_BDT5, *histpdf_MC_206Pb_QiS2_BDT5), 
                                                            RooArgList(*frac_210Pb_QiS2, *frac_210Bi_QiS2));
            pdf_MC_Pb_QiS2_BDT7         = new RooAddPdf(Form("pdf_MC_Pb_QiS2_BDT7_%d_%d", detnum[jdet], timenum[jdet]), "sum of all 210Pb components in BDT7", 
                                                            RooArgList(*histpdf_MC_210Pb_QiS2_BDT7, *histpdf_MC_210Bi_QiS2_BDT7, *histpdf_MC_206Pb_QiS2_BDT7), 
                                                            RooArgList(*frac_210Pb_QiS2, *frac_210Bi_QiS2));
            pdf_MC_Pb_QiS2_BDT10        = new RooAddPdf(Form("pdf_MC_Pb_QiS2_BDT10_%d_%d", detnum[jdet], timenum[jdet]), "sum of all 210Pb components in BDT10", 
                                                            RooArgList(*histpdf_MC_210Pb_QiS2_BDT10, *histpdf_MC_210Bi_QiS2_BDT10, *histpdf_MC_206Pb_QiS2_BDT10), 
                                                            RooArgList(*frac_210Pb_QiS2, *frac_210Bi_QiS2));
            pdf_MC_Pb_QiS2_BDT15        = new RooAddPdf(Form("pdf_MC_Pb_QiS2_BDT15_%d_%d", detnum[jdet], timenum[jdet]), "sum of all 210Pb components in BDT15", 
                                                            RooArgList(*histpdf_MC_210Pb_QiS2_BDT15, *histpdf_MC_210Bi_QiS2_BDT15, *histpdf_MC_206Pb_QiS2_BDT15), 
                                                            RooArgList(*frac_210Pb_QiS2, *frac_210Bi_QiS2));
        }
        

        RooAddPdf* pdf_bg_pt;
        RooAddPdf* pdf_bg_qimean;
        RooAddPdf* pdf_bg_ptVqimean;
        RooAddPdf* pdf_bg_prpart;
        RooAddPdf* pdf_bg_pzpart;
        RooAddPdf* pdf_bg_BDT5;
        RooAddPdf* pdf_bg_BDT7;
        RooAddPdf* pdf_bg_BDT10;
        RooAddPdf* pdf_bg_BDT15;
        if(sum_weights_Pb_QiS1 == 0)
        {
            pdf_bg_pt           = new RooAddPdf(Form("pdf_bg_pt_%d_%d", detnum[jdet], timenum[jdet]), "background pdf in pt",
                                            RooArgList(*histpdf_MC_gammas_pt, *pdf_MC_Pb_Qo_pt, *pdf_MC_Pb_QiS2_pt, *histpdf_MC_1keV_pt),
                                            RooArgList(*N_gammas, *N_210Pb_Qo, *N_210Pb_QiS2, *N_1keV));
            pdf_bg_qimean       = new RooAddPdf(Form("pdf_bg_qimean_%d_%d", detnum[jdet], timenum[jdet]), "background pdf in qimean",
                                            RooArgList(*histpdf_MC_gammas_qimean, *pdf_MC_Pb_Qo_qimean, *pdf_MC_Pb_QiS2_qimean, *histpdf_MC_1keV_qimean),
                                            RooArgList(*N_gammas, *N_210Pb_Qo, *N_210Pb_QiS2, *N_1keV));
            pdf_bg_ptVqimean    = new RooAddPdf(Form("pdf_bg_ptVqimean_%d_%d", detnum[jdet], timenum[jdet]), "background pdf in qimean",
                                            RooArgList(*histpdf_MC_gammas_ptVqimean, *pdf_MC_Pb_Qo_ptVqimean, *pdf_MC_Pb_QiS2_ptVqimean, *histpdf_MC_1keV_ptVqimean),
                                            RooArgList(*N_gammas, *N_210Pb_Qo, *N_210Pb_QiS2, *N_1keV));
            pdf_bg_prpart       = new RooAddPdf(Form("pdf_bg_prpart_%d_%d", detnum[jdet], timenum[jdet]), "background pdf in prpart",
                                            RooArgList(*histpdf_MC_gammas_prpart, *pdf_MC_Pb_Qo_prpart, *pdf_MC_Pb_QiS2_prpart, *histpdf_MC_1keV_prpart),
                                            RooArgList(*N_gammas, *N_210Pb_Qo, *N_210Pb_QiS2, *N_1keV));
            pdf_bg_pzpart       = new RooAddPdf(Form("pdf_bg_pzpart_%d_%d", detnum[jdet], timenum[jdet]), "background pdf in pzpart",
                                            RooArgList(*histpdf_MC_gammas_pzpart, *pdf_MC_Pb_Qo_pzpart, *pdf_MC_Pb_QiS2_pzpart, *histpdf_MC_1keV_pzpart),
                                            RooArgList(*N_gammas, *N_210Pb_Qo, *N_210Pb_QiS2, *N_1keV));
            pdf_bg_BDT5         = new RooAddPdf(Form("pdf_bg_BDT5_%d_%d", detnum[jdet], timenum[jdet]), "background pdf in BDT5",
                                            RooArgList(*histpdf_MC_gammas_BDT5, *pdf_MC_Pb_Qo_BDT5, *pdf_MC_Pb_QiS2_BDT5, *histpdf_MC_1keV_BDT5),
                                            RooArgList(*N_gammas, *N_210Pb_Qo, *N_210Pb_QiS2, *N_1keV));
            pdf_bg_BDT7         = new RooAddPdf(Form("pdf_bg_BDT7_%d_%d", detnum[jdet], timenum[jdet]), "background pdf in BDT7",
                                            RooArgList(*histpdf_MC_gammas_BDT7, *pdf_MC_Pb_Qo_BDT7, *pdf_MC_Pb_QiS2_BDT7, *histpdf_MC_1keV_BDT7),
                                            RooArgList(*N_gammas, *N_210Pb_Qo, *N_210Pb_QiS2, *N_1keV));
            pdf_bg_BDT10        = new RooAddPdf(Form("pdf_bg_BDT10_%d_%d", detnum[jdet], timenum[jdet]), "background pdf in BDT10",
                                            RooArgList(*histpdf_MC_gammas_BDT10, *pdf_MC_Pb_Qo_BDT10, *pdf_MC_Pb_QiS2_BDT10, *histpdf_MC_1keV_BDT10),
                                            RooArgList(*N_gammas, *N_210Pb_Qo, *N_210Pb_QiS2, *N_1keV));
            pdf_bg_BDT15        = new RooAddPdf(Form("pdf_bg_BDT15_%d_%d", detnum[jdet], timenum[jdet]), "background pdf in BDT15",
                                            RooArgList(*histpdf_MC_gammas_BDT15, *pdf_MC_Pb_Qo_BDT15, *pdf_MC_Pb_QiS2_BDT15, *histpdf_MC_1keV_BDT15),
                                            RooArgList(*N_gammas, *N_210Pb_Qo, *N_210Pb_QiS2, *N_1keV));
        }
        else
        {
            pdf_bg_pt           = new RooAddPdf(Form("pdf_bg_pt_%d_%d", detnum[jdet], timenum[jdet]), "background pdf in pt",
                                            RooArgList(*histpdf_MC_gammas_pt, *pdf_MC_Pb_Qo_pt, *pdf_MC_Pb_QiS1_pt, *pdf_MC_Pb_QiS2_pt, *histpdf_MC_1keV_pt),
                                            RooArgList(*N_gammas, *N_210Pb_Qo, *N_210Pb_QiS1, *N_210Pb_QiS2, *N_1keV));
            pdf_bg_qimean       = new RooAddPdf(Form("pdf_bg_qimean_%d_%d", detnum[jdet], timenum[jdet]), "background pdf in qimean",
                                            RooArgList(*histpdf_MC_gammas_qimean, *pdf_MC_Pb_Qo_qimean, *pdf_MC_Pb_QiS1_qimean, *pdf_MC_Pb_QiS2_qimean, *histpdf_MC_1keV_qimean),
                                            RooArgList(*N_gammas, *N_210Pb_Qo, *N_210Pb_QiS1, *N_210Pb_QiS2, *N_1keV));
            pdf_bg_ptVqimean    = new RooAddPdf(Form("pdf_bg_ptVqimean_%d_%d", detnum[jdet], timenum[jdet]), "background pdf in qimean",
                                            RooArgList(*histpdf_MC_gammas_ptVqimean, *pdf_MC_Pb_Qo_ptVqimean, *pdf_MC_Pb_QiS1_ptVqimean, *pdf_MC_Pb_QiS2_ptVqimean, *histpdf_MC_1keV_ptVqimean),
                                            RooArgList(*N_gammas, *N_210Pb_Qo, *N_210Pb_QiS1, *N_210Pb_QiS2, *N_1keV));
            pdf_bg_prpart       = new RooAddPdf(Form("pdf_bg_prpart_%d_%d", detnum[jdet], timenum[jdet]), "background pdf in prpart",
                                            RooArgList(*histpdf_MC_gammas_prpart, *pdf_MC_Pb_Qo_prpart, *pdf_MC_Pb_QiS1_prpart, *pdf_MC_Pb_QiS2_prpart, *histpdf_MC_1keV_prpart),
                                            RooArgList(*N_gammas, *N_210Pb_Qo, *N_210Pb_QiS1, *N_210Pb_QiS2, *N_1keV));
            pdf_bg_pzpart       = new RooAddPdf(Form("pdf_bg_pzpart_%d_%d", detnum[jdet], timenum[jdet]), "background pdf in pzpart",
                                            RooArgList(*histpdf_MC_gammas_pzpart, *pdf_MC_Pb_Qo_pzpart, *pdf_MC_Pb_QiS1_pzpart, *pdf_MC_Pb_QiS2_pzpart, *histpdf_MC_1keV_pzpart),
                                            RooArgList(*N_gammas, *N_210Pb_Qo, *N_210Pb_QiS1, *N_210Pb_QiS2, *N_1keV));
            pdf_bg_BDT5         = new RooAddPdf(Form("pdf_bg_BDT5_%d_%d", detnum[jdet], timenum[jdet]), "background pdf in BDT5",
                                            RooArgList(*histpdf_MC_gammas_BDT5, *pdf_MC_Pb_Qo_BDT5, *pdf_MC_Pb_QiS1_BDT5, *pdf_MC_Pb_QiS2_BDT5, *histpdf_MC_1keV_BDT5),
                                            RooArgList(*N_gammas, *N_210Pb_Qo, *N_210Pb_QiS1, *N_210Pb_QiS2, *N_1keV));
            pdf_bg_BDT7        = new RooAddPdf(Form("pdf_bg_BDT7_%d_%d", detnum[jdet], timenum[jdet]), "background pdf in BDT7",
                                            RooArgList(*histpdf_MC_gammas_BDT7, *pdf_MC_Pb_Qo_BDT7, *pdf_MC_Pb_QiS1_BDT7, *pdf_MC_Pb_QiS2_BDT7, *histpdf_MC_1keV_BDT7),
                                            RooArgList(*N_gammas, *N_210Pb_Qo, *N_210Pb_QiS1, *N_210Pb_QiS2, *N_1keV));
            pdf_bg_BDT10        = new RooAddPdf(Form("pdf_bg_BDT10_%d_%d", detnum[jdet], timenum[jdet]), "background pdf in BDT10",
                                            RooArgList(*histpdf_MC_gammas_BDT10, *pdf_MC_Pb_Qo_BDT10, *pdf_MC_Pb_QiS1_BDT10, *pdf_MC_Pb_QiS2_BDT10, *histpdf_MC_1keV_BDT10),
                                            RooArgList(*N_gammas, *N_210Pb_Qo, *N_210Pb_QiS1, *N_210Pb_QiS2, *N_1keV));
            pdf_bg_BDT15        = new RooAddPdf(Form("pdf_bg_BDT15_%d_%d", detnum[jdet], timenum[jdet]), "background pdf in BDT15",
                                            RooArgList(*histpdf_MC_gammas_BDT15, *pdf_MC_Pb_Qo_BDT15, *pdf_MC_Pb_QiS1_BDT15, *pdf_MC_Pb_QiS2_BDT15, *histpdf_MC_1keV_BDT15),
                                            RooArgList(*N_gammas, *N_210Pb_Qo, *N_210Pb_QiS1, *N_210Pb_QiS2, *N_1keV));
        }


        RooRealVar* exposure = new RooRealVar(Form("exposure_%d_%d", detnum[jdet], timenum[jdet]),
                                              Form("exposure for det=%d time=%d [kg d]", detnum[jdet], timenum[jdet]), 
                                              vec_exposures[jdet]);
        for(int jMass = 0; jMass < WIMPmasses.size(); jMass++)
        {
            RooDataSet data_cf("data_cf", "weighted Cf data", &data_cf_noweight, *data_cf_noweight.get(), 0, weights_cf[jMass].GetName());
            data_cf.Print("v");

            //***** SIGNAL PDFs*****//
            RooDataHist*     datahist_WIMP_pt           = new RooDataHist(Form("datahist_WIMP_pt_%.2f_GeV_%d_%d", WIMPmasses[jMass], detnum[jdet], timenum[jdet]), "data histogram of Cf", RooArgSet(pt), data_cf);
            RooDataHist*     datahist_WIMP_qimean       = new RooDataHist(Form("datahist_WIMP_qimean_%.2f_GeV_%d_%d", WIMPmasses[jMass], detnum[jdet], timenum[jdet]), "data histogram of Cf", RooArgSet(qimean), data_cf);
            RooDataHist*     datahist_WIMP_ptVqimean    = new RooDataHist(Form("datahist_WIMP_ptVqimean_%.2f_GeV_%d_%d", WIMPmasses[jMass], detnum[jdet], timenum[jdet]), "data histogram of Cf", RooArgSet(pt, qimean), data_cf);
            RooDataHist*     datahist_WIMP_prpart       = new RooDataHist(Form("datahist_WIMP_prpart_%.2f_GeV_%d_%d", WIMPmasses[jMass], detnum[jdet], timenum[jdet]), "data histogram of Cf", RooArgSet(prpart), data_cf);
            RooDataHist*     datahist_WIMP_pzpart       = new RooDataHist(Form("datahist_WIMP_pzpart_%.2f_GeV_%d_%d", WIMPmasses[jMass], detnum[jdet], timenum[jdet]), "data histogram of Cf", RooArgSet(pzpart), data_cf);
            RooDataHist*     datahist_WIMP_BDT5         = new RooDataHist(Form("datahist_WIMP_BDT5_%.2f_GeV_%d_%d", WIMPmasses[jMass], detnum[jdet], timenum[jdet]), "data histogram of Cf", RooArgSet(BDT5), data_cf);
            RooDataHist*     datahist_WIMP_BDT7         = new RooDataHist(Form("datahist_WIMP_BDT7_%.2f_GeV_%d_%d", WIMPmasses[jMass], detnum[jdet], timenum[jdet]), "data histogram of Cf", RooArgSet(BDT7), data_cf);
            RooDataHist*     datahist_WIMP_BDT10        = new RooDataHist(Form("datahist_WIMP_BDT10_%.2f_GeV_%d_%d", WIMPmasses[jMass], detnum[jdet], timenum[jdet]), "data histogram of Cf", RooArgSet(BDT10), data_cf);
            RooDataHist*     datahist_WIMP_BDT15        = new RooDataHist(Form("datahist_WIMP_BDT15_%.2f_GeV_%d_%d", WIMPmasses[jMass], detnum[jdet], timenum[jdet]), "data histogram of Cf", RooArgSet(BDT15), data_cf);
            RooHistPdf*      histpdf_WIMP_pt            = new RooHistPdf(Form("histpdf_WIMP_pt_%.2f_GeV_%d_%d", WIMPmasses[jMass], detnum[jdet], timenum[jdet]), "WIMP PDF in pt", RooArgSet(pt), *datahist_WIMP_pt, 0);
            RooHistPdf*      histpdf_WIMP_qimean        = new RooHistPdf(Form("histpdf_WIMP_qimean_%.2f_GeV_%d_%d", WIMPmasses[jMass], detnum[jdet], timenum[jdet]), "WIMP PDF in qimean", RooArgSet(qimean), *datahist_WIMP_qimean, 0);
            RooHistPdf*      histpdf_WIMP_ptVqimean     = new RooHistPdf(Form("histpdf_WIMP_ptVqimean_%.2f_GeV_%d_%d", WIMPmasses[jMass], detnum[jdet], timenum[jdet]), "WIMP PDF in qimean", RooArgSet(pt, qimean), *datahist_WIMP_ptVqimean, 0);
            RooHistPdf*      histpdf_WIMP_prpart        = new RooHistPdf(Form("histpdf_WIMP_prpart_%.2f_GeV_%d_%d", WIMPmasses[jMass], detnum[jdet], timenum[jdet]), "WIMP PDF in prpart", RooArgSet(prpart), *datahist_WIMP_prpart, 0);
            RooHistPdf*      histpdf_WIMP_pzpart        = new RooHistPdf(Form("histpdf_WIMP_pzpart_%.2f_GeV_%d_%d", WIMPmasses[jMass], detnum[jdet], timenum[jdet]), "WIMP PDF in pzpart", RooArgSet(pzpart), *datahist_WIMP_pzpart, 0);
            RooHistPdf*      histpdf_WIMP_BDT5          = new RooHistPdf(Form("histpdf_WIMP_BDT5_%.2f_GeV_%d_%d", WIMPmasses[jMass], detnum[jdet], timenum[jdet]), "WIMP PDF in BDT", RooArgSet(BDT5), *datahist_WIMP_BDT5, 0);
            RooHistPdf*      histpdf_WIMP_BDT7          = new RooHistPdf(Form("histpdf_WIMP_BDT7_%.2f_GeV_%d_%d", WIMPmasses[jMass], detnum[jdet], timenum[jdet]), "WIMP PDF in BDT", RooArgSet(BDT7), *datahist_WIMP_BDT7, 0);
            RooHistPdf*      histpdf_WIMP_BDT10         = new RooHistPdf(Form("histpdf_WIMP_BDT10_%.2f_GeV_%d_%d", WIMPmasses[jMass], detnum[jdet], timenum[jdet]), "WIMP PDF in BDT", RooArgSet(BDT10), *datahist_WIMP_BDT10, 0);
            RooHistPdf*      histpdf_WIMP_BDT15         = new RooHistPdf(Form("histpdf_WIMP_BDT15_%.2f_GeV_%d_%d", WIMPmasses[jMass], detnum[jdet], timenum[jdet]), "WIMP PDF in BDT", RooArgSet(BDT15), *datahist_WIMP_BDT15, 0);

            //***** SET WIMP RATE PARAMETERS *****//            
            RooRealVar* norm_pdf_WIMP = new RooRealVar(Form("norm_pdf_WIMP_%.2f_GeV_%d_%d", WIMPmasses[jMass], detnum[jdet], timenum[jdet]),
                                                    Form("normalization (%.2f GeV WIMP) for det=%d time=%d", WIMPmasses[jMass], detnum[jdet], timenum[jdet]), 
                                                    datahist_WIMP_pt->sumEntries());
            RooFormulaVar* N_WIMP = new RooFormulaVar(Form("N_WIMP_%.2f_GeV_%d_%d", WIMPmasses[jMass], detnum[jdet], timenum[jdet]), 
                                                    Form("relative_cross_sec * exposure_%d_%d * norm_pdf_WIMP_%.2f_GeV_%d_%d", detnum[jdet], timenum[jdet], WIMPmasses[jMass], detnum[jdet], timenum[jdet]),
                                                    RooArgList(*relative_cross_sec, *exposure, *norm_pdf_WIMP));

            std::cout << "sum of datahist_WIMP_pt entries = " << datahist_WIMP_pt->sumEntries() << std::endl;

            //***** COMBINE PDFs *****//
            RooAddPdf*       pdf_total_pt       = new RooAddPdf(Form("pdf_total_pt_%.2f_GeV_%d_%d", WIMPmasses[jMass], detnum[jdet], timenum[jdet]), "total pdf in pt",
                                                        RooArgList(*pdf_bg_pt, *histpdf_WIMP_pt),
                                                        RooArgList(*N_bg, *N_WIMP));
            RooAddPdf*       pdf_total_qimean   = new RooAddPdf(Form("pdf_total_qimean_%.2f_GeV_%d_%d", WIMPmasses[jMass], detnum[jdet], timenum[jdet]), "total pdf in qimean",
                                                        RooArgList(*pdf_bg_qimean, *histpdf_WIMP_qimean),
                                                        RooArgList(*N_bg, *N_WIMP));
            RooAddPdf*       pdf_total_ptVqimean   = new RooAddPdf(Form("pdf_total_ptVqimean_%.2f_GeV_%d_%d", WIMPmasses[jMass], detnum[jdet], timenum[jdet]), "total pdf in qimean",
                                                        RooArgList(*pdf_bg_ptVqimean, *histpdf_WIMP_ptVqimean),
                                                        RooArgList(*N_bg, *N_WIMP));
            RooAddPdf*       pdf_total_prpart   = new RooAddPdf(Form("pdf_total_prpart_%.2f_GeV_%d_%d", WIMPmasses[jMass], detnum[jdet], timenum[jdet]), "total pdf in prpart",
                                                        RooArgList(*pdf_bg_prpart, *histpdf_WIMP_prpart),
                                                        RooArgList(*N_bg, *N_WIMP));
            RooAddPdf*       pdf_total_pzpart   = new RooAddPdf(Form("pdf_total_pzpart_%.2f_GeV_%d_%d", WIMPmasses[jMass], detnum[jdet], timenum[jdet]), "total pdf in pzpart",
                                                        RooArgList(*pdf_bg_pzpart, *histpdf_WIMP_pzpart),
                                                        RooArgList(*N_bg, *N_WIMP));
            // sloppy coding... should have made the BDTs a loop :(
            RooAddPdf* pdf_total_BDT;
            if(WIMPmasses[jMass] == 5.0)
                pdf_total_BDT   = new RooAddPdf(Form("pdf_total_BDT_%.2f_GeV_%d_%d", WIMPmasses[jMass], detnum[jdet], timenum[jdet]), "total pdf in BDT5",
                                        RooArgList(*pdf_bg_BDT5, *histpdf_WIMP_BDT5),
                                        RooArgList(*N_bg, *N_WIMP));
            else if(WIMPmasses[jMass] == 7.0)
                pdf_total_BDT   = new RooAddPdf(Form("pdf_total_BDT_%.2f_GeV_%d_%d", WIMPmasses[jMass], detnum[jdet], timenum[jdet]), "total pdf in BDT7",
                                        RooArgList(*pdf_bg_BDT7, *histpdf_WIMP_BDT7),
                                        RooArgList(*N_bg, *N_WIMP));
            else if(WIMPmasses[jMass] == 10.0)
                pdf_total_BDT   = new RooAddPdf(Form("pdf_total_BDT_%.2f_GeV_%d_%d", WIMPmasses[jMass], detnum[jdet], timenum[jdet]), "total pdf in BDT10",
                                        RooArgList(*pdf_bg_BDT10, *histpdf_WIMP_BDT10),
                                        RooArgList(*N_bg, *N_WIMP));
            else if(WIMPmasses[jMass] == 15.0)
                pdf_total_BDT   = new RooAddPdf(Form("pdf_total_BDT_%.2f_GeV_%d_%d", WIMPmasses[jMass], detnum[jdet], timenum[jdet]), "total pdf in BDT15",
                                        RooArgList(*pdf_bg_BDT15, *histpdf_WIMP_BDT15),
                                        RooArgList(*N_bg, *N_WIMP));

            simul_pdf_bg[jMass].addPdf(*pdf_total_pt, Form("pt_sample_%d_%d", detnum[jdet], timenum[jdet]));
            simul_pdf_bg[jMass].addPdf(*pdf_total_qimean, Form("qimean_sample_%d_%d", detnum[jdet], timenum[jdet]));
            simul_pdf_bg[jMass].addPdf(*pdf_total_prpart, Form("prpart_sample_%d_%d", detnum[jdet], timenum[jdet]));
            simul_pdf_bg[jMass].addPdf(*pdf_total_pzpart, Form("pzpart_sample_%d_%d", detnum[jdet], timenum[jdet]));
            simul_pdf_bg[jMass].addPdf(*pdf_total_BDT, Form("BDT_sample_%d_%d", detnum[jdet], timenum[jdet]));

            simul_pdf_bg_2d[jMass].addPdf(*pdf_total_ptVqimean, Form("ptVqimean_sample_%d_%d", detnum[jdet], timenum[jdet]));
            simul_pdf_bg_2d[jMass].addPdf(*pdf_total_prpart, Form("prpart_sample_%d_%d", detnum[jdet], timenum[jdet]));
            simul_pdf_bg_2d[jMass].addPdf(*pdf_total_pzpart, Form("pzpart_sample_%d_%d", detnum[jdet], timenum[jdet]));
            simul_pdf_bg_2d[jMass].addPdf(*pdf_total_BDT, Form("BDT_sample_%d_%d", detnum[jdet], timenum[jdet]));
        }
    }



    // print separate file for each WIMP mass
    for(int jMass = 0; jMass < WIMPmasses.size(); jMass++)
    {
        RooWorkspace wSave("wSave","workspace");
        wSave.import(combData);
        wSave.import(simul_pdf_bg[jMass]);
        wSave.writeToFile(Form("LT_data+model_%.2f_GeV.root", WIMPmasses[jMass]));
    }

    for(int jMass = 0; jMass < WIMPmasses.size(); jMass++)
    {
        RooWorkspace wSave("wSave","workspace");
        wSave.import(combData2d);
        wSave.import(simul_pdf_bg_2d[jMass]);
        wSave.writeToFile(Form("LT_data+model_%.2f_2d_GeV.root", WIMPmasses[jMass]));
    }

    simul_pdf_bg_2d[2].Print();

    return 0;
}
