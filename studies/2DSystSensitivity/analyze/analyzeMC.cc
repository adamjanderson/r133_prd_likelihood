#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooAddPdf.h"
#include "RooWorkspace.h"
#include "RooPlot.h"
#include "RooFit.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "TFile.h"
#include "TH1.h"
#include "TString.h"
#include "Math/Interpolator.h"
#include "Math/Functor.h"

#include "TTree.h"
#include "TStyle.h"
#include "TColor.h"
#include "TApplication.h"
#include "TLegend.h"
#include "RooStats/ProfileLikelihoodCalculator.h"
#include "RooStats/LikelihoodInterval.h"
#include "RooStats/LikelihoodIntervalPlot.h"
#include "RooGaussian.h"
#include "RooPoisson.h"
#include "RooPolynomial.h"
#include "RooHistPdf.h"
#include "RooDataHist.h"
#include "RooFitResult.h"
#include "RooExtendPdf.h"
#include "RooBinning.h"
#include "RooCategory.h"
#include "RooSimultaneous.h"
#include "RooFunctor1DBinding.h"
#include "RooMinuit.h"
#include "RooNLLVar.h"
#include "RooMCStudy.h"
#include "Math/PdfFuncMathCore.h"

#include <iostream>
#include <fstream>


void buildLegend(std::string leg_names[], RooPlot* rp)
{
    TLegend* leg = new TLegend(0.6, 0.5, 0.88, 0.88);
    // std::string leg_names[7] = {"data","total background", "gammas", "sidewall surf. ev.", "S1 surf. ev.", "S2 surf. ev.", "1 keV"};
    // std::cout << "# of items = " << rp_1d_BDT10->numItems() << std::endl;
    for (int i=1; i<rp->numItems(); i++)
    {
        TString obj_name=rp->nameOf(i);
        TObject *obj = rp->findObject(obj_name.Data());
        leg->AddEntry(obj,leg_names[i].c_str(),"l");
    }
    leg->Draw();
}

int main(int argc, char *argv[])
{
    // parse arguments
    if(argc != 4)
    {
        std::cout << "Usage: analyzeMC [file name] [WIMP mass] [sigma]" << std::endl;
        return 0;
    }
    char* paramFilename = argv[1];
    double WIMPmass = atof(argv[2]);
    double sigma = atof(argv[3]);

    // preliminaries 
    gStyle->SetTitleSize(0.045, "xyz");
    gStyle->SetLabelSize(0.045, "xyz");
    gStyle->SetOptStat(0);
    TColor *myColor = new TColor(1001, 31./255., 120./255., 180./255.);
    TApplication* rootapp = new TApplication("myApp", &argc, argv);

    // open workspace
    TFile f_workspace(paramFilename);
    if(!f_workspace.IsOpen())
    {
        std::cout << Form("ERROR: Cannot open LT_data+model_%.2f_2d_GeV.root!", WIMPmass) << std::endl;
        return 0;
    }

    // Retrieve workspace from file
    RooWorkspace* w = (RooWorkspace*) f_workspace.Get("wSave");
    RooDataSet* paramData = (RooDataSet*)w->data(Form("fitParData_simul_pdf_bg_2d_%.2f_GeV", WIMPmass));
    RooDataSet* dLLData = (RooDataSet*)w->data("DeltaLLSigData");
    RooRealVar* relative_cross_sec = (RooRealVar*)w->var("relative_cross_sec");
    RooRealVar* relative_cross_secerr = (RooRealVar*)w->var("relative_cross_secerr");
    RooRealVar* N_gammas = (RooRealVar*)w->var("N_gammas_4_1");
    RooRealVar* dll = (RooRealVar*)w->var("dll_nullhypo_relative_cross_sec");

    paramData->Print("v");

    TCanvas* cSigmaDM = new TCanvas("cSigmaDM");
    relative_cross_sec->setRange(0, 1);
    RooPlot* rp_sigmaDM = relative_cross_sec->frame();
    paramData->plotOn(rp_sigmaDM, RooFit::DrawOption("EZ"));
    rp_sigmaDM->SetTitle(Form("#sigma_{true} = %.3f and <#hat{#sigma}> = %.3f", sigma, paramData->mean(*relative_cross_sec)));
    rp_sigmaDM->Draw();
    cSigmaDM->Update();
    cSigmaDM->Print(Form("figures/sigma_M=%.2f_sigma=%.2f.pdf", WIMPmass, sigma), "pdf");


    TCanvas* cNgammas = new TCanvas("cNgammas");
    // relative_cross_sec->setRange(8, 12);
    RooPlot* rp_Ngammas = N_gammas->frame();
    paramData->plotOn(rp_Ngammas);
    rp_Ngammas->Draw();
    cNgammas->Update();


    TH1D* h_UL = new TH1D("h_UL", "", 50, 0., 1.);
    for(int j=0; j < paramData->numEntries(); j++)
    {
        double err = paramData->get(j)->getRealValue("relative_cross_secerr");
        double val = paramData->get(j)->getRealValue("relative_cross_sec");
        h_UL->Fill(val + 1.28*err);
    }
    TCanvas* c_UL = new TCanvas("c_UL");
    h_UL->GetXaxis()->SetTitle("90% CL upper limit on #sigma_{#chiN} [10^{-42} cm^{2}]");
    h_UL->Draw();
    c_UL->Update();
    c_UL->Print(Form("figures/sigmaUL_M=%.2f_sigma=%.2f.pdf", WIMPmass, sigma), "pdf");

    std::vector<double> x;
    std::vector<double> chi2y;
    for(int j=0; j < 1000; j++)
    {
        x.push_back(3.0/1000.0 * (j+1));
        chi2y.push_back(500.*ROOT::Math::chisquared_pdf(x[j] * 2.0, 1) * 0.204 / 2.0);
    }
    TGraph* g_chi2 = new TGraph(1000, &x[0], &chi2y[0]);
    g_chi2->SetLineColor(kRed);
    double q[2];
    double quantiles[3] = {0.16, 0.5, 0.84};
    h_UL->GetQuantiles(3, q, quantiles);
    std::cout << "-1sig, median, +1sig = " << q[0] << ", " << q[1] << ", " << q[2] << std::endl;

    TCanvas* cDLL = new TCanvas("cDLL");
    dll->setRange(-0.1, 5);
    dll->setBins(25);
    RooPlot* rp_DLL = dll->frame();
    dLLData->plotOn(rp_DLL, RooFit::DrawOption("EZ"));
    rp_DLL->Draw();
    rp_DLL->SetTitle("");
    g_chi2->Draw("same");
    cDLL->SetLogy();
    cDLL->Update();
    TLegend* leg = new TLegend(0.6,0.7,0.88,0.88);
    leg->AddEntry(rp_DLL, "Monte Carlo", "EL");
    leg->AddEntry(g_chi2, "half-#chi^{2}(n=1)", "L");
    leg->Draw();
    cDLL->Print(Form("figures/logL_M=%.2f_sigma=%.2f.pdf", WIMPmass, sigma), "pdf");


    rootapp->Run();
    return 0;
}
