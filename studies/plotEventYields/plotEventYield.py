# plotEventYield.py
#
# A simple script for plotting the event yields output by the RooFit fits to
# the signal and control regions.
# 
# Adam Anderson
# adama@mit.edu
# 1 May 2015

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import os

mpl.rcParams['text.latex.preamble'] = [
       r'\usepackage{siunitx}',   # i need upright \micro symbols, but you need...
       r'\sisetup{detect-all}',   # ...this to force siunitx to actually use your fonts
       r'\usepackage{helvet}',    # set the normal font here
       r'\usepackage{sansmath}',  # load up the sansmath so that math -> helvet
       r'\sansmath'               # <- tricky! -- gotta actually tell tex to use!
]


det_string = ['1_1', '4_1', '5_1', '11_1', '12_1', '14_1', '15_1', '15_2']
type_string = ['gammas', '1keV', '210Pb_QiS1', '210Pb_QiS2', '210Pb_Qo']
type_labels = ['gammas', '1keV', '210Pb Qi S1', '210Pb Qi S2', '210Pb Qo']


# define function for reading in fit information
def read_fit_info(region_str):
	# Get the working path so that we can use relative paths every time that we 
	# import data from a text file.
	dir = os.path.dirname(__file__)
	fit_results_path = os.path.join(dir, '../bgOnlyPlots/results/')

	# Load data from the text files produced by RooFit
	control_fit = np.genfromtxt(fit_results_path + 'results_' + region_str + '.txt',
								skip_header=7,
								usecols=(0,1,3),
								dtype=['S100', 'd', 'd'])

	# Reorganize the detector numbers into a form that can easily be plotted
	xvals = np.zeros((len(det_string), len(type_string)))
	yvals = np.zeros((len(det_string), len(type_string)))
	yvalerrors = np.zeros((len(det_string), len(type_string)))
	for jBg in range(len(control_fit)):
		fitvar_split = control_fit[jBg][0].split('_')
		this_det_string = fitvar_split[-2] + '_' + fitvar_split[-1]
		jdet = det_string.index(this_det_string)

		jtype = 0
		for test_type in type_string:
			if test_type in control_fit[jBg][0]:
				jtype = type_string.index(test_type)

		xvals[jdet, jtype] = 10*(jdet+1) + jtype - 2
		yvals[jdet, jtype] = control_fit[jBg][1]
		yvalerrors[jdet, jtype] = control_fit[jBg][2]

	return [xvals, yvals, yvalerrors]


[xvals_control, yvals_control, yvalerrors_control] = read_fit_info('control')
[xvals_signal, yvals_signal, yvalerrors_signal] = read_fit_info('signal')
[xvals_signalcontrol, yvals_signalcontrol, yvalerrors_signalcontrol] = read_fit_info('signal,control')

collist = ['c', 'r', 'g', 'm', (1, 0.4, 0)]
plt.figure(1)
for jtype in range(len(type_string)):
	plt.errorbar(xvals_control[:,jtype], yvals_control[:,jtype], yerr=yvalerrors_control[:,jtype],
				 linestyle='none', capthick=0, elinewidth=2, marker='.', markersize=12, color=collist[jtype],
				 label=type_labels[jtype]) 
for jdet in range(len(det_string)):
	plt.plot([10*(jdet+1) + 5, 10*(jdet+1) + 5], [0, 1000], linewidth=3, color=(0.7, 0.7, 0.7))
plt.xticks(np.linspace(10,80,8), ['T1Z1', 'T2Z1', 'T2Z2', 'T4Z2', 'T4Z3', 'T5Z2', 'T5Z3\n(BS)', 'T5Z3\n(AS)'])
plt.axis([5, 85, 0, 800])
plt.ylabel('counts')
leg = plt.legend(loc=2, fontsize=14)
plt.savefig('bg_rates_' + 'control' + '.pdf')
plt.savefig('bg_rates_' + 'control' + '.png')

plt.axis([5, 85, 1, 800])
leg.remove()
plt.yscale('log')
plt.savefig('bg_rates_' + 'control' + '_log.pdf')
plt.savefig('bg_rates_' + 'control' + '_log.png')



plt.figure(2)
for jtype in range(len(type_string)):
	plt.errorbar(xvals_signalcontrol[:,jtype], yvals_signalcontrol[:,jtype], yerr=yvalerrors_signalcontrol[:,jtype],
				 linestyle='none', capthick=0, elinewidth=2, marker='.', markersize=12, color=collist[jtype],
				 label=type_labels[jtype]) 
for jdet in range(len(det_string)):
	plt.plot([10*(jdet+1) + 5, 10*(jdet+1) + 5], [0, 1000], linewidth=3, color=(0.7, 0.7, 0.7))
plt.xticks(np.linspace(10,80,8), ['T1Z1', 'T2Z1', 'T2Z2', 'T4Z2', 'T4Z3', 'T5Z2', 'T5Z3\n(BS)', 'T5Z3\n(AS)'])
plt.axis([5, 85, 0, 800])
plt.ylabel('counts')
leg = plt.legend(loc=2, fontsize=14)
plt.savefig('bg_rates_' + 'signal,control' + '.pdf')
plt.savefig('bg_rates_' + 'signal,control' + '.png')

plt.axis([5, 85, 1, 800])
leg.remove()
plt.yscale('log')
plt.savefig('bg_rates_' + 'signal,control' + '_log.pdf')
plt.savefig('bg_rates_' + 'signal,control' + '_log.png')



# construct the pulls
pull = (yvals_signal - yvals_control) / np.sqrt(yvalerrors_control**2 + yvalerrors_signal**2)
print pull
plt.figure(3)
for jtype in range(len(type_string)):
	plt.plot(xvals_control[:,jtype], pull[:,jtype],
				 linestyle='none', marker='.', markersize=12, color=collist[jtype],
				 label=type_labels[jtype])
for jdet in range(len(det_string)):
	plt.plot([10*(jdet+1) + 5, 10*(jdet+1) + 5], [-10, 10], linewidth=3, color=(0.7, 0.7, 0.7))
plt.xticks(np.linspace(10,80,8), ['T1Z1', 'T2Z1', 'T2Z2', 'T4Z2', 'T4Z3', 'T5Z2', 'T5Z3\n(BS)', 'T5Z3\n(AS)'])
plt.axis([5, 85, -10, 10])
plt.ylabel('$(\mu_s - \mu_{s+c}) / \sqrt{\sigma_s^2 + \sigma_{s+c}^2}$')
leg = plt.legend(loc=2, fontsize=14)
plt.savefig('bg_rates_' + 'pull' + '.pdf')
plt.savefig('bg_rates_' + 'pull' + '.png')



plt.show()

