% C0.m
% 
% This function computes the function C0, for the particular case of a WIMP
% cross section, as in equation 2 of Yellin's PRD 66, 032005 (2002). This
% gives the probability that the maximal gap is smaller than x, given a
% WIMP-nucleon cross section of sigma. In particular, this is useful for
% computing limits with the maximal gap method.
%
% Adam Anderson
% adama@mit.edu

function CL = C0(xAt42, muAt42, sigma)
    % normalize the parameters to the desired value of sigma
    x = xAt42 * 1e42 * sigma;
    mu = muAt42 * 1e42 * sigma;
    
    % compute some preliminary parameters
    m = floor(mu / x);
    k = 0:m;
    
    % compute C0 itself
    terms = exp(-k*x) .* (k*x - mu).^(k-1) .* (k*(x-1) - mu) ./ gamma(k+1);
    CL = sum(terms);
    
    % if cross section is negative, then CL must be zero to avoid negative
    % cross sections from being the upper limit
    if sigma < 0
        CL = 0;
    end
end