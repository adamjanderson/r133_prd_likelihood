function rate = dr_dpt(pt,A,M,sigma,alpha)

% function rate = dr_dpt(pt,A,M,sigma,alpha)
%
% expected differential recoil spectrum as a function of total phonon
% energy (NOT recoil energy) to be used for propagating systematics in R133
% low-threshold limit calculation
%
% uses drdq_corr to calculate recoil spectrum as a function of recoil
% energy, then converts recoil energy to total phonon energy according to
% the charge model discussed in R133 notes 18-LT and 26-LT
%
% If input alpha is not a vector, uses Lindhard model to compute spectrum
% 
% Inputs: pt - total phonon energy
%         A - mass number of target element
%         M - WIMP mass
%         sigma - spin-independent WIMP-nucleon cross section
%         alpha - 1x4 vector of charge model parameters as discussed in R133
%         notes 18-LT and 26-LT
%
% 2013/11/14 - KES: created
% 2013/11/19 - KES: updated charge model and included handling of
%                   nonphysical energies
% 2013/12/03 - KES: added Lindhard model

if length(alpha)>1
    useLindhard = 0;
else
    useLindhard = 1;
end

deltaV = 4; %iZIP bias voltage in V
eps_eh = 3; %energy in eV needed to create e/h pair
A = 72.64; 
Z = 32;

if useLindhard
    
    E_recoil = pt2prNR_133_lindhard(pt);
    [Y, dY_dEr] = YieldNR_Lindhard(E_recoil,A,Z);
    
    dEr_dpt = 1./(1 + (deltaV/eps_eh)*Y + (deltaV/eps_eh)*E_recoil.*dY_dEr);
    
else

    q = alpha(1) + alpha(2)*pt + 10^(alpha(3))*erf(-pt/(10^alpha(4)));
    dq_dpt = alpha(2) - 2*(10^(alpha(3))*exp(-pt.^2/(10^(2*alpha(4))))/(sqrt(pi)*10^alpha(4)));

    E_recoil = pt - (deltaV/eps_eh)*q;
    E_recoil(E_recoil<0) = 0; % eliminate unphysical recoil energies

    dEr_dpt = 1-(deltaV/eps_eh)*dq_dpt;
end

rate = drdq_corr(E_recoil,A,M,sigma).*dEr_dpt;
rate(E_recoil<0) = 0; % set rate = 0 if E_recoil < 0