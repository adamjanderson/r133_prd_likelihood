function rate = drdq_corr(Q,A,M,sigma)

% function rate = drdq(Q,A,M)
%
% Stripped down version of expected differential rate calculation for
% use within limit code.
%
% The efficiency and day options have been removed because they are not
% needed for this particular aplication
%
% A sigma input has been added such that a variable cross section can
% be specified
%
% 090401 ... RAB
% 100826 ... DCM  Updated to use corrected expression for rate from
%                 R123Note258

Q = Q(:);
dm_params;

mT = mN .* A;                 % mass of target nucleus in GeV
r = 4*M*mT/(M+mT)^2;          %unitless reduced mass parameter
E0 = (1/2)*M*(v0/c)^2*1e6;    %kinetic energy in keV (mx_g in GeV)

F2 = Helm(Q,A);               %Helm (Woods-Saxon) nuclear form factor


% SI cross=section on entire nucleus:
sigmaSI = F2 .* sigma .* (A^2) .* ((mT./(M+mT)).^2) ./ ((mN./(M+mN))).^2;



% Event rate per unit mass for vEarth=0 and vEsc = infinity
% (this is L&S 3.1 but I already put in the nuclear form factor;
% note R0factor includes unit conversions from cm to km and sec to days):
R0factor = 2 * 6.0223e26 / sqrt(pi) * 1e5 * (24*60^2);
R0 = R0factor * sigmaSI * rhoW * v0 / (M * A);

% S&L eqn 2.2 for k0/k1
kRatio = 1./(erf(vEsc./v0) - 2./sqrt(pi).*vEsc./v0.*exp(-(vEsc./v0).^2));

vEarth = vEarthMean;

vMin = v0 .* sqrt(Q./(E0.*r));

% S&L formula 3.13, uses formula 3.12 too
rate1 = kRatio .* ( R0./(E0.*r) .* sqrt(pi)/4 .* v0./vEarth ...
    .* ( erf( (vMin + vEarth)./v0 ) ...
    - erf( (vMin - vEarth)./v0 ) ) ...
    - ( R0./(E0.*r) .* exp( -(vEsc./v0).^2 ) ) );


rate2 = kRatio .* ( R0./(E0.*r) .* sqrt(pi)/4 .* v0./vEarth ...
    .* ( erf( (vEsc)./v0 ) ...
    - erf( (vMin - vEarth)./v0 ) ) ...
    - ( R0./(E0.*r) .* ( (vEsc + vEarth - vMin) / (2*vEarth) ) .* exp( -(vEsc./v0).^2 ) ) );


rate = zeros(size(Q));
rate(vMin < vEsc-vEarth & vMin > 0) = rate1(vMin < vEsc-vEarth & vMin > 0);
rate(vMin > vEsc-vEarth & vMin < vEsc + vEarth) = rate2(vMin > vEsc-vEarth & vMin < vEsc + vEarth);

