%function [UL90C0, UL90C0bs, UL90Cmax] =  FindubUL(Events,dRdQ,Qmin,Qmax,...
%                      kgdays,tol,dispflag,BackSpecFuncHandle,Noff);
%function [UL90C0, UL90Cmax] =  FindubUL(Events,dRdQ,Qmin,Qmax,...
%                      kgdays,tol,dispflag);
%
% Finds 90% CL upper limit on the normalization of the differential
% spectrum dRdQ over the range [Qmin,Qmax] consistent with the observed
% event energies in Events.  This upper limit is defined as that value
% which would result in a region of largest relative deficit of events
% "maximal gap") 10% of the time.  Uses Steve Yellin's unbinned upper-limit
% method.  This method can allow background substraction (with assumed
% negligible uncertainty only), but this is not yet coded.  The Cmax method
% can be run only for a single detector at a time.
%
%       Events  = matrix (i,j) containing the recoil energy of each event i
%                 detected in detector j
%       dRdQ    = function handle giving differential event spectrum
%                 (including detector efficiency)
%       Qmin    = minimum recoil energy considered (in keV) for detector j
%       Qmax    = maximum recoil energy considered (in keV) for detector j
%       kgdays  = mass of detector j (in kg) x number of days live x
%                 efficiency for detector j (not counting energy-dependent
%                 contribution included in effFuncName, if applicable)
%       tol     = relative precision of answer (0.1 gives 10% accuracy)
%       dispflag= 1 to display verbose output (default = 0)
%       BackSpecFuncHandle = array of function handles to be used to
%                 calculate the expected background as a function of
%                 energy
%       Noff    = Number of "off-source" background events (multiple neutrons)
%
%   Created by R. Schnee based on work by S. Yellin
%   061028  J. Filippini    Reorganized to isolate WIMP-specific code from
%                           limit-setting code
%   070220  J. Filippini    Replaced calls to eval with function handles

function [UL90C0, UL90C0bs, UL90Cmax] = ...
    FindubUL(Events,dRdQ,Qmin,Qmax,kgdays,tol,dispflag, ...
    BackSpecFuncHandle,Nback);

global MuAt42 Xat42 Xmaxat42 Mub1 Xb1 Noff minMub maxMub
global mx_g A_g FunctionName PrintFlag ConfCoef effFuncName_g

if nargin<7
    PrintFlag = 0;
else
    PrintFlag = dispflag;
end
%ConfCoef = 0.10;                  % 1 - confidence level
ConfCoef = 0.1;                  % 1 - confidence level

% Check number of detectors is same for all input variables:
tempSizes = ([ length(Qmin), length(Qmax), length(kgdays), ...
    size(Events,2) ]);
if max(tempSizes)~= min(tempSizes)
    disp('All variables must be filled for the same number of detectors.');
    disp('(this includes Qmin, Qmax, kgdays, Events)');
    return;
else
    nDet = tempSizes(1);				% number of detectors
end

MuAt42= zeros(1,nDet);
Xat42 = zeros(size(Events,1)+1,nDet);
Xmaxat42 = zeros(1,nDet);
Mub1     = zeros(1,nDet);
Xb1      = zeros(size(Events,1)+1,nDet);

for j=1:nDet

    if nargin<8
        BackSpecFun = [];
    elseif size(BackSpecFuncName,1)==1
        BackSpecFun = BackSpecFuncHandle;
    elseif size(BackSpecFuncName,1)==nDet
        BackSpecFun = BackSpecFuncHandle(j,:);
    else
        disp('Need BackSpecFuncHandle for each detector, or 1 for all detectors,');
        disp(' or none');
        return
    end

    %Put sorted energies of all events into vector SortedEvents.  The bin edges
    %for this method are placed at each event's energy as well as the lowest
    %and highest energies the user wants to consider:

    SortedEvents=sort(Events(:,j));
    SortedEvents=SortedEvents(SortedEvents>Qmin(j) & SortedEvents<Qmax(j));
    SortedEvents = [Qmin(j); SortedEvents; Qmax(j)];
    Nevents=length(SortedEvents);

    disp('Performing initial numerical integrations');
    MuAt42(j) = quad(dRdQ,Qmin(j),Qmax(j)) * kgdays(j);
    if ~isempty(BackSpecFun)
        %eval(['Mub1(j) = ' BackSpecFun '(Qmin(j),Qmax(j));' ]);
        Mub1(j) = BackSpecFun (Qmin(j),Qmax(j));
    end

    %For each resulting energy bin, find the expected number of events,
    %both for signal (for sigma=10^-42 cm^2) and for background (for mub=1):
    for i=1:Nevents-1
        Xat42(i,j) = quad(dRdQ,SortedEvents(i),SortedEvents(i+1))*kgdays(j);
    end
    if ~isempty(BackSpecFun)
        for i=1:Nevents-1
            %eval(['Xb1(i,j)= ' BackSpecFun '(SortedEvents(i),SortedEvents(i+1));' ]);
            Xb1(i,j)= BackSpecFun (SortedEvents(i),SortedEvents(i+1));
        end
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % METHOD C0
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %We need examine only the bin with the largest number of expected events;
    %by the placement of the binning, this is automatically the bin with the
    %most constraining deficit of events relative to the number expected.
    %Steve Y. likes to call this the "maximal gap."

    [Xmaxat42(j), imax] = max(Xat42(:,j),[],1);
    disp(['maximal gap between events at ' num2str(SortedEvents(imax)) ...
        ' and ' num2str(SortedEvents(imax+1)) ' keV']);

end

%Now call root-finding algorithm with the function that yields the CL for
% this method:

%FunctionName = 'C0_analytic';       % no subtraction
CLm10 = @(x) C0_analytic(x)-ConfCoef;
x1 = 1e-5; %0.1;
x2= 1; %1000;
UL90C0 = FindRoot(CLm10,x1,x2,tol);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Method C0 with background subtraction if desired by user:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin==9 & nargout==3
    % fill globals needed by called functions:
    Noff = Nback;
    if Noff==0
        minMub = 0;
    else
        %       minMub = (1e-6*gamma(Noff+1))^(1/Noff);
        minMub = exp( (gammaln(Noff+1)-13) / Noff );
    end
    maxMub = (Noff+1) + 5*sqrt(Noff+1);
    %FunctionName = 'C0_int_mub';       % no subtraction
    CLm10 = @(x) C0_int_mub(x)-ConfCoef;
    %   x1 = 0.1;
    %   x2=1000;
    UL90C0bs = FindRoot(CLm10,x1,x2,tol);
end

if (nargin==9&nargout==3) | (nargin<8&nargout==2)

    % METHOD CMax

    %WARNING THIS METHOD CURRENTLY CAN WORK ONLY FOR A SINGLE DETECTOR AT A
    %TIME  -- talk to Steve Y. about changing Cmax code or do it yourself!

    %Now it's the same except we divide bins into how many events they have in
    %them (and we no longer constrain ourselves only to empty bins)
    %We need examine only the bin with the largest number of expected events,
    %given an actual number m, for each value of m.
    %By the placement of the binning, this is automatically the bin with the
    %most constraining deficit of events relative to the number expected.

    Xtemp = Xat42;
    Xmaxat42 = zeros(Nevents-2,1);
    for m=0:Nevents-2			% number of events in gap
        Xtempm = zeros(Nevents-1-m,1);
        for i=1:Nevents-1-m	      % index of lowest gap with which to start
            Xtempm(i) = sum(Xtemp(i:i+m));
        end
        [Xmaxat42(m+1), imax] = max(Xtempm);
        disp(['maximal ' int2str(m) '-event gap between events at ' ...
            num2str(SortedEvents(imax)) ...
            ' and ' num2str(SortedEvents(imax+1+m)) ' keV (' ...
            num2str(Xmaxat42(m+1)) ' of ' num2str(MuAt42) ')']);
    end
    % Correct for possible roundoff error in the integrating:
    Xmaxat42(Xmaxat42>MuAt42) = MuAt42;

    %Now call root-finding algorithm with the function that yields the CL for
    % this method:

    %x1 = 0.1;
    %x2=1000;
    x1 = UL90C0*0.1;
    x2 = UL90C0*10;
    UL90Cmax = FindRoot(@Cmax,x1,x2,tol);

else
    UL90Cmax = [];
end

% deal with output arguments if not all assigned
if (nargin<8&nargout==2)
    UL90C0bs = UL90Cmax;
end