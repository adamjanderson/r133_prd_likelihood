function cmax90mc = Cmax90(mu)
% function cmax90 = CMax90(mu)
% Finds the maximum C_m at the 90% CL as a function of the mean, mu.
% rws: I have removed Steve's standard fo the following:
% A return value of -1.0 means that the function could not be evaluated.
global CMaxval muval

NCMaxmu=70; % length(muval)

if mu<muval(1)
    cmax90mc = 0.9;
    return;
elseif mu>muval(NCMaxmu)
    cmax90mc=CMaxval(NCMaxmu);
    disp(['WARNING: too many events expected in region (' ...
        num2str(mu) ')' ]);
    return;
end

% Find closest tabulated muval's:
toolow  = find(muval<=mu);
toohigh = find(muval>mu);

Low  = toolow(length(toolow));
High = toohigh(1);

%  interpolate.
cmax90mc = CMaxval(Low) + (mu-muval(Low))* ...
    (CMaxval(High)-CMaxval(Low))/(muval(High)-muval(Low));

