function CL= PbackC0(mub);
%function CL= PbackC0(mub);
%
%function returns confidence level of a given expected "off-source" 
%(multiple neutron) background mub, multiplied by the expected probability 
%of mub, based on global variables 
%        sigma_g the WIMP-nucleon cross-section  to be considered
%        MuAt42  the expected number of WIMP events in the entire region 
%                of interest for a wimp-nucleon cross-section of 10^-42 cm^2
%        Xat42   a matrix (i,j) giving the number of WIMP events expected in
%                gap i for detector j for a cross-section = 10^-42 cm^2
%        Mub1    the expected number of background events (single neutrons)
%                in the entire region of detector j, for mub=1
%        Xb1     a matrix (i,j) giving the number of events expected in gap i
%                for detector j, for mub=1
%        Noff    number of "off-source" background events (multiple neutrons)
%
% rws 99 May 11 based on Steve Yellin's CDMS memo, and C0_analytic.m
% rws 02 Jun 10 changed to use gammaln to prevent underflows
global sigma_g MuAt42 Xat42 Mub1 Xb1 Noff PrintFlag PrintFlag2

if isempty(PrintFlag2)
   PrintFlag2 = 0;
end

% keep any search algorithms from checking for negative cross-sections:
if sigma_g<0
  CL = -sigma_g*ones(size(mub));
  if PrintFlag
     disp(['confidence coefficient set to ' num2str(CL) ' for sigma_g=' ... 
            num2str(sigma_g) ]);
  end
  return
end

% need to return a vector out for a vector in, so do in loops:
nloops = length(mub);
for iloop = 1:nloops

   mu  = MuAt42 * sigma_g + Mub1*mub(iloop);  % vector over detector number
   xes = Xat42  * sigma_g +  Xb1*mub(iloop);  % matrix i,j gap,detector
   x = max(xes,[],1);                         % maximum gap per detector
   x(x>mu) = mu(x>mu);     % protects against round-off in numerical integration
   m = floor(mu./x);
   nDet = length(m);


   CL1 = zeros(1,nDet);
   for j=1:nDet
      %some limits suggested by Steve to keep this from crashing due to
      % roundoff error or overflows:
      if ( ( x(j)<0.03*mu(j) & mu(j)<60 ) | ( x(j)<1.8 & mu(j)>60 ) | ...
	( x(j)<4 & mu(j)>700 ) )
        CL1(j)=1;
	if PrintFlag
	    disp(['sigma_g=' num2str(sigma_g) ' sets CL1 to one for mu=' ...
		num2str(mu(j)) ...
		' and x=' num2str(x(j)) ]);
	end
      else
        k = 0:m(j);
        terms = exp(-k*x(j)) .* ( (k*x(j)-mu(j)).^k - ...
    			k.*(k*x(j)-mu(j)).^(k-1) ) ./ gamma(k+1);
%terms = (k*x-mu).^k  .* exp(-k*x) .* (1+ k./(mu-k*x) )./ gamma(k+1);
        CL1(j) = 1 - sum(terms(isfinite(terms)));
        if PrintFlag2
   	    disp(['sigma_g=' num2str(sigma_g) ', mub =' num2str(mub) ...
                  ' yields CL1=' num2str(CL1(j)) ]);
            if CL1(j)==0
                disp(['mu(j)=' num2str(mu(j)) ', x(j)=' num2str(x(j))]);
            end
        end
      end
   end      % loop over detector

   %combine individual confidence levels using incomplete gamma function:
   CL1(CL1<0 | CL1>1) = ones(1,sum(CL1<0 | CL1>1));
   prodCL = prod(CL1);
%   probmub = exp(-mub(iloop))*mub(iloop)^Noff/gamma(Noff+1);
   probmub = exp( -mub(iloop) + Noff*log(mub(iloop)) - gammaln(Noff+1) );
   if prodCL<=0
     CL(iloop) = 0;
   elseif mub(iloop)<=0 & Noff>0
     CL(iloop) = 0;
   elseif mub(iloop)<=0
     CL(iloop) = 1-gammainc(-log(prodCL),nDet);
   else
     CL(iloop) = 1-gammainc(-log(prodCL) + gammaln(Noff+1) ...
                            +mub(iloop)-Noff*log(mub(iloop)),nDet);
%     CL(iloop) = 1-gammainc(-log(prodCL/gamma(Noff+1)) ...
%                            +mub(iloop)-Noff*log(mub(iloop)),nDet);
   end
   if PrintFlag2
       disp(['probmub=' num2str(probmub) ', yielding CL=' num2str(CL(iloop))]);
   end
end    % loop over mub value

