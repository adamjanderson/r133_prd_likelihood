% ParseCLtable;
% Input the table.  xcl(m,J,I) is the maximum interval containing m points
% corresponding to mu=mean(I) and confidence level CL(J).  If Nxcl(m,I)=0,
% then the m is so large compared with mu that one hardly ever gets m
% events; so if x<mu, the confidence level is very small.  I assume that if
% xcl(m,J,I) is meaningful then so is xcl(m,J,I+1) and xcl(m,J-1,I)
% (unless J=1).

% N=50, Nmeans=41, NCLs=22, NCMaxmu=70
disp(['N=' int2str(N) ', Nmeans=' int2str(Nmeans) ', NCLs=' int2str(NCLs) ...
	', NCMaxmu =' int2str(NCMaxmu) ]);
muval=zeros(NCMaxmu,1);
CMaxval=zeros(NCMaxmu,1);
CL=zeros(NCLs,1);
meanv=zeros(Nmeans,1);
Nxcl=zeros(N+1,Nmeans);
xcl=zeros(N+1,NCLs,Nmeans);
meanlog=zeros(Nmeans,1);

eolstr=char(10);
disp('Reading CLtable.txt');
fidread = fopen('CLtable.txt','r');
[f1,count] = fread(fidread, inf);	
fclose(fidread);
fstr = char(f1)';
clear f1

% Read first line: muval
[str1,fstr] = strtok(fstr,eolstr);
disp([str1]);
for i=1:70
	[str2,str1] = strtok(str1);					
	muval(i) = str2num(str2);
end

% Read second line: CMaxval
[str1,fstr] = strtok(fstr,eolstr);
for i=1:70
	[str2,str1] = strtok(str1);					
	CMaxval(i) = str2num(str2);
end

% Read third line: CL
[str1,fstr] = strtok(fstr,eolstr);
disp([str1]);
for i=1:22
	[str2,str1] = strtok(str1);					
	CL(i) = str2num(str2);
end

for i=1:Nmeans
	[str1,fstr] = strtok(fstr,eolstr);
	[str2,str1] = strtok(str1);						
	meanv(i) = str2num(str2);
	for k=1:N+1
		[str2,str1] = strtok(str1);				
		Nxcl(k,i) = str2num(str2);  %note offset of 1 from fortran!!!!
	end
    	for K=1:N+1
        	if(Nxcl(K,i)>0) 
			[str1,fstr] = strtok(fstr,eolstr);
			for J=1:Nxcl(K,i)
				[str2,str1] = strtok(str1);			
				xcl(K,J,i) = str2num(str2); 
			end
		end
 	end
   	meanlog(i)=log(meanv(i));
end       	
