function s = gauss_smear(x,f,res)
% 
% Convolve a function with a gaussian of given width
%
% Input arguments:
%   x - values at for which the input function is sampled
%   f - values of the input function at x
%   res - constant width of the gaussian to smear with
%
% Output arguments:
%   s - smeared function, sampled at the values of x
%
%
%  2010/08/26 DCM

RES = 1e5; %number of points to use in convolution
GAUSS_WIDTH = 10; %width of gaussian to use (in sigma)

%first resample x to be equally spaced

x2 = linspace(min(x),max(x),RES);
spacing = mean(diff(x2));
f2 = interp1(x,f,x2);

gs = normpdf(-GAUSS_WIDTH*res:spacing:GAUSS_WIDTH*res,0,res);

sce = conv(f2,gs) * spacing;
conve = (-GAUSS_WIDTH*res):spacing:(GAUSS_WIDTH*res+(x2(end)-x2(1)));

%reinterp back to initial x
s = interp1(conve,sce,x);
   