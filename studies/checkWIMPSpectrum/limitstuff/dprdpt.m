% jacobian for julien's charge models

function jacobian = dprdpt(pt, detnum)
    load('MCMC_bestfit_charge_parameters_v53_LT');
    alpha = alph_bestfit{detnum-1100};

    dV = 4.0;
    weh = 3.0;
    
    jacobian = 1 - dV / weh * (alpha(2) - ((10^alpha(3)) / (10^alpha(4))) * ...
                2 / sqrt(pi) * exp(-1*(pt / (10^alpha(4))).^2));
end