function sigma_UL = UL_maximal_gap(evt_energies, exp_energy_bin_edges, exp_vals, range, m_WIMP, conf_level)
    %%%% PRELIMINARY STUFF %%%%
    edges = sort([range(1), evt_energies(inrange(evt_energies, range(1), range(2))), range(2)]);
    x_at42 = zeros(1,length(evt_energies)+1);
    dRdQ_at42 = @(Er) drdq_corr_Ge(Er, m_WIMP, 1e-42);

    
    %%%% FIND MAXIMAL GAP %%%%
    % compute x_i for each gap assuming a cross section of 10^-42 cm^2
    for jEdge = 1:length(x_at42)
        % need to sub-bin the gaps between events to be able to apply the
        % binned exposures
        exp_edges_in_xbin = [edges(jEdge); ...
                             exp_energy_bin_edges(exp_energy_bin_edges < edges(jEdge+1) & ...
                                                  exp_energy_bin_edges > edges(jEdge)); ...
                             edges(jEdge+1)];
        exp_vals_in_xbin = exp_vals((exp_energy_bin_edges < edges(jEdge+1) & ...
                                    exp_energy_bin_edges > edges(jEdge)) | ...
                                    exp_energy_bin_edges == max(exp_energy_bin_edges(exp_energy_bin_edges <= edges(jEdge))));
                                
        for jEffEdge = 1:(length(exp_edges_in_xbin)-1)
            x_at42(jEdge) = x_at42(jEdge) + ...
                            exp_vals_in_xbin(jEffEdge) * ...
                            quad(dRdQ_at42, exp_edges_in_xbin(jEffEdge), exp_edges_in_xbin(jEffEdge+1));
        end;
    end
    
    % find the maximal gap
    x_max_at42 = max(x_at42);
    
    % find mu for the entire rate at 1e-42 cm^2
    mu_at42 = sum(x_at42);
    
    
    %%%% SOLVE FOR THE LIMIT %%%%
    % set up the C0 function
    C0_function = @(sigma) -conf_level + C0(x_max_at42, mu_at42, sigma);
    
    % solve for sigma at this CL
    sigma_UL = fzero(C0_function, 1e-42);
    
    
    %%%% PRINT OUTPUT %%%%
    disp([num2str(conf_level) ' UL at ' num2str(m_WIMP) ' = ' num2str(sigma_UL) ' cm^2'])
end