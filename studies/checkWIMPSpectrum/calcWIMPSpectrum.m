% calcWIMPSpectrum.m
%
% This short script calculates the WIMP energy spectrum in total phonon
% energy, corrected for efficiency, on each detector. The purpose of this
% is to provide a cross-check of the signal pdf in RooFit.
%
% Adam Anderson
% adama@mit.edu
% 15 May 2015

function calcWIMPSpectrum()
    close all;
    
    WIMPmass = 10;
    detnum = [1101 1104 1105 1111 1112 1114 1115];
    ntimes = [1 1 1 1 1 2 2];
    exposure = {80.21, 82.92, 80.89, 87.40, 83.8, [38.80,95.67], [18.69, 60.55]};
    addpath('limitstuff');
    
    for jdet = 1:length(detnum)
        for jtime = 1:ntimes(jdet)
            load('MCMC_bestfit_charge_parameters_v53_LT');
            alpha = alph_bestfit{detnum(jdet)-1100};
            pt = linspace(2,13,1000);
            drdpt = dr_dpt(pt', 72.64, WIMPmass, 1e-42, alpha);
            if(detnum(jdet) == 1114)
                eff = combined_eff_unbinned_v53_LT(detnum(jdet), pt, 1, 'fullconsistency',3)';
            else
                eff = combined_eff_unbinned_v53_LT(detnum(jdet), pt, jtime, 'fullconsistency',3)';
            end

            figure(10*jdet + jtime);
            plot(pt, drdpt * exposure{jdet}(jtime), 'b');
            hold on;
            plot(pt, drdpt .* eff * exposure{jdet}(jtime), 'r');
            hold off;
            xlim([2, 13]);
            
            spectrum = drdpt .* eff * exposure{jdet}(jtime);
            disp([num2str(detnum(jdet)) ': time ' num2str(jtime)])
            sum(spectrum) * (pt(2)-pt(1))
            datamat = [pt', spectrum];
            dlmwrite(['WIMPspectrum_' num2str(detnum(jdet)) '_' num2str(jtime) '.txt'], datamat, '\t');
        end;
    end
end