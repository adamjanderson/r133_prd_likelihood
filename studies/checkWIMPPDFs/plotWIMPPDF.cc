#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooAddPdf.h"
#include "RooAbsReal.h"
#include "RooWorkspace.h"
#include "RooPlot.h"
#include "RooFit.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "TFile.h"
#include "TH1.h"
#include "TString.h"
#include "Math/Interpolator.h"
#include "Math/Functor.h"

#include "TTree.h"
#include "TStyle.h"
#include "TColor.h"
#include "TApplication.h"
#include "TLegend.h"
#include "RooStats/ProfileLikelihoodCalculator.h"
#include "RooStats/LikelihoodInterval.h"
#include "RooStats/LikelihoodIntervalPlot.h"
#include "RooGaussian.h"
#include "RooPoisson.h"
#include "RooPolynomial.h"
#include "RooHistPdf.h"
#include "RooDataHist.h"
#include "RooFitResult.h"
#include "RooExtendPdf.h"
#include "RooBinning.h"
#include "RooCategory.h"
#include "RooSimultaneous.h"
#include "RooFunctor1DBinding.h"
#include "RooMinuit.h"
#include "RooNLLVar.h"

#include <iostream>
#include <fstream>


void buildLegend(std::string leg_names[], RooPlot* rp)
{
    TLegend* leg = new TLegend(0.6, 0.5, 0.88, 0.88);
    // std::string leg_names[7] = {"data","total background", "gammas", "sidewall surf. ev.", "S1 surf. ev.", "S2 surf. ev.", "1 keV"};
    // std::cout << "# of items = " << rp_1d_BDT10->numItems() << std::endl;
    for (int i=1; i<rp->numItems(); i++)
    {
        TString obj_name=rp->nameOf(i);
        TObject *obj = rp->findObject(obj_name.Data());
        leg->AddEntry(obj,leg_names[i].c_str(),"l");
    }
    leg->Draw();
}

int main(int argc, char *argv[])
{
    gStyle->SetTitleSize(0.045, "xyz");
    gStyle->SetLabelSize(0.045, "xyz");
    TColor *myColor = new TColor(1001, 31./255., 120./255., 180./255.);
    TApplication* rootapp = new TApplication("myApp", &argc, argv);

    bool bgOnlyFit = true;
    bool sigBgFit = false;

    int mindet = 0;
    int maxdet = 8;
    std::string detNames[] = {"T1Z1", "T2Z1", "T2Z2", "T4Z2", "T4Z3", "T5Z2 (w/QOS1)", "T5Z2 (w/o QOS1)", "T5Z3 (BS)", "T5Z3 (AS)"};
    std::string leg_names_bg[8] = {"data","total background", "gammas", "sidewall surf. ev.", "S1 surf. ev.", "S2 surf. ev.", "1 keV", "WIMP"};
    int detnum[] = {1, 4, 5, 11, 12, 14, 14, 15, 15};
    int timenum[] = {1, 1, 1, 1, 1, 1, 2, 1, 2};

    double WIMPmass = 10.;

    
    TFile f_workspace(Form("LT_data+model_%.2f_GeV.root", WIMPmass));

    // Retrieve workspace from file
    RooWorkspace* w = (RooWorkspace*) f_workspace.Get("wSave");


    //***** GET DATA + MODEL FROM FILE *****//
    RooDataSet* combData = (RooDataSet*)w->data("combData");
    RooSimultaneous* model = (RooSimultaneous*)w->pdf(Form("simul_pdf_bg_%.2f_GeV", 10.0));
    RooRealVar* pt = (RooRealVar*)w->var("pt");
    RooRealVar* qimean = (RooRealVar*)w->var("qimean");
    RooRealVar* prpart = (RooRealVar*)w->var("prpart");
    RooRealVar* pzpart = (RooRealVar*)w->var("pzpart");
    RooRealVar* BDT = (RooRealVar*)w->var("BDT10");
    RooCategory* sampleType = (RooCategory*)w->cat("sampleType");
    RooRealVar* relative_cross_sec = (RooRealVar*)w->var("relative_cross_sec");


    RooBinning bin_pt(2.0, 13.0);
    bin_pt.addUniform(44, 2.0, 13.);
    RooBinning bin_qimean(-2.0, 7.0);
    bin_qimean.addUniform(45, -2.0, 7.0);
    RooBinning bin_prpart(0.15, 0.35);
    bin_prpart.addUniform(50, 0.15, 0.35);
    RooBinning bin_pzpart(-0.4, 0.4);
    bin_pzpart.addUniform(40, -0.4, 0.4);
    RooBinning bin_BDT(-1.0, 1.0);
    bin_BDT.addUniform(40, -1.0, 1.0);


    for(int jdet = mindet; jdet <= maxdet; jdet++)
    {
        RooRealVar* N_1keV = (RooRealVar*)w->var(Form("N_1keV_%d_%d", detnum[jdet], timenum[jdet]));
        RooRealVar* N_gammas = (RooRealVar*)w->var(Form("N_gammas_%d_%d", detnum[jdet], timenum[jdet]));
        RooRealVar* N_210Pb_Qo = (RooRealVar*)w->var(Form("N_210Pb_Qo_%d_%d", detnum[jdet], timenum[jdet]));
        RooRealVar* N_210Pb_QiS1 = (RooRealVar*)w->var(Form("N_210Pb_QiS1_%d_%d", detnum[jdet], timenum[jdet]));
        RooRealVar* N_210Pb_QiS2 = (RooRealVar*)w->var(Form("N_210Pb_QiS2_%d_%d", detnum[jdet], timenum[jdet]));
        N_1keV->setVal(1.0);
        N_gammas->setVal(1.0);
        N_210Pb_Qo->setVal(1.0);
        N_210Pb_QiS1->setVal(1.0);
        N_210Pb_QiS2->setVal(1.0);
        N_1keV->setConstant(kTRUE);
        N_gammas->setConstant(kTRUE);
        N_210Pb_Qo->setConstant(kTRUE);
        N_210Pb_QiS1->setConstant(kTRUE);
        N_210Pb_QiS2->setConstant(kTRUE);
    }

    relative_cross_sec->setVal(1000);
    RooDataSet* MCdata = model->generate(RooArgSet(*pt, *qimean, *pzpart, *prpart, *BDT, *sampleType), RooFit::Extended());
    MCdata->Print("v");



    
    if(true)
    {
        for(int jdet = mindet; jdet <= maxdet; jdet++)
        {
            // load the WIMP spectrum from the text file
            char* filename = Form("WIMPspectrum_%d_%d.txt", 1100+detnum[jdet], timenum[jdet]);
            std::cout << "filename = " << filename << std::endl;
            std::ifstream ifs(filename, std::ifstream::in);
            std::vector<double> vec_energy, vec_spectrum;
            double ptEnergy, ptSpectrum;
            while(ifs >> ptEnergy >> ptSpectrum)
            {
                vec_energy.push_back(ptEnergy);
                vec_spectrum.push_back(ptSpectrum * bin_pt.binWidth(1));
            }
            TGraph* tg_spectrum = new TGraph(vec_energy.size(), &vec_energy[0], &vec_spectrum[0]);
            double cdmstools_spectrum_integral = 0;
            for(int jenergy = 0; jenergy < vec_spectrum.size(); jenergy++)
            {
                cdmstools_spectrum_integral += vec_spectrum[jenergy]*(vec_energy[1] - vec_energy[0]); 
            }

            RooHistPdf* pdf_WIMP = (RooHistPdf*)w->pdf(Form("histpdf_WIMP_pt_%.2f_GeV_%d_%d", WIMPmass, detnum[jdet], timenum[jdet])); 
            RooFormulaVar* N_WIMP = (RooFormulaVar*)w->function(Form("N_WIMP_%.2f_GeV_%d_%d", WIMPmass, detnum[jdet], timenum[jdet]));
            RooFormulaVar* N_bg = (RooFormulaVar*)w->function(Form("N_bg_%d_%d", detnum[jdet], timenum[jdet]));
            RooRealVar* norm_pdf = (RooRealVar*)w->var(Form("norm_pdf_WIMP_%.2f_GeV_%d_%d", WIMPmass, detnum[jdet], timenum[jdet]));
            RooRealVar* exposure = (RooRealVar*)w->var(Form("exposure_%d_%d", detnum[jdet], timenum[jdet]));
            std::cout << detNames[jdet] << std::endl;
            std::cout << "relative_cross_sec = " << relative_cross_sec->getVal() << std::endl;
            std::cout << "norm_pdf = " << norm_pdf->getVal() << std::endl;
            std::cout << "N_WIMP = " << N_WIMP->getVal() << std::endl;
            std::cout << "cdmstools_spectrum_integral = " << cdmstools_spectrum_integral << std::endl;
            std::cout << std::endl;
            
            

            TCanvas* c_pt_data = new TCanvas(Form("c_pt_data_%d_%d", detnum[jdet], timenum[jdet]));
            RooPlot* rp_1d_pt = pt->frame();
            MCdata->plotOn(rp_1d_pt, RooFit::DrawOption("EZ"), RooFit::LineWidth(2), 
                                    RooFit::Cut(Form("sampleType==sampleType::pt_sample_%d_%d", detnum[jdet], timenum[jdet])), 
                                    RooFit::Name(Form("data_%d_%d", detnum[jdet], timenum[jdet])));
            model->plotOn(rp_1d_pt, RooFit::Slice(*sampleType,Form("pt_sample_%d_%d", detnum[jdet], timenum[jdet])),
                                    RooFit::ProjWData(*sampleType, *MCdata), RooFit::LineColor(kRed+1), RooFit::LineWidth(2), 
                                    RooFit::Name(Form("model_%d_%d", detnum[jdet], timenum[jdet])));
            rp_1d_pt->Draw();
            tg_spectrum->SetLineWidth(3);
            tg_spectrum->SetLineStyle(7);
            tg_spectrum->SetLineColor(kBlue-4);
            tg_spectrum->Draw("same,L");
            TLegend* leg = new TLegend(0.5, 0.7, 0.88, 0.88);
            leg->AddEntry(rp_1d_pt->findObject(Form("data_%d_%d", detnum[jdet], timenum[jdet])),"fake data","le");
            leg->AddEntry(rp_1d_pt->findObject(Form("model_%d_%d", detnum[jdet], timenum[jdet])),"RooFit WIMP model","l");
            leg->AddEntry(tg_spectrum,"cdmstools WIMP model","l");
            leg->Draw();
            rp_1d_pt->SetTitle(Form("%s", detNames[jdet].c_str()));
            c_pt_data->Update();
            c_pt_data->Print(Form("figures/WIMPPDF_pt_%d_%d.png", detnum[jdet], timenum[jdet]), "png");
            c_pt_data->Update();
            c_pt_data->Print(Form("figures/WIMPPDF_pt_%d_%d.pdf", detnum[jdet], timenum[jdet]), "pdf");


            TCanvas* c_qimean_data = new TCanvas(Form("c_qimean_data_%d_%d", detnum[jdet], timenum[jdet]));
            RooPlot* rp_1d_qimean = qimean->frame();
            model->plotOn(rp_1d_qimean, RooFit::Slice(*sampleType,Form("qimean_sample_%d_%d", detnum[jdet], timenum[jdet])),
                            RooFit::ProjWData(*sampleType,*MCdata),RooFit::Components(Form("histpdf_WIMP_qimean_%.2f_GeV_%d_%d", WIMPmass, detnum[jdet], timenum[jdet])),RooFit::LineColor(kRed+1));
            rp_1d_qimean->Draw();
            rp_1d_qimean->SetTitle(Form("%s", detNames[jdet].c_str()));
            rp_1d_qimean->GetYaxis()->SetTitle("");
            c_qimean_data->Update();
            c_qimean_data->Print(Form("figures/WIMPPDF_qimean_%d_%d.png", detnum[jdet], timenum[jdet]), "png");
            c_qimean_data->Update();
            c_qimean_data->Print(Form("figures/WIMPPDF_qimean_%d_%d.pdf", detnum[jdet], timenum[jdet]), "pdf");

            TCanvas* c_prpart_data = new TCanvas(Form("c_prpart_data_%d_%d", detnum[jdet], timenum[jdet]));
            RooPlot* rp_1d_prpart = prpart->frame();
            model->plotOn(rp_1d_prpart, RooFit::Slice(*sampleType,Form("prpart_sample_%d_%d", detnum[jdet], timenum[jdet])),
                            RooFit::ProjWData(*sampleType,*MCdata),RooFit::Components(Form("histpdf_WIMP_prpart_%.2f_GeV_%d_%d", WIMPmass, detnum[jdet], timenum[jdet])),RooFit::LineColor(kRed+1));
            rp_1d_prpart->Draw();
            rp_1d_prpart->SetTitle(Form("%s", detNames[jdet].c_str()));
            rp_1d_prpart->GetYaxis()->SetTitle("");
            c_prpart_data->Update();
            c_prpart_data->Print(Form("figures/WIMPPDF_prpart_%d_%d.png", detnum[jdet], timenum[jdet]), "png");
            c_prpart_data->Update();
            c_prpart_data->Print(Form("figures/WIMPPDF_prpart_%d_%d.pdf", detnum[jdet], timenum[jdet]), "pdf");

            TCanvas* c_pzpart_data = new TCanvas(Form("c_pzpart_data_%d_%d", detnum[jdet], timenum[jdet]));
            RooPlot* rp_1d_pzpart = pzpart->frame();
            model->plotOn(rp_1d_pzpart, RooFit::Slice(*sampleType,Form("pzpart_sample_%d_%d", detnum[jdet], timenum[jdet])),
                            RooFit::ProjWData(*sampleType,*MCdata),RooFit::Components(Form("histpdf_WIMP_pzpart_%.2f_GeV_%d_%d", WIMPmass, detnum[jdet], timenum[jdet])),RooFit::LineColor(kRed+1));
            rp_1d_pzpart->Draw();
            rp_1d_pzpart->SetTitle(Form("%s", detNames[jdet].c_str()));
            rp_1d_pzpart->GetYaxis()->SetTitle("");
            c_pzpart_data->Update();
            c_pzpart_data->Print(Form("figures/WIMPPDF_pzpart_%d_%d.png", detnum[jdet], timenum[jdet]), "png");
            c_pzpart_data->Update();
            c_pzpart_data->Print(Form("figures/WIMPPDF_pzpart_%d_%d.pdf", detnum[jdet], timenum[jdet]), "pdf");

            TCanvas* c_BDT_data = new TCanvas(Form("c_BDT_data_%d_%d", detnum[jdet], timenum[jdet]));
            RooPlot* rp_1d_BDT = BDT->frame();
            model->plotOn(rp_1d_BDT, RooFit::Slice(*sampleType,Form("BDT_sample_%d_%d", detnum[jdet], timenum[jdet])),
                            RooFit::ProjWData(*sampleType,*MCdata),RooFit::Components(Form("histpdf_WIMP_BDT10_%.2f_GeV_%d_%d", WIMPmass, detnum[jdet], timenum[jdet])),RooFit::LineColor(kRed+1));
            rp_1d_BDT->Draw();
            rp_1d_BDT->SetTitle(Form("%s", detNames[jdet].c_str()));
            rp_1d_BDT->GetYaxis()->SetTitle("");
            c_BDT_data->Update();
            c_BDT_data->Print(Form("figures/WIMPPDF_BDT_%d_%d.png", detnum[jdet], timenum[jdet]), "png");
            c_BDT_data->Update();
            c_BDT_data->Print(Form("figures/WIMPPDF_BDT_%d_%d.pdf", detnum[jdet], timenum[jdet]), "pdf");
        }
    }



    rootapp->Run();
    return 0;
}
