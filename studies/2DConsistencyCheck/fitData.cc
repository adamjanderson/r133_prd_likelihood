#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooAddPdf.h"
#include "RooWorkspace.h"
#include "RooPlot.h"
#include "RooFit.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "TFile.h"
#include "TH1.h"
#include "TString.h"
#include "Math/Interpolator.h"
#include "Math/Functor.h"

#include "TTree.h"
#include "TStyle.h"
#include "TColor.h"
#include "TApplication.h"
#include "TLegend.h"
#include "RooStats/ProfileLikelihoodCalculator.h"
#include "RooStats/LikelihoodInterval.h"
#include "RooStats/LikelihoodIntervalPlot.h"
#include "RooGaussian.h"
#include "RooPoisson.h"
#include "RooPolynomial.h"
#include "RooHistPdf.h"
#include "RooDataHist.h"
#include "RooFitResult.h"
#include "RooExtendPdf.h"
#include "RooBinning.h"
#include "RooCategory.h"
#include "RooSimultaneous.h"
#include "RooFunctor1DBinding.h"
#include "RooMinuit.h"
#include "RooNLLVar.h"

#include <iostream>
#include <fstream>


void buildLegend(std::string leg_names[], RooPlot* rp)
{
    TLegend* leg = new TLegend(0.6, 0.5, 0.88, 0.88);
    // std::string leg_names[7] = {"data","total background", "gammas", "sidewall surf. ev.", "S1 surf. ev.", "S2 surf. ev.", "1 keV"};
    // std::cout << "# of items = " << rp_1d_BDT10->numItems() << std::endl;
    for (int i=1; i<rp->numItems(); i++)
    {
        TString obj_name=rp->nameOf(i);
        TObject *obj = rp->findObject(obj_name.Data());
        leg->AddEntry(obj,leg_names[i].c_str(),"l");
    }
    leg->Draw();
}

int main(int argc, char *argv[])
{
    // parse arguments
    if(argc != 2)
    {
        std::cout << "Usage: fitData [WIMP mass]" << std::endl;
        return 0;
    }
    double WIMPmass = atof(argv[1]);

    gStyle->SetTitleSize(0.045, "xyz");
    gStyle->SetLabelSize(0.045, "xyz");
    gStyle->SetOptStat(0);
    TColor *myColor = new TColor(1001, 31./255., 120./255., 180./255.);
    TApplication* rootapp = new TApplication("myApp", &argc, argv);

    bool bgOnlyFit = true;
    bool sigBgFit = false;

    int mindet = 0; // bad detectors: 4
    int maxdet = 8;
    std::vector<int> detlist = {0,1,2,3,4,5,6,7,8}; //{0,1,2}; //{0,1,2,3,4,5,6,7,8};
    std::string detNames[] = {"T1Z1", "T2Z1", "T2Z2", "T4Z2", "T4Z3", "T5Z2 (w/QOS1)", "T5Z2 (w/o QOS1)", "T5Z3 (BS)", "T5Z3 (AS)"};
    std::string leg_names_bg[8] = {"data","total background", "gammas", "sidewall surf. ev.", "S1 surf. ev.", "S2 surf. ev.", "1 keV", "WIMP"};
    int detnum[] = {1, 4, 5, 11, 12, 14, 14, 15, 15};
    int timenum[] = {1, 1, 1, 1, 1, 1, 2, 1, 2};

    
    TFile f_workspace(Form("LT_data+model_%.2f_2d_GeV.root", WIMPmass));

    // Retrieve workspace from file
    RooWorkspace* w = (RooWorkspace*) f_workspace.Get("wSave");


    //***** GET DATA + MODEL FROM FILE *****//
    RooDataSet* combData = (RooDataSet*)w->data("combData2d");
    RooSimultaneous* model = (RooSimultaneous*)w->pdf(Form("simul_pdf_bg_2d_%.2f_GeV", WIMPmass));
    RooRealVar* pt = (RooRealVar*)w->var("pt");
    RooRealVar* qimean = (RooRealVar*)w->var("qimean");
    RooRealVar* prpart = (RooRealVar*)w->var("prpart");
    RooRealVar* pzpart = (RooRealVar*)w->var("pzpart");
    RooRealVar* BDT = (RooRealVar*)w->var("BDT10"); //(Form("BDT%.0f",WIMPmass));
    RooCategory* sampleType = (RooCategory*)w->cat("sampleType2d");
    RooRealVar* relative_cross_sec = (RooRealVar*)w->var("relative_cross_sec");


    RooBinning bin_pt(2.0, 13.0);
    bin_pt.addUniform(44, 2.0, 13.);
    RooBinning bin_qimean(-1.4, 5.0);
    bin_qimean.addUniform(32, -1.4, 5.0);
    RooBinning bin_prpart(0.15, 0.35);
    bin_prpart.addUniform(50, 0.15, 0.35);
    RooBinning bin_pzpart(-0.4, 0.4);
    bin_pzpart.addUniform(40, -0.4, 0.4);
    RooBinning bin_BDT(-1.0, 1.0);
    bin_BDT.addUniform(40, -1.0, 1.0);

    //***** FITTING *****//
    char rangeName[] = "control";

    // define ranges
    // control regions only
    std::string selection = "";
    std::vector<std::string> det_selection(detlist.size());
    for(int jdet = 0; jdet < detlist.size(); jdet++)
    {
        det_selection[jdet] = "";
        det_selection[jdet] = det_selection[jdet] + Form("ptVqimean_sample_%d_%d,", detnum[detlist[jdet]], timenum[detlist[jdet]]);
        det_selection[jdet] = det_selection[jdet] + Form("prpart_sample_%d_%d,", detnum[detlist[jdet]], timenum[detlist[jdet]]);
        det_selection[jdet] = det_selection[jdet] + Form("pzpart_sample_%d_%d,", detnum[detlist[jdet]], timenum[detlist[jdet]]);
        selection = selection + det_selection[jdet];
    }
    sampleType->setRange("control", selection.c_str());
    // sampleType->Print("v");
    // std::cout << "control = " << selection << std::endl;

    // signal region only
    selection = "";
    for(int jdet = 0; jdet < detlist.size(); jdet++)
    {
        selection = selection + Form("BDT_sample_%d_%d,", detnum[detlist[jdet]], timenum[detlist[jdet]]);
    }
    sampleType->setRange("signal", selection.c_str());
    // sampleType->Print("v");
    // std::cout << "signal = " << selection << std::endl;

    model->Print("v");

    // do fitting
    RooDataSet* subset = (RooDataSet*)combData->reduce(RooFit::CutRange(rangeName));
    // RooNLLVar nll("nll","nll",*model,*subset) ;
    // RooMinuit m(nll) ;
    // m.setStrategy(0) ;
    // m.migrad() ;

    std::vector<RooRealVar*> N_1keV, N_gammas, N_210Pb_Qo, N_210Pb_QiS1, N_210Pb_QiS2, N_WIMP;
    for(int jdet = mindet; jdet <= maxdet; jdet++)
    {
        N_1keV.push_back((RooRealVar*)w->var(Form("N_1keV_%d_%d", detnum[jdet], timenum[jdet])));
        N_gammas.push_back((RooRealVar*)w->var(Form("N_gammas_%d_%d", detnum[jdet], timenum[jdet])));
        N_210Pb_Qo.push_back((RooRealVar*)w->var(Form("N_210Pb_Qo_%d_%d", detnum[jdet], timenum[jdet])));
        N_210Pb_QiS1.push_back((RooRealVar*)w->var(Form("N_210Pb_QiS1_%d_%d", detnum[jdet], timenum[jdet])));
        N_210Pb_QiS2.push_back((RooRealVar*)w->var(Form("N_210Pb_QiS2_%d_%d", detnum[jdet], timenum[jdet])));
        N_WIMP.push_back((RooRealVar*)w->function(Form("N_WIMP_%.2f_GeV_%d_%d", WIMPmass, detnum[jdet], timenum[jdet])));
    }
    N_210Pb_QiS1[7]->setVal(0.0);
    N_210Pb_QiS1[7]->setConstant(kTRUE);
    N_210Pb_QiS1[8]->setVal(0.0);
    N_210Pb_QiS1[8]->setConstant(kTRUE);


    // build the model for selected detectors only
    RooSimultaneous* subset_model = new RooSimultaneous("model_det_selection", "simultaneous pdf", *sampleType);
    for(int jdet = 0; jdet < detlist.size(); jdet++)
    {
        // RooCategory* det_sampleType = new RooCategory(Form("det_sampleType_%d", detnum[detlist[jdet]]), "categories for single detector");
        // det_sampleType->defineType(Form("ptVqimean_sample_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]]));
        // det_sampleType->defineType(Form("prpart_sample_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]]));
        // det_sampleType->defineType(Form("pzpart_sample_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]]));
        RooAddPdf* pdf_total_ptVqimean = (RooAddPdf*)w->pdf(Form("pdf_total_ptVqimean_%.2f_GeV_%d_%d", WIMPmass, detnum[detlist[jdet]], timenum[detlist[jdet]]));
        RooAddPdf* pdf_total_prpart = (RooAddPdf*)w->pdf(Form("pdf_total_prpart_%.2f_GeV_%d_%d", WIMPmass, detnum[detlist[jdet]], timenum[detlist[jdet]]));
        RooAddPdf* pdf_total_pzpart = (RooAddPdf*)w->pdf(Form("pdf_total_pzpart_%.2f_GeV_%d_%d", WIMPmass, detnum[detlist[jdet]], timenum[detlist[jdet]]));
        subset_model->addPdf(*pdf_total_ptVqimean, Form("ptVqimean_sample_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]]));
        subset_model->addPdf(*pdf_total_prpart, Form("prpart_sample_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]]));
        subset_model->addPdf(*pdf_total_pzpart, Form("pzpart_sample_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]]));
    }

    // sampleType->setRange(Form("control_%d", detnum[detlist[jdet]]), det_selection[detlist[jdet]].c_str());
    RooDataSet* det_subset = (RooDataSet*)combData->reduce(RooFit::CutRange("control"));
    det_subset->Print("v");
    RooFitResult* fitResult_selection = subset_model->fitTo(*det_subset, RooFit::Save(kTRUE), RooFit::Minimizer("Minuit2","Hesse"));
    double sigmafit = relative_cross_sec->getVal();
    double sigmaerr = relative_cross_sec->getPropagatedError(*fitResult_selection);
    relative_cross_sec->setVal(0.0);
    RooAbsReal* detpll = subset_model->createProfile(*relative_cross_sec);
    double pll = detpll->getVal();


    // print the results
    std::cout << "sigma = " << sigmafit << " +/- " << sigmaerr << std::endl;
    std::cout << "excess = " << sigmafit / sigmaerr << " sigma" << std::endl;
    // std::cout << selection.c_str() << std::endl;

    
    if(false)
    {
        for(int jdet = 0; jdet < detlist.size(); jdet++)
        {
            TCanvas* c_2d = new TCanvas(Form("c_2d_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]]));
            TH1* h_2d_data = det_subset->createHistogram(Form("h_2d_data_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]]),
                                        *pt, RooFit::YVar(*qimean),
                                        RooFit::Cut(Form("sampleType2d==sampleType2d::ptVqimean_sample_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])));
            h_2d_data->GetYaxis()->SetRangeUser(-1.5, 6.0);
            h_2d_data->SetTitle(Form("%s: data", detNames[detlist[jdet]].c_str()));
            h_2d_data->Draw("box");
            c_2d->Update();
            c_2d->Print(Form("figures/%s/ptVqimean_data_%.2f_GeV_%d_%d.pdf", rangeName, WIMPmass, detnum[detlist[jdet]], timenum[detlist[jdet]]), "pdf");
            c_2d->Print(Form("figures/%s/ptVqimean_data_%.2f_GeV_%d_%d.png", rangeName, WIMPmass, detnum[detlist[jdet]], timenum[detlist[jdet]]), "png");


            TCanvas* c_2d_pdf = new TCanvas(Form("c_2d_pdf_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]]));
            sampleType->setLabel(Form("ptVqimean_sample_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]]));
            TH1* h_2d_pdf_data = subset_model->createHistogram(Form("h_2d_pdf_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]]),
                                        *pt, RooFit::YVar(*qimean), RooFit::Extended(kTRUE));
            h_2d_pdf_data->SetLineColor(2);
            h_2d_pdf_data->SetTitle(Form("%s: model", detNames[detlist[jdet]].c_str()));
            h_2d_pdf_data->Draw("box");
            c_2d_pdf->Update();
            c_2d_pdf->Print(Form("figures/%s/ptVqimean_model_%.2f_GeV_%d_%d.pdf", rangeName, WIMPmass, detnum[detlist[jdet]], timenum[detlist[jdet]]), "pdf");
            c_2d_pdf->Print(Form("figures/%s/ptVqimean_model_%.2f_GeV_%d_%d.png", rangeName, WIMPmass, detnum[detlist[jdet]], timenum[detlist[jdet]]), "png");


            // subtract pdf and compute residuals
            TH2D* h_2d_diff = (TH2D*)h_2d_data->Clone(Form("c_2d_diff_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]]));
            for(int jbinX = 0; jbinX < h_2d_pdf_data->GetNbinsX(); jbinX++)
            {
                for(int jbinY = 0; jbinY < h_2d_pdf_data->GetNbinsY(); jbinY++)
                {
                    double nSigma_diff = (h_2d_data->GetBinContent(jbinX, jbinY) - h_2d_pdf_data->GetBinContent(jbinX, jbinY)) / 
                                            sqrt(h_2d_data->GetBinContent(jbinX, jbinY));
                    if(std::isfinite(nSigma_diff))
                        h_2d_diff->SetBinContent(jbinX, jbinY, sqrt(nSigma_diff*nSigma_diff));
                }
            }
            TCanvas* c_2d_diff = new TCanvas(Form("c_2d_diff_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]]));
            h_2d_diff->GetZaxis()->SetRangeUser(0.0, 5.0);
            h_2d_diff->Draw("colz");
            h_2d_diff->SetTitle(Form("%s: (data - model) / #sqrt{data}", detNames[detlist[jdet]].c_str()));
            c_2d_diff->Update();
            c_2d_diff->Print(Form("figures/%s/ptVqimean_resid_%.2f_GeV_%d_%d.pdf", rangeName, WIMPmass, detnum[detlist[jdet]], timenum[detlist[jdet]]), "pdf");
            c_2d_diff->Print(Form("figures/%s/ptVqimean_resid_%.2f_GeV_%d_%d.png", rangeName, WIMPmass, detnum[detlist[jdet]], timenum[detlist[jdet]]), "png");



            TCanvas* c_pt_data = new TCanvas(Form("c_pt_data_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]]));
            pt->setRange(2.0, 13.0);
            RooPlot* rp_1d_pt = pt->frame();
            det_subset->plotOn(rp_1d_pt, RooFit::DrawOption("EZ"), RooFit::LineWidth(2), RooFit::Cut(Form("sampleType2d==sampleType2d::ptVqimean_sample_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])), RooFit::Binning(bin_pt), RooFit::Name(Form("data_ptVqimean_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])));
            subset_model->plotOn(rp_1d_pt, RooFit::Slice(*sampleType,Form("ptVqimean_sample_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])), RooFit::ProjWData(*sampleType,*subset), RooFit::Name(Form("pdf_ptVqimean_%.2f_GeV_%d_%d", WIMPmass, detnum[detlist[jdet]], timenum[detlist[jdet]])));
            subset_model->plotOn(rp_1d_pt, RooFit::Slice(*sampleType,Form("ptVqimean_sample_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("histpdf_MC_gammas_ptVqimean_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])), RooFit::LineStyle(kDashed), RooFit::LineColor(kCyan));
            subset_model->plotOn(rp_1d_pt, RooFit::Slice(*sampleType,Form("ptVqimean_sample_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("pdf_MC_Pb_Qo_ptVqimean_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])), RooFit::LineStyle(kDashed), RooFit::LineColor(kRed));
            subset_model->plotOn(rp_1d_pt, RooFit::Slice(*sampleType,Form("ptVqimean_sample_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("pdf_MC_Pb_QiS1_ptVqimean_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])), RooFit::LineStyle(kDashed), RooFit::LineColor(kOrange));
            subset_model->plotOn(rp_1d_pt, RooFit::Slice(*sampleType,Form("ptVqimean_sample_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("pdf_MC_Pb_QiS2_ptVqimean_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])), RooFit::LineStyle(kDashed), RooFit::LineColor(kOrange+3));
            subset_model->plotOn(rp_1d_pt, RooFit::Slice(*sampleType,Form("ptVqimean_sample_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("histpdf_MC_1keV_ptVqimean_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])), RooFit::LineStyle(kDashed), RooFit::LineColor(kGreen));
            subset_model->plotOn(rp_1d_pt, RooFit::Slice(*sampleType,Form("ptVqimean_sample_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("histpdf_WIMP_ptVqimean_%.2f_GeV_%d_%d", WIMPmass, detnum[detlist[jdet]], timenum[detlist[jdet]])), RooFit::LineStyle(kDashed), RooFit::LineColor(kMagenta));
            rp_1d_pt->Draw();
            buildLegend(leg_names_bg, rp_1d_pt);
            double pt_chi2 = rp_1d_pt->chiSquare(Form("pdf_ptVqimean_%.2f_GeV_%d_%d", WIMPmass, detnum[detlist[jdet]], timenum[detlist[jdet]]), 
                                                 Form("data_ptVqimean_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]]), 6);
            // c_pt_data->SetLogy();
            rp_1d_pt->SetTitle(Form("%s: #chi^2 / ndf = %.2f", detNames[detlist[jdet]].c_str(), pt_chi2));
            c_pt_data->Update();
            c_pt_data->Print(Form("figures/%s/bgOnly_pt_%.2f_GeV_%d_%d.png", rangeName, WIMPmass, detnum[detlist[jdet]], timenum[detlist[jdet]]), "png");
            c_pt_data->Update();
            c_pt_data->Print(Form("figures/%s/bgOnly_pt_%.2f_GeV_%d_%d.pdf", rangeName, WIMPmass, detnum[detlist[jdet]], timenum[detlist[jdet]]), "pdf");

            TCanvas* c_qimean_data = new TCanvas(Form("c_qimean_data_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]]));
            // qimean->setRange(2.0, 13.0);
            RooPlot* rp_1d_qimean = qimean->frame();
            det_subset->plotOn(rp_1d_qimean, RooFit::DrawOption("EZ"), RooFit::LineWidth(2), RooFit::Cut(Form("sampleType2d==sampleType2d::ptVqimean_sample_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])), RooFit::Binning(bin_qimean), RooFit::Name(Form("data_qimean_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])));
            subset_model->plotOn(rp_1d_qimean, RooFit::Slice(*sampleType,Form("ptVqimean_sample_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])), RooFit::ProjWData(*sampleType,*subset), RooFit::Name(Form("pdf_qimean_%.2f_GeV_%d_%d", WIMPmass, detnum[detlist[jdet]], timenum[detlist[jdet]])));
            subset_model->plotOn(rp_1d_qimean, RooFit::Slice(*sampleType,Form("ptVqimean_sample_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("histpdf_MC_gammas_ptVqimean_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])),RooFit::LineStyle(kDashed),RooFit::LineColor(kCyan));
            subset_model->plotOn(rp_1d_qimean, RooFit::Slice(*sampleType,Form("ptVqimean_sample_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("pdf_MC_Pb_Qo_ptVqimean_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])),RooFit::LineStyle(kDashed),RooFit::LineColor(kRed));
            subset_model->plotOn(rp_1d_qimean, RooFit::Slice(*sampleType,Form("ptVqimean_sample_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("pdf_MC_Pb_QiS1_ptVqimean_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])),RooFit::LineStyle(kDashed),RooFit::LineColor(kOrange));
            subset_model->plotOn(rp_1d_qimean, RooFit::Slice(*sampleType,Form("ptVqimean_sample_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("pdf_MC_Pb_QiS2_ptVqimean_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])),RooFit::LineStyle(kDashed),RooFit::LineColor(kOrange+3));
            subset_model->plotOn(rp_1d_qimean, RooFit::Slice(*sampleType,Form("ptVqimean_sample_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("histpdf_MC_1keV_ptVqimean_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])),RooFit::LineStyle(kDashed),RooFit::LineColor(kGreen));
            subset_model->plotOn(rp_1d_qimean, RooFit::Slice(*sampleType,Form("ptVqimean_sample_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("histpdf_WIMP_ptVqimean_%.2f_GeV_%d_%d", WIMPmass, detnum[detlist[jdet]], timenum[detlist[jdet]])),RooFit::LineStyle(kDashed),RooFit::LineColor(kMagenta));
            rp_1d_qimean->Draw();
            // buildLegend(leg_names_bg, rp_1d_qimean);
            double qimean_chi2 = rp_1d_qimean->chiSquare(Form("pdf_qimean_%.2f_GeV_%d_%d", WIMPmass, detnum[detlist[jdet]], timenum[detlist[jdet]]), 
                                                 Form("data_qimean_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]]), 6);
            // c_qimean_data->SetLogy();
            rp_1d_qimean->SetTitle(Form("%s: #chi^2 / ndf = %.2f", detNames[detlist[jdet]].c_str(), qimean_chi2));
            c_qimean_data->Update();
            c_qimean_data->Print(Form("figures/%s/bgOnly_qimean_%.2f_GeV_%d_%d.png", rangeName, WIMPmass, detnum[detlist[jdet]], timenum[detlist[jdet]]), "png");
            c_qimean_data->Update();
            c_qimean_data->Print(Form("figures/%s/bgOnly_qimean_%.2f_GeV_%d_%d.pdf", rangeName, WIMPmass, detnum[detlist[jdet]], timenum[detlist[jdet]]), "pdf");

            TCanvas* c_prpart_data = new TCanvas(Form("c_prpart_data_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]]));
            // prpart->setRange(2.0, 13.0);
            RooPlot* rp_1d_prpart = prpart->frame();
            det_subset->plotOn(rp_1d_prpart, RooFit::DrawOption("EZ"), RooFit::LineWidth(2), RooFit::Cut(Form("sampleType2d==sampleType2d::prpart_sample_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])), RooFit::Binning(bin_prpart), RooFit::Name(Form("data_prpart_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])));
            subset_model->plotOn(rp_1d_prpart, RooFit::Slice(*sampleType,Form("prpart_sample_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])), RooFit::ProjWData(*sampleType,*subset), RooFit::Name(Form("pdf_prpart_%.2f_GeV_%d_%d", WIMPmass, detnum[detlist[jdet]], timenum[detlist[jdet]])));
            subset_model->plotOn(rp_1d_prpart, RooFit::Slice(*sampleType,Form("prpart_sample_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("histpdf_MC_gammas_prpart_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])),RooFit::LineStyle(kDashed),RooFit::LineColor(kCyan));
            subset_model->plotOn(rp_1d_prpart, RooFit::Slice(*sampleType,Form("prpart_sample_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("pdf_MC_Pb_Qo_prpart_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])),RooFit::LineStyle(kDashed),RooFit::LineColor(kRed));
            subset_model->plotOn(rp_1d_prpart, RooFit::Slice(*sampleType,Form("prpart_sample_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("pdf_MC_Pb_QiS1_prpart_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])),RooFit::LineStyle(kDashed),RooFit::LineColor(kOrange));
            subset_model->plotOn(rp_1d_prpart, RooFit::Slice(*sampleType,Form("prpart_sample_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("pdf_MC_Pb_QiS2_prpart_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])),RooFit::LineStyle(kDashed),RooFit::LineColor(kOrange+3));
            subset_model->plotOn(rp_1d_prpart, RooFit::Slice(*sampleType,Form("prpart_sample_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("histpdf_MC_1keV_prpart_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])),RooFit::LineStyle(kDashed),RooFit::LineColor(kGreen));
            subset_model->plotOn(rp_1d_prpart, RooFit::Slice(*sampleType,Form("prpart_sample_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("histpdf_WIMP_prpart_%.2f_GeV_%d_%d", WIMPmass, detnum[detlist[jdet]], timenum[detlist[jdet]])),RooFit::LineStyle(kDashed),RooFit::LineColor(kMagenta));
            rp_1d_prpart->Draw();
            // buildLegend(leg_names_bg, rp_1d_prpart);
            double prpart_chi2 = rp_1d_prpart->chiSquare(Form("pdf_prpart_%.2f_GeV_%d_%d", WIMPmass, detnum[detlist[jdet]], timenum[detlist[jdet]]), 
                                                 Form("data_prpart_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]]), 6);
            // c_prpart_data->SetLogy();
            rp_1d_prpart->SetTitle(Form("%s: #chi^2 / ndf = %.2f", detNames[detlist[jdet]].c_str(), prpart_chi2));
            c_prpart_data->Update();
            c_prpart_data->Print(Form("figures/%s/bgOnly_prpart_%.2f_GeV_%d_%d.png", rangeName, WIMPmass, detnum[detlist[jdet]], timenum[detlist[jdet]]), "png");
            c_prpart_data->Update();
            c_prpart_data->Print(Form("figures/%s/bgOnly_prpart_%.2f_GeV_%d_%d.pdf", rangeName, WIMPmass, detnum[detlist[jdet]], timenum[detlist[jdet]]), "pdf");

            TCanvas* c_pzpart_data = new TCanvas(Form("c_pzpart_data_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]]));
            // pt->setRange(2.0, 13.0);
            RooPlot* rp_1d_pzpart = pzpart->frame();
            det_subset->plotOn(rp_1d_pzpart, RooFit::DrawOption("EZ"), RooFit::LineWidth(2), RooFit::Cut(Form("sampleType2d==sampleType2d::pzpart_sample_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])), RooFit::Binning(bin_pzpart), RooFit::Name(Form("data_pzpart_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])));
            subset_model->plotOn(rp_1d_pzpart, RooFit::Slice(*sampleType,Form("pzpart_sample_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])), RooFit::ProjWData(*sampleType,*subset), RooFit::Name(Form("pdf_pzpart_%.2f_GeV_%d_%d", WIMPmass, detnum[detlist[jdet]], timenum[detlist[jdet]])));
            subset_model->plotOn(rp_1d_pzpart, RooFit::Slice(*sampleType,Form("pzpart_sample_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("histpdf_MC_gammas_pzpart_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])),RooFit::LineStyle(kDashed),RooFit::LineColor(kCyan));
            subset_model->plotOn(rp_1d_pzpart, RooFit::Slice(*sampleType,Form("pzpart_sample_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("pdf_MC_Pb_Qo_pzpart_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])),RooFit::LineStyle(kDashed),RooFit::LineColor(kRed));
            subset_model->plotOn(rp_1d_pzpart, RooFit::Slice(*sampleType,Form("pzpart_sample_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("pdf_MC_Pb_QiS1_pzpart_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])),RooFit::LineStyle(kDashed),RooFit::LineColor(kOrange));
            subset_model->plotOn(rp_1d_pzpart, RooFit::Slice(*sampleType,Form("pzpart_sample_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("pdf_MC_Pb_QiS2_pzpart_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])),RooFit::LineStyle(kDashed),RooFit::LineColor(kOrange+3));
            subset_model->plotOn(rp_1d_pzpart, RooFit::Slice(*sampleType,Form("pzpart_sample_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("histpdf_MC_1keV_pzpart_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])),RooFit::LineStyle(kDashed),RooFit::LineColor(kGreen));
            subset_model->plotOn(rp_1d_pzpart, RooFit::Slice(*sampleType,Form("pzpart_sample_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("histpdf_WIMP_pzpart_%.2f_GeV_%d_%d", WIMPmass, detnum[detlist[jdet]], timenum[detlist[jdet]])),RooFit::LineStyle(kDashed),RooFit::LineColor(kMagenta));
            rp_1d_pzpart->Draw();
            // buildLegend(leg_names_bg, rp_1d_pzpart);
            double pzpart_chi2 = rp_1d_pzpart->chiSquare(Form("pdf_pzpart_%.2f_GeV_%d_%d", WIMPmass, detnum[detlist[jdet]], timenum[detlist[jdet]]), 
                                                 Form("data_pzpart_%d_%d", detnum[detlist[jdet]], timenum[detlist[jdet]]), 6);
            // c_pzpart_data->SetLogy();
            rp_1d_pzpart->SetTitle(Form("%s: #chi^2 / ndf = %.2f", detNames[detlist[jdet]].c_str(), pzpart_chi2));
            c_pzpart_data->Update();
            c_pzpart_data->Print(Form("figures/%s/bgOnly_pzpart_%.2f_GeV_%d_%d.png", rangeName, WIMPmass, detnum[detlist[jdet]], timenum[detlist[jdet]]), "png");
            c_pzpart_data->Update();
            c_pzpart_data->Print(Form("figures/%s/bgOnly_pzpart_%.2f_GeV_%d_%d.pdf", rangeName, WIMPmass, detnum[detlist[jdet]], timenum[detlist[jdet]]), "pdf");

            // TCanvas* c_BDT_data = new TCanvas(Form("c_BDT_data_%d_%d", detnum[jdet], timenum[jdet]));
            // // pt->setRange(2.0, 13.0);
            // RooPlot* rp_1d_BDT = BDT->frame();
            // RooPlot* rp_BDT = subset->plotOn(rp_1d_BDT, RooFit::DrawOption("EZ"), RooFit::LineWidth(2), RooFit::Cut(Form("BDT10>-0.8 & sampleType==sampleType::BDT_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::Binning(bin_BDT), RooFit::Name(Form("data_BDT_%d_%d", detnum[jdet], timenum[jdet])));
            // model->plotOn(rp_1d_BDT, RooFit::Slice(*sampleType,Form("BDT_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset), RooFit::Name(Form("pdf_BDT_%.2f_GeV_%d_%d", WIMPmass, detnum[jdet], timenum[jdet])));
            // model->plotOn(rp_1d_BDT, RooFit::Slice(*sampleType,Form("BDT_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("histpdf_MC_gammas_BDT10_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kCyan));
            // model->plotOn(rp_1d_BDT, RooFit::Slice(*sampleType,Form("BDT_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("pdf_MC_Pb_Qo_BDT10_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kRed));
            // model->plotOn(rp_1d_BDT, RooFit::Slice(*sampleType,Form("BDT_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("pdf_MC_Pb_QiS1_BDT10_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kOrange));
            // model->plotOn(rp_1d_BDT, RooFit::Slice(*sampleType,Form("BDT_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("pdf_MC_Pb_QiS2_BDT10_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kOrange+3));
            // model->plotOn(rp_1d_BDT, RooFit::Slice(*sampleType,Form("BDT_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("histpdf_MC_1keV_BDT10_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kGreen));
            // model->plotOn(rp_1d_BDT, RooFit::Slice(*sampleType,Form("BDT_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("histpdf_WIMP_BDT10_%.2f_GeV_%d_%d", WIMPmass, detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kMagenta));
            // rp_1d_BDT->Draw();
            // // buildLegend(leg_names_bg, rp_1d_BDT);
            // double BDT_chi2 = rp_1d_BDT->chiSquare(Form("pdf_BDT_%.2f_GeV_%d_%d", WIMPmass, detnum[jdet], timenum[jdet]), 
            //                                         Form("data_BDT_%d_%d", detnum[jdet], timenum[jdet]), 6);
            // rp_1d_BDT->GetYaxis()->SetRangeUser(0., 40.);
            // // c_BDT_data->SetLogy();
            // rp_1d_BDT->SetTitle(Form("%s: #chi^2 / ndf = %.2f", detNames[jdet].c_str(), BDT_chi2));
            // c_BDT_data->Update();
            // c_BDT_data->Print(Form("figures/%s/bgOnly_BDT_%d_%d.png", rangeName, detnum[jdet], timenum[jdet]), "png");
            // c_BDT_data->Update();
            // c_BDT_data->Print(Form("figures/%s/bgOnly_BDT_%d_%d.pdf", rangeName, detnum[jdet], timenum[jdet]), "pdf");
        }
    }



    rootapp->Run();
    return 0;
}
