#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooAddPdf.h"
#include "RooWorkspace.h"
#include "RooPlot.h"
#include "RooFit.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "TFile.h"
#include "TH1.h"
#include "TString.h"
#include "Math/Interpolator.h"
#include "Math/Functor.h"

#include "TTree.h"
#include "TStyle.h"
#include "TColor.h"
#include "TApplication.h"
#include "TLegend.h"
#include "RooStats/ProfileLikelihoodCalculator.h"
#include "RooStats/LikelihoodInterval.h"
#include "RooStats/LikelihoodIntervalPlot.h"
#include "RooGaussian.h"
#include "RooPoisson.h"
#include "RooPolynomial.h"
#include "RooHistPdf.h"
#include "RooDataHist.h"
#include "RooFitResult.h"
#include "RooExtendPdf.h"
#include "RooBinning.h"
#include "RooCategory.h"
#include "RooSimultaneous.h"
#include "RooFunctor1DBinding.h"
#include "RooEffProd.h"

#include <iostream>
#include <fstream>


void buildLegend(std::string leg_names[], RooPlot* rp)
{
    TLegend* leg = new TLegend(0.6, 0.5, 0.88, 0.88);
    // std::string leg_names[7] = {"data","total background", "gammas", "sidewall surf. ev.", "S1 surf. ev.", "S2 surf. ev.", "1 keV"};
    // std::cout << "# of items = " << rp_1d_BDT10->numItems() << std::endl;
    for (int i=1; i<rp->numItems(); i++)
    {
        TString obj_name=rp->nameOf(i);
        TObject *obj = rp->findObject(obj_name.Data());
        leg->AddEntry(obj,leg_names[i].c_str(),"l");
    }
    leg->Draw();
}

int main(int argc, char *argv[])
{
    gStyle->SetTitleSize(0.045, "xyz");
    gStyle->SetLabelSize(0.045, "xyz");
    TColor *myColor = new TColor(1001, 31./255., 120./255., 180./255.);
    TApplication* rootapp = new TApplication("myApp", &argc, argv);

    bool bgOnlyFit = true;
    bool sigBgFit = false;

    int mindet = 0;
    int maxdet = 8;
    std::string detNames[] = {"T1Z1", "T2Z1", "T2Z2", "T4Z2", "T4Z3", "T5Z2 (w/QOS1)", "T5Z2 (w/o QOS1)", "T5Z3 (BS)", "T5Z3 (AS)"};
    std::string leg_names_bg[7] = {"data","total background", "gammas", "sidewall surf. ev.", "S1 surf. ev.", "S2 surf. ev.", "1 keV"};
    int detnum[] = {1, 4, 5, 11, 12, 14, 14, 15, 15};
    int timenum[] = {1, 1, 1, 1, 1, 1, 2, 1, 2};

    
    TFile f_workspace("LT_data+model_<10keV.root") ;

    // Retrieve workspace from file
    RooWorkspace* w = (RooWorkspace*) f_workspace.Get("wSave");


    //***** GET DATA + MODEL FROM FILE *****//
    RooDataSet* combData = (RooDataSet*)w->data("combData");
    RooSimultaneous* model = (RooSimultaneous*)w->pdf("simul_pdf_bg");
    RooRealVar* pt = (RooRealVar*)w->var("pt");
    RooRealVar* qimean = (RooRealVar*)w->var("qimean");
    RooRealVar* prpart = (RooRealVar*)w->var("prpart");
    RooRealVar* pzpart = (RooRealVar*)w->var("pzpart");
    RooRealVar* BDT = (RooRealVar*)w->var("BDT10");
    RooCategory* sampleType = (RooCategory*)w->cat("sampleType");

    RooBinning bin_pt(2.0, 13.0);
    bin_pt.addUniform(44, 2.0, 13.);
    RooBinning bin_qimean(-2.0, 7.0);
    bin_qimean.addUniform(45, -2.0, 7.0);
    RooBinning bin_prpart(0.15, 0.35);
    bin_prpart.addUniform(50, 0.15, 0.35);
    RooBinning bin_pzpart(-0.4, 0.4);
    bin_pzpart.addUniform(40, -0.4, 0.4);
    RooBinning bin_BDT(-1.0, 1.0);
    bin_BDT.addUniform(40, -1.0, 1.0);


    //***** FITTING *****//
    char rangeName[] = "control";

    // define ranges
    // control regions only
    std::string selection = "";
    for(int jdet = mindet; jdet <= maxdet; jdet++)
    {
        selection = selection + Form("pt_sample_%d_%d,", detnum[jdet], timenum[jdet]);
        selection = selection + Form("qimean_sample_%d_%d,", detnum[jdet], timenum[jdet]);
        selection = selection + Form("prpart_sample_%d_%d,", detnum[jdet], timenum[jdet]);
        selection = selection + Form("pzpart_sample_%d_%d,", detnum[jdet], timenum[jdet]);
    }
    sampleType->setRange("control", selection.c_str());
    sampleType->Print("v");
    std::cout << "control = " << selection << std::endl;

    // signal region only
    selection = "";
    for(int jdet = mindet; jdet <= maxdet; jdet++)
    {
        selection = selection + Form("BDT_sample_%d_%d,", detnum[jdet], timenum[jdet]);
    }
    sampleType->setRange("signal", selection.c_str());
    sampleType->Print("v");
    std::cout << "signal = " << selection << std::endl;

    // do fitting
    RooDataSet* subset = (RooDataSet*)combData->reduce(RooFit::CutRange(rangeName));
    RooFitResult* fitResult = model->fitTo(*subset, RooFit::Save(kTRUE), RooFit::Minos(kFALSE));

    // write fit results to file
    std::filebuf file;
    file.open(Form("results/results_%s.txt", rangeName), std::ios::out);
    std::ostream os_results(&file);
    fitResult->printMultiline(os_results, 1, false);
    file.close();


    
    if(true)
    {
        for(int jdet = mindet; jdet <= maxdet; jdet++)
        {
            TCanvas* c_pt_data = new TCanvas(Form("c_pt_data_%d_%d", detnum[jdet], timenum[jdet]));
            // pt->setRange(2.0, 13.0);
            RooPlot* rp_1d_pt = pt->frame();
            rp_1d_pt->SetTitle(detNames[jdet].c_str());
            subset->plotOn(rp_1d_pt, RooFit::DrawOption("EZ"), RooFit::LineWidth(2), RooFit::Cut(Form("sampleType==sampleType::pt_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::Binning(bin_pt));
            model->plotOn(rp_1d_pt, RooFit::Slice(*sampleType,Form("pt_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset));
            model->plotOn(rp_1d_pt, RooFit::Slice(*sampleType,Form("pt_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("histpdf_MC_gammas_pt_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kCyan));
            model->plotOn(rp_1d_pt, RooFit::Slice(*sampleType,Form("pt_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("pdf_MC_Pb_Qo_pt_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kRed));
            model->plotOn(rp_1d_pt, RooFit::Slice(*sampleType,Form("pt_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("pdf_MC_Pb_QiS1_pt_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kGreen));
            model->plotOn(rp_1d_pt, RooFit::Slice(*sampleType,Form("pt_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("pdf_MC_Pb_QiS2_pt_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kMagenta));
            model->plotOn(rp_1d_pt, RooFit::Slice(*sampleType,Form("pt_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("histpdf_MC_1keV_pt_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kOrange));
            rp_1d_pt->Draw();
            c_pt_data->SetLogy();
            c_pt_data->Update();
            c_pt_data->Print(Form("figures/%s/bgOnly_pt_%d_%d.png", rangeName, detnum[jdet], timenum[jdet]));
            c_pt_data->Update();
            c_pt_data->Print(Form("figures/%s/bgOnly_pt_%d_%d.pdf", rangeName, detnum[jdet], timenum[jdet]));

            TCanvas* c_qimean_data = new TCanvas(Form("c_qimean_data_%d_%d", detnum[jdet], timenum[jdet]));
            // qimean->setRange(2.0, 13.0);
            RooPlot* rp_1d_qimean = qimean->frame();
            rp_1d_qimean->SetTitle(detNames[jdet].c_str());
            subset->plotOn(rp_1d_qimean, RooFit::DrawOption("EZ"), RooFit::LineWidth(2), RooFit::Cut(Form("sampleType==sampleType::qimean_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::Binning(bin_qimean));
            model->plotOn(rp_1d_qimean, RooFit::Slice(*sampleType,Form("qimean_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset));
            model->plotOn(rp_1d_qimean, RooFit::Slice(*sampleType,Form("qimean_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("histpdf_MC_gammas_qimean_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kCyan));
            model->plotOn(rp_1d_qimean, RooFit::Slice(*sampleType,Form("qimean_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("pdf_MC_Pb_Qo_qimean_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kRed));
            model->plotOn(rp_1d_qimean, RooFit::Slice(*sampleType,Form("qimean_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("pdf_MC_Pb_QiS1_qimean_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kGreen));
            model->plotOn(rp_1d_qimean, RooFit::Slice(*sampleType,Form("qimean_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("pdf_MC_Pb_QiS2_qimean_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kMagenta));
            model->plotOn(rp_1d_qimean, RooFit::Slice(*sampleType,Form("qimean_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("histpdf_MC_1keV_qimean_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kOrange));
            rp_1d_qimean->Draw();
            c_qimean_data->SetLogy();
            c_qimean_data->Update();
            c_qimean_data->Print(Form("figures/%s/bgOnly_qimean_%d_%d.png", rangeName, detnum[jdet], timenum[jdet]));
            c_qimean_data->Update();
            c_qimean_data->Print(Form("figures/%s/bgOnly_qimean_%d_%d.pdf", rangeName, detnum[jdet], timenum[jdet]));

            TCanvas* c_prpart_data = new TCanvas(Form("c_prpart_data_%d_%d", detnum[jdet], timenum[jdet]));
            // prpart->setRange(2.0, 13.0);
            RooPlot* rp_1d_prpart = prpart->frame();
            rp_1d_prpart->SetTitle(detNames[jdet].c_str());
            subset->plotOn(rp_1d_prpart, RooFit::DrawOption("EZ"), RooFit::LineWidth(2), RooFit::Cut(Form("sampleType==sampleType::prpart_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::Binning(bin_prpart));
            model->plotOn(rp_1d_prpart, RooFit::Slice(*sampleType,Form("prpart_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset));
            model->plotOn(rp_1d_prpart, RooFit::Slice(*sampleType,Form("prpart_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("histpdf_MC_gammas_prpart_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kCyan));
            model->plotOn(rp_1d_prpart, RooFit::Slice(*sampleType,Form("prpart_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("pdf_MC_Pb_Qo_prpart_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kRed));
            model->plotOn(rp_1d_prpart, RooFit::Slice(*sampleType,Form("prpart_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("pdf_MC_Pb_QiS1_prpart_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kGreen));
            model->plotOn(rp_1d_prpart, RooFit::Slice(*sampleType,Form("prpart_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("pdf_MC_Pb_QiS2_prpart_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kMagenta));
            model->plotOn(rp_1d_prpart, RooFit::Slice(*sampleType,Form("prpart_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("histpdf_MC_1keV_prpart_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kOrange));
            rp_1d_prpart->Draw();
            c_prpart_data->SetLogy();
            c_prpart_data->Update();
            c_prpart_data->Print(Form("figures/%s/bgOnly_prpart_%d_%d.png", rangeName, detnum[jdet], timenum[jdet]));
            c_prpart_data->Update();
            c_prpart_data->Print(Form("figures/%s/bgOnly_prpart_%d_%d.pdf", rangeName, detnum[jdet], timenum[jdet]));

            TCanvas* c_pzpart_data = new TCanvas(Form("c_pzpart_data_%d_%d", detnum[jdet], timenum[jdet]));
            // pt->setRange(2.0, 13.0);
            RooPlot* rp_1d_pzpart = pzpart->frame();
            rp_1d_pzpart->SetTitle(detNames[jdet].c_str());
            subset->plotOn(rp_1d_pzpart, RooFit::DrawOption("EZ"), RooFit::LineWidth(2), RooFit::Cut(Form("sampleType==sampleType::pzpart_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::Binning(bin_pzpart));
            model->plotOn(rp_1d_pzpart, RooFit::Slice(*sampleType,Form("pzpart_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset));
            model->plotOn(rp_1d_pzpart, RooFit::Slice(*sampleType,Form("pzpart_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("histpdf_MC_gammas_pzpart_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kCyan));
            model->plotOn(rp_1d_pzpart, RooFit::Slice(*sampleType,Form("pzpart_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("pdf_MC_Pb_Qo_pzpart_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kRed));
            model->plotOn(rp_1d_pzpart, RooFit::Slice(*sampleType,Form("pzpart_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("pdf_MC_Pb_QiS1_pzpart_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kGreen));
            model->plotOn(rp_1d_pzpart, RooFit::Slice(*sampleType,Form("pzpart_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("pdf_MC_Pb_QiS2_pzpart_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kMagenta));
            model->plotOn(rp_1d_pzpart, RooFit::Slice(*sampleType,Form("pzpart_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("histpdf_MC_1keV_pzpart_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kOrange));
            rp_1d_pzpart->Draw();
            // c_pzpart_data->SetLogy();
            c_pzpart_data->SetLogy();
            c_pzpart_data->Update();
            c_pzpart_data->Print(Form("figures/%s/bgOnly_pzpart_%d_%d.png", rangeName, detnum[jdet], timenum[jdet]));
            c_pzpart_data->Update();
            c_pzpart_data->Print(Form("figures/%s/bgOnly_pzpart_%d_%d.pdf", rangeName, detnum[jdet], timenum[jdet]));

            TCanvas* c_BDT_data = new TCanvas(Form("c_BDT_data_%d_%d", detnum[jdet], timenum[jdet]));
            // pt->setRange(2.0, 13.0);
            RooPlot* rp_1d_BDT = BDT->frame();
            rp_1d_BDT->SetTitle(detNames[jdet].c_str());
            RooPlot* rp_BDT = subset->plotOn(rp_1d_BDT, RooFit::DrawOption("EZ"), RooFit::LineWidth(2), RooFit::Cut(Form("BDT10>-0.8 & sampleType==sampleType::BDT_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::Binning(bin_BDT));
            model->plotOn(rp_1d_BDT, RooFit::Slice(*sampleType,Form("BDT_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset));
            model->plotOn(rp_1d_BDT, RooFit::Slice(*sampleType,Form("BDT_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("histpdf_MC_gammas_BDT10_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kCyan));
            model->plotOn(rp_1d_BDT, RooFit::Slice(*sampleType,Form("BDT_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("pdf_MC_Pb_Qo_BDT10_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kRed));
            model->plotOn(rp_1d_BDT, RooFit::Slice(*sampleType,Form("BDT_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("pdf_MC_Pb_QiS1_BDT10_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kGreen));
            model->plotOn(rp_1d_BDT, RooFit::Slice(*sampleType,Form("BDT_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("pdf_MC_Pb_QiS2_BDT10_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kMagenta));
            model->plotOn(rp_1d_BDT, RooFit::Slice(*sampleType,Form("BDT_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("histpdf_MC_1keV_BDT10_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kOrange));
            rp_1d_BDT->Draw();
            rp_1d_BDT->GetYaxis()->SetRangeUser(0., 40.);
            // c_BDT_data->SetLogy();
            c_BDT_data->SetLogy();
            c_BDT_data->Update();
            c_BDT_data->Print(Form("figures/%s/bgOnly_BDT_%d_%d.png", rangeName, detnum[jdet], timenum[jdet]));
            c_BDT_data->Update();
            c_BDT_data->Print(Form("figures/%s/bgOnly_BDT_%d_%d.pdf", rangeName, detnum[jdet], timenum[jdet]));
        }
    }



    rootapp->Run();
    return 0;
}
