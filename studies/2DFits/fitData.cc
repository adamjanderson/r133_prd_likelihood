#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooAddPdf.h"
#include "RooWorkspace.h"
#include "RooPlot.h"
#include "RooFit.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "TFile.h"
#include "TH1.h"
#include "TString.h"
#include "Math/Interpolator.h"
#include "Math/Functor.h"

#include "TTree.h"
#include "TStyle.h"
#include "TColor.h"
#include "TApplication.h"
#include "TLegend.h"
#include "RooStats/ProfileLikelihoodCalculator.h"
#include "RooStats/LikelihoodInterval.h"
#include "RooStats/LikelihoodIntervalPlot.h"
#include "RooGaussian.h"
#include "RooPoisson.h"
#include "RooPolynomial.h"
#include "RooHistPdf.h"
#include "RooDataHist.h"
#include "RooFitResult.h"
#include "RooExtendPdf.h"
#include "RooBinning.h"
#include "RooCategory.h"
#include "RooSimultaneous.h"
#include "RooFunctor1DBinding.h"
#include "RooMinuit.h"
#include "RooNLLVar.h"

#include <iostream>
#include <fstream>


void buildLegend(RooPlot* rp)
{
    TLegend* leg = new TLegend(0.6, 0.5, 0.88, 0.88);
    std::vector<std::string> leg_keys = {"MC_gammas", "MC_Pb_Qo", "MC_Pb_QiS1", "MC_Pb_QiS2", "MC_1keV", "WIMP"};
    std::vector<std::string> leg_names = {"gammas", "sidewall surf. ev.", "S1 surf. ev.", "S2 surf. ev.", "1 keV", "WIMP"};
    
    for (int i=1; i<rp->numItems(); i++)
    {
        TString obj_name=rp->nameOf(i);
        TObject *obj = rp->findObject(obj_name.Data());
        std::string str_obj_name(obj_name);

        std::cout << str_obj_name << std::endl;
        std::cout << obj_name << std::endl;
        
        int jKey = 0;
        while(jKey < leg_keys.size() && str_obj_name.find(leg_keys[jKey]) == std::string::npos)
            jKey++;
        if(jKey < leg_keys.size())
            leg->AddEntry(obj,leg_names[jKey].c_str(),"l");
        else
            leg->AddEntry(obj,"total background","l");
    }
    leg->Draw();
}

int main(int argc, char *argv[])
{
    // parse arguments
    if(argc != 2)
    {
        std::cout << "Usage: fitData [WIMP mass]" << std::endl;
        return 0;
    }
    double WIMPmass = atof(argv[1]);

    gStyle->SetTitleSize(0.045, "xyz");
    gStyle->SetLabelSize(0.045, "xyz");
    gStyle->SetOptStat(0);
    TColor *myColor = new TColor(1001, 31./255., 120./255., 180./255.);
    TApplication* rootapp = new TApplication("myApp", &argc, argv);

    bool bgOnlyFit = true;
    bool sigBgFit = false;

    int mindet = 0; // bad detectors: 4
    int maxdet = 8;
    std::string detNames[] = {"T1Z1", "T2Z1", "T2Z2", "T4Z2", "T4Z3", "T5Z2 (w/QOS1)", "T5Z2 (w/o QOS1)", "T5Z3 (BS)", "T5Z3 (AS)"};
    std::string leg_names_bg[8] = {"data","total background", "gammas", "sidewall surf. ev.", "S1 surf. ev.", "S2 surf. ev.", "1 keV", "WIMP"};
    int detnum[] = {1, 4, 5, 11, 12, 14, 14, 15, 15};
    int timenum[] = {1, 1, 1, 1, 1, 1, 2, 1, 2};

    
    TFile f_workspace(Form("LT_data+model_%.2f_2d_GeV.root", WIMPmass));

    // Retrieve workspace from file
    RooWorkspace* w = (RooWorkspace*) f_workspace.Get("wSave");


    //***** GET DATA + MODEL FROM FILE *****//
    RooDataSet* combData = (RooDataSet*)w->data("combData2d");
    RooSimultaneous* model = (RooSimultaneous*)w->pdf(Form("simul_pdf_bg_2d_%.2f_GeV", WIMPmass));
    RooRealVar* pt = (RooRealVar*)w->var("pt");
    RooRealVar* qimean = (RooRealVar*)w->var("qimean");
    RooRealVar* prpart = (RooRealVar*)w->var("prpart");
    RooRealVar* pzpart = (RooRealVar*)w->var("pzpart");
    RooRealVar* BDT = (RooRealVar*)w->var("BDT10"); //(Form("BDT%.0f",WIMPmass));
    RooCategory* sampleType = (RooCategory*)w->cat("sampleType2d");
    RooRealVar* relative_cross_sec = (RooRealVar*)w->var("relative_cross_sec");


    RooBinning bin_pt(2.0, 13.0);
    bin_pt.addUniform(44, 2.0, 13.);
    RooBinning bin_qimean(-1.4, 5.0);
    bin_qimean.addUniform(32, -1.4, 5.0);
    RooBinning bin_prpart(0.15, 0.35);
    bin_prpart.addUniform(50, 0.15, 0.35);
    RooBinning bin_pzpart(-0.4, 0.4);
    bin_pzpart.addUniform(40, -0.4, 0.4);
    RooBinning bin_BDT(-1.0, 1.0);
    bin_BDT.addUniform(40, -1.0, 1.0);


    //***** FITTING *****//
    char rangeName[] = "control";

    // define ranges
    // control regions only
    std::string selection = "";
    std::vector<std::string> det_selection(maxdet + 1);
    for(int jdet = mindet; jdet <= maxdet; jdet++)
    {
        det_selection[jdet] = "";
        det_selection[jdet] = det_selection[jdet] + Form("ptVqimean_sample_%d_%d,", detnum[jdet], timenum[jdet]);
        det_selection[jdet] = det_selection[jdet] + Form("prpart_sample_%d_%d,", detnum[jdet], timenum[jdet]);
        det_selection[jdet] = det_selection[jdet] + Form("pzpart_sample_%d_%d,", detnum[jdet], timenum[jdet]);
        selection = selection + det_selection[jdet];
    }
    sampleType->setRange("control", selection.c_str());
    // sampleType->Print("v");
    // std::cout << "control = " << selection << std::endl;

    // signal region only
    selection = "";
    for(int jdet = mindet; jdet <= maxdet; jdet++)
    {
        selection = selection + Form("BDT_sample_%d_%d,", detnum[jdet], timenum[jdet]);
    }
    sampleType->setRange("signal", selection.c_str());
    // sampleType->Print("v");
    // std::cout << "signal = " << selection << std::endl;

    model->Print("v");

    // do fitting
    RooDataSet* subset = (RooDataSet*)combData->reduce(RooFit::CutRange(rangeName));
    // RooNLLVar nll("nll","nll",*model,*subset) ;
    // RooMinuit m(nll) ;
    // m.setStrategy(0) ;
    // m.migrad() ;


    std::vector<RooRealVar*> N_1keV, N_gammas, N_210Pb_Qo, N_210Pb_QiS1, N_210Pb_QiS2, N_WIMP;
    for(int jdet = 0; jdet <= 8; jdet++)
    {
        N_1keV.push_back((RooRealVar*)w->var(Form("N_1keV_%d_%d", detnum[jdet], timenum[jdet])));
        N_gammas.push_back((RooRealVar*)w->var(Form("N_gammas_%d_%d", detnum[jdet], timenum[jdet])));
        N_210Pb_Qo.push_back((RooRealVar*)w->var(Form("N_210Pb_Qo_%d_%d", detnum[jdet], timenum[jdet])));
        N_210Pb_QiS1.push_back((RooRealVar*)w->var(Form("N_210Pb_QiS1_%d_%d", detnum[jdet], timenum[jdet])));
        N_210Pb_QiS2.push_back((RooRealVar*)w->var(Form("N_210Pb_QiS2_%d_%d", detnum[jdet], timenum[jdet])));
        N_WIMP.push_back((RooRealVar*)w->function(Form("N_WIMP_%.2f_GeV_%d_%d", WIMPmass, detnum[jdet], timenum[jdet])));
        
        // N_1keV[jdet]->setVal(0.1);
        // N_gammas[jdet]->setVal(0.1);
        // N_210Pb_Qo[jdet]->setVal(0.1);
        // N_210Pb_QiS1[jdet]->setVal(0.01);
        // N_210Pb_QiS2[jdet]->setVal(0.01);
        // N_1keV[jdet]->setConstant(kTRUE);
        // N_gammas[jdet]->setConstant(kTRUE);
        // N_210Pb_Qo[jdet]->setConstant(kTRUE);
        // N_210Pb_QiS1[jdet]->setConstant(kTRUE);
        // N_210Pb_QiS2[jdet]->setConstant(kTRUE);
    }
    // N_1keV[2]->setConstant(kTRUE);
    // N_gammas[2]->setConstant(kTRUE);
    // N_210Pb_Qo[2]->setConstant(kTRUE);
    // N_210Pb_QiS1[2]->setConstant(kTRUE);
    // N_210Pb_QiS2[2]->setConstant(kTRUE);
    N_210Pb_QiS1[7]->setVal(0.0);
    N_210Pb_QiS1[7]->setConstant(kTRUE);
    N_210Pb_QiS1[8]->setVal(0.0);
    N_210Pb_QiS1[8]->setConstant(kTRUE);

    RooFitResult* fitResult = model->fitTo(*subset, RooFit::Save(kTRUE), RooFit::Minimizer("Minuit2","Hesse"));
    fitResult->correlationHist()->Draw("colz") ; 
    relative_cross_sec->setRange(0, relative_cross_sec->getVal() + 5.0*relative_cross_sec->getError());
    // fitResult = model->fitTo(*subset, RooFit::Save(kTRUE));

    // Construct unbinned likelihood
    RooAbsReal* nll = model->createNLL(*subset,RooFit::NumCPU(1)) ;
    RooMinuit(*nll).migrad() ;
    RooAbsReal* pll = nll->createProfile(*relative_cross_sec) ;

    // TCanvas* cPLL = new TCanvas("cPLL");
    // double x[] = {relative_cross_sec->getVal() + 1.28*relative_cross_sec->getError(), relative_cross_sec->getVal() + 1.28*relative_cross_sec->getError()};
    // double y[] = {0, 5};
    // TGraph* tg_limit = new TGraph(2, x, y);
    // tg_limit->SetLineStyle(7);
    // tg_limit->SetLineColor(kGreen);
    // RooPlot* rp_PLL = relative_cross_sec->frame(RooFit::Bins(30));
    // nll->plotOn(rp_PLL,RooFit::LineColor(kRed), RooFit::ShiftToZero(), RooFit::Precision(1)) ;
    // pll->plotOn(rp_PLL,RooFit::LineColor(kBlue), RooFit::ShiftToZero(), RooFit::Precision(1)) ;
    // rp_PLL->GetYaxis()->SetRangeUser(0, 5);
    // rp_PLL->GetYaxis()->SetTitle("-log(L)");
    // rp_PLL->GetXaxis()->SetTitle("#sigma_{n#chi} [10^{-42} cm^{2}]");
    // rp_PLL->SetTitle("");
    // rp_PLL->Draw();
    // tg_limit->Draw("same");
    // cPLL->Update();
    // cPLL->Print(Form("figures/%s/pll_%.2f_GeV.pdf", rangeName, WIMPmass), "pdf");


    // // write fit results to file
    // std::filebuf file;
    // file.open(Form("results/results_%s.txt", rangeName), std::ios::out);
    // std::ostream os_results(&file);
    // fitResult->printMultiline(os_results, 1, false);
    // file.close();


    
    if(true)
    {
        for(int jdet = mindet; jdet <= maxdet; jdet++)
        {
            TCanvas* c_2d = new TCanvas(Form("c_2d_%d_%d", detnum[jdet], timenum[jdet]));
            TH1* h_2d_data = subset->createHistogram(Form("h_2d_data_%d_%d", detnum[jdet], timenum[jdet]),
                                        *pt, RooFit::YVar(*qimean),
                                        RooFit::Cut(Form("sampleType2d==sampleType2d::ptVqimean_sample_%d_%d", detnum[jdet], timenum[jdet])));
            h_2d_data->GetYaxis()->SetRangeUser(-1.5, 6.0);
            h_2d_data->SetTitle(Form("%s: data", detNames[jdet].c_str()));
            h_2d_data->Draw("box");
            c_2d->Update();
            c_2d->Print(Form("figures/%s/ptVqimean_data_%.2f_GeV_%d_%d.pdf", rangeName, WIMPmass, detnum[jdet], timenum[jdet]), "pdf");
            c_2d->Print(Form("figures/%s/ptVqimean_data_%.2f_GeV_%d_%d.png", rangeName, WIMPmass, detnum[jdet], timenum[jdet]), "png");


            TCanvas* c_2d_pdf = new TCanvas(Form("c_2d_pdf_%d_%d", detnum[jdet], timenum[jdet]));
            sampleType->setLabel(Form("ptVqimean_sample_%d_%d", detnum[jdet], timenum[jdet]));
            TH1* h_2d_pdf_data = model->createHistogram(Form("h_2d_pdf_%d_%d", detnum[jdet], timenum[jdet]),
                                        *pt, RooFit::YVar(*qimean), RooFit::Extended(kTRUE));
            h_2d_pdf_data->SetLineColor(2);
            h_2d_pdf_data->SetTitle(Form("%s: model", detNames[jdet].c_str()));
            h_2d_pdf_data->Draw("box");
            c_2d_pdf->Update();
            c_2d_pdf->Print(Form("figures/%s/ptVqimean_model_%.2f_GeV_%d_%d.pdf", rangeName, WIMPmass, detnum[jdet], timenum[jdet]), "pdf");
            c_2d_pdf->Print(Form("figures/%s/ptVqimean_model_%.2f_GeV_%d_%d.png", rangeName, WIMPmass, detnum[jdet], timenum[jdet]), "png");


            // subtract pdf and compute residuals
            TH2D* h_2d_diff = (TH2D*)h_2d_data->Clone(Form("c_2d_diff_%d_%d", detnum[jdet], timenum[jdet]));
            for(int jbinX = 0; jbinX < h_2d_pdf_data->GetNbinsX(); jbinX++)
            {
                for(int jbinY = 0; jbinY < h_2d_pdf_data->GetNbinsY(); jbinY++)
                {
                    double nSigma_diff = (h_2d_data->GetBinContent(jbinX, jbinY) - h_2d_pdf_data->GetBinContent(jbinX, jbinY)) / 
                                            sqrt(h_2d_data->GetBinContent(jbinX, jbinY));
                    if(std::isfinite(nSigma_diff))
                        h_2d_diff->SetBinContent(jbinX, jbinY, sqrt(nSigma_diff*nSigma_diff));
                }
            }
            TCanvas* c_2d_diff = new TCanvas(Form("c_2d_diff_%d_%d", detnum[jdet], timenum[jdet]));
            h_2d_diff->GetZaxis()->SetRangeUser(0.0, 5.0);
            h_2d_diff->Draw("colz");
            h_2d_diff->SetTitle(Form("%s: (data - model) / #sqrt{data}", detNames[jdet].c_str()));
            c_2d_diff->Update();
            c_2d_diff->Print(Form("figures/%s/ptVqimean_resid_%.2f_GeV_%d_%d.pdf", rangeName, WIMPmass, detnum[jdet], timenum[jdet]), "pdf");
            c_2d_diff->Print(Form("figures/%s/ptVqimean_resid_%.2f_GeV_%d_%d.png", rangeName, WIMPmass, detnum[jdet], timenum[jdet]), "png");



            TCanvas* c_pt_data = new TCanvas(Form("c_pt_data_%d_%d", detnum[jdet], timenum[jdet]));
            pt->setRange(2.0, 13.0);
            RooPlot* rp_1d_pt = pt->frame();
            subset->plotOn(rp_1d_pt, RooFit::DrawOption("EZ"), RooFit::LineWidth(2), RooFit::Cut(Form("sampleType2d==sampleType2d::ptVqimean_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::Binning(bin_pt), RooFit::Name(Form("data_ptVqimean_%d_%d", detnum[jdet], timenum[jdet])));
            model->plotOn(rp_1d_pt, RooFit::Slice(*sampleType,Form("ptVqimean_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset), RooFit::Name(Form("pdf_ptVqimean_%.2f_GeV_%d_%d", WIMPmass, detnum[jdet], timenum[jdet])));
            model->plotOn(rp_1d_pt, RooFit::Slice(*sampleType,Form("ptVqimean_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("histpdf_MC_gammas_ptVqimean_%d_%d", detnum[jdet], timenum[jdet])), RooFit::LineStyle(kDashed), RooFit::LineColor(kCyan));
            model->plotOn(rp_1d_pt, RooFit::Slice(*sampleType,Form("ptVqimean_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("pdf_MC_Pb_Qo_ptVqimean_%d_%d", detnum[jdet], timenum[jdet])), RooFit::LineStyle(kDashed), RooFit::LineColor(kRed));
            model->plotOn(rp_1d_pt, RooFit::Slice(*sampleType,Form("ptVqimean_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("pdf_MC_Pb_QiS1_ptVqimean_%d_%d", detnum[jdet], timenum[jdet])), RooFit::LineStyle(kDashed), RooFit::LineColor(kOrange));
            model->plotOn(rp_1d_pt, RooFit::Slice(*sampleType,Form("ptVqimean_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("pdf_MC_Pb_QiS2_ptVqimean_%d_%d", detnum[jdet], timenum[jdet])), RooFit::LineStyle(kDashed), RooFit::LineColor(kOrange+3));
            model->plotOn(rp_1d_pt, RooFit::Slice(*sampleType,Form("ptVqimean_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("histpdf_MC_1keV_ptVqimean_%d_%d", detnum[jdet], timenum[jdet])), RooFit::LineStyle(kDashed), RooFit::LineColor(kGreen));
            model->plotOn(rp_1d_pt, RooFit::Slice(*sampleType,Form("ptVqimean_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("histpdf_WIMP_ptVqimean_%.2f_GeV_%d_%d", WIMPmass, detnum[jdet], timenum[jdet])), RooFit::LineStyle(kDashed), RooFit::LineColor(kMagenta));
            rp_1d_pt->Draw();
            buildLegend(rp_1d_pt);
            double pt_chi2 = rp_1d_pt->chiSquare(Form("pdf_ptVqimean_%.2f_GeV_%d_%d", WIMPmass, detnum[jdet], timenum[jdet]), 
                                                 Form("data_ptVqimean_%d_%d", detnum[jdet], timenum[jdet]), 6);
            // c_pt_data->SetLogy();
            rp_1d_pt->SetTitle(Form("%s: #chi^2 / ndf = %.2f", detNames[jdet].c_str(), pt_chi2));
            c_pt_data->Update();
            c_pt_data->Print(Form("figures/%s/bgOnly_pt_%.2f_GeV_%d_%d.png", rangeName, WIMPmass, detnum[jdet], timenum[jdet]), "png");
            c_pt_data->Update();
            c_pt_data->Print(Form("figures/%s/bgOnly_pt_%.2f_GeV_%d_%d.pdf", rangeName, WIMPmass, detnum[jdet], timenum[jdet]), "pdf");

            TCanvas* c_qimean_data = new TCanvas(Form("c_qimean_data_%d_%d", detnum[jdet], timenum[jdet]));
            // qimean->setRange(2.0, 13.0);
            RooPlot* rp_1d_qimean = qimean->frame();
            subset->plotOn(rp_1d_qimean, RooFit::DrawOption("EZ"), RooFit::LineWidth(2), RooFit::Cut(Form("sampleType2d==sampleType2d::ptVqimean_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::Binning(bin_qimean), RooFit::Name(Form("data_qimean_%d_%d", detnum[jdet], timenum[jdet])));
            model->plotOn(rp_1d_qimean, RooFit::Slice(*sampleType,Form("ptVqimean_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset), RooFit::Name(Form("pdf_qimean_%.2f_GeV_%d_%d", WIMPmass, detnum[jdet], timenum[jdet])));
            model->plotOn(rp_1d_qimean, RooFit::Slice(*sampleType,Form("ptVqimean_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("histpdf_MC_gammas_ptVqimean_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kCyan));
            model->plotOn(rp_1d_qimean, RooFit::Slice(*sampleType,Form("ptVqimean_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("pdf_MC_Pb_Qo_ptVqimean_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kRed));
            model->plotOn(rp_1d_qimean, RooFit::Slice(*sampleType,Form("ptVqimean_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("pdf_MC_Pb_QiS1_ptVqimean_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kOrange));
            model->plotOn(rp_1d_qimean, RooFit::Slice(*sampleType,Form("ptVqimean_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("pdf_MC_Pb_QiS2_ptVqimean_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kOrange+3));
            model->plotOn(rp_1d_qimean, RooFit::Slice(*sampleType,Form("ptVqimean_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("histpdf_MC_1keV_ptVqimean_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kGreen));
            model->plotOn(rp_1d_qimean, RooFit::Slice(*sampleType,Form("ptVqimean_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("histpdf_WIMP_ptVqimean_%.2f_GeV_%d_%d", WIMPmass, detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kMagenta));
            rp_1d_qimean->Draw();
            // buildLegend(leg_names_bg, rp_1d_qimean);
            double qimean_chi2 = rp_1d_qimean->chiSquare(Form("pdf_qimean_%.2f_GeV_%d_%d", WIMPmass, detnum[jdet], timenum[jdet]), 
                                                 Form("data_qimean_%d_%d", detnum[jdet], timenum[jdet]), 6);
            // c_qimean_data->SetLogy();
            rp_1d_qimean->SetTitle(Form("%s: #chi^2 / ndf = %.2f", detNames[jdet].c_str(), qimean_chi2));
            c_qimean_data->Update();
            c_qimean_data->Print(Form("figures/%s/bgOnly_qimean_%.2f_GeV_%d_%d.png", rangeName, WIMPmass, detnum[jdet], timenum[jdet]), "png");
            c_qimean_data->Update();
            c_qimean_data->Print(Form("figures/%s/bgOnly_qimean_%.2f_GeV_%d_%d.pdf", rangeName, WIMPmass, detnum[jdet], timenum[jdet]), "pdf");

            TCanvas* c_prpart_data = new TCanvas(Form("c_prpart_data_%d_%d", detnum[jdet], timenum[jdet]));
            // prpart->setRange(2.0, 13.0);
            RooPlot* rp_1d_prpart = prpart->frame();
            subset->plotOn(rp_1d_prpart, RooFit::DrawOption("EZ"), RooFit::LineWidth(2), RooFit::Cut(Form("sampleType2d==sampleType2d::prpart_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::Binning(bin_prpart), RooFit::Name(Form("data_prpart_%d_%d", detnum[jdet], timenum[jdet])));
            model->plotOn(rp_1d_prpart, RooFit::Slice(*sampleType,Form("prpart_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset), RooFit::Name(Form("pdf_prpart_%.2f_GeV_%d_%d", WIMPmass, detnum[jdet], timenum[jdet])));
            model->plotOn(rp_1d_prpart, RooFit::Slice(*sampleType,Form("prpart_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("histpdf_MC_gammas_prpart_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kCyan));
            model->plotOn(rp_1d_prpart, RooFit::Slice(*sampleType,Form("prpart_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("pdf_MC_Pb_Qo_prpart_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kRed));
            model->plotOn(rp_1d_prpart, RooFit::Slice(*sampleType,Form("prpart_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("pdf_MC_Pb_QiS1_prpart_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kOrange));
            model->plotOn(rp_1d_prpart, RooFit::Slice(*sampleType,Form("prpart_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("pdf_MC_Pb_QiS2_prpart_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kOrange+3));
            model->plotOn(rp_1d_prpart, RooFit::Slice(*sampleType,Form("prpart_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("histpdf_MC_1keV_prpart_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kGreen));
            model->plotOn(rp_1d_prpart, RooFit::Slice(*sampleType,Form("prpart_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("histpdf_WIMP_prpart_%.2f_GeV_%d_%d", WIMPmass, detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kMagenta));
            rp_1d_prpart->Draw();
            double prpart_chi2 = rp_1d_prpart->chiSquare(Form("pdf_prpart_%.2f_GeV_%d_%d", WIMPmass, detnum[jdet], timenum[jdet]), 
                                                 Form("data_prpart_%d_%d", detnum[jdet], timenum[jdet]), 6);
            // c_prpart_data->SetLogy();
            rp_1d_prpart->SetTitle(Form("%s: #chi^2 / ndf = %.2f", detNames[jdet].c_str(), prpart_chi2));
            c_prpart_data->Update();
            c_prpart_data->Print(Form("figures/%s/bgOnly_prpart_%.2f_GeV_%d_%d.png", rangeName, WIMPmass, detnum[jdet], timenum[jdet]), "png");
            c_prpart_data->Update();
            c_prpart_data->Print(Form("figures/%s/bgOnly_prpart_%.2f_GeV_%d_%d.pdf", rangeName, WIMPmass, detnum[jdet], timenum[jdet]), "pdf");

            TCanvas* c_pzpart_data = new TCanvas(Form("c_pzpart_data_%d_%d", detnum[jdet], timenum[jdet]));
            // pt->setRange(2.0, 13.0);
            RooPlot* rp_1d_pzpart = pzpart->frame();
            subset->plotOn(rp_1d_pzpart, RooFit::DrawOption("EZ"), RooFit::LineWidth(2), RooFit::Cut(Form("sampleType2d==sampleType2d::pzpart_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::Binning(bin_pzpart), RooFit::Name(Form("data_pzpart_%d_%d", detnum[jdet], timenum[jdet])));
            model->plotOn(rp_1d_pzpart, RooFit::Slice(*sampleType,Form("pzpart_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset), RooFit::Name(Form("pdf_pzpart_%.2f_GeV_%d_%d", WIMPmass, detnum[jdet], timenum[jdet])));
            model->plotOn(rp_1d_pzpart, RooFit::Slice(*sampleType,Form("pzpart_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("histpdf_MC_gammas_pzpart_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kCyan));
            model->plotOn(rp_1d_pzpart, RooFit::Slice(*sampleType,Form("pzpart_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("pdf_MC_Pb_Qo_pzpart_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kRed));
            model->plotOn(rp_1d_pzpart, RooFit::Slice(*sampleType,Form("pzpart_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("pdf_MC_Pb_QiS1_pzpart_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kOrange));
            model->plotOn(rp_1d_pzpart, RooFit::Slice(*sampleType,Form("pzpart_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("pdf_MC_Pb_QiS2_pzpart_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kOrange+3));
            model->plotOn(rp_1d_pzpart, RooFit::Slice(*sampleType,Form("pzpart_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("histpdf_MC_1keV_pzpart_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kGreen));
            model->plotOn(rp_1d_pzpart, RooFit::Slice(*sampleType,Form("pzpart_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("histpdf_WIMP_pzpart_%.2f_GeV_%d_%d", WIMPmass, detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kMagenta));
            rp_1d_pzpart->Draw();
            double pzpart_chi2 = rp_1d_pzpart->chiSquare(Form("pdf_pzpart_%.2f_GeV_%d_%d", WIMPmass, detnum[jdet], timenum[jdet]), 
                                                 Form("data_pzpart_%d_%d", detnum[jdet], timenum[jdet]), 6);
            // c_pzpart_data->SetLogy();
            rp_1d_pzpart->SetTitle(Form("%s: #chi^2 / ndf = %.2f", detNames[jdet].c_str(), pzpart_chi2));
            c_pzpart_data->Update();
            c_pzpart_data->Print(Form("figures/%s/bgOnly_pzpart_%.2f_GeV_%d_%d.png", rangeName, WIMPmass, detnum[jdet], timenum[jdet]), "png");
            c_pzpart_data->Update();
            c_pzpart_data->Print(Form("figures/%s/bgOnly_pzpart_%.2f_GeV_%d_%d.pdf", rangeName, WIMPmass, detnum[jdet], timenum[jdet]), "pdf");

            // TCanvas* c_BDT_data = new TCanvas(Form("c_BDT_data_%d_%d", detnum[jdet], timenum[jdet]));
            // // pt->setRange(2.0, 13.0);
            // RooPlot* rp_1d_BDT = BDT->frame();
            // RooPlot* rp_BDT = subset->plotOn(rp_1d_BDT, RooFit::DrawOption("EZ"), RooFit::LineWidth(2), RooFit::Cut(Form("BDT10>-0.8 & sampleType==sampleType::BDT_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::Binning(bin_BDT), RooFit::Name(Form("data_BDT_%d_%d", detnum[jdet], timenum[jdet])));
            // model->plotOn(rp_1d_BDT, RooFit::Slice(*sampleType,Form("BDT_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset), RooFit::Name(Form("pdf_BDT_%.2f_GeV_%d_%d", WIMPmass, detnum[jdet], timenum[jdet])));
            // model->plotOn(rp_1d_BDT, RooFit::Slice(*sampleType,Form("BDT_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("histpdf_MC_gammas_BDT10_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kCyan));
            // model->plotOn(rp_1d_BDT, RooFit::Slice(*sampleType,Form("BDT_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("pdf_MC_Pb_Qo_BDT10_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kRed));
            // model->plotOn(rp_1d_BDT, RooFit::Slice(*sampleType,Form("BDT_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("pdf_MC_Pb_QiS1_BDT10_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kOrange));
            // model->plotOn(rp_1d_BDT, RooFit::Slice(*sampleType,Form("BDT_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("pdf_MC_Pb_QiS2_BDT10_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kOrange+3));
            // model->plotOn(rp_1d_BDT, RooFit::Slice(*sampleType,Form("BDT_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("histpdf_MC_1keV_BDT10_%d_%d", detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kGreen));
            // model->plotOn(rp_1d_BDT, RooFit::Slice(*sampleType,Form("BDT_sample_%d_%d", detnum[jdet], timenum[jdet])), RooFit::ProjWData(*sampleType,*subset),RooFit::Components(Form("histpdf_WIMP_BDT10_%.2f_GeV_%d_%d", WIMPmass, detnum[jdet], timenum[jdet])),RooFit::LineStyle(kDashed),RooFit::LineColor(kMagenta));
            // rp_1d_BDT->Draw();
            // // buildLegend(leg_names_bg, rp_1d_BDT);
            // double BDT_chi2 = rp_1d_BDT->chiSquare(Form("pdf_BDT_%.2f_GeV_%d_%d", WIMPmass, detnum[jdet], timenum[jdet]), 
            //                                         Form("data_BDT_%d_%d", detnum[jdet], timenum[jdet]), 6);
            // rp_1d_BDT->GetYaxis()->SetRangeUser(0., 40.);
            // // c_BDT_data->SetLogy();
            // rp_1d_BDT->SetTitle(Form("%s: #chi^2 / ndf = %.2f", detNames[jdet].c_str(), BDT_chi2));
            // c_BDT_data->Update();
            // c_BDT_data->Print(Form("figures/%s/bgOnly_BDT_%d_%d.png", rangeName, detnum[jdet], timenum[jdet]), "png");
            // c_BDT_data->Update();
            // c_BDT_data->Print(Form("figures/%s/bgOnly_BDT_%d_%d.pdf", rangeName, detnum[jdet], timenum[jdet]), "pdf");
        }
    }



    rootapp->Run();
    return 0;
}
